<?php

use yii\db\Migration;

/**
 * Handles the creation of table `chat`.
 */
class m181026_095202_create_chat_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('chat', [
            'id' => $this->bigPrimaryKey(),
            'user_from_id' => $this->integer()->notNull(),
            'user_to_id' => $this->integer()->notNull(),
            'message' => $this->string(1000),
            'attachment' => $this->text(),
            'status' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->integer()->notNull()
        ]);

        $this->createIndex(
            'idx-chat-user_from_id',
            'chat',
            'user_from_id'
        );
        $this->createIndex(
            'idx-chat-user_to_id',
            'chat',
            'user_from_id'
        );

        //Uncomment from prod
        $this->addForeignKey(
            'fk-chat-user_from_id',
            'chat',
            'user_from_id',
            'user',
            'id'
        );

        $this->addForeignKey(
            'fk-chat-user_to_id',
            'chat',
            'user_to_id',
            'user',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('chat');
    }
}
