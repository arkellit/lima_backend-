<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customer_notice`.
 */
class m190319_064353_create_customer_notice_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('customer_notice', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull(),
            'customer_personal_id' => $this->integer()->defaultValue(0),
            'personal_role' => $this->string(255),
            'order_id' => $this->integer(),
            'order_place_id' => $this->integer(),
            'worker_id' => $this->integer()->defaultValue(0),
            'category_notice' => $this->string(255),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'is_delete' => $this->smallInteger()->defaultValue(0)->notNull()
        ]);

        $this->createIndex(
            'idx-customer_notice-customer_id',
            'customer_notice',
            'customer_id'
        );

        $this->createIndex(
            'idx-customer_notice-customer_personal_id',
            'customer_notice',
            'customer_personal_id'
        );

        $this->createIndex(
            'idx-customer_notice-personal_role',
            'customer_notice',
            'personal_role'
        );

        $this->createIndex(
            'idx-customer_notice-order_id',
            'customer_notice',
            'order_id'
        );

        $this->createIndex(
            'idx-customer_notice-order_place_id',
            'customer_notice',
            'order_place_id'
        );

        $this->createIndex(
            'idx-customer_notice-worker_id',
            'customer_notice',
            'worker_id'
        );

        $this->createIndex(
            'idx-customer_notice-category_notice',
            'customer_notice',
            'order_place_id'
        );
        $this->createIndex(
            'idx-customer_notice-is_delete',
            'customer_notice',
            'is_delete'
        );

        $this->addForeignKey(
            'fk-customer_notice-customer-id',
            'customer_notice',
            'customer_id',
            'customer',
            'id'
        );

        $this->addForeignKey(
            'fk-customer_notice-order-id',
            'customer_notice',
            'order_id',
            'order',
            'id'
        );

        $this->addForeignKey(
            'fk-printing_template-order_place-id',
            'customer_notice',
            'order_place_id',
            'order_place',
            'id'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('customer_notice');
    }
}
