<?php

use yii\db\Migration;

/**
 * Handles the creation of table `eye_color`.
 */
class m180820_091513_create_worker_eye_color_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worker_eye_color', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('worker_eye_color');
    }
}
