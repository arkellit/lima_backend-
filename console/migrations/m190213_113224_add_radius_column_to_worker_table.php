<?php

use yii\db\Migration;

/**
 * Handles adding radius to table `worker`.
 */
class m190213_113224_add_radius_column_to_worker_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('worker', 'radius_search', $this->double()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('worker', 'radius_search');
    }
}
