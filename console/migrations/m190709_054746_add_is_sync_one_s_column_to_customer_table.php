<?php

use yii\db\Migration;

/**
 * Handles adding is_sync_one_s to table `customer`.
 */
class m190709_054746_add_is_sync_one_s_column_to_customer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('customer', 'is_sync_ones', $this->smallInteger()
        ->defaultValue(0)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('customer', 'is_sync_ones');
    }
}
