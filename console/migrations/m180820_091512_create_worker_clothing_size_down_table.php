<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clothing_size_down`.
 */
class m180820_091512_create_worker_clothing_size_down_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worker_clothing_size_down', [
            'id' => $this->primaryKey(),
            'name' => $this->string(10)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('worker_clothing_size_down');
    }
}
