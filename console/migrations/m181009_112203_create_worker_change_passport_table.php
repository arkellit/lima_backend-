<?php

use yii\db\Migration;

/**
 * Handles the creation of table `application_change_passport`.
 */
class m181009_112203_create_worker_change_passport_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worker_change_passport', [
            'id' => $this->primaryKey(),
            'worker_id' => $this->integer()->notNull(),
            'passport_photo_one' => $this->text()->notNull(),
            'passport_photo_two' => $this->text()->notNull(),
            'is_changed' => $this->smallInteger()->defaultValue(0)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->createIndex(
            'idx-worker_change_passport-worker_id',
            'worker_change_passport',
            'worker_id'
        );

        $this->createIndex(
            'idx-worker_change_passport-is_changed',
            'worker_change_passport',
            'is_changed'
        );

        $this->addForeignKey(
            'fk-worker_change_passport-worker_id',
            'worker_change_passport',
            'worker_id',
            'worker',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('worker_change_passport');
    }
}
