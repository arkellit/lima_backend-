<?php

use yii\db\Migration;

/**
 * Handles the creation of table `body_complexion`.
 */
class m180820_075251_create_worker_body_complexion_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worker_body_complexion', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('worker_body_complexion');
    }
}
