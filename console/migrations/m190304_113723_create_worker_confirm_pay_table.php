<?php

use yii\db\Migration;

/**
 * Handles the creation of table `worker_confirm_pay`.
 */
class m190304_113723_create_worker_confirm_pay_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worker_confirm_pay', [
            'id' => $this->primaryKey(),
            'worker_id' => $this->integer()->notNull()->unique(),
            'payment_id' => $this->string(255)->notNull(),
            'status' => $this->string(50)->notNull(),
            'confirm_refund' => $this->smallInteger()->defaultValue(0)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ]);

        $this->createIndex(
            'idx-worker_confirm_pay-worker_id',
            'worker_confirm_pay',
            'worker_id'
        );
        $this->createIndex(
            'idx-worker_confirm_pay-payment_id',
            'worker_confirm_pay',
            'payment_id'
        );
        $this->createIndex(
            'idx-worker_confirm_pay-is_pending',
            'worker_confirm_pay',
            'status'
        );
        $this->createIndex(
            'idx-worker_confirm_pay-confirm_refund',
            'worker_confirm_pay',
            'confirm_refund'
        );

        $this->addForeignKey(
            'fk-worker_confirm_pay-worker_id',
            'worker_confirm_pay',
            'worker_id',
            'worker',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('worker_confirm_pay');
    }
}
