<?php

use yii\db\Migration;

/**
 * Handles the creation of table `region`.
 */
class m181121_074935_create_region_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('region', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'region_code' => $this->integer()->notNull(),
            'lat' => $this->double()->defaultValue(0)->notNull(),
            'lng' => $this->double()->defaultValue(0)->notNull()
        ]);

        $this->createIndex(
            'idx-region-region_code',
            'region',
            'region_code'
        );

        $this->insert('region', [
            'name' => 'Московская область',
            'region_code' => 77
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('region');
    }
}
