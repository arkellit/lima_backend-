<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%logs1c}}`.
 */
class m191206_064850_create_logs1c_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = " 
            CREATE TABLE logs1c (
                id serial NOT NULL PRIMARY KEY, 
                text text,
                created_at integer
            );
        ";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%logs1c}}');
    }
}
