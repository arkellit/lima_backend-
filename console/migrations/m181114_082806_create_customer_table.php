<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customer`.
 */
class m181114_082806_create_customer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('customer', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->unique(),
            'name' => $this->string(255)->notNull(),
            'last_name' => $this->string(255)->notNull(),
            'middle_name' => $this->string(255),
            'is_register_complete' => $this->smallInteger()->defaultValue(0)->notNull(),
            'photo' => $this->string(255)
        ]);

        $this->createIndex(
            'idx-customer-user_id',
            'customer',
            'user_id'
        );

        $this->addForeignKey(
            'fk-customer-user_id-user',
            'customer',
            'user_id',
            'user',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-customer-user_id', 'customer');
        $this->dropIndex('idx-customer-inn_company', 'customer');
        $this->dropForeignKey('fk-customer-user_id-user', 'customer');
        $this->dropTable('customer');
    }
}
