<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180817_091036_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'phone' => $this->string(15)->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'access_token' => $this->string(64)->notNull()->unique(),
            'password' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'registration_ip' => $this->string(255),
            'is_blocked' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'is_confirm' => $this->smallInteger()->defaultValue(0)->notNull(),
            'is_social' =>  $this->smallInteger()->defaultValue(0)->notNull(),
            'is_confirm_forgot_code' => $this->smallInteger()->defaultValue(0)->notNull(),
            'is_invite' => $this->integer()->null(),
            'fcm_token' => $this->string(255)->null(),
            'is_delete' => $this->smallInteger()->defaultValue(0)->notNull(),
        ]);

        $this->createIndex(
          'ids-phone-user',
          'user',
          'phone'
        );


        $this->createIndex(
            'ids-access_token-user',
            'user',
            'access_token'
        );

        $this->createIndex(
            'ids-is_blocked-user',
            'user',
            'is_blocked'
        );
        $this->createIndex(
            'ids-is_confirm-user',
            'user',
            'is_confirm'
        );
        $this->createIndex(
            'ids-is_social-user',
            'user',
            'is_social'
        );
     
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
