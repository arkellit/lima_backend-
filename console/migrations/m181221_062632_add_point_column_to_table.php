<?php

use yii\db\Migration;

/**
 * Class m181221_062632_add_point_column_to_table
 */
class m181221_062632_add_point_column_to_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("SELECT AddGeometryColumn('worker', 'point', 4326, 'POINT', 2);");
        $this->execute("CREATE INDEX idx_worker_point ON worker USING gist(point);");
        $this->execute("SELECT AddGeometryColumn('order_address', 'point', 4326, 'POINT', 2);");
        $this->execute("CREATE INDEX idx_order_address_point ON order_address USING gist(point);");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('worker', 'point');
        $this->dropColumn('order_address', 'point');
    }
}
