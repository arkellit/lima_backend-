<?php

use yii\db\Migration;

/**
 * Handles the creation of table `passport_data`.
 */
class m180820_123636_create_worker_passport_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worker_passport', [
            'worker_id' => $this->integer()->notNull(),
            'passport_name' => $this->text()->notNull(),
            'passport_last_name' => $this->text()->notNull(),
            'passport_middle_name' => $this->text(),
            'passport_series' => $this->text()->notNull(),
            'passport_number' => $this->text()->notNull(),
            //'passport_birthplace' => $this->text()->notNull(),
            'passport_birth_date' => $this->text()->notNull(),

            'passport_whom' => $this->text()->notNull(),
            'passport_when' => $this->text()->notNull(),
            'passport_code' => $this->text()->notNull(),
            'passport_residence_address' => $this->text()->notNull(),
            'passport_photo_one' => $this->text()->notNull(),
            'passport_photo_two' => $this->text()->notNull(),
            'PRIMARY KEY(worker_id)',
        ]);

        $this->createIndex(
            'idx-worker_passport-worker_id',
            'worker_passport',
            'worker_id'
        );

        $this->addForeignKey(
            'fk-worker_passport-worker_id',
            'worker_passport',
            'worker_id',
            'worker',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('worker_passport');
    }
}
