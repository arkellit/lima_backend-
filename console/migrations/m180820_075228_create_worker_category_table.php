<?php

use yii\db\Migration;

/**
 * Handles the creation of table `type_worker`.
 */
class m180820_075228_create_worker_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worker_category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'is_active' => $this->smallInteger()->defaultValue(1)
        ]);

        $this->createIndex(
            'idx-worker_category-is_active',
            'worker_category',
            'is_active'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('worker_category');
    }
}
