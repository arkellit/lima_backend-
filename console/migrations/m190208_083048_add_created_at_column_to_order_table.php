<?php

use yii\db\Migration;

/**
 * Handles adding created_at to table `order`.
 */
class m190208_083048_add_created_at_column_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'created_at', $this->integer()->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'created_at');
    }
}
