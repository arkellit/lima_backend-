<?php

use yii\db\Migration;

/**
 * Handles the creation of table `social_account`.
 */
class m180903_114436_create_user_social_account_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_social_account', [
            'id' => $this->primaryKey(),
            'user_id'    => $this->integer()->defaultValue(0)->notNull(),
            'provider'   => $this->string()->notNull(),
            'client_id'  => $this->bigInteger()->notNull(),
            'first_name' => $this->string()->null(),
            'last_name' => $this->string()->null(),
            'sex' => $this->smallInteger()->null(),
            'birth_date'=> $this->integer()->null(),
            'email'=> $this->string()->null(),
            'photo'=> $this->string()->null(),
            'mobile_phone' => $this->string()->null(),
            'city' => $this->string()->null(),
        ]);

        $this->createIndex(
            'idx-user_social_account-user_id',
            'user_social_account',
            'user_id'
        );

        $this->createIndex(
            'idx-user_social_account-provider',
            'user_social_account',
            'provider'
        );

        $this->createIndex(
            'idx-user_social_account-client_id',
            'user_social_account',
            'client_id'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-user_social_account-user_id', 'user_social_account');
        $this->dropIndex('idx-user_social_account-provider', 'user_social_account');
        $this->dropIndex('idx-user_social_account-client_id', 'user_social_account');
        $this->dropTable('user_social_account');
    }
}
