<?php

use yii\db\Migration;

/**
 * Handles the creation of table `token`.
 */
class m180905_105821_create_user_token_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_token', [
            'user_id'    => $this->integer()->unique()->notNull(),
            'code'       => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'type'       => $this->smallInteger()->notNull(),
            'PRIMARY KEY(user_id)',
        ]);

        $this->createIndex(
            'idx-token-user_id',
            'user_token',
            'user_id'
        );
        $this->createIndex(
            'idx-token-code',
            'user_token',
            'code'
        );
        $this->createIndex(
            'idx-token-type',
            'user_token',
            'type'
        );

        $this->addForeignKey(
            'fk-user_token-user_id-user-id',
            'user_token',
            'user_id',
            'user',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-user_token-user_id',  'user_token');
        $this->dropIndex('idx-user_token-code',  'user_token');
        $this->dropIndex('idx-user_token-type',  'user_token');
        $this->dropForeignKey('fk-user_token-user_id-user-id', 'user_token');
        $this->dropTable('user_token');
    }
}
