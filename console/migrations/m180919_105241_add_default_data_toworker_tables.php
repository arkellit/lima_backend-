<?php

use yii\db\Migration;

/**
 * Class m180820_091719_add_default_data_toworker_tables
 */
class m180919_105241_add_default_data_toworker_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('worker_category', [
            'name' => 'Маникен'
        ]);

        $this->insert('worker_category', [
            'name' => 'Промоутер'
        ]);



        $this->insert('worker_body_complexion', [
            'name' => 'Худой'
        ]);

        $this->insert('worker_body_complexion', [
            'name' => 'Средний'
        ]);

        $this->insert('worker_body_complexion', [
            'name' => 'Полный'
        ]);



        $this->insert('worker_hair_color', [
            'name' => 'Русые'
        ]);

        $this->insert('worker_hair_color', [
            'name' => 'Черные'
        ]);

        $this->insert('worker_hair_color', [
            'name' => 'Светлые'
        ]);


        $this->insert('worker_hair_length', [
            'name' => 'Короткие'
        ]);

        $this->insert('worker_hair_length', [
            'name' => 'Средние'
        ]);

        $this->insert('worker_hair_length', [
            'name' => 'Длинные'
        ]);



        $this->insert('worker_clothing_size_up', [
            'name' => 'M'
        ]);

        $this->insert('worker_clothing_size_up', [
            'name' => 'L'
        ]);

        $this->insert('worker_clothing_size_up', [
            'name' => 'X'
        ]);

        $this->insert('worker_clothing_size_up', [
            'name' => 'XL'
        ]);

        $this->insert('worker_clothing_size_down', [
            'name' => '28'
        ]);

        $this->insert('worker_clothing_size_down', [
            'name' => '29'
        ]);

        $this->insert('worker_clothing_size_down', [
            'name' => '30'
        ]);

        $this->insert('worker_clothing_size_down', [
            'name' => '31'
        ]);

        $this->insert('worker_appearance', [
            'name' => 'Славянская'
        ]);

        $this->insert('worker_appearance', [
            'name' => 'Европейская'
        ]);

        $this->insert('worker_appearance', [
            'name' => 'Восточная'
        ]);




        $this->insert('worker_eye_color', [
            'name' => 'Карие'
        ]);

        $this->insert('worker_eye_color', [
            'name' => 'Голубые'
        ]);

        $this->insert('worker_eye_color', [
            'name' => 'Зеленые'
        ]);

        $this->insert('worker_eye_color', [
            'name' => 'Черные'
        ]);


        $this->insert('city', [
            'name' => 'Москва'
        ]);

        $this->insert('worker_category_city', [
            'category_worker_id' => 1,
            'city_id' => 1,
            'price' => 100,
            'advance_price' => 120
        ]);
        $this->insert('worker_category_city', [
            'category_worker_id' => 2,
            'city_id' => 1,
            'price' => 150,
            'advance_price' => 200
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('worker_type');
        $this->truncateTable('worker_body_complexion');
        $this->truncateTable('worker_hair_color');
        $this->truncateTable('worker_appearance');
        $this->truncateTable('worker_clothing_size_up');
        $this->truncateTable('worker_clothing_down_up');
    }
}
