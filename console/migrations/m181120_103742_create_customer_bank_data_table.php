<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customer_bank_data`.
 */
class m181120_103742_create_customer_bank_data_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('customer_bank_data', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull()->unique(),
            'customer_rs' => $this->text()->notNull(),
            'bank_bik' => $this->string(9)->notNull(),
            'bank_city' => $this->string(255)->notNull(),
            'bank_name' => $this->string(255)->notNull(),
            'bank_ks' => $this->string(20)->notNull(),
//            'bank_kpp' => $this->string(9),
//            'bank_inn' => $this->string(12)->notNull()
        ]);

        $this->createIndex(
            'idx-customer_bank_data-worker_id',
            'customer_bank_data',
            'customer_id'
        );

        $this->addForeignKey(
            'fk-customer_bank_data-customer-id',
            'customer_bank_data',
            'customer_id',
            'customer',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('customer_bank_data');
    }
}
