<?php

use yii\db\Migration;

/**
 * Handles adding rating to table `worker`.
 */
class m190204_085947_add_rating_column_to_worker_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('worker', 'rating', $this->double()->defaultValue(70));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('worker', 'rating');
    }
}
