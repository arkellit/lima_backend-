<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_worker`.
 */
class m190214_090207_create_order_worker_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_worker', [
            'order_id' => $this->integer()->notNull(),
            'worker_id' => $this->integer()->notNull(),
            'order_place_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'attention' => $this->smallInteger()->notNull()->defaultValue(0),
            'PRIMARY KEY(order_id, worker_id)',
        ]);

        $this->createIndex(
            'idx-order_worker-order_id',
            'order_worker',
            'order_id'
        );

        $this->createIndex(
            'idx-order_worker-worker_id',
            'order_worker',
            'worker_id'
        );

        $this->createIndex(
            'idx-order_worker-order_place',
            'order_worker',
            'order_place_id'
        );

        $this->addForeignKey(
            'fk-order_worker-order-id',
            'order_worker',
            'order_id',
            'order',
            'id'
        );

        $this->addForeignKey(
            'fk-order_worker-worker-id',
            'order_worker',
            'worker_id',
            'worker',
            'id'
        );

        $this->addForeignKey(
            'fk-order_worker-order_place-id',
            'order_worker',
            'order_place_id',
            'order_place',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('order_worker');
    }
}
