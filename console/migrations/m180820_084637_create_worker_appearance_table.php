<?php

use yii\db\Migration;

/**
 * Handles the creation of table `appearance`.
 */
class m180820_084637_create_worker_appearance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worker_appearance', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('worker_appearance');
    }
}
