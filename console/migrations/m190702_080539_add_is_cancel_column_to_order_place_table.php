<?php

use yii\db\Migration;

/**
 * Handles adding is_cancel to table `order_place`.
 */
class m190702_080539_add_is_cancel_column_to_order_place_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_place', 'is_cancel', $this->smallInteger(0)->defaultValue(1)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order_place', 'is_cancel');
    }
}
