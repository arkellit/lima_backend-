<?php

use yii\db\Migration;

/**
 * Class m190226_114349_rename_order_request_id_to_order_start_end_table
 */
class m190226_114349_rename_order_request_id_to_order_start_end_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('order_start_end', 'order_request_id',
            'order_id');
        $this->dropForeignKey('fk-order_worker_accept-order_request-id',
            'order_start_end');
        $this->dropIndex('idx-order_worker_accept-order_request_id', 'order_start_end');

        $this->createIndex(
            'idx-order_start_end-order_id',
            'order_start_end',
            'order_id'
        );

        $this->addForeignKey(
            'fk-order_start_end-order_id-order-id',
            'order_start_end',
            'order_id',
            'order',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('order_id', 'order_start_end', 'order_request_id');
        $this->addForeignKey(
            'fk-order_worker_accept-order_request-id',
            'order_worker_accept',
            'order_request_id',
            'order_request',
            'id'
        );
        $this->createIndex(
            'idx-order_worker_accept-order_request_id',
            'order_worker_accept',
            'order_request_id'
        );
    }
}
