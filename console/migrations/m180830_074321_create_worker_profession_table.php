<?php

use yii\db\Migration;

/**
 * Handles the creation of table `worker_profession`.
 */
class m180830_074321_create_worker_profession_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worker_profession', [
            'worker_id' => $this->bigInteger()->notNull(),
            'category_worker_id' => $this->bigInteger()->notNull(),
            'is_working' => $this->smallInteger()->defaultValue(0),
            'PRIMARY KEY(worker_id, category_worker_id)',
        ]);

        $this->createIndex(
            'idx-worker_profession-worker_id',
            'worker_profession',
            'worker_id'
        );

        $this->createIndex(
            'idx-worker_profession-category_worker_id',
            'worker_profession',
            'category_worker_id'
        );

        $this->createIndex(
            'idx-worker_profession-is_working',
            'worker_profession',
            'is_working'
        );

        $this->addForeignKey(
            'fk-worker_profession-worker-id',
            'worker_profession',
            'worker_id',
            'worker',
            'id'
        );

        $this->addForeignKey(
            'fk-worker_profession-type_worker-id',
            'worker_profession',
            'category_worker_id',
            'worker_category',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//
        $this->dropIndex('idx-worker_profession-worker_id', 'worker_profession');
        $this->dropIndex('idx-worker_profession-type_worker_id', 'worker_profession');

        $this->dropForeignKey('fk-worker_profession-worker-id', 'worker_profession');
        $this->dropForeignKey('fk-worker_profession-type_worker-id', 'worker_profession');
        $this->dropTable('worker_profession');

    }
}
