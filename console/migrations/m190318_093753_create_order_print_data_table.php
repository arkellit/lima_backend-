<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_print_data`.
 */
class m190318_093753_create_order_print_data_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_place', 'order_print_id',
            $this->integer()->defaultValue(0));

        $this->createTable('order_print_data', [
            'id' => $this->primaryKey(),
//            'order_id' => $this->integer()->notNull(),
//            'order_place_id' => $this->integer()->notNull(),
            'edition_id' => $this->integer()->notNull(),
            'print_type_id' => $this->integer()->notNull(),
            'print_size_id' => $this->integer()->notNull(),
            'paper_density_id' => $this->integer()->notNull(),
            'print_time_id' => $this->integer()->notNull(),
            'courier_price_id' => $this->integer()->notNull()->defaultValue(0),
            'urls' => $this->json(),
            'flyer_title' => $this->string(1000)->notNull(),
            'flyer_description' => $this->text()->notNull(),
            'comment_order_print' => $this->string(1000),
            'is_courier' => $this->smallInteger()->defaultValue(0),
            'courier_address' => $this->string(1000),
            'printing_price' => $this->double()->notNull()->defaultValue(0)
        ]);

//        $this->createIndex(
//            'idx-order_print_data-order_id',
//            'order_print_data',
//            'order_id'
//        );
//
//        $this->createIndex(
//            'idx-order_print_data-order_place',
//            'order_print_data',
//            'order_place_id'
//        );




        $this->createIndex(
            'idx-order_print_data-edition_id',
            'order_print_data',
            'edition_id'
        );

        $this->createIndex(
            'idx-order_print_data-print_type_id',
            'order_print_data',
            'print_type_id'
        );

        $this->createIndex(
            'idx-order_print_data-print_size_id',
            'order_print_data',
            'print_size_id'
        );

        $this->createIndex(
            'idx-order_print_data-paper_density_id',
            'order_print_data',
            'paper_density_id'
        );

        $this->createIndex(
            'idx-order_print_data-print_time_id',
            'order_print_data',
            'print_time_id'
        );

        $this->createIndex(
            'idx-order_print_data-is_courier',
            'order_print_data',
            'is_courier'
        );


//        $this->addForeignKey(
//            'fk-order_print_data-order-id',
//            'order_print_data',
//            'order_id',
//            'order',
//            'id'
//        );
//
//        $this->addForeignKey(
//            'fk-order_print_data-order_place-id',
//            'order_print_data',
//            'order_place_id',
//            'order_place',
//            'id'
//        );

        $this->addForeignKey(
            'fk-printing_template-order_print_data-edition_id',
            'order_print_data',
            'edition_id',
            'printing_template',
            'id'
        );
        $this->addForeignKey(
            'fk-printing_template-order_print_data-print_type_id',
            'order_print_data',
            'print_type_id',
            'printing_template',
            'id'
        );
        $this->addForeignKey(
            'fk-printing_template-order_print_data-print_size_id',
            'order_print_data',
            'print_size_id',
            'printing_template',
            'id'
        );
        $this->addForeignKey(
            'fk-printing_template-order_print_data-paper_density_id',
            'order_print_data',
            'paper_density_id',
            'printing_template',
            'id'
        );
        $this->addForeignKey(
            'fk-printing_template-order_print_data-print_time_id',
            'order_print_data',
            'print_time_id',
            'printing_template',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order_place', 'order_print_id');
        $this->dropTable('order_print_data');
    }
}
