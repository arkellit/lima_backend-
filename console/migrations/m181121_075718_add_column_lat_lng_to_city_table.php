<?php

use yii\db\Migration;

/**
 * Class m181121_075718_add_column_lat_lng_to_city_table
 */
class m181121_075718_add_column_lat_lng_to_city_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('city', 'region_id', $this->integer()->defaultValue(1)->notNull());
        $this->addColumn('city', 'lat', $this->double()->defaultValue(0)->notNull());
        $this->addColumn('city', 'lng', $this->double()->defaultValue(0)->notNull());
       // $this->renameTable('city', 'region_city');

        $this->createIndex(
            'idx-city-region_id',
            'city',
            'region_id'
        );

        $this->addForeignKey(
            'fk-city-region-id',
            'city',
            'region_id',
            'region',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //$this->renameTable('region_city', 'city');
        $this->dropColumn('city', 'region_id');
        $this->dropColumn('city', 'lat');
        $this->dropColumn('city', 'lng');

        $this->dropIndex('idx-city-region_id', 'city');
        $this->dropForeignKey('fk-city-region-id', 'city');
    }
}
