<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lima_help_question`.
 */
class m190424_060255_create_lima_help_question_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('lima_help_question', [
            'id' => $this->primaryKey(),
            'type' => $this->string(50)->notNull(),
            'question' => $this->string(255)->notNull(),
            'answer' => $this->string(2000)->notNull(),
            'sort' => $this->integer()->defaultValue(0)
        ]);

        $this->createIndex(
            'idx-lima_help_question-type',
            'lima_help_question',
            'type'
        );
        $this->createIndex(
            'idx-lima_help_question-sort',
            'lima_help_question',
            'sort'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('lima_help_question');
    }
}
