<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%order}}`.
 */
class m210125_055840_add_is_published_column_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'is_published',
            $this->smallInteger(1)->defaultValue(1));
        $this->createIndex(
            'idx-order-is_published',
            'order',
            'is_published'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'is_published');
    }
}
