<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customer_personal_invite`.
 */
class m190124_120329_alter_customer_personal_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
//        $this->alterColumn('customer_personal', 'name', $this->string(255));
//        $this->alterColumn('customer_personal', 'last_name', $this->string(255));
        $this->addColumn('customer_personal', 'is_invite', $this->smallInteger()->notNull()->defaultValue(0));
        $this->addColumn('customer_personal', 'is_register', $this->smallInteger()->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//        $this->alterColumn('customer_personal', 'name', $this->string(255)->notNull());
//        $this->alterColumn('customer_personal', 'last_name', $this->string(255)->notNull());
        $this->dropColumn('customer_personal', 'is_invite');
        $this->dropColumn('customer_personal', 'is_register');
    }
}
