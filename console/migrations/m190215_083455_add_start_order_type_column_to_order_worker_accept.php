<?php

use yii\db\Migration;

/**
 * Class m190215_083455_add_start_order_type_column_to_order_worker_accept
 */
class m190215_083455_add_start_order_type_column_to_order_worker_accept extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameTable('order_worker_accept', 'order_start_end');
        $this->addColumn('order_start_end', 'start_order_type',
            $this->smallInteger()->defaultValue(0)->notNull());
        $this->addColumn('order_start_end', 'is_cancel_order',
            $this->smallInteger()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order_start_end', 'start_order_type');
        $this->renameTable('order_start_end', 'order_worker_accept');
    }
}
