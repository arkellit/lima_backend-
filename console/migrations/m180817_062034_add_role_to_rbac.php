<?php

use yii\db\Migration;

/**
 * Class m180817_062034_add_role_to_rbac
 */
class m180817_062034_add_role_to_rbac extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;

        $admin = $auth->createRole('admin');
        $admin->description = 'Admin';
        $auth->add($admin);

        $customer = $auth->createRole('customer');
        $customer->description = 'Customer';
        $auth->add($customer);

        $worker = $auth->createRole('worker');
        $worker->description = 'Worker';
        $auth->add($worker);

        $administrator = $auth->createRole('administrator');
        $administrator->description = 'Administrator';
        $auth->add($administrator);

        $supervisor = $auth->createRole('supervisor');
        $supervisor->description = 'SuperVisor';
        $auth->add($supervisor);

        $personal = $auth->createRole('personal');
        $personal->description = 'Personal';
        $auth->add($personal);
    }

    public function down()
    {
        Yii::$app->authManager->removeAll();
    }
}
