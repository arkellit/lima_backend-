<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customer_company_data`.
 */
class m181127_105046_create_customer_company_data_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('customer_company_data', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull()->unique(),
            'name_company' => $this->string(255)->notNull(),
            'inn_company' => $this->string(15)->notNull()->unique(),
            'kpp_company' => $this->string(9)->notNull(),
            'actual_address' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull(),
            'contact_phone' => $this->string(20)->notNull(),
            'logo_company' => $this->string(255),
            'deposit' => $this->double()->defaultValue(0)->notNull(),
        ]);

        $this->createIndex(
            'idx-customer_company_data-email',
            'customer_company_data',
            'email'
        );

        $this->createIndex(
            'idx-customer_company_data-inn_company',
            'customer_company_data',
            'inn_company'
        );

        $this->addForeignKey(
            'fk-customer_company_data-customer-id',
            'customer_company_data',
            'customer_id',
            'customer',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('customer_company_data');
    }
}
