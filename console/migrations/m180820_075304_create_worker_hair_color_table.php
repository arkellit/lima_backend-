<?php

use yii\db\Migration;

/**
 * Handles the creation of table `hair_color`.
 */
class m180820_075304_create_worker_hair_color_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worker_hair_color', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('worker_hair_color');
    }
}
