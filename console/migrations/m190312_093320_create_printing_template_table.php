<?php

use yii\db\Migration;

/**
 * Handles the creation of table `printing_template`.
 */
class m190312_093320_create_printing_template_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('printing_template', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'category' => $this->string(255)->notNull(),
            'name' => $this->string(255)->notNull(),
            'price' => $this->double()->notNull()->defaultValue(0),
//            'edition' => $this->json()->notNull(),
//            'print_type' => $this->json()->notNull(),
//            'print_size' => $this->json()->notNull(),
//            'paper_density' => $this->json()->notNull(),
//            'print_time' => $this->json()->notNull(),
//            'courier_price' => $this->double()->notNull()->defaultValue(0)
        ]);

        $this->createIndex(
            'idx-printing_template-city_id',
            'printing_template',
            'city_id');

        $this->createIndex(
            'idx-printing_template-category',
            'printing_template',
            'category');

        $this->addForeignKey(
            'fk-printing_template-city_id-city-id',
            'printing_template',
            'city_id',
            'city',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('printing_template');
    }
}
