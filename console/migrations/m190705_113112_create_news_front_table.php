<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news_front`.
 */
class m190705_113112_create_news_front_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('news_front', [
            'id' => $this->primaryKey(),
            'title' => $this->string(1000)->notNull(),
            'image' => $this->string(255)->notNull(),
            'short_description' => $this->text()->notNull(),
            'description' => $this->text()->notNull(),
            'is_published' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('news_front');
    }
}
