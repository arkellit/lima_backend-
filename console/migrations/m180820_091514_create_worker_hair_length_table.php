<?php

use yii\db\Migration;

/**
 * Handles the creation of table `hair_length`.
 */
class m180820_091514_create_worker_hair_length_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worker_hair_length', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('worker_hair_length');
    }
}
