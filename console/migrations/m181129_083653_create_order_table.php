<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m181129_083653_create_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull(),
            'administrator_id' => $this->integer()->defaultValue(0)->notNull(),
            'name_order' => $this->string(255)->notNull(),
            'number_order' => $this->string(50)->unique(),
            'type_order' => $this->smallInteger()->defaultValue(0)->notNull(),
            'total_price' => $this->double()->defaultValue(0)->notNull(),
            'is_pay' => $this->smallInteger()->defaultValue(0)->notNull(),
            'is_complete_order' => $this->smallInteger()->defaultValue(0)->notNull(),
        ]);

        $this->createIndex(
            'idx-order-customer_id',
            'order',
            'customer_id'
        );

        $this->createIndex(
            'idx-order-number_order',
            'order',
            'number_order'
        );

        $this->createIndex(
            'idx-order-is_pay',
            'order',
            'is_pay'
        );

        $this->createIndex(
            'idx-order-is_complete_order',
            'order',
            'is_complete_order'
        );

        $this->addForeignKey(
            'fk-order-customer-id',
            'order',
            'customer_id',
            'customer',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('order');
    }
}
