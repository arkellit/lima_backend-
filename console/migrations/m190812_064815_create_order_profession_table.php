<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_profession`.
 */
class m190812_064815_create_order_profession_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_profession', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'order_place_id' => $this->integer()->notNull(),
            'worker_category_id' => $this->integer()->notNull(),
            'count_worker' => $this->integer()->notNull(),
            'type_job' => $this->smallInteger()->notNull()
        ]);

        $this->createIndex(
            'idx-order_profession-order_id',
            'order_profession',
            'order_id'
        );

        $this->createIndex(
            'idx-order_profession-order_place_id',
            'order_profession',
            'order_place_id'
        );

        $this->createIndex(
            'idx-order_profession-worker_category_id',
            'order_profession',
            'worker_category_id'
        );

        $this->createIndex(
            'idx-order_profession-count_worker',
            'order_profession',
            'count_worker'
        );

        $this->createIndex(
            'idx-order_profession-type_job',
            'order_profession',
            'type_job'
        );

        $this->addForeignKey(
            'fk-order_profession-order_id-order-id',
            'order_profession',
            'order_id',
            'order',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-order_profession-worker_category_id-worker_category-id',
            'order_profession',
            'worker_category_id',
            'worker_category',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-order_profession-order_place_id-order_place-id',
            'order_profession',
            'order_place_id',
            'order_place',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('order_profession');
    }
}
