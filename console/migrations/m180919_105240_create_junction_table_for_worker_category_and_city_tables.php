<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category_worker_city`.
 * Has foreign keys to the tables:
 *
 * - `category_worker`
 * - `city`
 */
class m180919_105240_create_junction_table_for_worker_category_and_city_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worker_category_city', [
            'category_worker_id' => $this->integer(),
            'city_id' => $this->integer(),
            'price' => $this->double()->notNull(),
            'advance_price' => $this->double()->defaultValue(0)->notNull(),
            'PRIMARY KEY(category_worker_id, city_id)',
        ]);

        // creates index for column `category_worker_id`
        $this->createIndex(
            'idx-worker_category_city-category_worker_id',
            'worker_category_city',
            'category_worker_id'
        );

        // add foreign key for table `category_worker`
        $this->addForeignKey(
            'fk-worker_category_city-category_worker_id',
            'worker_category_city',
            'category_worker_id',
            'worker_category',
            'id'
        );

        // creates index for column `city_id`
        $this->createIndex(
            'idx-worker_category_city-city_id',
            'worker_category_city',
            'city_id'
        );

        // add foreign key for table `city`
        $this->addForeignKey(
            'fk-worker_category_city-city_id',
            'worker_category_city',
            'city_id',
            'city',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `category_worker`
        $this->dropForeignKey(
            'fk-worker_category_city-category_worker_id',
            'worker_category_city'
        );

        // drops index for column `category_worker_id`
        $this->dropIndex(
            'idx-worker_category_city-category_worker_id',
            'worker_category_city'
        );

        // drops foreign key for table `city`
        $this->dropForeignKey(
            'fk-worker_category_city-city_id',
            'worker_category_city'
        );

        // drops index for column `city_id`
        $this->dropIndex(
            'idx-worker_category_city-city_id',
            'worker_category_city'
        );

        $this->dropTable('worker_category_city');
    }
}
