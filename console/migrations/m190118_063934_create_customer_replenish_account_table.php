<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customer_replenish_account`.
 */
class m190118_063934_create_customer_replenish_account_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('customer_replenish_account', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull(),
            'account_number' => $this->string()->notNull(),
            'date' => $this->integer()->notNull(),
            'description' => $this->text(),
            'amount' => $this->double()->notNull(),
            'created_at' => $this->integer()
        ]);

        $this->createIndex(
            'idx-customer_replenish_account-customer_id',
            'customer_replenish_account',
            'customer_id'
        );

        $this->addForeignKey(
            'fk-customer_replenish_account-customer-id',
            'customer_replenish_account',
            'customer_id',
            'customer',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('customer_replenish_account');
    }
}
