<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_request`.
 */
class m181207_075731_create_order_request_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_request', [
            'id' => $this->primaryKey(),
            'worker_id' => $this->integer()->notNull(),
            'order_id' => $this->integer()->notNull(),
            'order_place_id' => $this->integer()->notNull(),
            'is_lima_exchange' => $this->smallInteger()->defaultValue(0)->notNull(),
            'status' => $this->smallInteger()->defaultValue(0)->notNull(),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'updated_at' => $this->integer()->notNull()->defaultValue(0)
        ]);

        $this->createIndex(
            'idx-order_request-worker_id',
            'order_request',
            'worker_id'
        );
        $this->createIndex(
            'idx-order_request-order_id',
            'order_request',
            'order_id'
        );
        $this->createIndex(
            'idx-order_request-order_place_id',
            'order_request',
            'order_place_id'
        );
        $this->createIndex(
            'idx-order_request-is_lima_exchange',
            'order_request',
            'is_lima_exchange'
        );
        $this->createIndex(
            'idx-order_request-status',
            'order_request',
            'status'
        );

        $this->addForeignKey(
            'fk-order_request-worker-id',
            'order_request',
            'worker_id',
            'worker',
            'id'
        );

        $this->addForeignKey(
            'fk-order_request-order-id',
            'order_request',
            'order_id',
            'order',
            'id'
        );

        $this->addForeignKey(
            'fk-order_request-order_place-id',
            'order_request',
            'order_place_id',
            'order_place',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('order_request');
    }
}
