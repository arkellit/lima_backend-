<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customer_personal`.
 */
class m181129_081413_create_customer_personal_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('customer_personal', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->unique(),
            'customer_id' => $this->integer()->notNull(),
            'name' => $this->string(255),
            'last_name' => $this->string(255),
            'middle_name' => $this->string(255),
            'email' => $this->string(255),
            'photo' => $this->string(255),
            'role' => $this->string(50)->notNull(),
            'branch_id' => $this->integer(),
            'is_delete' => $this->smallInteger()->defaultValue(0)
        ]);

        $this->createIndex(
            'idx-customer_personal-user_id',
            'customer_personal',
            'user_id'
        );

        $this->createIndex(
            'idx-customer_personal-customer_id',
            'customer_personal',
            'customer_id'
        );

        $this->createIndex(
            'idx-customer_personal-is_delete',
            'customer_personal',
            'is_delete'
        );

        $this->addForeignKey(
            'fk-customer_personal-user-id',
            'customer_personal',
            'user_id',
            'user',
            'id'
        );

//        $this->addForeignKey(
//            'fk-customer_personal-customer-id',
//            'customer_personal',
//            'customer_id',
//            'customer',
//            'id'
//        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('customer_personal');
    }
}
