<?php

use yii\db\Migration;

/**
 * Handles the creation of table `change_user`.
 */
class m181004_164812_create_user_change_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_change', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->unique(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'type' => $this->smallInteger()->notNull()->defaultValue(0),
        ]);

        $this->createIndex(
            'idx-user_change-user_id',
            'user_change',
            'user_id'
        );


        $this->addForeignKey(
            'fk-user_change-user_id-user-id',
            'user_change',
            'user_id',
            'user',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_change');
    }
}
