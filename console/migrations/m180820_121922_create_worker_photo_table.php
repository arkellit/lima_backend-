<?php

use yii\db\Migration;

/**
 * Handles the creation of table `image_worker`.
 */
class m180820_121922_create_worker_photo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worker_photo', [
            'worker_id' => $this->integer()->notNull(),
            'photo_porter' => $this->string(255)-> notNull(),
            'photo_full_length' => $this->string(255)->notNull(),
            'PRIMARY KEY(worker_id)',
        ]);

        $this->createIndex(
            'idx-worker_photo-worker_id',
            'worker_photo',
            'worker_id'
        );

        $this->addForeignKey(
            'fk-worker_photo-worker_id',
            'worker_photo',
            'worker_id',
            'worker',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('worker_photo');
    }
}
