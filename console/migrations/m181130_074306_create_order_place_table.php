<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_place`.
 */
class m181130_074306_create_order_place_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_place', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'order_address_id' => $this->integer()->notNull(),
            'worker_category_id' => $this->integer()->notNull(),
            'type_job' => $this->smallInteger()->notNull(),
            'date_time_start' => $this->bigInteger()->notNull(),
            'date_time_end' => $this->bigInteger()->notNull(),
            'count_hour' => $this->double()->defaultValue(0)->notNull(),
            'count_worker' => $this->integer()->defaultValue(0)->notNull(),
            'comment' => $this->string(500),
            // Если 0, то супервайзер сам заказчик
            'personal_id' => $this->integer()->defaultValue(0)->notNull(),
            'order_price' => $this->double()->defaultValue(0)->notNull(),
            'is_complete_worker' => $this->smallInteger()->defaultValue(0)->notNull(),
            'is_complete_order' => $this->smallInteger()->defaultValue(0)->notNull(),
        ]);

        $this->createIndex(
            'idx-order_place-order_id',
            'order_place',
            'order_id'
        );

        $this->createIndex(
            'idx-order_place-order_address_id',
            'order_place',
            'order_address_id'
        );

        $this->createIndex(
            'idx-order_place-worker_category_id',
            'order_place',
            'worker_category_id'
        );

        $this->createIndex(
            'idx-order_place-type_job',
            'order_place',
            'type_job'
        );

        $this->createIndex(
            'idx-order_place-personal_id',
            'order_place',
            'personal_id'
        );

        $this->createIndex(
            'idx-order_place-is_complete_worker',
            'order_place',
            'is_complete_worker'
        );

        $this->createIndex(
            'idx-order_place-is_complete_order',
            'order_place',
            'is_complete_order'
        );

        $this->addForeignKey(
            'fk-order_place-order-id',
            'order_place',
            'order_id',
            'order',
            'id'
        );

        $this->addForeignKey(
            'fk-order_place-order_address-id',
            'order_place',
            'order_address_id',
            'order_address',
            'id'
        );

        $this->addForeignKey(
            'fk-order_place-worker_category-id',
            'order_place',
            'worker_category_id',
            'worker_category',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('order_place');
    }
}
