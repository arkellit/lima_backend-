<?php

use yii\db\Migration;

/**
 * Handles the creation of table `worker`.
 */
class m180820_114947_create_worker_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worker', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'last_name' => $this->string(255)->notNull(),
            'middle_name' => $this->string(255),
            'sex' => $this->smallInteger()->notNull(),
            'birth_date' => $this->date()->notNull(),
            'height' => $this->integer()->notNull(),
            'weight' => $this->integer()->notNull(),

            'body_complexion_id' => $this->integer()->notNull(),
            'hair_color_id' => $this->integer()->notNull(),
            'hair_length_id' => $this->integer()->notNull(),
            'appearance_id' => $this->integer()->notNull(),
            'eye_color_id' => $this->integer()->notNull(),
            'city_id' => $this->integer()->notNull(),

            'last_location_lat' => $this->double()->defaultValue(0)->notNull(),
            'last_location_lng' => $this->double()->defaultValue(0)->notNull(),

            'is_register_complete' => $this->smallInteger()->notNull()->defaultValue(0),
            'is_instruction_complete' => $this->smallInteger()->notNull()->defaultValue(0),
        ]);

//        $this->execute("SELECT AddGeometryColumn('worker', 'point', 4326, 'POINT', 2)");

        $this->createIndex(
            'idx-worker-user_id',
            'worker',
            'user_id'
        );

//        $this->createIndex(
//            'ids-worker-point',
//            'worker',
//            'point'
//        );

        $this->createIndex(
            'idx-worker-eye_color_id',
            'worker',
            'eye_color_id'
        );

        $this->createIndex(
            'idx-worker-sex',
            'worker',
            'sex'
        );

        $this->createIndex(
            'idx-worker-birth_date',
            'worker',
            'birth_date'
        );

        $this->createIndex(
            'idx-worker-height',
            'worker',
            'height'
        );

        $this->createIndex(
            'idx-worker-hair_length_id',
            'worker',
            'hair_length_id'
        );


        $this->createIndex(
            'idx-worker-body_complexion_id',
            'worker',
            'body_complexion_id'
        );
        $this->createIndex(
            'idx-worker-hair_color_id',
            'worker',
            'hair_color_id'
        );

        $this->createIndex(
            'idx-worker-appearance_id',
            'worker',
            'appearance_id'
        );
        $this->createIndex(
            'idx-worker-city_id',
            'worker',
            'city_id'
        );


        $this->createIndex(
            'idx-worker-is_register_complete',
            'worker',
            'is_instruction_complete'
        );

        $this->addForeignKey(
          'fk-user-worker-user_id',
          'worker',
          'user_id',
          'user',
          'id'
        );



        $this->addForeignKey(
            'fk-body_complexion-worker-body_complexion_id',
            'worker',
            'body_complexion_id',
            'worker_body_complexion',
            'id'
        );

        $this->addForeignKey(
            'fk-hair_color-worker-hair_color_id',
            'worker',
            'hair_color_id',
            'worker_hair_color',
            'id'
        );

        $this->addForeignKey(
            'fk-hair_color-worker-hair_length_id',
            'worker',
            'hair_length_id',
            'worker_hair_length',
            'id'
        );

        $this->addForeignKey(
            'fk-appearance-worker-appearance_id',
            'worker',
            'appearance_id',
            'worker_appearance',
            'id'
        );
        $this->addForeignKey(
            'fk-eye_color-worker-eye_color_id',
            'worker',
            'eye_color_id',
            'worker_eye_color',
            'id'
        );
        $this->addForeignKey(
            'fk-city-worker-city_id',
            'worker',
            'city_id',
            'city',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('worker');
    }
}
