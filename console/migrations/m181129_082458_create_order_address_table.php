<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_address`.
 */
class m181129_082458_create_order_address_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_address', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull(),
            'city_id' => $this->integer()->notNull(),
            'branch_id' => $this->integer()->defaultValue(0)->notNull(),
            'place_order' => $this->string(255)->notNull(),
            'address_order' => $this->string(255)->notNull(),
            'metro_station' => $this->string(255),
            'landmark' => $this->string(255),
            'lat' => $this->double()->defaultValue(0)->notNull(),
            'lng' => $this->double()->defaultValue(0)->notNull(),
        ]);

        $this->createIndex(
            'idx-order_address-customer_id',
            'order_address',
            'customer_id'
        );

        $this->createIndex(
            'idx-order_address-city_id',
            'order_address',
            'city_id'
        );

        $this->createIndex(
            'idx-order_address-branch_id',
            'order_address',
            'branch_id'
        );

        $this->addForeignKey(
            'fk-order_address-customer-id',
            'order_address',
            'customer_id',
            'customer',
            'id'
        );

        $this->addForeignKey(
            'fk-order_address-city-id',
            'order_address',
            'city_id',
            'city',
            'id'
        );

//        $this->addForeignKey(
//            'fk-order_address-customer_branch-id',
//            'order_address',
//            'branch_id',
//            'customer_branch',
//            'id'
//        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('order_address');
    }
}
