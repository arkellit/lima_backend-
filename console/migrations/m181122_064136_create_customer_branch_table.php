<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customer_branch`.
 */
class m181122_064136_create_customer_branch_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('customer_branch', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull(),
            'city_id' => $this->integer()->notNull(),
            'branch' => $this->string(255)->notNull(),
            'is_delete' => $this->smallInteger()->defaultValue(0)
        ]);

        $this->createIndex(
            'idx-customer_branch-customer_id',
            'customer_branch',
            'customer_id'
        );

        $this->createIndex(
            'idx-customer_branch-city_id',
            'customer_branch',
            'city_id'
        );

        $this->createIndex(
            'idx-customer_branch-is_delete',
            'customer_branch',
            'is_delete'
        );

        $this->addForeignKey(
            'fk-customer_branch-customer-id',
            'customer_branch',
            'customer_id',
            'customer',
            'id'
        );

        $this->addForeignKey(
            'fk-customer_branch-city-id',
            'customer_branch',
            'city_id',
            'city',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('customer_branch');
    }
}
