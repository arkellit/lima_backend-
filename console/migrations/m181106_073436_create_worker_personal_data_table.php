<?php

use yii\db\Migration;

/**
 * Handles the creation of table `worker_card_details`.
 */
class m181106_073436_create_worker_personal_data_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worker_personal_data', [
            'id' => $this->primaryKey(),
            'worker_id' => $this->bigInteger()->notNull(),
            'worker_inn' => $this->text()->notNull(),
            'worker_snils' => $this->text()->notNull(),
            'worker_ls_bank' => $this->text()->notNull(),
            'bank_inn' => $this->text(),
            'bank_kpp' => $this->text()->notNull(),
            'bank_ks' => $this->text()->notNull(),
            'bank_name' => $this->text()->notNull(),
            'bank_bik' => $this->text()->notNull()
        ]);

        $this->createIndex(
            'idx-worker_personal_data-worker_id',
            'worker_personal_data',
            'worker_id'
        );

        $this->addForeignKey(
            'fk-worker_personal_data-worker-id',
            'worker_personal_data',
            'worker_id',
            'worker',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('worker_personal_data');
    }
}
