<?php

use yii\db\Migration;

/**
 * Handles the creation of table `image_news`.
 */
class m181002_070428_create_news_image_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('news_image', [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer()->notNull(),
            'image' => $this->string(255)->notNull()
        ]);

        $this->createIndex(
            'idx-news_image-news_id',
            'news_image',
            'news_id'
        );

        $this->addForeignKey(
            'fk-news_image-user_id-news-id',
            'news_image',
            'news_id',
            'news',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('news_image');
    }
}
