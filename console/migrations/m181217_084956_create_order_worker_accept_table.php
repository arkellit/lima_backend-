<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_worker_accept`.
 */
class m181217_084956_create_order_worker_accept_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_worker_accept', [
            'id' => $this->primaryKey(),
            'order_request_id' => $this->integer()->unique()->notNull(),
            //'status_order' => '',
            'is_start_job' => $this->smallInteger()->defaultValue(0)->notNull(),
            'is_end_job' => $this->smallInteger()->defaultValue(0)->notNull(),
            'is_finished_job' => $this->smallInteger()->defaultValue(0)->notNull(),
        ]);

        $this->createIndex(
            'idx-order_worker_accept-order_request_id',
            'order_worker_accept',
            'order_request_id'
        );

        $this->createIndex(
            'idx-order_worker_accept-is_start_job',
            'order_worker_accept',
            'is_start_job'
        );

        $this->createIndex(
            'idx-order_worker_accept-is_end_job',
            'order_worker_accept',
            'is_end_job'
        );

        $this->createIndex(
            'idx-order_worker_accept-is_finished_job',
            'order_worker_accept',
            'is_finished_job'
        );

        $this->addForeignKey(
            'fk-order_worker_accept-order_request-id',
            'order_worker_accept',
            'order_request_id',
            'order_request',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('order_worker_accept');
    }
}
