<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m181002_070427_create_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'image' => $this->string(255)->notNull(),
            'short_description' => $this->string(1000)->notNull(),
            'full_description' => $this->text()->notNull(),
            'created_at' => $this->integer(),
            'is_send_push' => $this->smallInteger()->defaultValue(0)->notNull(),
            'is_published' => $this->smallInteger()->defaultValue(0)->notNull()
        ]);


        $this->createIndex(
            'idx-news-is_send_push',
            'news',
            'is_send_push'
        );

        $this->createIndex(
            'idx-news-is_published',
            'news',
            'is_published'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('news');
    }
}
