<?php

class m181022_095222_create_chat_collection extends \yii\mongodb\Migration
{
    public function up()
    {
        $this->createCollection('chat');
        $this->createIndex('chat', 'user_from_id');
        $this->createIndex('chat', 'user_to_id');
    }

    public function down(){
        $this->dropIndex('chat', 'user_from_id');
        $this->dropIndex('chat', 'user_to_id');
        $this->dropCollection('chat');
    }
}
