<?php

namespace console\controllers;

use api\modules\v1\models\Api;
use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\order\Order;
use api\modules\v1\models\order\OrderPlace;
use api\modules\v1\models\order\OrderRequest;
use api\modules\v1\models\order\OrderStartEnd;
use api\modules\v1\models\order\OrderWorker;
use api\modules\v1\models\worker\PersonalData;
use common\helpers\DifferenceTimeHelpers;
use common\helpers\EncryptionHelpers;
use frontend\models\User;
use yii\console\Controller;

/**
 * Class OrderStartEndController
 * @package console\controllers
 */
class OrderStartEndController extends Controller
{
    public function actionIndex(){
        $order_starts = OrderStartEnd::find()
            ->where(['is_start_job' => 0])
            ->orWhere(['is_end_job' => 0])
            ->joinWith('orderRequest')
            ->all();

        if (!empty($order_starts)){
            foreach ($order_starts as $order_start) {
                $order_place = OrderPlace::find()
                    ->where(['order_id' => $order_start->order_id])
                    ->andWhere(['is_cancel' => OrderPlace::CANCEL_FALSE])
                    ->one();
                if (!empty($order_place)) {
                    if ($order_start->is_start_job == 0){
                        if (DifferenceTimeHelpers::differenceTime($order_place->date_time_start) >= 5){
//                        $order_worker = OrderWorker::find()
//                            ->where(['order_id' => $order_start->order_id])
//                            ->andWhere(['status' => OrderWorker::STATUS_START_JOB])
//                            ->count();
//
//                        if ($order_worker == (int)$order_place->count_worker){
//                            $order_place->is_complete_worker = 1;
//                            $order_start->start_order_type = OrderStartEnd::ORDER_NORM_START;
//                        }
//                        else{
//                            $order_start->start_order_type = OrderStartEnd::ORDER_BAD_START;
//                        }
                            //                        $order_place->save();

                            $count_worker_complete_requests = OrderRequest::find()
                                ->where(['order_place_id' => $order_place->id])
                                ->andWhere(['status' => OrderRequest::STATUS_ACCEPT])
                                ->all();
                            if (count($count_worker_complete_requests) != 0) {
                                $order_start->is_start_job = 1;
                                $order_start->save();
                                $order_start->trigger(OrderStartEnd::EVENT_SET_START_ORDER);
                                $customer = Customer::findOne($order_place->order->customer_id);
                                Api::sendOrder(
                                    $customer->companyData->name_company,
                                    $customer->companyData->inn_company,
                                    $customer->id,
                                    $customer->user->created_at,
                                    EncryptionHelpers::dec_enc('decrypt', $customer->bankData->customer_rs),
                                    $customer->bankData->bank_bik,
                                    'Услуга по заказу №' . $order_place->order->number_order,
                                    $order_place->order->created_at,
                                    $order_place->order->total_price + ($order_place->order->total_price * 0.14),//$customer->companyData->deposit,
                                    $order_start->order_id,
                                    $customer->companyData->email,
                                    $customer->last_name . ' ' . $customer->name . ' ' . $customer->middle_name,
                                    $customer->companyData->kpp_company);
                            }
                            else {
                                Order::updateAll(['is_complete_order' => 1], ['id' => $order_place->order_id]);
                                $order_place->is_cancel = OrderPlace::CANCEL_TRUE;
                                $order_place->is_complete_order = 1;
                                $order_place->save();
                            }

                        }
                    }
                    elseif ($order_start['is_start_job'] == 1){
                        if (DifferenceTimeHelpers::differenceTime($order_place->date_time_start) >= 900
                            && DifferenceTimeHelpers::differenceTime($order_place->date_time_start) <= 1200){
                            $order_worker = OrderWorker::find()
                                ->where(['order_id' => $order_start->order_id])
                                ->andWhere(['status' => OrderWorker::STATUS_START_JOB])
                                ->count();

                            if ($order_worker == (int)$order_place->count_worker){
                                $order_place->is_complete_worker = 1;
                                $order_start->start_order_type = OrderStartEnd::ORDER_NORM_START;
                            }
                            else{
                                $order_start->start_order_type = OrderStartEnd::ORDER_BAD_START;
                            }
                            $order_place->save();
                            $order_start->save();
                        }
                        if (DifferenceTimeHelpers::differenceTime($order_place->date_time_end) >= 5){
                            $order = $order_place->order;
                            $order_start->is_end_job = 1;
                            $order_start->is_finished_job = 1;
                            $order_place->is_complete_order = 1;
                            $order->is_complete_order = 1;
                            $order_place->save();
                            $order->save();
                            $order_start->save();
                            $order_start->trigger(OrderStartEnd::EVENT_SET_END_ORDER);

                            $order_workers = OrderWorker::find()
                                ->where(['order_place_id' => $order_place->id])
                                ->andWhere(['NOT', ['attention' => OrderWorker::ATTENTION_DID_COME_WORK]])
                                ->all();
                            foreach ($order_workers as $order_worker) {
                                $worker = $order_worker->worker;
                                $user = User::findOne($worker->user_id);
                                $worker_data = PersonalData::findOne(['worker_id' => $worker->id]);
                                $customer = Customer::findOne($order_place->order->customer_id);
                                $sum_one_worker = $order_place->order->total_price / count($order_workers);
                                $api_1c_worker = Api::sendWorker(
                                    $worker->last_name . ' ' . $worker->name,
                                    EncryptionHelpers::dec_enc('decrypt', $worker_data->worker_inn),
                                    $worker->id,
                                    $user->created_at, EncryptionHelpers::dec_enc('decrypt', $worker_data->worker_ls_bank),
                                    EncryptionHelpers::dec_enc('decrypt', $worker_data->bank_bik),
                                    $order_place->date_time_end,
                                    $order_place->id,
                                    'Услуга по заказу №' . $order_place->order->number_order . ' ' .
                                    'с исполнителем ' . $worker->last_name . ' ' . $worker->name,
                                    $sum_one_worker + ($sum_one_worker * 0.06),
                                    $customer->companyData->inn_company, $customer->id,
                                    $customer->user->created_at,
                                    $order_place->order->created_at,
                                    $customer->companyData->deposit,
                                    $worker->last_name . ' ' . $worker->name .
                                ' ' . $worker->middle_name);
                            }
                        }
                    }
                }
            }
        }
    }
}