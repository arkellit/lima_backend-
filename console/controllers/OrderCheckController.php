<?php

namespace console\controllers;

use api\modules\v1\models\order\Order;
use api\modules\v1\models\order\OrderRequest;
use api\modules\v1\models\Sending;
use common\helpers\DifferenceTimeHelpers;
use yii\console\Controller;

/**
 * Class OrderCheckController
 * @package console\controllers
 */
class OrderCheckController extends Controller
{
    public function actionIndex(){
        $orders = Order::find()
            ->where(['is_pay' => Order::IS_PAY,
                'order.is_complete_order' => Order::NO_COMPLETE_ORDER])
            ->joinWith('orderPlace')
            ->andWhere(['<', 'order_place.date_time_start', time()])
            ->all();

        foreach ($orders as $order) {
            if ((time() - DifferenceTimeHelpers::differenceTime($order->
                orderPlace->date_time_start)) <= 3*60*60){
                $order_place_count_worker = $order->orderPlace->count_worker;
                $count_worker_complete_request = OrderRequest::find()
                    ->where(['order_place_id' => $order->orderPlace->id])
                    ->andWhere(['status' => OrderRequest::STATUS_ACCEPT])->count();
                if ($order_place_count_worker < $count_worker_complete_request) {
                    $user_id = $order->customer->user_id;
                    Sending::sendNotice($user_id, 'Lima',
                        'На Вашем заказе №' . $order->number_order . '  ' .
                        'не набралось нужное кол-во исполнителей');
                }
            }
        }
    }
}