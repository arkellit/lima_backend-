<?php

namespace console\controllers;

use api\modules\v1\models\customer\Customer;
use common\helpers\EncryptionHelpers;
use yii\console\Controller;
use api\modules\v1\models\Api;

/**
 * Sync customer data from 1C server
 *
 * Class SyncOnesController
 * @package console\controllers
 */
class SyncOnesController extends Controller
{
    public function actionIndex(){
        $customers = Customer::findAll(['is_sync_ones' => Customer::NO_SYNC_ONES]);
        foreach ($customers as $customer) {
            $model_api = Api::sendCustomer($customer->companyData->name_company,
                $customer->companyData->inn_company,
                $customer->id, time(), EncryptionHelpers::dec_enc('decrypt',
                    $customer->bankData->customer_rs),
                $customer->bankData->bank_bik,
                $customer->last_name . ' ' .
                $customer->name . ' ' . $customer->middle_name,
                $customer->companyData->kpp_company);
            if ($model_api == true) {
                $customer->is_sync_ones = Customer::IS_SYNC_ONES;
                $customer->save();
            }
        }

    }
}