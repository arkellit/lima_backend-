<?php


namespace console\controllers;


use api\modules\v1\models\auth\User;
use yii\console\Controller;
use Yii;

class AddAdminController extends Controller
{
    public function actionIndex(){
        $user = new User();
        $user->phone = '89000000000';
        $user->auth_key = Yii::$app->security->generateRandomString();
        $user->access_token = User::setAccessToken($user->phone);
        $user->setPassword('$2y$10$4npqAWSj8/eTpKLXxg9svOA2YCBMgygppI9459UP8rCUnIk2OO4Wm');
        $user->registration_ip = '127.0.0.1';
        $user->created_at = time();
        $user->updated_at = time();
        $user->is_confirm = 1;
        if ($user->save()){
            $auth = Yii::$app->authManager;
            $role = $auth->getRole('admin');
            $auth->assign($role, $user->id);
        }
        var_dump($user->getErrors());

    }
}