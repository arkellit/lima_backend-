<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 04.03.19
 * Time: 18:27
 */

namespace console\controllers;

use api\modules\v1\models\worker\WorkerConfirmPay;
use common\helpers\WorkerPayHelpers;
use YandexCheckout\Client;
use Yii;
use yii\console\Controller;

/**
 * Class WorkerCheckPayController
 * @package console\controllers
 */
class WorkerCheckPayController extends Controller
{
    public function actionIndex(){
        $model_workers_pays = WorkerConfirmPay::findAll([
            'status' => WorkerConfirmPay::PENDING,
            'confirm_refund' => WorkerConfirmPay::NO_CONFIRM]);
        $client = new Client();
        $client->setAuth(Yii::$app->params['shop_id_yandex'], Yii::$app->params['token_password_yandex']);
        foreach ($model_workers_pays as $model_workers_pay){
             $payment = $client->getPaymentInfo($model_workers_pay->payment_id);
             if($payment->status == WorkerConfirmPay::SUCCEEDED){
                 $refund_pay = WorkerPayHelpers::RefundPay($client, $model_workers_pay->payment_id);
                 if ($refund_pay->status == WorkerConfirmPay::SUCCEEDED){
                     $model_workers_pay->status = WorkerConfirmPay::SUCCEEDED;
                     $model_workers_pay->confirm_refund = WorkerConfirmPay::CONFIRM_REFUND;
                     $model_workers_pay->save();
                 }
             }
        }
    }
}