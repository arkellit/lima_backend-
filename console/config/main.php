<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],

    'controllerMap' => [
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
          ],
        'mongodb-migrate' => [
            'class' => 'yii\mongodb\console\controllers\MigrateController',
            'migrationPath' => '@console/mongo-migrations/', // allows to disable not namespaced migration completely
          ],
    ],
    'components' => [
        'authManager'  => [
            'class'    => 'yii\rbac\DbManager',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'fcm' => [
            'class' => 'understeam\fcm\Client',
            'apiKey' => 'AAAApeXKsNc:APA91bFnL5ghNk1sOKpvBeykrHGMd0pprQK1BhltGcCfpJ01yelHixXTx9JTDBTYCRTf8ziuxrHWpSLeRuaQ-TOj5ljxU2gWCt4OnFu-jejZd2oxd1FKKO9eoXGuej7fsHZn2cIbVhC6', // Server API Key (you can get it here: https://firebase.google.com/docs/server/setup#prerequisites)
        ],
    ],
    'params' => $params,
];
