<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'bootstrap' => ['log'],
    'language' => 'ru',
    'homeUrl'=> '/',
    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\Module',
        ],
    ],
    'components' => [
        'request' => [
            'baseUrl' => '',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            'csrfParam' => '_csrf-api',
            'enableCookieValidation' => false,
            'enableCsrfValidation' => false,
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                if ($response->data !== null && Yii::$app->request->get('suppress_response_code')) {
                    $response->data = [
                        'success' => $response->isSuccessful,
                        'data' => $response->data,
                    ];
                    $response->statusCode = 200;
                }
            },
        ],
        'user' => [
            'identityClass' => 'api\modules\v1\models\auth\User',
            'enableSession' => false
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-api',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/user',
                    'only' => ['registration', 'login', 'index', 'forgot-password',
                        'confirm-forgot','forgot-resend-code',
                        'reset-password', 'confirm-phone', 'delete-user', 'resend-code',
                        'update-fcm-token', 'logout', 'social-login', 'social-phone',
                        'send-push', 'delete-customer', 'test-send-push'],
                    'extraPatterns' => [
                        'POST login' => 'login',
                        'POST registration' => 'registration',
                        'POST forgotPassword' => 'forgot-password',
                        'POST forgotResendCode' => 'forgot-resend-code',
                        'POST confirmForgot' => 'confirm-forgot',
                        'POST resetPassword' => 'reset-password',
                        'POST confirmPhone' => 'confirm-phone',
                        'POST delete' => 'delete-user',
                        'POST deleteCustomer' => 'delete-customer',
                        'POST resendCode' => 'resend-code',
                        'POST updateFcmToken' => 'update-fcm-token',
                        'POST logout' => 'logout',
                        'POST socialLogin' => 'social-login',
                        'POST socialPhone' => 'social-phone',
                        'POST sendPush' => 'send-push',
                        'POST testSendPush' => 'test-send-push'
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/worker' => 'v1/worker/data'],
                    'only' => ['get-parameters'],

                    'extraPatterns' => [
                        'GET getParameters' => 'get-parameters',
                    ]
                ],

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/worker' => 'v1/worker/worker-api'],
                    'only' => ['checkFreelancer'],

                    'extraPatterns' => [
                        'POST checkFreelancer' => 'checkFreelancer',
                    ]
                ],

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/limaHelp' => 'v1/limahelp/lima-help'],
                    'only' => ['create', 'get-dialog', 'read-message'],
                    'extraPatterns' => [
                        'POST create' => 'create',
                        'POST readMessage' => 'read-message',
                        'GET getDialog' => 'get-dialog',
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/worker' => 'v1/worker/worker'],
                    'only' => ['create', 'complete-instruction', 'change-password',
                        'change-passport', 'get-info', 'get-edit-profile', 'update',
                        'temp-photo', 'get-my-orders', 'set-location',
                        'get-lima-exchange', 'get-load-lima-exchange',
                        'get-sort-lima-exchange', 'get-load-sort-lima-exchange',
                        'reject-order', 'get-profile', 'pay', 'refund-payment',
                        'repeat-payment'],
                    'extraPatterns' => [
                        'POST create' => 'create',
                        'POST tempPhoto' => 'temp-photo',
                        'PUT update' => 'update',
                        'PATCH update' => 'update',
                        'POST changePassword' => 'change-password',
                        'POST changePassport' => 'change-passport',
                        'PUT completeInstruction' => 'complete-instruction',
                        'GET getInfo' => 'get-info',
                        'GET getEditProfile' => 'get-edit-profile',
                        'GET getMyOrders' => 'get-my-orders',
                        'PATCH completeInstruction' => 'complete-instruction',
                        'POST setLocation' => 'set-location',
                        'POST rejectOrder' => 'reject-order',
                        'POST pay' => 'pay',
                        'POST refundPayment' => 'refund-payment',
                        'POST repeatPayment' => 'repeat-payment',
                        'GET getLimaExchange' => 'get-lima-exchange',
                        'GET getLoadLimaExchange/<id>' => 'get-load-lima-exchange',
                        'GET getSortLimaExchange' => 'get-sort-lima-exchange',
                        'GET getProfile' => 'get-profile'
//                        'GET getLoadSortLimaExchange/<category_worker>/<radius_search>/<sort_type>/<number>' => 'get-load-sort-lima-exchange'
                    ]
                ],
//                [
//                    'class' => 'yii\rest\UrlRule',
//                    'controller' => ['v1/customer' => 'v1/customer/manager'],
//                    'only' => ['create', 'complete-instruction'],
//                    'extraPatterns' => [
//                        'POST create' => 'create',
//                        'PUT completeInstruction' => 'complete-instruction',
//                        'PATCH completeInstruction' => 'complete-instruction'
//                    ]
//                ],

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/customer' => 'v1/customer/customer',
                        ],
                    'only' => ['create', 'createBranch', 'createPersonal',
                        'createNewPersonal', 'getRequestPersonal',
                        'addRole', 'createInvitePersonal', 'getBranch', 'getProfile',
                        'getRequestOrder', 'editProfile', 'changePassword', 'uploadPhoto',
                        'getSupervisor', 'getPersonal', 'getOrders', 'getLoadOrders',
                        'getFilterRequestWorker', 'getLoadFilterWorker',
                        'setPersonalBranch', 'revokePersonalBranch',
                        'deleteBranch', 'makeDeposit', 'getOrderWorkers', 'readQrCode',
                        'removeWorkerFromRequest', 'confirmWorkerRequest',
                        'getConfirmWorker'],
                    'extraPatterns' => [
                        'POST create' => 'create',
                        'POST createBranch' => 'createBranch',
                        'POST createPersonal' => 'createPersonal',
                        'POST createNewPersonal' => 'createNewPersonal',
                        'POST addRole' => 'addRole',
                        'POST createInvitePersonal' => 'createInvitePersonal',
                        'POST editProfile' => 'editProfile',
                        'POST changePassword' => 'changePassword',
                        'POST uploadPhoto' => 'uploadPhoto',
                        'POST makeDeposit' => 'makeDeposit',
                        'POST readQrCode' => 'readQrCode',
                        'POST removeWorkerFromRequest' => 'removeWorkerFromRequest',
                        'POST confirmWorkerRequest' => 'confirmWorkerRequest',
                        'GET getRequestPersonal' => 'getRequestPersonal',
                        'GET getBranch' => 'getBranch',
                        'GET getProfile' => 'getProfile',
                        'GET getRequestOrder' => 'getRequestOrder',
                        'GET getSupervisor' => 'getSupervisor',
                        'GET getPersonal' => 'getPersonal',
                        'GET getOrders' => 'getOrders',
                        //'GET getLoadOrders/<type>/<number>' => 'getLoadOrders',
                        'GET getFilterRequestWorker' => 'getFilterRequestWorker',
                        'GET getOrderWorkers' => 'getOrderWorkers',
                        'GET getConfirmWorker' => 'getConfirmWorker',
                        //'GET getLoadFilterWorker/<category_worker>/<city_id>/<radius_search>/<lat>/<lng>/<sex>/<height_start>/<height_end>/<eye_color_id>/<hair_length_id>/<body_complexion_id>/<appearance_id>/<age_start>/<age_end>/<number>' => 'getLoadFilterWorker'
                        'PUT setPersonalBranch' => 'setPersonalBranch',
                        'PUT revokePersonalBranch' => 'revokePersonalBranch',
                        'DELETE deleteBranch' => 'deleteBranch',

                    ]
                ],

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/customer' => 'v1/customer/payment'],
                    'only' => ['replenishAccount'],
                    'extraPatterns' => [
                        'POST replenishAccount' => 'replenishAccount',
                    ]
                ],

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/order' => 'v1/order/order'],
                    'only' => ['create', 'typeSelectWorker', 'addRequest',
                        'responseRequest', 'getLimaExchange', 'getLoadLimaExchange',
                        'getOrder', 'startOrder'],
                    'extraPatterns' => [
                        'POST create' => 'create',
                        'POST typeSelectWorker' => 'typeSelectWorker',
                        'POST addRequest' => 'addRequest',
                        'POST responseRequest' => 'responseRequest',
                        'GET getLimaExchange' => 'getLimaExchange',
                        'GET getLoadLimaExchange/<id>' => 'getLoadLimaExchange',
                        'GET getOrder' => 'getOrder',
                        'POST startOrder' => 'startOrder'
                    ]
                ],

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/personal' => 'v1/customer/personal'],
                    'only' => ['create', 'confirm-phone', 'send-login'],
                    'extraPatterns' => [
                        'POST create' => 'create',
                        'POST confirmPhone' => 'confirm-phone',
                        'POST sendLogin' => 'send-login'
                    ]
                ],

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/notice' => 'v1/notification'],
                    'only' => ['get-all', 'get-all-customer'],
                    'extraPatterns' => [
                        'GET getAll' => 'get-all',
                        'GET getAllCustomer' => 'get-all-customer'
                    ]
                ],

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/static' => 'v1/data'],
                    'only' => ['get-city', 'get-category', 'get-price',
                        'get-print-data', 'get-lima-help'],
                    'extraPatterns' => [
                        'GET getCity' => 'get-city',
                        'GET getCategoryWorker' => 'get-category',
                        'GET getPrice' => 'get-price',
                        'GET getPrintData' => 'get-print-data',
                        'GET getLimaHelp' => 'get-lima-help'
                    ]
                ],
                '/' => '/site/index',
            ],
        ],
    ],
    'params' => $params,
];
