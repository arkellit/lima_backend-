<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 26.09.18
 * Time: 12:12
 */

namespace api\modules\v1\models;


use yii\base\Model;

/**
 * @property int $send_users
 * @property int $type
 *
 * @property string $image
 * @property string $title
 * @property string $place
 * @property string $description
 *
 * Class NotificationForm
 * @package app\modules\v1\models
 */
class NotificationForm extends Model
{
    public $send_users;
    public $type;
    public $image;
    public $title;
    public $place;
    public $description;
    public $sound;

    const TYPE_INFO = 1;
    const TYPE_NEWS = 2;

    const IS_SOUND = 1;
    const NO_SOUND = 0;

    const SEND_ALL_USERS = 1;
    const SEND_ALL_WORKER = 2;
    const SEND_ALL_CUSTOMER = 3;

    public function rules()
    {
        return [
//            [['type', 'title', 'description', 'created_at'], 'required'],
            ['type', 'integer'],
            ['type', 'in', 'range' => [self::TYPE_INFO, self::TYPE_NEWS]],
            ['sound', 'in', 'range' => [self::NO_SOUND, self::IS_SOUND]],
            ['send_users', 'in', 'range' => [self::SEND_ALL_USERS, self::SEND_ALL_WORKER, self::SEND_ALL_CUSTOMER]],
            [['image', 'title', 'description', 'place'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'type' => 'Тип',
            'image' => 'Изображение',
            'title' => 'Заголовок',
            'place' => 'Место',
            'description' => 'Описание',
            'sound' => 'Звуковое оповещение',
            'send_users' => 'Кому отправить'
        ];
    }
}