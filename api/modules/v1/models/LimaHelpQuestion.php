<?php

namespace api\modules\v1\models;

use Yii;
use \yii\db\ActiveRecord;
/**
 * This is the model class for table "{{%lima_help_question}}".
 *
 * @property int $id
 * @property string $question
 * @property string $answer
 * @property string $type
 *
 * @property integer $sort
 */
class LimaHelpQuestion extends ActiveRecord
{
    const TYPE_WORKER = 'worker';
    const TYPE_CUSTOMER = 'customer';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%lima_help_question}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['question', 'answer', 'type'], 'required'],
            [['question'], 'string', 'max' => 255],
            ['type', 'string', 'max' => 50],
            [['answer'], 'string', 'max' => 2000],
            ['type', 'in', 'range' => [self::TYPE_CUSTOMER, self::TYPE_WORKER]],
            ['sort', 'integer']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип лимы',
            'question' => 'Вопрос',
            'answer' => 'Ответ',
            'sort' => 'Сортировка'
        ];
    }
}
