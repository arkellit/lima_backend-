<?php

namespace api\modules\v1\models;

use yii\base\Model;
use yii\httpclient\Client;
use Yii;

class Api extends Model
{
    const BASE_URL = 'http://85.21.240.106:8080';

    /**
     * Function send data to 1C
     *
     * @param $name
     * @param $inn
     * @param $contract_number
     * @param $contract_date
     * @param $account_number
     * @param $bik
     * @param null $full_name
     * @param null $kpp
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public static function sendCustomer($name,  $inn,
        $contract_number, $contract_date, $account_number,
        $bik, $full_name = null,  $kpp = null){

        $auth = base64_encode("Admin:Admin");
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => self::BASE_URL . "/BUH_LIMA/hs/lima_customers",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 100,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"name\":\"$name\",\"full_name\":
            \"$full_name\",\"inn\":\"$inn\",\"kpp\":\"$kpp\",
            \"contract_number\":\"$contract_number\",\"contract_date\":\"$contract_date\",
            \"аccount_number\":\"$account_number\",\"bik\":\"$bik\"}",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic $auth",
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: 6483c56a-a4c4-33e3-5bfd-047758ed3885"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
           return false;
        } else {
            $log = new Logs();
            $log->text = '_CUSTOMER:: ' . "{\"name\":\"$name\",\"full_name\":
            \"$full_name\",\"inn\":\"$inn\",\"kpp\":\"$kpp\",
            \"contract_number\":\"$contract_number\",\"contract_date\":\"$contract_date\",
            \"аccount_number\":\"$account_number\",\"bik\":\"$bik\"}";
            $log->save();
            return true;
        }
//        $client = new Client(['baseUrl' => self::BASE_URL]);
//        $response = $client->createRequest()
//            ->addHeaders(['content-type' => 'application/json'])
//            ->addHeaders(['Authorization' =>
//                'Basic '.base64_encode("Admin:Admin")])
//            ->setMethod('POST')
//            ->addHeaders(['token' =>
//                Yii::$app->session->get('token')])
//            ->setUrl( '/BUH_LIMA/hs/lima_customers')
//            ->setData(['name' => $name, 'full_name' => $full_name,
//                'inn' => $inn, 'kpp' => $kpp, 'contract_number' => $contract_number,
//                'contract_date' => $contract_date,
//                'account_number' => $account_number, 'bik' => $bik])
//            ->send();
//        if ($response->isOk) {
//            return true;
//        }
//        elseif($response->headers['http-code'] == 500){
//            Yii::$app->session->setFlash('error', 'Произошла ошибка сервера');
//            return $response;
//        }
//        return $response;
    }

    /**
     * Function send order to 1C
     *
     * @param $name
     * @param $inn
     * @param $contract_number
     * @param $contract_date
     * @param $account_number
     * @param $bik
     * @param $service_name
     * @param $service_amount
     * @param $inn_customer
     * @param $invoice_date
     * @param $invoice_amount
     * @param $order_id,
     * @param $email
     * @param null $full_name
     * @param null $kpp
     * @return mixed|string
     */
    public static function sendOrder(
            $name,
            $inn,
            $contract_number,
            $contract_date,
            $account_number,
            $bik,
            $service_name,
            $invoice_date,
            $invoice_amount,
            $order_id,
            $email,
            $full_name = null,
            $kpp = null
    ){
            $auth = base64_encode("Admin:Admin");

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => self::BASE_URL . "/BUH_LIMA/hs/lima_invoice",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 100,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "{
                \"name\":\"$name\",
                \"full_name\":\"$full_name\",
                \"inn\":\"$inn\",
                \"kpp\":\"$kpp\",
                \"contract_number\":\"$contract_number\",
                \"contract_date\":\"$contract_date\",
                \"аccount_number\":\"$account_number\",
                \"bik\":\"$bik\",
                \"service_name\":\"$service_name\",
                \"invoice_date\":\"$invoice_date\",
                \"invoice_amount\":\"$invoice_amount\",
                \"order_id\":\"$order_id\",
                \"email\":\"$email\"
                }",
                CURLOPT_HTTPHEADER => array(
                    "authorization: Basic $auth",
                    "cache-control: no-cache",
                    "content-type: application/json",
                    "postman-token: 0db89df6-a47e-12c3-2d00-6a92150df0e5"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return $err;
            } else {
                $log = new Logs();
                $log->text = '_ONE:: ' . "{
                \"name\":\"$name\",
                \"full_name\":\"$full_name\",
                \"inn\":\"$inn\",
                \"kpp\":\"$kpp\",
                \"contract_number\":\"$contract_number\",
                \"contract_date\":\"$contract_date\",
                \"аccount_number\":\"$account_number\",
                \"bik\":\"$bik\",
                \"service_name\":\"$service_name\",
                \"invoice_date\":\"$invoice_date\",
                \"invoice_amount\":\"$invoice_amount\"}";
                $log->save();
                return $response;
            }
    }

    /**
     * Function send order-worker to 1C
     *
     * @param $name
     * @param $inn
     * @param $contract_number
     * @param $contract_date
     * @param $account_number
     * @param $bik
     * @param $act_date
     * @param $act_number
     * @param $service_name
     * @param $service_amount
     * @param $inn_customer
     * @param $contract_number_customer
     * @param $contract_date_customer
     * @param $invoice_date
     * @param $invoice_amount
     * @param $full_name
     * @return mixed|string
     */
    public static function sendWorker(
            $name,
            $inn,
            $contract_number,
            $contract_date,
            $account_number,
            $bik,
            $act_date,
            $act_number,
            $service_name,
            $service_amount,
            $inn_customer, $contract_number_customer,
            $contract_date_customer, $invoice_date, $invoice_amount, $full_name){
        $auth = base64_encode("Admin:Admin");
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => self::BASE_URL . "/BUH_LIMA/hs/lima_act_acceptance",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 100,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"name\":\"$name\",
            \"full_name\":\"$full_name\",\"inn\":\"$inn\",
            \"contract_number\":\"$contract_number\",\"contract_date\":\"$contract_date\",
            \"аccount_number\":\"$account_number\",\"bik\":\"$bik\",
            \"act_date\":\"$act_date\",\"act_number\":\"$act_number\",
            \"service_name\":\"$service_name\",
            \"service_amount\":\"$service_amount\",\"inn_customer\":\"$inn_customer\",
            \"contract_number_customer\":\"$contract_number_customer\",
            \"contract_date_customer\":\"$contract_date_customer\",
            \"invoice_date\":\"$invoice_date\",\"invoice_amount\":\"$invoice_amount\"}",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic $auth",
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: 6e370783-bd9b-9453-13b7-f53c336b0215"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return $err;
        } else {
            $log = new Logs();
            $log->text = '_TWO:: ' .
            "{\"name\":\"$name\",
            \"full_name\":\"$full_name\",
            \"inn\":\"$inn\",
            \"contract_number\":\"$contract_number\",
            \"contract_date\":\"$contract_date\",
            \"аccount_number\":\"$account_number\",
            \"bik\":\"$bik\",
            \"act_date\":\"$act_date\",
            \"act_number\":\"$act_number\",
            \"service_name\":\"$service_name\",
            \"service_amount\":\"$service_amount\",
            \"inn_customer\":\"$inn_customer\",
            \"contract_number_customer\":\"$contract_number_customer\",
            \"contract_date_customer\":\"$contract_date_customer\",
            \"invoice_date\":\"$invoice_date\",
            \"invoice_amount\":\"$invoice_amount\"}";
            $log->save();
            return $response;
        }
    }
}