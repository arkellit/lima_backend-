<?php

namespace api\modules\v1\models\customer\payment;

use api\modules\v1\models\customer\Customer;
use Yii;
use yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%customer_replenish_account}}".
 *
 * @property int $id
 * @property int $customer_id
 * @property string $account_number
 * @property int $date
 * @property string $description
 * @property double $amount
 * @property int $created_at
 *
 * @property Customer $customer
 */
class CustomerReplenishAccount extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%customer_replenish_account}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'account_number', 'date', 'amount'], 'required'],
            [['customer_id', 'date', 'created_at'], 'integer'],
            [['description'], 'string'],
            [['amount'], 'number'],
            [['account_number'], 'string', 'max' => 255],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp'  => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'account_number' => 'Номер счета',
            'date' => 'Дата поступления средств',
            'description' => 'Описание товара',
            'amount' => 'Сумма прихода',
            'created_at' => 'Дата создания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }
}
