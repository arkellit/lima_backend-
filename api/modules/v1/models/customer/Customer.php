<?php

namespace api\modules\v1\models\customer;

use api\modules\v1\models\auth\User;
use Yii;
use \yii\db\ActiveRecord;
use yii\web\HttpException;

/**
 * This is the model class for table "{{%customer}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $last_name
 * @property string $middle_name
 * @property string $photo
 * @property int $is_register_complete
 * @property int $is_sync_ones
 *
 * @property User $user
 * @property CustomerCompanyData $companyData
 * @property CustomerBankData $bankData
 * @property CustomerNotice $dateRegister
 */
class Customer extends ActiveRecord
{
    const IS_REGISTER_COMPLETE_TRUE = 1;
    const IS_REGISTER_COMPLETE_FALSE = 0;

    const EVENT_CONFIRMED_CUSTOMER_NOTICE = 'customer-confirmed-notice';

    public $phone;

    const IS_SYNC_ONES = 1;
    const NO_SYNC_ONES = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%customer}}';
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->on(self::EVENT_CONFIRMED_CUSTOMER_NOTICE, [$this, 'setConfirmedNotice']);
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'last_name'], 'required'],
            ['user_id', 'unique', 'message' => 'Заказчик уже зарегистрирован'],
            [['name', 'last_name', 'middle_name', 'photo'], 'string', 'max' => 255],
            [['is_register_complete'], 'default', 'value' => 0],
            [['user_id', 'is_register_complete'], 'integer'],
            ['is_sync_ones', 'default', 'value' => self::NO_SYNC_ONES],
            ['is_sync_ones', 'in', 'range' => [self::IS_SYNC_ONES, self::NO_SYNC_ONES]],
            ['is_register_complete', 'in', 'range' => [self::IS_REGISTER_COMPLETE_TRUE, self::IS_REGISTER_COMPLETE_FALSE]],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function setConfirmedNotice(){
        $customer_notice = new CustomerNotice();
        $customer_notice->customer_id = $this->id;
        $customer_notice->category_notice = CustomerNotice::CUSTOMER_CONFIRMED;
        $customer_notice->save();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Имя',
            'last_name' => 'Фамилия',
            'middle_name' => 'Отчество',
            'is_register_complete' => 'Регистрация завершена',
            'photo' => 'Фотография',
            'is_sync_ones' => 'Синхронизированно с 1С'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyData()
    {
        return $this->hasOne(CustomerCompanyData::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankData()
    {
        return $this->hasOne(CustomerBankData::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDateRegister(){
        return $this->hasOne(CustomerNotice::className(),
            ['customer_id' => 'id'])->where(['category_notice' =>
                CustomerNotice::CUSTOMER_CONFIRMED]);
    }

    public static function getCustomerProfile(){
        $customer = Customer::find()
            ->where(['user_id' => Yii::$app->user->getId()])
            ->with(['user', 'companyData'])
            ->asArray()
            ->one();
        if ($customer['is_register_complete'] == Customer::IS_REGISTER_COMPLETE_FALSE){
            throw new HttpException(429, 'Ваш профиль еще не прошел модерацию');
        }

        unset($customer['user_id']);
        unset($customer['is_register_complete']);
        $customer['phone'] = $customer['user']['phone'];//Yii::$app->user->identity->phone;
        $customer['role'] = 'customer';
        $customer['email'] = $customer['companyData']['email'];
        $customer['name_company'] = $customer['companyData']['name_company'];
        $customer['deposit'] = (double)$customer['companyData']['deposit'];
        unset($customer['user']);
        unset($customer['companyData']);
        return $customer;
    }
}
