<?php

namespace api\modules\v1\models\customer;

use api\modules\v1\models\City;
use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%customer_branch}}".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $city_id
 * @property int $is_delete
 * @property string $branch
 *
 * @property City $city
 * @property Customer $customer
 */
class CustomerBranch extends ActiveRecord
{
    const IS_DELETE = 1;
    const NO_DELETE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%customer_branch}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'city_id', 'branch'], 'required'],
            [['customer_id', 'city_id'], 'integer'],
            [['branch'], 'string', 'max' => 255],
            ['is_delete', 'in', 'range' => [self::IS_DELETE, self::NO_DELETE]],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'city_id' => 'Город',
            'branch' => 'Филиал',
            'is_delete' => 'Удален'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }
}
