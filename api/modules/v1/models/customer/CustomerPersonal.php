<?php

namespace api\modules\v1\models\customer;

use api\modules\v1\models\auth\User;
use Yii;
use \yii\db\ActiveRecord;
use yii\web\HttpException;

/**
 * This is the model class for table "{{%customer_personal}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $customer_id
 * @property string $name
 * @property string $last_name
 * @property string $middle_name
 * @property string $email
 * @property string $photo
 * @property string $role
 * @property int $branch_id
 * @property int $is_invite
 * @property int $is_register
 * @property int $is_delete
 *
 * @property Customer $customer
 * @property User $user
 * @property CustomerBranch $branch
 */
class CustomerPersonal extends ActiveRecord
{
    const ROLE_ADMINISTRATOR = 'administrator';
    const ROLE_SUPERVISOR = 'supervisor';

    const SCENARIO_INVITE = 1;
    const SCENARIO_NO_INVITE = 2;

    const IS_INVITE = 1;
    const NO_INVITE = 0;

    const IS_REGISTER_COMPLETE = 1;
    const NO_REGISTER_COMPLETE = 0;

    const IS_DELETE = 1;
    const NO_DELETE = 0;

    const EVENT_CONFIRMED_CUSTOMER_PERSONAL_NOTICE = 'customer-confirmed-personal-notice';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%customer_personal}}';
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->on(self::EVENT_CONFIRMED_CUSTOMER_PERSONAL_NOTICE, [$this, 'setConfirmedPersonalNotice']);
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'last_name', 'customer_id', 'role'], 'required', 'on' => self::SCENARIO_NO_INVITE],
            [['user_id', 'customer_id', 'role'], 'required', 'on' => self::SCENARIO_INVITE],
            [['user_id', 'customer_id', 'branch_id', 'is_delete'], 'integer'],
            [['name', 'last_name', 'middle_name', 'email', 'photo'], 'string', 'max' => 255],
            [['user_id'], 'unique'],
            [['user_id', 'branch_id'], 'unique', 'targetAttribute' => ['user_id', 'branch_id'], 'message' => 'Пользователь уже привязан к филиалу'],
            ['role', 'in', 'range' => [self::ROLE_ADMINISTRATOR, self::ROLE_SUPERVISOR]],
            ['role', 'string', 'max' => 50],
            ['email', 'email'],
            ['is_invite', 'default', 'value' => self::IS_INVITE, 'on' => self::SCENARIO_INVITE],
            ['is_register', 'default', 'value' => self::NO_REGISTER_COMPLETE, 'on' => self::SCENARIO_INVITE],
            ['is_delete', 'in', 'range' => [self::IS_DELETE, self::NO_DELETE]],
            [['customer_id', 'branch_id'], 'default', 'value' => 0],
            //[['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function setConfirmedPersonalNotice(){
        $customer_notice = new CustomerNotice();
        $customer_notice->customer_id = $this->customer_id;
        $customer_notice->customer_personal_id = $this->id;
        $customer_notice->personal_role = $this->role;
        $customer_notice->category_notice =
            $this->role == $this::ROLE_ADMINISTRATOR ?
                CustomerNotice::ADMINISTRATOR_CONFIRMED :
                CustomerNotice::SUPERVISOR_CONFIRMED;
        $customer_notice->save();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'customer_id' => 'Заказчик',
            'name' => 'Имя',
            'last_name' => 'Фамилия',
            'middle_name' => 'Отчество',
            'email' => 'Email',
            'photo' => 'Фото',
            'role' => 'Должность',
            'branch_id' => 'Филиал',
            'is_invite' => 'Под инвайтом',
            'is_register' => 'Зареган',
            'is_delete' => 'Удален'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranch()
    {
        return $this->hasOne(CustomerBranch::className(), ['id' => 'branch_id']);
    }

    public static function getCustomerPersonalProfile(){
        $customer = CustomerPersonal::find()
            ->where(['user_id' => Yii::$app->user->getId()])
            ->andWhere(['is_register' => CustomerPersonal::IS_REGISTER_COMPLETE])
            ->with(['user', 'branch'])
            ->asArray()
            ->one();
        if (!$customer) throw new HttpException(400, 'Пользователь не найден');

        $customer_name_company = CustomerCompanyData::find()
            ->select(['customer_id', 'name_company'])
            ->where(['customer_id' => $customer['customer_id']])
            ->asArray()
            ->one();
        $customer['phone'] = $customer['user']['phone'];
        $role = $customer['role'] == 'supervisor' ? 'supervisor' : 'administrator';
        unset($customer['user']);
        unset($customer['role']);
        unset($customer['user_id']);
        unset($customer['customer_id']);
        $customer['branch_name'] = $customer['branch']['branch'];
        $customer['role'] = $role;
        $customer['name_company'] = $customer_name_company['name_company'];
        unset($customer['branch']);
        unset($customer['branch_id']);
        unset($customer['is_invite']);
        unset($customer['is_register']);
        return $customer;
    }
}
