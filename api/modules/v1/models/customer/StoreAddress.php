<?php

namespace api\modules\v1\models\customer;

use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "store_address".
 *
 * @property int $id
 * @property int $customer_id
 * @property double $lat
 * @property double $lng
 * @property string $name_store
 * @property string $address
 *
 * @property Customer $customer
 */
class StoreAddress extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%customer_store_address}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'lat', 'lng', 'name_store', 'address'], 'required'], 
            [['customer_id'], 'integer'],
            [['lat', 'lng'], 'double'],
            [['name_store', 'address'], 'string', 'max' => 255],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'lat' => 'Широта',
            'lng' => 'Долгота',
            'name_store' => 'Название магазина',
            'address' => 'Адрес магазина'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * Function create store address
     *
     * @param $customer_id
     * @param $addressList
     * @return array
     */
    public function create($customer_id, $addressList)
    {
        foreach ($addressList as $value){
            $model = new StoreAddress();
            $model->customer_id = $customer_id;
            $model->lat = $value['lat'];
            $model->lng = $value['lng'];
            $model->name_store =$value['name_store'];
            $model->address = $value['address'];
            $is_save = $model->save();
            if($is_save == false){
                return array('is_success' => null, 'error' => $model->getErrors());
            }
        }
        return array('is_success' => true);
    }
}
