<?php

namespace api\modules\v1\models\customer;

use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%customer_company_data}}".
 *
 * @property int $id
 * @property int $customer_id
 * @property string $name_company
 * @property string $inn_company
 * @property string $kpp_company
 * @property string $actual_address
 * @property string $email
 * @property string $contact_phone
 * @property string $logo_company
 * @property double $deposit
 *
 * @property Customer $customer
 */
class CustomerCompanyData extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%customer_company_data}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'name_company', 'inn_company', 'actual_address', 'email', 'contact_phone'], 'required'],
            [['customer_id'], 'integer'],
            [['deposit'], 'number'],
            ['deposit', 'number', 'min' => 0],
            ['email', 'email'],
            [['name_company', 'actual_address', 'email', 'logo_company'], 'string', 'max' => 255],
            [['inn_company'], 'string', 'length' => [9, 15]],
            [['kpp_company'], 'string', 'length' => [9, 9]],
            [['contact_phone'], 'string', 'max' => 20],
            [['customer_id'], 'unique', 'message' => 'Заказчик уже зарегистрирован'],
            [['inn_company'], 'unique', 'message' => 'Заказчик c таким ИНН уже зарегистрирован'],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'name_company' => 'Наименование компании',
            'inn_company' => 'ИНН компании',
            'kpp_company' => 'КПП компании',
            'actual_address' => 'Фактический адрес',
            'email' => 'Email',
            'contact_phone' => 'Контактный телефон',
            'logo_company' => 'Логотип компании',
            'deposit' => 'Депозит',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }
}
