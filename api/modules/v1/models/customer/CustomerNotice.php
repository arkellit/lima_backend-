<?php

namespace api\modules\v1\models\customer;

use api\modules\v1\models\order\Order;
use api\modules\v1\models\order\OrderAddress;
use api\modules\v1\models\order\OrderPlace;
use Yii;
use yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%customer_notice}}".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $customer_personal_id
 * @property string $personal_role
 * @property int $order_id
 * @property int $order_place_id
 * @property int $worker_id
 * @property string $category_notice
 * @property int $created_at
 * @property int $updated_at
 * @property int $is_delete
 *
 * @property Customer $customer
 * @property Order $order
 * @property OrderPlace $orderPlace
 */
class CustomerNotice extends ActiveRecord
{
    const ORDER_CREATE = 'order_create';
    const ORDER_START = 'order_start';
    const ORDER_END = 'order_end';
    const ADMINISTRATOR_CONFIRMED = 'administrator_confirmed';
    const CUSTOMER_CONFIRMED = 'customer_confirmed';
    const SUPERVISOR_CONFIRMED = 'supervisor_confirmed';
    const SUPERVISOR_ORDER_ASSIGN = 'supervisor_order_assign';
//    const ORDER_COMPLETED = 'order_completed';
    const NEW_REQUEST = 'new_request';
    const ACCEPT_REQUEST = 'accept_request';
//    const REJECT_REQUEST = 'reject_request';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%customer_notice}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id'], 'required'],
            [['customer_personal_id', 'worker_id', 'is_delete'], 'default', 'value' => 0],
            [['customer_id', 'customer_personal_id', 'order_id', 'order_place_id',
                'worker_id', 'created_at', 'updated_at', 'is_delete'], 'integer'],
            [['personal_role', 'category_notice'], 'string', 'max' => 255],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['order_place_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderPlace::className(), 'targetAttribute' => ['order_place_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'customer_personal_id' => 'Customer Personal ID',
            'personal_role' => 'Personal Role',
            'order_id' => 'Order ID',
            'order_place_id' => 'Order Place ID',
            'worker_id' => 'Worker ID',
            'category_notice' => 'Category Notice',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'is_delete' => 'Is Delete',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPlace()
    {
        return $this->hasOne(OrderPlace::className(), ['id' => 'order_place_id']);
    }
}
