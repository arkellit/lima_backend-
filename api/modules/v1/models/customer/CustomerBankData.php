<?php

namespace api\modules\v1\models\customer;

use common\helpers\EncryptionHelpers;
use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%customer_bank_data}}".
 *
 * @property int $id
 * @property int $customer_id
 * @property string $customer_rs
 * @property string $bank_bik
 * @property string $bank_city
 * @property string $bank_name
 * @property string $bank_ks
 * @property string $bank_inn
 *
 * @property Customer $customer
 */
class CustomerBankData extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%customer_bank_data}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'customer_rs', 'bank_bik', 'bank_city',
                'bank_name', 'bank_ks'/*, 'bank_inn', 'bank_kpp'*/], 'required'],
            [['customer_id'], 'integer'],
            [['bank_bik'/*, 'bank_kpp'*/], 'string', 'length' => [9, 9]],
            [['customer_rs', 'bank_ks'], 'string', 'length' => [20, 20]],
            [['bank_name', 'bank_city'], 'string', 'max' => 255],
//            [['bank_inn'], 'string', 'length' => [10, 12]],
//            [['customer_rs', 'bank_bik', 'bank_city', 'bank_name', 'bank_ks', 'bank_inn'], 'string'],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->customer_rs = EncryptionHelpers::dec_enc('encrypt', $this->customer_rs);
            return parent::beforeSave($insert);
        } else {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'customer_rs' => 'Расчетный счет организации',
            'bank_bik' => 'БИК банка',
            'bank_city' => 'Город банка',
            'bank_name' => 'Название банка',
            'bank_ks' => 'Корреспондентский счет банка',
//            'bank_inn' => 'ИНН банка',
//            'bank_kpp' => 'КПП банка'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }
}
