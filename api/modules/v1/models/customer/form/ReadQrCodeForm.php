<?php

namespace api\modules\v1\models\customer\form;

use api\modules\v1\models\order\OrderRequest;
use api\modules\v1\models\order\OrderStartEnd;
use common\helpers\DifferenceTimeHelpers;
use yii\base\Model;
use yii\web\HttpException;

/**
 * Class ReadQrCodeForm
 *
 * @property integer $worker_id
 * @property integer $order_id
 * @property integer $customer_id
 * @property integer $_is_type
 *
 * @property string $number_order
 *
 * @property object $_order_accept
 *
 * @package api\modules\v1\models\customer\form
 */
class ReadQrCodeForm extends Model
{
    public $worker_id;
    public $number_order;
    public $customer_id;
    public $order_id;

    public $_order_accept;
    public $_type;
    public $_order_start_end;

    const START_JOB = 1;
    const END_JOB = 2;

    public function rules()
    {
        return [
            [['worker_id', 'order_id', 'customer_id'], 'required'],
            [['worker_id', 'number_order', 'order_id', 'customer_id'], 'validateForm'],
        ];
    }

    public function validateForm(){
        $model_request = OrderRequest::findOne(['worker_id' => $this->worker_id,
            'order_id' => $this->order_id, 'status' => OrderRequest::STATUS_ACCEPT]);

        if (!$model_request){
            throw new HttpException(400, 'Неверные данные');
        }

        if ($model_request->order->customer_id != $this->customer_id){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $model_request_accept = $model_request->orderStartEnd;
        if (!$model_request_accept){
            throw new HttpException(400, 'Неверные данные');
        }

        if ($model_request_accept->is_start_job == 0){
            if (DifferenceTimeHelpers::differenceTime($model_request->orderPlace->
            date_time_start) > 3600){
                throw new HttpException(400, 'Регистрировать исполнителя можно за час до начала');
            }
        }
        elseif ($model_request_accept->is_end_job == 1 && $model_request_accept->
            is_finished_job == 1){
            throw new HttpException(400, 'Заказ уже завершен');
        }

        $this->_order_accept = $model_request->orderPlace;
        $this->_type = $model_request_accept->is_start_job == 0 ? 0 : 1;
        $this->_order_start_end = $model_request_accept->is_start_job;
    }
}