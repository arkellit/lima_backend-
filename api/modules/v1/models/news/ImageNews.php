<?php

namespace api\modules\v1\models\news;

use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%image_news}}".
 *
 * @property int $id
 * @property int $news_id
 * @property string $image
 *
 * @property News $news
 */
class ImageNews extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%news_image}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            [['news_id', 'image'], 'required'],
            [['news_id'], 'integer'],
            [['image'], 'string', 'max' => 255],
            [['news_id'], 'exist', 'skipOnError' => true, 'targetClass' => News::className(), 'targetAttribute' => ['news_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news_id' => 'News ID',
            'image' => 'Изображения'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }
}
