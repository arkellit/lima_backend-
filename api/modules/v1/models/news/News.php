<?php

namespace api\modules\v1\models\news;

use Yii;
use yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property int $id
 * @property string $title
 * @property string $image
 * @property string $short_description
 * @property string $full_description
 *
 * @property int $created_at
 * @property int $is_send_push
 * @property int $is_published
 *
 * @property ImageNews[] $imageNews
 */
class News extends ActiveRecord
{
    const IS_SEND_PUSH_YES = 1;
    const IS_SEND_PUSH_NO = 0;

    const PUBLISH = 1;
    const NO_PUBLISH = 0;

    const SCENARIO_CREATE = '1';
    const SCENARIO_UPDATE = '2';

    public $array_image;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'image', 'short_description', 'is_send_push', 
            'full_description','is_published'], 'required',
                'on' => self::SCENARIO_CREATE],
            [['is_send_push', 'created_at'], 'integer'],
            [['full_description'], 'string'],
            [['title', 'image'], 'string', 'max' => 255],
            [['short_description'], 'string', 'max' => 1000, 'on' => self::SCENARIO_UPDATE],
            ['is_send_push','default', 'value' => self::IS_SEND_PUSH_NO],
            ['is_published','default', 'value' => self::NO_PUBLISH],
            ['is_send_push', 'in', 'range' => [self::IS_SEND_PUSH_YES, self::IS_SEND_PUSH_NO]],
            ['is_published', 'in', 'range' => [self::PUBLISH, self::NO_PUBLISH]],
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp'  => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'image' => 'Главное изоражение',
            'short_description' => 'Краткое описание',
            'full_description' => 'Полное описание',
            'created_at' => 'Дата создания',
            'is_send_push' => 'Рассылка',
            'is_published' => 'Опубликован',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImageNews()
    {
        return $this->hasMany(ImageNews::className(), ['news_id' => 'id']);
    }
}
