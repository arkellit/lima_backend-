<?php

namespace api\modules\v1\models\printing;

use api\modules\v1\models\City;
use \yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%printing_template}}".
 *
 * @property int $id
 * @property int $city_id
 * @property string $category
 * @property string $name
 * @property double $price
 *
 * @property City $city
 */
class PrintingTemplate extends ActiveRecord
{
    const EDITION = 'edition';
    const PRINT_TYPE = 'print_type';
    const PRINT_SIZE = 'print_size';
    const PAPER_DENSITY = 'paper_density';
    const PRINT_TIME = 'print_time';
    const COURIER_PRICE = 'courier_price';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%printing_template}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id', 'category', 'name'], 'required'],
            [['price'], 'default', 'value' => 0],
            [['city_id'], 'integer'],
            [['price'], 'number'],
            [['category', 'name'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'skipOnError' => true,
                'targetClass' => City::className(), 'targetAttribute' =>
                ['city_id' => 'id']],
        ];
    }

    public function setData($array_data)
    {
        foreach ($array_data[self::EDITION] as $array_datum) {
            $model = new PrintingTemplate();
            $model->category = self::EDITION;
            $model->city_id = $array_data['city_id'];
            $model->price = (double)$array_datum['price'];
            $model->name = $array_datum['name'];
            $model->save();

        }

        foreach ($array_data[self::PRINT_TYPE] as $array_datum) {
            $model = new PrintingTemplate();
            $model->category = self::PRINT_TYPE;
            $model->city_id = $array_data['city_id'];
            $model->price = (double)$array_datum['price'];
            $model->name = $array_datum['name'];
            $model->save();
        }

        foreach ($array_data[self::PRINT_SIZE] as $array_datum) {
            $model = new PrintingTemplate();
            $model->category = self::PRINT_SIZE;
            $model->city_id = $array_data['city_id'];
            $model->price = (double)$array_datum['price'];
            $model->name = $array_datum['name'];
            $model->save();
        }

        foreach ($array_data[self::PAPER_DENSITY] as $array_datum) {
            $model = new PrintingTemplate();
            $model->category = self::PAPER_DENSITY;
            $model->city_id = $array_data['city_id'];
            $model->price = (double)$array_datum['price'];
            $model->name = $array_datum['name'];
            $model->save();

        }

        foreach ($array_data[self::PRINT_TIME] as $array_datum) {
            $model = new PrintingTemplate();
            $model->category = self::PRINT_TIME;
            $model->city_id = $array_data['city_id'];
            $model->price = (double)$array_datum['price'];
            $model->name = $array_datum['name'];
            $model->save();
        }

        if (!is_null($array_data['courier_price']) && $array_data['courier_price'] != 0){
            $model = new PrintingTemplate();
            $model->category = self::COURIER_PRICE;
            $model->city_id = $array_data['city_id'];
            $model->price = $array_data['courier_price'];
            $model->name = 'Курьер';
            $model->save();
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'Город',
            'category' => 'Категория',
            'name' => 'Название',
            'price' => 'Цена',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }
}
