<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 12.03.19
 * Time: 16:35
 */

namespace api\modules\v1\models\printing;

use yii\base\Model;

/**
 * Class PrintingForm
 *
 * @property int $city_id
 * @property array $edition
 * @property array $print_type
 * @property array $print_size
 * @property array $paper_density
 * @property array $print_time
 * @property double $courier_price
 *
 * @package api\modules\v1\models\printing
 */
class PrintingCategoryForm extends Model
{
    public $city_id;
    public $edition;
    public $print_type;
    public $print_size;
    public $paper_density;
    public $print_time;
    public $courier_price;

    public function rules()
    {
        return [
            [['city_id'], 'required'],
            ['city_id', 'integer'],
            [['edition', 'print_type', 'print_size', 'courier_price',
                'paper_density', 'print_time'], 'string'],
            ['courier_price', 'number', 'min' => 0.1]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'Город',
            'edition' => 'Тираж',
            'print_type' => 'Тип печати',
            'print_size' => 'Размер листовок',
            'paper_density' => 'Плотность бумаги',
            'print_time' => 'Время печати',
            'courier_price' => 'Цена курьера',
        ];
    }
}