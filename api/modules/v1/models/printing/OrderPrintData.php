<?php

namespace api\modules\v1\models\printing;

use api\modules\v1\models\order\Order;
use api\modules\v1\models\order\OrderPlace;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%order_print_data}}".
 *
 * @property int $id
 * @property int $order_id
 * @property int $order_place_id
 * @property int $edition_id
 * @property int $print_type_id
 * @property int $print_size_id
 * @property int $paper_density_id
 * @property int $print_time_id
 * @property int $courier_price_id
 * @property array $urls
 * @property string $flyer_title
 * @property string $flyer_description
 * @property string $comment_order_print
 * @property int $is_courier$is_courier
 * @property string $courier_address
 * @property double $printing_price
 *
 * @property Order $order
 * @property OrderPlace $orderPlace
 * @property PrintingTemplate $edition
 * @property PrintingTemplate $printType
 * @property PrintingTemplate $printSize
 * @property PrintingTemplate $paperDensity
 * @property PrintingTemplate $printTime
 */
class OrderPrintData extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order_print_data}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[//'order_id', 'order_place_id',
                'edition_id',
                'print_type_id', 'print_size_id', 'paper_density_id',
                'print_time_id', 'flyer_title', 'flyer_description',
                'printing_price'], 'required'],
            [
                ['courier_address', 'courier_price_id'],
                'required',
                'when' => function($model) {
                    return  $model->is_courier != 0;
                },
            ],
            [['is_courier'], 'default', 'value' => 0],
            [[ 'edition_id',
                'print_type_id', 'print_size_id', 'paper_density_id', 'print_time_id',
                'courier_price_id', 'is_courier'], 'integer'],
            [['urls'], 'safe'],
            ['printing_price', 'number'],
            ['printing_price', 'default', 'value' => 0],
            [['flyer_description'], 'string'],
            [['flyer_title', 'courier_address', 'comment_order_print'], 'string', 'max' => 1000],
//            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
//            [['order_place_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderPlace::className(), 'targetAttribute' => ['order_place_id' => 'id']],
            [['edition_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrintingTemplate::className(), 'targetAttribute' => ['edition_id' => 'id']],
            [['print_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrintingTemplate::className(), 'targetAttribute' => ['print_type_id' => 'id']],
            [['print_size_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrintingTemplate::className(), 'targetAttribute' => ['print_size_id' => 'id']],
            [['paper_density_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrintingTemplate::className(), 'targetAttribute' => ['paper_density_id' => 'id']],
            [['print_time_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrintingTemplate::className(), 'targetAttribute' => ['print_time_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function afterValidate()
    {
        if (parent::afterValidate()) {
            $this->urls = json_encode($this->urls);
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
//            'order_id' => 'Заказ',
//            'order_place_id' => 'Место заказа',
            'edition_id' => 'Тираж',
            'print_type_id' => 'Печать',
            'print_size_id' => 'Размер листовки',
            'paper_density_id' => 'Плотность бумаги',
            'print_time_id' => 'Время печати',
            'courier_price_id' => 'Цена курьера',
            'urls' => 'Ссылки на макеты',
            'flyer_title' => 'Заголовок листовки',
            'flyer_description' => 'Текст листовки',
            'is_courier' => 'Нужен курьер',
            'courier_address' => 'Адрес курьера',
            'printing_price' => 'Стоимость печати'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPlace()
    {
        return $this->hasOne(OrderPlace::className(), ['id' => 'order_place_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEdition()
    {
        return $this->hasOne(PrintingTemplate::className(), ['id' => 'edition_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrintType()
    {
        return $this->hasOne(PrintingTemplate::className(), ['id' => 'print_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrintSize()
    {
        return $this->hasOne(PrintingTemplate::className(), ['id' => 'print_size_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaperDensity()
    {
        return $this->hasOne(PrintingTemplate::className(), ['id' => 'paper_density_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrintTime()
    {
        return $this->hasOne(PrintingTemplate::className(), ['id' => 'print_time_id']);
    }
}
