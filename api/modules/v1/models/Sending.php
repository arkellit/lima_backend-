<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 10.09.18
 * Time: 12:37
 */

namespace api\modules\v1\models;

use api\modules\v1\models\auth\User;
use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\order\Order;
use api\modules\v1\models\order\OrderPlace;
use api\modules\v1\models\worker\Worker;
use paragraph1\phpFCM\Recipient\Device;
use Yii;
use yii\base\Model;
use yii\web\HttpException;
use yii\db\Query;
use common\helpers\FirebaseHelpers;

/**
 * Class Sending
 * @package app\modules\v1\models
 */
class Sending extends Model
{
    /**
     * Function send sms smsc.ru
     *
     * @param $phone
     * @param $code
     * @return bool|null
     */
    public static function sendCode($phone, $code){
        $sms = Yii::$app->sms;
        $result = $sms->send_sms($phone, 'Ваш код для подтверждения: '. $code, 0, 0, 0, 0, 'Lima');
        if (!$sms->isSuccess($result)) {
            return null;
        }
        return true;
    }

    /**
     * The function of sending data to enter the email
     *
     * @param $email
     * @param $phone
     * @param $password
     * @return bool
     */
    public static function sendLoginToEmail($email, $phone, $password)
    {
        mail($email, 'Данные для входа в Lima', 'Ваш логин для входа в Lima: '
        . $phone . ', ' . 'Пароль: ' . $password);
        return true;
    }

    /**
     * The function of sending data to enter the sms
     *
     * @param $phone_recipient
     * @param $password
     * @return bool|null
     */
    public static function sendLoginToSms($phone_recipient, $password)
    {
        $sms = Yii::$app->sms;
        $result = $sms->send_sms($phone_recipient, 'Ваш пароль для входа в Lima: ' . $password, 0, 0, 0, 0, 'Lima');
        if (!$sms->isSuccess($result)) {
            return null;
        }
        return true;
    }


    /**
     * Function send notice one user
     *
     * @param $user_id
     * @param $title
     * @param $description
     */
    public static function sendNotice($user_id, $title, $description){
        $user = User::findOne($user_id);
        if(!empty($user) && !is_null($user->fcm_token)){
            $note = Yii::$app->fcm->createNotification($title, $description);

            $message = Yii::$app->fcm->createMessage();
            $note->setIcon('/img/lima-logo.jpg')
                    ->setSound('default')
                    ->setColor('#ffffff');

            $message->addRecipient(new Device($user->fcm_token));
            $message->setNotification($note)
                ->setData([
                    'type' => 'message'
                ]);
            Yii::$app->fcm->send($message);
        }
    }

    public static function sendNoticeForCloseQr($user_id, $status){
        $user = User::findOne($user_id);
        if(!empty($user) && !is_null($user->fcm_token)){
            FirebaseHelpers::closeQrPagePush($user->fcm_token, $status);
        }
    }

    /**
     * Function send notice one worker
     *
     * @param $worker_id
     * @param $title
     * @param $description
     * @param $id_request
     */
    public static function sendNoticeWorkerRequest($worker_id, $title, $description, $id_request, $order_id){
        $user = Worker::findOne($worker_id);
        if(!empty($user) && !is_null($user->user->fcm_token)){
            $note = Yii::$app->fcm->createNotification($title, $description);

            $message = Yii::$app->fcm->createMessage();
            $note->setIcon('/img/lima-logo.jpg')
                ->setSound('default')
                ->setColor('#ffffff');

            $message->addRecipient(new Device($user->user->fcm_token));
            $message->setNotification($note)
                ->setData([
                    'type' => 'order_request',
                    'id_request' => $id_request,
                    'order_id' => $order_id
                ]);
            Yii::$app->fcm->send($message);
        }
    }

    /**
     * Function send notice one customer
     *
     * @param $order_id
     * @param $title
     * @param $description
     */
    public static function sendNoticeCustomerRequest($order_id, $title, $description){
        $order = OrderPlace::findOne(['order_id' => $order_id]);
        $customer_id = $order->order->customer->id;
        $branch = $order->orderAddress->branch_id;
        if ($branch == 0){
            $user = Customer::findOne($customer_id);
        }
        else{
            $user = CustomerPersonal::findOne(['branch_id' => $branch, 'role' => CustomerPersonal::ROLE_ADMINISTRATOR]);
        }

        if(!empty($user) && !is_null($user->user->fcm_token)){
            $note = Yii::$app->fcm->createNotification($title, $description);

            $message = Yii::$app->fcm->createMessage();
            $note->setIcon('/img/lima-logo.jpg')
                ->setSound('default')
                ->setColor('#ffffff');

            $message->addRecipient(new Device($user->user->fcm_token));
            $message->setNotification($note)
                ->setData([
                    'type' => 'order_request'
                ]);
            Yii::$app->fcm->send($message);
        }
    }

    /**
     * Function send notice one worker
     *
     * @param $order_id
     * @param $worker_id
     * @param $title
     * @param $description_customer
     * @param $description_worker
     */
    public static function sendNoticeCustomerWorkerRequest($order_id, $worker_id, $title, $description_customer, $description_worker){
        $order = OrderPlace::findOne(['order_id' => $order_id]);
        $customer_id = $order->order->customer->id;
        $branch = $order->orderAddress->branch_id;
        if ($branch == 0){
            $user = Customer::findOne($customer_id);
        }
        else{
            $user = CustomerPersonal::findOne(['branch_id' => $branch, 'role' => CustomerPersonal::ROLE_ADMINISTRATOR]);
        }

        $user_worker = Worker::findOne($worker_id);
        if(!empty($user) && !is_null($user->user->fcm_token)){
            $note = Yii::$app->fcm->createNotification($title, $description_customer);
            $message = Yii::$app->fcm->createMessage();
            $note->setIcon('/img/lima-logo.jpg')
                ->setSound('default')
                ->setColor('#ffffff');

            $message->addRecipient(new Device($user->user->fcm_token));
            $message->setNotification($note)
                ->setData([
                    'type' => 'order_request'
                ]);
            Yii::$app->fcm->send($message);
        }

        if(!empty($user_worker) && !is_null($user_worker->user->fcm_token)){
            $note = Yii::$app->fcm->createNotification($title, $description_worker);
            $message = Yii::$app->fcm->createMessage();
            $note->setIcon('/img/lima-logo.jpg')
                ->setSound('default')
                ->setColor('#ffffff');

            $message->addRecipient(new Device($user_worker->user->fcm_token));
            $message->setNotification($note)
                ->setData([
                    'type' => 'order_request'
                ]);
            Yii::$app->fcm->send($message);
        }
    }

    /**
     * Function send notice
     *
     * @param null $role
     * @param $title
     * @param $description
     * @param $type
     * @param $sound
     * @param null $id
     * @return array|bool|int|null
     */
    public static function sendNotification($role = null, $title, $description, $type, $sound, $id = null){
        if(!is_null($role)){
            $user_ids = Yii::$app->authManager->getUserIdsByRole($role);
            $query = (new Query())
                ->from('user')
                ->orderBy('id')
                ->select(['id', 'fcm_token'])
                ->where(['NOT', ['fcm_token' => null]])
                ->andWhere(['id' => $user_ids]);
        }
        else{
            $query = (new Query())
                ->from('user')
                ->orderBy('id')
                ->select(['id', 'fcm_token'])
                ->where(['NOT', ['fcm_token' => null]]);
        }

        switch ($type){
            case NotificationForm::TYPE_INFO:
                $status = true;
                foreach ($query->batch(1000) as $users) {
                    $new_client = [];
                    foreach ($users as $user) {
                        $new_client[] = $user['fcm_token'];
                    }
                    $status = FirebaseHelpers::sendAll($new_client, $title, $description, $type, null,  $sound);
                }
                break;
            case NotificationForm::TYPE_NEWS:
                $status = true;
                foreach ($query->batch(1000) as $users) {
                    $new_client = [];
                    foreach ($users as $user) {
                        $new_client[] = $user['fcm_token'];
                    }
                    $status = FirebaseHelpers::sendAll($new_client, $title, $description, $type, ['id' => $id],  $sound);
                }
                break;
            default:
                return null;
        }
        return true;//empty($status) ? true : $result;
    }

    public static function checkFreelancer($inn){
        $date = date('Y-m-d');
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://statusnpd.nalog.ru/api/v1/tracker/taxpayer_status",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode(['inn' => $inn, 'requestDate' => $date]),
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        } else {
            return json_decode($response);
        }
    }

    /**
     * Function send push all users
     *
     * @param $title
     * @param $description
     * @param null $sound
     * @return bool
     */
    public static function sendAllUsers($title, $description, $sound){
        $query = (new Query())
            ->from('user')
            ->orderBy('id')
            ->select('fcm_token')
            ->where(['NOT', ['fcm_token' => null]]);
        $status = true;
        foreach ($query->batch(1000) as $users) {
            $new_client = [];
            foreach ($users as $user) {
                $new_client[] = $user['fcm_token'];
            }
            $status = FirebaseHelpers::sendAll($new_client, $title, $description, $sound);
        }
        return $status;
    }
}