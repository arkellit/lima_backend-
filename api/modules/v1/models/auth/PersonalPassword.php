<?php

namespace api\modules\v1\models\auth;

use Yii;

/**
 * This is the model class for table "{{%personal_password}}".
 *
 * @property int $user_id
 * @property string $password_enc
 *
 * @property User $user
 */
class PersonalPassword extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%personal_password}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'password_enc'], 'required'],
            [['user_id'], 'integer'],
            [['password_enc'], 'string'],
            [['user_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'password_enc' => 'Password Enc',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
