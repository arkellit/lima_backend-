<?php

namespace api\modules\v1\models\auth;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%token}}".
 *
 * @property int $user_id
 * @property int $code
 * @property int $created_at
 * @property int $updated_at
 * @property int $type
 *
 * @property string $phone
 *
 * @property User $user
 */
class Token extends ActiveRecord
{
    const TYPE_CONFIRMATION = 0;
    const TYPE_CREATE_MANAGER = 1;
    const TYPE_CREATE_ADMINISTRATOR = 2;
    const TYPE_FORGOT_PASSWORD = 3;

    public $phone;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_token}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'code', 'type'], 'required'],
            [['user_id', 'code', 'created_at', 'updated_at', 'type'], 'integer'],
            [['code'], 'number', 'min' => 1000, 'max' => 9999],
            ['user_id', 'unique'],
            ['type', 'in', 'range' => [self::TYPE_CONFIRMATION, self::TYPE_CREATE_MANAGER, self::TYPE_CREATE_ADMINISTRATOR
            , self::TYPE_FORGOT_PASSWORD]],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'code' => 'Код',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'type' => 'Тип',
        ];
    }

    /**
     * One-to-one user
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['user_id', 'code', 'type'];
    }

    /**
     * Function create code
     *
     * @return int
     */
    public static function getRandCode(){
        return rand(1000, 9999);
    }

    /**
     * Function create date created
     *
     * @return int
     */
    public static function getCratedAt(){
        return time();
    }

    public static function getUpdatedAt(){
        return time();
    }
}
