<?php

namespace api\modules\v1\models\auth;

use api\modules\v1\models\customer\CustomerPersonal;
use common\helpers\GetErrorsHelpers;
use Yii;
use yii\base\Model;
use yii\web\HttpException;

/**
 * @property string $phone
 * @property string $password
 *
 * @property string $social_name
 * @property string $access_token
 * @property string $fcm_token
 *
 * Class LoginForm
 * @package app\modules\v1\models\auth
 */
class LoginForm extends Model
{
    public $phone;
    public $password;
    public $fcm_token;

    private $_user = false;

    public $access_token;
    public $social_name;

    const SCENARIO_SOCIAL_LOGIN = 1;
    const SCENARIO_LOGIN = 2;

    const SOCIAL_VKONTAKTE = 'vkontakte';
    const SOCIAL_FACEBOOK = 'facebook';
    const SOCIAL_GOOGLE = 'google';
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'password'], 'required', 'on' => self::SCENARIO_LOGIN],
            ['phone', 'string', 'length' => [11, 12], 'on' => self::SCENARIO_LOGIN],
            ['fcm_token', 'string', 'max' => 255, 'on' => self::SCENARIO_LOGIN],
            ['password', 'validatePassword', 'on' => self::SCENARIO_LOGIN],
            [['password'], 'string', 'max' => 64, 'tooLong' => 'Неверное значение пароля', 'on' => self::SCENARIO_LOGIN],

            [['access_token', 'social_name'], 'required', 'on' => self::SCENARIO_SOCIAL_LOGIN],
            [['access_token', 'social_name'], 'string', 'on' => self::SCENARIO_SOCIAL_LOGIN],
            ['fcm_token', 'string', 'max' => 255, 'on' => self::SCENARIO_SOCIAL_LOGIN],
            ['social_name', 'in', 'range' => [self::SOCIAL_VKONTAKTE, self::SOCIAL_FACEBOOK, self::SOCIAL_GOOGLE], 'on' => self::SCENARIO_SOCIAL_LOGIN]
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->phone = User::normalizePhone($this->phone);
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'phone' => 'Номер телефона',
            'password' => 'Пароль',
            'social_name' => 'Название соц сети',
            'fcm_token' => 'Fcm токен'
        ];
    }

    /**
     * Function return login token
     *
     * @return array|null
     * @throws HttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function login(){
        if (!$this->validate()) {
            $message =  GetErrorsHelpers::getError($this->getErrors());
            throw new HttpException(400, $message);
        }
        $user = $this->getUser();
        $user->access_token = User::setAccessToken($user->phone);;
        if(!is_null($this->fcm_token)){
            $user->fcm_token = $this->fcm_token;
        }
        if ($user->save()){
            /**
             * Is invite user
             */
            if ($user->is_invite == User::IS_INVITE){
                $user_personal = CustomerPersonal::findOne(['user_id' => $user->id]);
                return array('success' => 1, 'data' => [
                    'token' => $user->access_token,
                    'is_invite' => true, 'is_personal_registration' =>
                    $user_personal->is_register == CustomerPersonal::NO_REGISTER_COMPLETE
                        ? false : true,
                    'id' => $user->id
                ], 'status' => 200);
            }
            return array('success' => 1, 'data' => ['token' => $user->access_token,
                'id' => $user->id], 'status' => 200);
        }
        throw new HttpException(400, 'Неверные данные');
    }

    /**
     * Function return login token via social network
     *
     * @return array|null
     * @throws HttpException
     */
    public function socialLogin(){
        if (!$this->validate()) {
            return null;
        }

        $social_user = new SocialSendForm();
        $create_social = $social_user->sendSocial($this->social_name, $this->access_token);
        if($create_social['is_success'] == null){
            return null;
        }

        if(isset($create_social['is_new_create'])){
            return array('is_new_create' => true,
                'client_id' => $create_social['client_id'],
                'phone' => $create_social['phone']);
        }

        $login = SocialAccount::find()->with('user')
            ->where(['AND', ['client_id' => $create_social['client_id']], ['provider' => $this->social_name]])->one();

        if (is_null($login->user)){
            throw new HttpException(400, 'Пользователь не зарегистрирован', 400);
        }

        if($login->user->is_confirm == User::NO_CONFIRM_PHONE){
            throw new HttpException(419, 'Номер телефона не подтвержден');
        }

        if ($login != null){
            $token = User::setAccessToken($login->user->phone);
            if(!is_null($this->fcm_token)){
                User::updateAll(['access_token' => $token, 'fcm_token' => $this->fcm_token],
                    ['id' => $login->user->id]);
            }
            User::updateAll(['access_token' => $token],
                ['id' => $login->user->id]);
            return array('token' => $token);
        }

        return null;
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неверный пароль');
            }
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByPhone($this->phone);
        }

        return $this->_user;
    }

}