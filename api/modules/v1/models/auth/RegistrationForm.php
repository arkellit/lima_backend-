<?php

namespace api\modules\v1\models\auth;

use common\helpers\GetErrorsHelpers;
use common\helpers\TokenCodeHelpers;
use api\modules\v1\models\Sending;
use Yii;
use \yii\base\Model;
use yii\web\HttpException;

/**
 * @property string $phone
 * @property string $password
 * @property integer $role
 *
 * @property string $social_name
 * @property string $access_token
 * @property string $fcm_token
 *
 * Class RegistrationForm
 * @package app\modules\v1\models\auth
 */
class RegistrationForm extends Model
{
    public $phone;
    public $password;
    public $role;
    public $fcm_token;

    public $access_token;
    public $social_name;

    const CUSTOMER = 1;
    const WORKER = 2;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['phone', 'trim'],
            [['phone', 'password'], 'required'],
//            ['role', 'required', 'message' => 'Отсутствует роль'],

            ['phone', 'validatePhone'],
            ['phone', 'unique', 'targetClass' => '\api\modules\v1\models\auth\User', 'message' => 'Номер телефона уже зарегистрирован'],
            ['phone', 'string', 'length' => [11, 12]],
            ['fcm_token', 'string', 'max' => 255],
            [['access_token', 'social_name'], 'string'],
            [['password'], 'string', 'max' => 64, 'tooLong' => 'Неверное значение пароля'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->phone = User::normalizePhone($this->phone);
            return true;
        }
        return false;
    }

    /**
     * Validate phone
     *
     * @param $attribute
     * @throws HttpException
     */
    public function validatePhone($attribute)
    {
        $phone_check = User::findOne(['phone' => $this->$attribute]);

        if($phone_check != null){
            if ($phone_check->is_confirm == User::NO_CONFIRM_PHONE) {
                throw new HttpException(419, 'Номер телефона не подтвержден');
            }
            elseif ($phone_check->is_confirm == User::CONFIRM_PHONE){
                throw new HttpException(418, 'Номер телефона уже зарегистирован и подтвержден');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'phone' => 'Номер телефона',
            'password' => 'Пароль'
        ];
    }

    /**
     * Function create new User
     *
     * @param null $is_social_create
     * @param null $is_confirm
     * @return array|null
     * @throws HttpException
     * @throws \yii\base\Exception
     */
    public function create($is_social_create = null, $is_confirm = null){
        if (!$this->validate()) {
            $message =  GetErrorsHelpers::getError($this->getErrors());
            throw new HttpException(400, $message);
        }

        $user = new User();
        $user->phone = $this->phone;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->access_token = User::setAccessToken($user->phone);
        $user->fcm_token = $this->fcm_token;
        $user->is_social = is_null($is_social_create) ? User::NO_SOCIAL_ACCOUNT : User::SOCIAL_ACCOUNT;
        $user->is_confirm = is_null($is_confirm) ? User::NO_CONFIRM_PHONE : User::CONFIRM_PHONE;

        if($user->save()){
            if(is_null($is_social_create)){
                TokenCodeHelpers::createSend($user->id, Token::TYPE_CONFIRMATION,
                    $user->phone);
            }

            return array('phone' => $user->phone,
                'token' => $user->access_token, 'id' => $user->id);
        }
        return null;
    }


    /**
     * Function create new Personal User
     *
     * @param $is_invite
     * @return array|null
     * @throws HttpException
     * @throws \Throwable
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function createPersonal($is_invite){
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->phone = $this->phone;
        $password_hash = hash('sha256', $this->password);
        $user->setPassword($password_hash);
        $user->generateAuthKey();
        $user->access_token = User::setAccessToken($user->phone);
        $user->fcm_token = $this->fcm_token;
        $user->is_social = User::NO_SOCIAL_ACCOUNT;
        $user->is_confirm = User::CONFIRM_PHONE;
        $user->is_invite = is_null($is_invite) ? User::NO_INVITE : User::IS_INVITE;

        if($user->save()){
            $token = TokenCodeHelpers::create($user->id, Token::TYPE_CONFIRMATION);
            if ($token == null){
                $user->delete();
                throw new HttpException(400, 'Произошла ошибка');
            }
            return array('phone' => $user->phone, 'id' => $user->id);
        }
        return null;
    }
//            $role = $this->role;
//            switch ($role){
//                case RegistrationForm::CUSTOMER:
//                    $userRole = Yii::$app->authManager->getRole('customer');
//                    break;
//                case RegistrationForm::WORKER:
//                    $userRole = Yii::$app->authManager->getRole('worker');
//                    break;
//                default:
//                    $this->addErrors([$this->role, ['Неверная роль']]);
//                    $user->delete();
//                    return null;
//            }

//            if ($userRole != null){
//                Yii::$app->authManager->assign($userRole, $user->id);
//                $token = new Token();
//                $token->user_id = $user->id;
//                $token->code = $token->getRandCode();
//                $token->created_at = $token->getCratedAt();
//                $token->type = Token::TYPE_CONFIRMATION;
//                if(!$token->save()){
//                    $this->addErrors([$this->role, ['Не удалось создать токен']]);
//                    return null;
//                }
//                $send_code = true;// = $token->sendCode($user->phone, $token->code);
//                if($this->access_token && $this->social_name){
//                    return array('id' => $user->id, 'token' => $user->access_token, 'role' => $userRole->name,
//                        'social_data' => $create_social['data']);
//                }
//                return array('phone' => $user->phone, 'message' => $send_code != null ? 'Код успешно отправлен' : $send_code);
//            }



}