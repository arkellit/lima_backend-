<?php

namespace api\modules\v1\models\auth;

use Yii;
use yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%change_user}}".
 *
 * @property int $user_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $type
 *
 * @property User $user
 */
class ChangeUser extends ActiveRecord
{
    const TYPE_CHANGE_PHONE = 1;
    const TYPE_CHANGE_PASSWORD = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_change}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            ['type', 'in', 'range' => [self::TYPE_CHANGE_PASSWORD, self::TYPE_CHANGE_PHONE]],
            [['user_id', 'created_at', 'updated_at', 'type'], 'integer'],
            [['user_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'created_at' => 'Created At',
            'updated_at' => 'Update At',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
