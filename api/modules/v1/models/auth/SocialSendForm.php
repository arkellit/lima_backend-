<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 04.09.18
 * Time: 13:01
 */

namespace api\modules\v1\models\auth;

use Yii;
use yii\base\Model;
use yii\web\HttpException;

/**
 * Class SocialSendForm
 *
 * @property string $access_token
 * @property string $uids
 * @property string $fields
 * @property string $social_name
 *
 * @property integer $user_id
 *
 * @package app\modules\v1\models\auth
 */
class SocialSendForm extends Model
{
    public $access_token;
    public $fields = 'uid,first_name,last_name,sex,contacts,bdate,photo_big,city';
    public $social_name;
    public $user_id;

    const VK_URL = 'https://api.vk.com/method/users.get';
    const GOOGLE_URL = 'https://www.googleapis.com/oauth2/v1/userinfo';
    const FB_URL = 'https://graph.facebook.com/me';

    const SCENARIO_FB_GOOGLE = 1;
    const SCENARIO_VK = 2;

    public function rules()
    {
        return [
            [['access_token', 'fields', 'social_name'], 'required', 'on' => self::SCENARIO_VK],
            [['access_token', 'social_name'], 'required', 'on' => self::SCENARIO_FB_GOOGLE],
            [['access_token', 'fields', 'social_name'], 'string'],
            ['user_id', 'integer']
        ];
    }

    public function sendSocial($soc_name, $soc_token){
//        $model = new SocialSendForm();
        switch ($soc_name){
            case SocialAccount::SOCIAL_VKONTAKTE:
                $this->scenario = SocialSendForm::SCENARIO_VK;
                $this->access_token = $soc_token;
                $this->social_name = SocialAccount::SOCIAL_VKONTAKTE;
                $response = $this->getUserInfoVk();
                break;
            case SocialAccount::SOCIAL_FACEBOOK:
                $this->scenario = SocialSendForm::SCENARIO_FB_GOOGLE;
                $this->access_token = $soc_token;
                $this->social_name = SocialAccount::SOCIAL_FACEBOOK;
                $response = $this->getUserInfoFacebook();
                break;
            case SocialAccount::SOCIAL_GOOGLE:
                $this->scenario = SocialSendForm::SCENARIO_FB_GOOGLE;
                $this->access_token = $soc_token;
                $this->social_name = SocialAccount::SOCIAL_GOOGLE;
                $response = $this->getUserInfoGoogle();
                break;
            default:
                throw new HttpException(400, 'Неверные данные');
        }

        return $response;
    }


    public function getUserInfoVk()
    {
        $params = array(
            'fields'       => $this->fields,
            'access_token' => $this->access_token,
            'v' => 5.84
        );

        $userInfo = json_decode(file_get_contents( $this::VK_URL . '?' . urldecode(http_build_query($params))), true);
        if (isset($userInfo['response'][0]['id'])) {
            $social_check = SocialAccount::findOne(['client_id' => $userInfo['response'][0]['id'], 'provider' => SocialAccount::SOCIAL_VKONTAKTE]);
            if(is_null($social_check)){
                $social_user = new SocialAccount();
                $social_user->provider = SocialAccount::SOCIAL_VKONTAKTE;
                $social_user->client_id = $userInfo['response'][0]['id'];
                $social_user->first_name = $userInfo['response'][0]['first_name'] ?? null;
                $social_user->last_name = $userInfo['response'][0]['last_name'] ?? null;
                $sex = $userInfo['response'][0]['sex'] ?? null;
                if(!is_null($sex)){
                    $social_user->sex = $sex == 1 ? SocialAccount::WOMAN : SocialAccount::MAN;
                }
                else{
                    $social_user->sex = null;
                }
                $social_user->birth_date = isset($userInfo['response'][0]['bdate']) ? (int)Yii::$app->formatter->asTimestamp($userInfo['response'][0]['bdate']) : null;
                $social_user->city = $userInfo['response'][0]['city']['title'] ?? null;
                $social_user->photo = $userInfo['response'][0]['photo_big'] ?? null;
                $social_user->mobile_phone = isset($userInfo['response'][0]['mobile_phone']) ? $userInfo['response'][0]['mobile_phone'] : null;

                return $social_user->save() ? array('is_success' => true,
                    'is_new_create' => true,
                    'phone' =>  $social_user->mobile_phone,
                    'client_id' =>  $social_user->client_id) :
                    array('is_success' => null, 'error' => $social_user->getErrors());
            }
            return array('is_success' => true, 'client_id' =>  $social_check->client_id,
                'social_name' => SocialAccount::SOCIAL_VKONTAKTE);
        }
        return  array('is_success' => null);
    }

    public function getUserInfoFacebook(){
        $params = array(
            'access_token' => $this->access_token
        );

        $userInfo = json_decode(file_get_contents( $this::FB_URL . '?' .
            urldecode(http_build_query($params))), true);
        if (isset($userInfo['response'][0]['id'])) {
            $social_check = SocialAccount::findOne(['AND', ['client_id' => $userInfo['response'][0]['id']], ['provider' => SocialAccount::SOCIAL_VKONTAKTE]]);
            if(is_null($social_check)) {
                $social_user = new SocialAccount();
                $social_user->provider = SocialAccount::SOCIAL_FACEBOOK;
                $social_user->client_id = $userInfo['response'][0]['id'];
                $social_user->first_name = $userInfo['response'][0]['first_name'] ?? null;
                $social_user->last_name = $userInfo['response'][0]['last_name'] ?? null;
                $sex = $userInfo['response'][0]['gender'] ?? null;
                if(!is_null($sex)){
                    $social_user->sex = $sex == 'male' ? SocialAccount::MAN : SocialAccount::WOMAN;
                }
                else{
                    $social_user->sex = null;
                }
                $social_user->birth_date = isset($userInfo['response'][0]['birthday']) ? (int)Yii::$app->formatter->asTimestamp($userInfo['response'][0]['birthday']) : null;
                $social_user->city = $userInfo['response'][0]['location']['name'] ?? null;
                $social_user->mobile_phone = isset($userInfo['response'][0]['mobile_phone']) ? $userInfo['response'][0]['mobile_phone'] : null;

                return $social_user->save() ? array('is_success' => true,
                    'is_new_create' => true,
                    'phone' =>  $social_user->mobile_phone,
                    'client_id' =>  $social_user->client_id) :
                    array('is_success' => null, 'error' => $social_user->getErrors());
            }
            return array('is_success' => true, 'client_id' =>  $social_check->client_id,
                'social_name' => SocialAccount::SOCIAL_FACEBOOK);
        }

        return  array('is_success' => null);
    }

    public function getUserInfoGoogle(){
        $params = array(
            'access_token' => $this->access_token
        );

        $userInfo = json_decode(file_get_contents( $this::GOOGLE_URL . '?' .
            urldecode(http_build_query($params))), true);
        if (isset($userInfo['response'][0]['id'])) {
            $social_check = SocialAccount::findOne(['AND', ['client_id' => $userInfo['response'][0]['id']], ['provider' => SocialAccount::SOCIAL_VKONTAKTE]]);
            if(is_null($social_check)) {
                $social_user = new SocialAccount();
                $social_user->provider = SocialAccount::SOCIAL_GOOGLE;
                $social_user->client_id = $userInfo['response'][0]['id'];
                $social_user->first_name = $userInfo['response'][0]['given_name'] ?? null;
                $social_user->last_name = $userInfo['response'][0]['family_name'] ?? null;
                $sex = $userInfo['response'][0]['gender'] ?? null;
                if(!is_null($sex)){
                    $social_user->sex = $sex == 'male' ? SocialAccount::MAN : SocialAccount::WOMAN;
                }
                else{
                    $social_user->sex = null;
                }
                $social_user->photo = $userInfo['response'][0]['picture'] ?? null;
                $social_user->email = $userInfo['response'][0]['email'] ?? null;

                return $social_user->save() ? array('is_success' => true,
                    'is_new_create' => true,
                    'phone' =>  $social_user->mobile_phone,
                    'client_id' =>  $social_user->client_id) :
                    array('is_success' => null, 'error' => $social_user->getErrors());
            }
            return array('is_success' => true, 'client_id' =>  $social_check->client_id,
                'social_name' => SocialAccount::SOCIAL_GOOGLE);
        }
        return  array('is_success' => null);
    }


}