<?php

namespace api\modules\v1\models\auth;

use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%social_account}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $provider
 * @property string $client_id
 * @property string $first_name
 * @property string $last_name
 * @property string $sex
 * @property string $birth_date
 * @property string $email
 * @property string $photo
 * @property string $city
 * @property string $mobile_phone
 */
class SocialAccount extends ActiveRecord
{
    const SOCIAL_VKONTAKTE = 'vkontakte';
    const SOCIAL_FACEBOOK = 'facebook';
    const SOCIAL_GOOGLE = 'google';

    const MAN = 1;
    const WOMAN = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_social_account}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['provider', 'client_id'], 'required'],
            [['user_id', 'sex', 'birth_date', 'client_id'], 'integer'],
            ['user_id', 'default', 'value' => 0],
            ['sex', 'in', 'range' => [self::MAN, self::WOMAN]],
            [['provider', 'first_name', 'last_name', 'photo', 'email', 'city', 'mobile_phone'], 'string', 'max' => 255],
//            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'provider' => 'Социальная сеть',
            'client_id' => 'ИД соц сети',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'sex' => 'Пол',
            'birth_date' => 'Дата рождения',
            'email' => 'Email',
            'photo' => 'Фотография',
            'city' => 'Город проживания',
            'mobile_phone' => 'Номер телефона'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
