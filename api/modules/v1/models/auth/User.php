<?php

namespace api\modules\v1\models\auth;

use common\helpers\EncryptionHelpers;
use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\worker\Worker;
use frontend\models\customer\CustomerPersonal;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\HttpException;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $phone
 * @property string $auth_key
 * @property string $access_token
 * @property string $password
 * @property string $password_reset_token
 * @property string $fcm_token
 * @property string $registration_ip
 *
 * @property int $is_blocked
 * @property int $created_at
 * @property int $updated_at
 * @property int $is_confirm
 * @property int $is_social
 * @property int $is_invite
 * @property int $is_confirm_forgot_code
 * @property int $is_delete
 *
 * @property Token $code
 *
 */
class User extends ActiveRecord implements IdentityInterface
{

    const STATUS_BLOCKED = 1;
    const STATUS_NO_BLOCKED = 0;

    const CONFIRM_PHONE = 1;
    const NO_CONFIRM_PHONE = 0;

    const SOCIAL_ACCOUNT = 1;
    const NO_SOCIAL_ACCOUNT = 0;

    const CONFIRM_CODE = 1;
    const NO_CONFIRM_CODE = 0;

    const NO_INVITE = null;
    const IS_INVITE = 1;

    const IS_DELETE = 1;
    const NO_DELETE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phone', 'auth_key', 'access_token', 'password'], 'required'],
            [['is_blocked'], 'default', 'value' => User::STATUS_NO_BLOCKED],
            [['is_delete'], 'default', 'value' => User::STATUS_NO_BLOCKED],
            [['is_blocked', 'created_at', 'updated_at', 'is_confirm', 'is_social', 'is_invite'], 'integer'],
            ['phone', 'string', 'length' => [11, 15]],
            [['auth_key'], 'string', 'max' => 32],
            [['access_token'], 'string', 'length' => [64, 64]],
            [['password', 'password_reset_token'], 'string', 'max' => 64],
            ['fcm_token', 'string', 'max' => 255],
            [['access_token'], 'unique'],
            ['registration_ip', 'string', 'max' => 45],
            [['password_reset_token'], 'unique'],
            [['phone'], 'unique', 'message' => 'Такой номер телефона уже зарегистрирован'],
            ['is_confirm', 'in', 'range' => [self::CONFIRM_PHONE, self::NO_CONFIRM_PHONE]],
            [['is_confirm'], 'default', 'value' => User::NO_CONFIRM_PHONE],
            [['is_invite'], 'default', 'value' => null],
            ['is_invite', 'in', 'range' => [self::NO_INVITE, self::IS_INVITE]],
            ['is_social', 'in', 'range' => [self::SOCIAL_ACCOUNT, self::NO_SOCIAL_ACCOUNT]],
            [['is_social'], 'default', 'value' => User::NO_SOCIAL_ACCOUNT],
            ['is_confirm_forgot_code', 'in', 'range' => [self::CONFIRM_CODE, self::NO_CONFIRM_CODE]],
            ['is_confirm_forgot_code', 'default', 'value' => User::NO_CONFIRM_PHONE],
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->registration_ip = Yii::$app->request->userIP;
            return parent::beforeSave($insert);
        } else {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Номер телефона',
            'auth_key' => 'Auth Key',
            'access_token' => 'Access Token',
            'password' => 'Пароль',
            'password_reset_token' => 'Password Reset Token',
            'is_blocked' => 'Блокирован',
            'is_confirm' => 'Подтвержден номер телефона',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'is_social' => 'Через соц сеть',
            'is_invite' => 'Зареган под инвайтом',
            'fcm_token' => 'Fcm токен',
            'is_confirm_forgot_code' => 'Подтверждение кода',
            'registration_ip' => 'Ip адрес',
            'is_delete' => 'Удален'
        ];
    }

    /**
     * @return bool Whether the user is confirmed or not.
     */
    public function getIsConfirmed()
    {
        return $this->is_confirm == User::CONFIRM_PHONE;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findByPhone($phone)
    {
        $user = static::findOne(['phone' => $phone, 'is_blocked' => self::STATUS_NO_BLOCKED,
            'is_delete' => User::NO_DELETE]);
        if (empty($user)){
            throw new HttpException(400, 'Номер телефона не найден');
        }
        if($user->is_confirm == User::NO_CONFIRM_PHONE){
            throw new HttpException(419, 'Номер телефона не подтвержден');
        }
        return $user;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAccessToken()
    {
        return $this->access_token;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Generates access token
     *
     * @param $phone
     * @return string
     */
    public static function setAccessToken($phone)
    {
       return hash('sha256', $phone . rand(100, 9999) . time());
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * @param mixed $token
     * @param null $type
     * @return null|static
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne([
            'access_token' => $token,
            'is_blocked' => self::STATUS_NO_BLOCKED
        ]);
    }

    public static function findByPasswordResetToken($token)
    {

        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'is_blocked' => self::STATUS_NO_BLOCKED,
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {

        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocial()
    {
        return $this->hasOne(SocialAccount::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCode(){
        return $this->hasOne(Token::className(), ['user_id' => 'id']);
    }

    public function isSocial(){
        return $this->is_social == $this::SOCIAL_ACCOUNT;
    }

    /**
     * Function get encrypt password to administrator or manager
     *
     * @param $id
     * @return bool|string
     */
    public static function getEncPassword($id){
        $hash_password = PersonalPassword::findOne(['user_id' => $id]);

        return EncryptionHelpers::dec_enc('decrypt', $hash_password->password_enc);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerPersonal(){
        return $this->hasOne(CustomerPersonal::className(), ['user_id' => 'id']);
    }

    /**
     * Function normalize format number
     *
     * @param $phone
     * @return mixed
     */
    public static function normalizePhone($phone) {
        $resPhone = preg_replace("/[^0-9]/", "", $phone);

        if (strlen($resPhone) === 11) {
            $resPhone = preg_replace("/^7/", "8", $resPhone);
        }
        return $resPhone;
    }

    /**
     * Delete user
     *
     * @return bool|null
     */
    public function setDelete(){
        $this->is_delete = $this::IS_DELETE;
        $this->phone = $this->checkPhoneDelete($this->phone);
        if ($this->save()){
            return true;
        }
        return null;
    }

    /**
     * Function return new delete phone
     *
     * @param $phone
     * @return string
     */
    public function checkPhoneDelete($phone){
        if (/*$this->phone != 'D' . $phone &&*/ !$this::find()->where(['phone' => 'D' . $phone])->exists()){
            return 'D' . $phone;
        }
        for ($i = 0; $i <= 100; $i++){
            if (!$this::find()->where(['phone' => 'D' . $i . $phone])->exists()){
                return 'D' . $i . $phone;
            }
        }
    }
}