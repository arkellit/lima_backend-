<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 24.08.18
 * Time: 15:11
 */

namespace api\modules\v1\models\auth;


use common\helpers\ChangeUserHelpers;
use common\helpers\DifferenceTimeHelpers;
use common\helpers\TokenCodeHelpers;
use Yii;
use yii\base\Model;
use yii\web\HttpException;

class PasswordResetForm extends Model
{
    public $phone;
    public $password_reset_token;
    public $password;

    const SCENARIO_GENERATE_TOKEN =  1;
    const SCENARIO_SET_NEW_PASSWORD = 2;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['phone', 'required', 'on' => self::SCENARIO_GENERATE_TOKEN],
            ['phone', 'string', 'length' => [11, 12], 'on' => self::SCENARIO_GENERATE_TOKEN],
            [['password_reset_token', 'password'], 'required', 'on' => self::SCENARIO_SET_NEW_PASSWORD]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'phone' => 'Номер телефона',
            'password_reset_token' => 'Токен'
        ];
    }

    /**
     * Function return login token
     *
     * @return null|string
     * @throws HttpException
     */
    public function generateToken()
    {

        $user = User::findOne(['phone' => $this->phone, 'is_blocked' => User::STATUS_NO_BLOCKED]);

        if (!$user) {
            return null;
        }

        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return null;
            }
        }

//        $token = Token::findOne(['user_id' => $user->id]);
//        if ($token->type == Token::TYPE_FORGOT_PASSWORD && DifferenceTimeHelpers::differenceTime($token->updated_at) < 3600){
//            throw new HttpException(423, 'Сброс пароля разрешен раз в час');
//        }

        $change_user = ChangeUser::findOne(['user_id' => $user->id]);
        if (!empty($change_user)){
            $token = Token::findOne(['user_id' => $user->id]);

            if ($token->type == Token::TYPE_FORGOT_PASSWORD
                && DifferenceTimeHelpers::differenceTime($token->updated_at) < 3600){
                if ($change_user->type == ChangeUser::TYPE_CHANGE_PASSWORD
                    && DifferenceTimeHelpers::differenceTime($change_user->updated_at) < 3600){

                    throw new HttpException(423, 'Сброс пароля разрешен раз в час');
                }

                throw new HttpException(427, 'Код был выслан');
            }

            TokenCodeHelpers::updateSend($user->id, Token::TYPE_FORGOT_PASSWORD,
                $user->phone);

            return $user->password_reset_token;
        }

        $token = Token::findOne(['user_id' => $user->id]);
        if ($token->type == Token::TYPE_FORGOT_PASSWORD
            && DifferenceTimeHelpers::differenceTime($token->updated_at) < 3600){
            throw new HttpException(427, 'Код был выслан');
        }

        TokenCodeHelpers::updateSend($user->id, Token::TYPE_FORGOT_PASSWORD,
            $user->phone);

        return $user->password_reset_token;
    }

    /**
     * Function set new password
     *
     * @param $token
     * @param $password
     * @param null $fcm_token
     * @return array|null|string
     * @throws HttpException
     * @throws \yii\base\Exception
     */
    public function setNewPassword($token, $password, $fcm_token = null)
    {
        if (empty($token) || !is_string($token)) {
            return null;
        }

        $user = User::findByPasswordResetToken($token);

        if (!$user) {
            return null;
        }

        if ($user->is_confirm_forgot_code == User::NO_CONFIRM_CODE){
            throw new HttpException(419, 'Подтвердите действие через смс');
        }

        $user->setPassword($password);
        $user->access_token = User::setAccessToken($user->phone);
        if(!is_null($fcm_token)){
            $user->fcm_token = $fcm_token;
        }
        $user->removePasswordResetToken();
        $user->is_confirm_forgot_code = User::NO_CONFIRM_CODE;
        if($user->save()){
            $change_user = ChangeUser::findOne(['user_id' => $user->id]);
            if (empty($change_user)){
                $change = ChangeUserHelpers::create($user->id, ChangeUser::TYPE_CHANGE_PASSWORD);
                if(!$change){
                    throw new HttpException(400, 'Неверные данные');
                }

                return array('token' => $user->access_token, 'id' => $user->id);
            }

            $change_user->type = ChangeUser::TYPE_CHANGE_PASSWORD;
            $change_user->touch('updated_at');
            if(!$change_user->save()){
                throw new HttpException(400, 'Неверные данные');
            }

            return array('token' => $user->access_token, 'id' => $user->id);
        }

        return null;
    }


}