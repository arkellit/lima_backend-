<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 07.09.18
 * Time: 15:45
 */

namespace api\modules\v1\models\auth;


use common\helpers\EncryptionHelpers;
use api\modules\v1\models\Sending;
use Yii;
use yii\base\Model;
use yii\web\HttpException;

/**
 * @property string $phone
 * @property string $customer_phone
 * @property string $password
 * @property int $personal_type
 * @property int $customer_user_id
 *
 * Class PersonalRegistrationForm
 * @package app\modules\v1\models\auth
 */
class PersonalRegistrationForm extends Model
{
    public $customer_user_id;
    public $customer_phone;
    public $phone;
    public $password;
    public $personal_type;

    const MANAGER = 3;
    const ADMINISTRATOR = 4;

    public function rules()
    {
        return [
            [['customer_user_id', 'customer_phone', 'phone', 'password', 'personal_type'], 'required'],
            [['phone', 'customer_phone'], 'string', 'length' => [11, 12]],
            [['customer_user_id', 'personal_type'], 'integer'],
            ['personal_type', 'in', 'range' => [self::MANAGER, self::ADMINISTRATOR]],
            [['password'], 'string', 'max' => 64, 'tooLong' => 'Неверное значение пароля'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'phone' => 'Номер телефона',
            'password' => 'Пароль',
            'personal_type' => 'Тип персонала',
            'customer_phone' => 'Номер телефона заказчика',
            'customer_user_id' => 'Id заказчика'
        ];
    }

    public function create()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->phone = $user->normalizePhone($this->phone);

        $password = $this->password;
        $hash_password = hash('sha256', $password);
        $user->setPassword($hash_password);
        $user->generateAuthKey();
        $user->setAccessToken($this->phone);
        $user->customer_id = $this->customer_user_id;

        if($user->save()){
            $password_set_enc = new PersonalPassword();
            $password_set_enc->user_id = $user->id;
            $password_set_enc->password_enc = EncryptionHelpers::dec_enc('encrypt', $password);

            if(!$password_set_enc->save()){
                $user->delete();
                throw new HttpException(500, 'Ошибка сервера');
            }

            switch ($this->personal_type){
                case $this->personal_type == $this::MANAGER:
                    $userRole = Yii::$app->authManager->getRole('manager');
                    break;
                case $this->personal_type == $this::ADMINISTRATOR:
                    $userRole = Yii::$app->authManager->getRole('administrator');
                    break;
                default:
                    $password_set_enc->delete();
                    $user->delete();
                    throw new HttpException(400, 'Неверная роль');
            }
            $create_role = Yii::$app->authManager->assign($userRole, $user->id);
            if (!$create_role){
                $password_set_enc->delete();
                $user->delete();
                throw new HttpException(400, 'Не удалось создать роль');
            }

            $token_code = Token::getRandCode();
            $update_token_customer = Token::updateAll(['code' => $token_code,
                'created_at' => Token::getCratedAt(), 'type' =>
                    $this->personal_type == $this::MANAGER ? Token::TYPE_CREATE_MANAGER :
                        Token::TYPE_CREATE_ADMINISTRATOR], ['user_id' => $this->customer_user_id]);

            $token = new Token();
            $token->user_id = $user->id;
            $token->code = Token::getRandCode();
            $token->type = Token::TYPE_CONFIRMATION;

            if(!$update_token_customer || !$token->save()){
                $password_set_enc->delete();
                $user->delete();
                throw new HttpException(400, 'Не удалось создать токен');
            }

            $send_token = Sending::sendCode($this->customer_phone, $token_code);

            if(!$send_token){
                $password_set_enc->delete();
                $user->delete();
                throw new HttpException(421, 'Произошла ошибка отправки сообщения');
            }

            return true;
        }
        $this->addErrors($user->getErrors());
        return null;
    }
}