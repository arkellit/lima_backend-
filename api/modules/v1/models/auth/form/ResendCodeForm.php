<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 28.01.19
 * Time: 18:40
 */

namespace api\modules\v1\models\auth\form;

use api\modules\v1\models\auth\User;
use common\helpers\DifferenceTimeHelpers;
use yii\base\Model;
use yii\web\HttpException;

/**
 * @property string $phone
 * @property object $_user
 *
 * Class ResendCodeForm
 * @package api\modules\v1\models\auth\form
 */
class ResendCodeForm extends Model
{
    public $phone;
    public $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['phone', 'required'],
            ['phone', 'validateSendNewToken']
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->phone = User::normalizePhone($this->phone);
            return true;
        }
        return false;
    }

    /**
     * Validate function
     *
     * @inheritdoc
     */
    public function validateSendNewToken(){
        $user = User::findOne(['phone' => $this->phone]);
        if (!$user){
            throw new HttpException(400, 'Пользователь не найден');
        }
        $this->_user = $user;
        switch ($user){
            case $user->is_confirm == User::CONFIRM_PHONE:
                throw new HttpException(418, 'Номер уже подтвержден');
                break;
            case DifferenceTimeHelpers::differenceTime($user->code->updated_at) < 120:
                throw new HttpException(423, 'Отправка cмс разрешена раз в 2 мин');
                break;
            default:
                break;
        }
    }
}