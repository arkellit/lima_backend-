<?php

namespace api\modules\v1\models\auth\form;

use api\modules\v1\models\auth\User;
use common\helpers\DifferenceTimeHelpers;
use \yii\base\Model;
use yii\web\HttpException;

/**
 * @property string $phone
 * @property integer $code
 * @property object $_user
 *
 * Class ConfirmPhoneForm
 * @package api\modules\v1\models\auth\form
 */
class ConfirmPhoneForm extends Model
{
    public $phone;
    public $code;

    public $_user;

    public function rules()
    {
        return [
            [['phone', 'code'], 'required'],
            [['phone', 'code'], 'validateUserData'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->phone = User::normalizePhone($this->phone);
            return true;
        }
        return false;
    }

    /**
     * Validate function
     *
     * @throws HttpException
     */
    public function validateUserData()
    {
        $user = User::findOne(['phone' => $this->phone]);
        if (!$user){
            throw new HttpException(400, 'Пользователь не найден');
        }
        $this->_user = $user;
        switch ($user){
            case $user->is_confirm == User::CONFIRM_PHONE:
                throw new HttpException(418, 'Номер уже подтвержден');
                break;
            case $user->code->code != $this->code:
                throw new HttpException(400, 'Неверный код подтверждения');
                break;
            case DifferenceTimeHelpers::differenceTime($user->code->updated_at) > 300:
                throw new HttpException(423, 'Срок действия кода истек');
                break;
        }
    }
}