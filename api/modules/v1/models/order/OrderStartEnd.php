<?php

namespace api\modules\v1\models\order;

use api\modules\v1\models\customer\CustomerNotice;
use api\modules\v1\models\customer\CustomerPersonal;
use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%order_start_end}}".
 *
 * @property int $id
 * @property int $order_id
 * @property int $is_start_job
 * @property int $is_end_job
 * @property int $is_finished_job
 * @property int $start_order_type
 * @property int $is_cancel_order
 *
 * @property Order $order
 * @property OrderRequest $orderRequest
 * @property OrderPlace $orderPlace
 */
class OrderStartEnd extends ActiveRecord
{
    const ORDER_NORM_START = 0;
    const ORDER_BAD_START = 1;

    const IS_CANCEL_WORKER = 1;
    const NO_CANCEL_WORKER = 0;

    const EVENT_SET_START_ORDER = 'order-notice-start';
    const EVENT_SET_END_ORDER = 'order-notice-end';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order_start_end}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['is_start_job', 'is_end_job', 'is_finished_job', 'start_order_type'], 'default', 'value' => 0],
            [['order_id', 'is_start_job', 'is_end_job', 'is_finished_job'], 'integer'],
            [['order_id'], 'unique'],
            ['start_order_type', 'in', 'range' => [self::ORDER_NORM_START, self::ORDER_BAD_START]],
            ['is_cancel_order', 'in', 'range' => [self::IS_CANCEL_WORKER, self::NO_CANCEL_WORKER]],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->on(self::EVENT_SET_START_ORDER, [$this, 'setOrderStartNotice']);
        $this->on(self::EVENT_SET_END_ORDER, [$this, 'setOrderEndNotice']);
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function setOrderStartNotice(){
        $order = $this->order;
        $personal_id =  $this->orderPlace->personal_id;
        $customer_personal = CustomerPersonal::find()
            ->where(['id' => $personal_id])
            ->andWhere(['role' => 'supervisor'])
            ->asArray()
            ->one();
        $customer_notice = new CustomerNotice();
        $customer_notice->customer_id = $order->customer_id;
        $customer_notice->order_id = $this->order_id;
        $customer_notice->order_place_id = $order->orderPlace->id;
        $customer_notice->category_notice = CustomerNotice::ORDER_START;
        $customer_notice->save();
        if ($order->administrator_id != 0){
            $customer_notice = new CustomerNotice();
            $customer_notice->customer_id = $order->customer_id;
            $customer_notice->personal_role = 'administrator';
            $customer_notice->customer_personal_id = $order->administrator_id;
            $customer_notice->order_id = $this->order_id;
            $customer_notice->order_place_id = $order->orderPlace->id;
            $customer_notice->category_notice = CustomerNotice::ORDER_START;
            $customer_notice->save();
        }
        if (!empty($customer_personal)){
            $customer_notice = new CustomerNotice();
            $customer_notice->customer_id = $order->customer_id;
            $customer_notice->personal_role = 'administrator';
            $customer_notice->customer_personal_id = $personal_id;
            $customer_notice->order_id = $this->order_id;
            $customer_notice->order_place_id = $order->orderPlace->id;
            $customer_notice->category_notice = CustomerNotice::ORDER_START;
            $customer_notice->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function setOrderEndNotice(){
        $order = $this->order;
        $personal_id =  $this->orderPlace->personal_id;
        $customer_personal = CustomerPersonal::find()
            ->where(['id' => $personal_id])
            ->andWhere(['role' => 'supervisor'])
            ->asArray()
            ->one();
        $customer_notice = new CustomerNotice();
        $customer_notice->customer_id = $order->customer_id;
        $customer_notice->order_id = $this->order_id;
        $customer_notice->order_place_id = $order->orderPlace->id;
        $customer_notice->category_notice = CustomerNotice::ORDER_END;
        $customer_notice->save();
        if ($order->administrator_id != 0){
            $customer_notice = new CustomerNotice();
            $customer_notice->customer_id = $order->customer_id;
            $customer_notice->personal_role = 'administrator';
            $customer_notice->customer_personal_id = $order->administrator_id;
            $customer_notice->order_id = $this->order_id;
            $customer_notice->order_place_id = $order->orderPlace->id;
            $customer_notice->category_notice = CustomerNotice::ORDER_END;
            $customer_notice->save();
        }
        if (!empty($customer_personal)){
            $customer_notice = new CustomerNotice();
            $customer_notice->customer_id = $order->customer_id;
            $customer_notice->personal_role = 'supervisor';
            $customer_notice->customer_personal_id = $personal_id;
            $customer_notice->order_id = $this->order_id;
            $customer_notice->order_place_id = $order->orderPlace->id;
            $customer_notice->category_notice = CustomerNotice::ORDER_END;
            $customer_notice->save();
        }
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'is_start_job' => 'Началась работа',
            'is_end_job' => 'Окончилась работа',
            'is_finished_job' => 'Завершилось выполнение заказа',
            'start_order_type' => 'Норм ли начался заказ',
            'is_cancel_order' => 'Отменен пользователем'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderRequest(){
        return $this->hasOne(OrderRequest::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPlace(){
        return $this->hasOne(OrderPlace::className(), ['order_id' => 'order_id']);
    }
}
