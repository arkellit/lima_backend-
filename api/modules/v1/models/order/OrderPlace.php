<?php

namespace api\modules\v1\models\order;

use api\modules\v1\models\customer\CustomerNotice;
use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\worker\CategoryWorker;
use api\modules\v1\models\worker\WorkerProfession;
use common\helpers\CustomerHelpers;
use \yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%order_place}}".
 *
 * @property int $id
 * @property int $order_id
 * @property int $order_address_id
 * @property int $worker_category_id
 * @property int $type_job
 * @property int $date_time_start
 * @property int $date_time_end
 * @property double $count_hour
 * @property int $count_worker
 * @property string $comment
 * @property int $personal_id
 * @property double $order_price
 * @property int $is_complete_worker
 * @property int $is_complete_order
 * @property int $order_print_id
 * @property int $is_cancel
 *
 * @property integer $is_print_flyers
 *
 * @property Order $order
 * @property OrderAddress $orderAddress
 * @property WorkerProfession $workerCategory
 * @property OrderRequest $orderRequest
 */
class OrderPlace extends ActiveRecord
{
    const TYPE_JOB_STREET = 1;
    const TYPE_JOB_ROOM = 2;

    const IS_PRINTING_FLAYER = 1;
    const NO_PRINTING_FLAYER = 0;

    public $is_print_flyers;

    const EVENT_SET_NOTICE = 'new-notice';
    const EVENT_SET_SUPERVISOR_NOTICE = 'new-set-supervisor-notice';

    const CANCEL_TRUE = 1;
    const CANCEL_FALSE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order_place}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'order_address_id', /*'worker_category_id',*/
                /*'type_job',*/ 'date_time_start',
                'date_time_end', 'count_hour', 'count_worker', 'order_price'], 'required'],
           // [['order_id', 'order_address_id', 'worker_category_id', 'type_job', 'date_time_start', 'date_time_end', 'count_worker', 'personal_id', 'is_complete_worker', 'is_complete_order'], 'default', 'value' => null],
            'typeInteger' => [['order_id', 'order_address_id', 'worker_category_id', 'type_job', 'date_time_start', 'date_time_end', 'count_worker', 'personal_id', 'is_complete_worker', 'is_complete_order'], 'integer'],
            [['count_hour', 'order_price'], 'number'],
            [['comment'], 'string', 'max' => 500],
            ['is_cancel', 'default', 'value' => self::CANCEL_FALSE],
            ['is_cancel', 'in', 'range' => [self::CANCEL_FALSE, self::CANCEL_TRUE]],
            ['order_print_id', 'default', 'value' => self::NO_PRINTING_FLAYER],
            'typeJob' => ['type_job', 'in', 'range' => [self::TYPE_JOB_STREET, self::TYPE_JOB_ROOM]],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['order_address_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderAddress::className(), 'targetAttribute' => ['order_address_id' => 'id']],
            [['worker_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoryWorker::className(), 'targetAttribute' => ['worker_category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'order_address_id' => 'Адрес проведения',
            'worker_category_id' => 'Категория работника',
            'type_job' => 'Тип работы(на улице/в помещении)',
            'date_time_start' => 'Дата и время начала',
            'date_time_end' => 'Дата и время окончания',
            'count_hour' => 'Количество часов',
            'count_worker' => 'Количество работников',
            'comment' => 'Комментарий',
            'personal_id' => 'Супервайзер',
            'order_price' => 'Цена заказа',
            'is_complete_worker' => 'Исполнители набраны',
            'is_complete_order' => 'Заказ выполнен',
            'is_cancel' => 'Отмене'
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->on(self::EVENT_SET_NOTICE, [$this, 'setNewNotice']);
        $this->on(self::EVENT_SET_SUPERVISOR_NOTICE, [$this, 'setNoticeSupervisor']);
        parent::init();
    }

    /**
     * Trigger
     */
    public function setNewNotice(){
        $user_id = Yii::$app->user->getId();
        $user = CustomerHelpers::getCustomerIdAndRole($user_id);
        if ($user['role'] == 'customer'){
            $customer_notice = new CustomerNotice();
            $customer_notice->customer_id = $user['id'];
            $customer_notice->order_id = $this->order_id;
            $customer_notice->order_place_id = $this->id;
            $customer_notice->category_notice = CustomerNotice::ORDER_CREATE;
            $customer_notice->save();
        }
        else {
            $customer_notice = new CustomerNotice();
            $customer_notice->customer_id = $user['id'];
            $customer_notice->customer_personal_id = $user['personal_id'];
            $customer_notice->personal_role = $user['role'];
            $customer_notice->order_id = $this->order_id;
            $customer_notice->order_place_id = $this->id;
            $customer_notice->category_notice = CustomerNotice::ORDER_CREATE;
            $customer_notice->save();
        }
    }

    public function setNoticeSupervisor(){
        $customer_personal = CustomerPersonal::find()
            ->where(['id' => $this->personal_id])
            ->andWhere(['role' => 'supervisor'])
            ->asArray()
            ->one();
        if ($this->personal_id != 0 && !empty($customer_personal)){
            $user_id = $this->order->customer_id;
            $customer_notice = new CustomerNotice();
            $customer_notice->customer_id = $user_id;
            $customer_notice->order_id = $this->order_id;
            $customer_notice->customer_personal_id = $this->personal_id;
            $customer_notice->personal_role = 'supervisor';
            $customer_notice->order_place_id = $this->id;
            $customer_notice->category_notice = CustomerNotice::SUPERVISOR_ORDER_ASSIGN;
            $customer_notice->save();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderAddress()
    {
        return $this->hasOne(OrderAddress::className(), ['id' => 'order_address_id'])
            ->joinWith('city');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerCategory()
    {
        return $this->hasOne(CategoryWorker::className(), ['id' => 'worker_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderRequest()
    {
        return $this->hasOne(OrderRequest::className(), ['order_id' => 'order_id',
            'order_place_id' => 'id']);
    }

    public function getOrderProfession() {
        return $this->hasMany(OrderProfession::className(), ['order_place_id' => 'id']);
    }
}
