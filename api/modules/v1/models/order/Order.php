<?php

namespace api\modules\v1\models\order;

use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerPersonal;
use Yii;
use yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $administrator_id
 * @property string $name_order
 * @property string $number_order
 * @property double $total_price
 * @property int $type_order
 * @property int $is_pay
 * @property int $is_complete_order
 * @property int $is_published
 *
 * @property Customer $customer
 * @property OrderPlace[] $orderPlace
 * @property OrderRequest[] $orderRequest
 * @property CustomerPersonal $administrator

 * @property OrderPlace[] $orderPlaces
 */
class Order extends ActiveRecord
{
    const TYPE_CREATE = 0;
    const TYPE_LIMA_EXCHANGE = 1;
    const TYPE_MANUAL_SELECTION = 2;

    const COMPLETE_ORDER = 1;
    const NO_COMPLETE_ORDER = 0;

    const IS_PAY = 1;
    const NO_PAY = 0;

    const IS_PUBLISHED = 1;
    const NO_PUBLISHED = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'name_order'], 'required'],
            [['is_pay', 'is_complete_order', 'type_order', 'administrator_id'], 'default', 'value' => 0],
            [['customer_id', 'is_pay', 'is_complete_order', 'type_order', 'is_published'], 'integer'],
            [['total_price'], 'number'],
            [['name_order'], 'string', 'max' => 255],
            [['number_order'], 'string', 'max' => 50],
            ['is_pay', 'in', 'range' => [self::IS_PAY, self::NO_PAY]],
            ['is_complete_order', 'in', 'range' => [self::COMPLETE_ORDER, self::NO_COMPLETE_ORDER]],
            ['type_order', 'in', 'range' => [self::TYPE_CREATE, self::TYPE_LIMA_EXCHANGE, self::TYPE_MANUAL_SELECTION]],
            ['type_order', 'default', 'value' => self::TYPE_LIMA_EXCHANGE],
            ['is_published','in', 'range' => [self::IS_PUBLISHED, self::NO_PUBLISHED]],
            ['is_published', 'default', 'value' => self::IS_PUBLISHED],
            [['number_order'], 'unique'],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp'  => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Заказчик',
            'administrator_id' => 'Администратор',
            'name_order' => 'Название заказа',
            'number_order' => 'Номер заказа',
            'type_order' => 'Размещение заказа',
            'total_price' => 'Цена',
            'is_pay' => 'Оплачен',
            'is_complete_order' => 'Заказ выполнен',
            'is_published' => 'Заказ опубликован'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPlace()
    {
        return $this->hasOne(OrderPlace::className(), ['order_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdministrator()
    {
        return $this->hasOne(CustomerPersonal::className(), ['customer_id' => 'administrator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderRequest()
    {
        return $this->hasOne(OrderRequest::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPlaces()
    {
        return $this->hasMany(OrderPlace::className(), ['order_id' => 'id']);
    }

    public function getAddress()
    {
        return $this->hasOne(OrderPlace::className(), ['order_id' => 'id'])
            ->viaTable('order_address', ['id' => 'order_place.order_address_id']);
    }
}
