<?php

namespace api\modules\v1\models\order;

use api\modules\v1\models\order\Order;
use api\modules\v1\models\order\OrderPlace;
use api\modules\v1\models\worker\Worker;
use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%order_worker}}".
 *
 * @property int $order_id
 * @property int $worker_id
 * @property int $order_place_id
 * @property int $status
 * @property int $attention
 *
 * @property Order $order
 * @property OrderPlace $orderPlace
 * @property Worker $worker
 */
class OrderWorker extends ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_START_JOB = 1;
    const STATUS_END_JOB = 2;
    const STATUS_WORKER_CANCEL = 3;

    // Норм отработал
    const ATTENTION_NORM = 0;
    // Опоздал на работу меньше 10 мин
    const ATTENTION_BEING_LATE_LESS_10 = 1;
    // Опоздал больше 10 мин
    const ATTENTION_BEING_LATE_MORE_10 = 2;
    // Совсем не пришел
    const ATTENTION_DID_COME_WORK = 3;
    // Форс-мажор
    const ATTENTION_BLOCK = 4;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order_worker}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'worker_id', 'order_place_id'], 'required'],
            [['order_id', 'worker_id', 'status', 'attention'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_NEW],
            ['status', 'in', 'range' => [self::STATUS_NEW, self::STATUS_START_JOB, self::STATUS_END_JOB,
                self::STATUS_WORKER_CANCEL]],
            ['attention', 'in', 'range' => [
                self::ATTENTION_NORM,
                self::ATTENTION_BEING_LATE_LESS_10,
                self::ATTENTION_BEING_LATE_MORE_10,
                self::ATTENTION_DID_COME_WORK,
                self::ATTENTION_BLOCK]
            ],
//            ['attention', 'default', 'value' => self::ATTENTION_DID_COME_WORK],
            [['order_id', 'worker_id'], 'unique', 'targetAttribute' => ['order_id', 'worker_id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['worker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Worker::className(), 'targetAttribute' => ['worker_id' => 'id']],
            [['order_place_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderPlace::className(), 'targetAttribute' => ['order_place_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'worker_id' => 'Worker ID',
            'order_place_id' => 'Order Place ID',
            'status' => 'Статус',
            'attention' => 'Как отработал'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPlace()
    {
        return $this->hasOne(OrderPlace::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['id' => 'worker_id']);
    }
}
