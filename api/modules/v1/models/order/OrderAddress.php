<?php

namespace api\modules\v1\models\order;

use api\modules\v1\models\City;
use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerBranch;
use nanson\postgis\behaviors\GeometryBehavior;
use \yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%order_address}}".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $city_id
 * @property int $branch_id
 * @property string $place_order
 * @property string $address_order
 * @property string $metro_station
 * @property string $landmark
 * @property double $lat
 * @property double $lng
 *
 * @property string $point
 *
 * @property City $city
 * @property Customer $customer
 * @property CustomerBranch $branch
 * @property OrderPlace[] $orderPlaces
 */
class OrderAddress extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order_address}}';
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->point = [$this->lat, $this->lng];
            return true;
        }
        return false;
    }

    public function behaviors()
    {
        return [
            [
                'class' => GeometryBehavior::className(),
                'type' => GeometryBehavior::GEOMETRY_POINT,
                'attribute' => 'point',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'city_id', 'place_order', 'address_order', 'lat', 'lng', 'point'], 'required'],
            [['branch_id'], 'default', 'value' => 0],
            [['customer_id', 'city_id', 'branch_id'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['place_order', 'address_order', 'metro_station', 'landmark'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
//            [['branch_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerBranch::className(), 'targetAttribute' => ['branch_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Заказчик',
            'city_id' => 'Город',
            'branch_id' => 'Филиал',
            'place_order' => 'Место выполнения заказа',
            'address_order' => 'Адрес работы',
            'metro_station' => 'Ближайшая станция метро',
            'landmark' => 'Ориентир на местности',
            'lat' => 'Широта',
            'lng' => 'Долгота',
            'point' => 'Координаты PostGis'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranch()
    {
        return $this->hasOne(CustomerBranch::className(), ['id' => 'branch_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPlaces()
    {
        return $this->hasMany(OrderPlace::className(), ['order_address_id' => 'id']);
    }
}
