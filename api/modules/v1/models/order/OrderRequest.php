<?php

namespace api\modules\v1\models\order;

use api\modules\v1\models\customer\CustomerNotice;
use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\worker\ImageWorker;
use api\modules\v1\models\worker\Worker;
use Yii;
use yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;
use yii\web\HttpException;

/**
 * This is the model class for table "{{%order_request}}".
 *
 * @property int $id
 * @property int $worker_id
 * @property int $order_id
 * @property int $order_place_id
 * @property int $is_lima_exchange
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $is_show_in_filter
 *
 * @property Order $order
 * @property OrderPlace $orderPlace
 * @property Worker $worker
 * @property OrderStartEnd $orderStartEnd
 */
class OrderRequest extends ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_ACCEPT = 1;
    const STATUS_NO_ACCEPT = 2;
    const STATUS_WORKER_CANCEL = 3;

    const IS_LIMA_EXCHANGE = 1;
    const NO_LIMA_EXCHANGE = 0;

    const SHOW_IN_FILTER = 1;
    const NO_SHOW_IN_FILTER = 0;

    const EVENT_SET_NEW_REQUEST_NOTICE = 'set-new-request-notice';
    const EVENT_ACCEPT_REQUEST_NOTICE = 'accept-request-notice';
//    const EVENT_REJECT_REQUEST_NOTICE = 'reject-request-notice';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order_request}}';
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->on(self::EVENT_SET_NEW_REQUEST_NOTICE, [$this, 'setNewRequestNotice']);
        $this->on(self::EVENT_ACCEPT_REQUEST_NOTICE, [$this, 'acceptRequestNotice']);
//        $this->on(self::EVENT_REJECT_REQUEST_NOTICE, [$this, 'rejectRequestNotice']);
        parent::init();
    }

    public function setNewRequestNotice(){
        $order = $this->order;
        $personal_id =  $this->orderPlace->personal_id;
        $customer_personal = CustomerPersonal::find()
            ->where(['id' => $personal_id])
            ->andWhere(['role' => 'administrator'])
            ->asArray()
            ->one();
        $customer_notice = new CustomerNotice();
        $customer_notice->customer_id = $order->customer_id;
        $customer_notice->order_id = $this->order_id;
        $customer_notice->order_place_id = $order->orderPlace->id;
        $customer_notice->worker_id = $this->worker_id;
        $customer_notice->category_notice = CustomerNotice::NEW_REQUEST;
        $customer_notice->save();
        if (!empty($customer_personal)){
            $customer_notice = new CustomerNotice();
            $customer_notice->customer_id = $order->customer_id;
            $customer_notice->personal_role = 'administrator';
            $customer_notice->customer_personal_id = $personal_id;
            $customer_notice->order_id = $this->order_id;
            $customer_notice->worker_id = $this->worker_id;
            $customer_notice->order_place_id = $order->orderPlace->id;
            $customer_notice->category_notice = CustomerNotice::NEW_REQUEST;
            $customer_notice->save();
        }
    }
    public function acceptRequestNotice(){
        $order = $this->order;
        $personal_id =  $this->orderPlace->personal_id;
        $customer_personal = CustomerPersonal::find()
            ->where(['id' => $personal_id])
            ->andWhere(['role' => 'administrator'])
            ->asArray()
            ->one();
        $customer_notice = new CustomerNotice();
        $customer_notice->customer_id = $order->customer_id;
        $customer_notice->order_id = $this->order_id;
        $customer_notice->order_place_id = $order->orderPlace->id;
        $customer_notice->worker_id = $this->worker_id;
        $customer_notice->category_notice = CustomerNotice::ACCEPT_REQUEST;
        $customer_notice->is_delete = 1;
        $customer_notice->save();
        if (!empty($customer_personal)){
            $customer_notice = new CustomerNotice();
            $customer_notice->customer_id = $order->customer_id;
            $customer_notice->personal_role = 'administrator';
            $customer_notice->customer_personal_id = $personal_id;
            $customer_notice->worker_id = $this->worker_id;
            $customer_notice->order_id = $this->order_id;
            $customer_notice->order_place_id = $order->orderPlace->id;
            $customer_notice->is_delete = 1;
            $customer_notice->category_notice = CustomerNotice::ACCEPT_REQUEST;
            $customer_notice->save();
        }

        $customer_notice_new_request = CustomerNotice::findOne([
                'customer_id' => $order->customer_id,
                'order_id' => $this->order_id,
                'worker_id' => $this->worker_id,
                'category_notice' => CustomerNotice::NEW_REQUEST
            ]);
        if (!empty($customer_notice_new_request)){
            $customer_notice_new_request->is_delete = 1;
            $customer_notice_new_request->save();
        }
    }
//    public function rejectRequestNotice(){
//        $order = $this->order;
//        $personal_id =  $this->orderPlace->personal_id;
//        $customer_personal = CustomerPersonal::find()
//            ->where(['id' => $personal_id])
//            ->andWhere(['role' => 'administrator'])
//            ->asArray()
//            ->one();
//        $customer_notice = new CustomerNotice();
//        $customer_notice->customer_id = $order->customer_id;
//        $customer_notice->order_id = $this->order_id;
//        $customer_notice->order_place_id = $order->orderPlace->id;
//        $customer_notice->worker_id = $this->worker_id;
//        $customer_notice->category_notice = CustomerNotice::REJECT_REQUEST;
//        $customer_notice->is_delete = 1;
//        $customer_notice->save();
//        if (!empty($customer_personal)){
//            $customer_notice = new CustomerNotice();
//            $customer_notice->customer_id = $order->customer_id;
//            $customer_notice->personal_role = 'administrator';
//            $customer_notice->customer_personal_id = $personal_id;
//            $customer_notice->order_id = $this->order_id;
//            $customer_notice->order_place_id = $order->orderPlace->id;
//            $customer_notice->worker_id = $this->worker_id;
//            $customer_notice->category_notice = CustomerNotice::REJECT_REQUEST;
//            $customer_notice->is_delete = 1;
//            $customer_notice->save();
//        }
//    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['worker_id', 'order_id', 'order_place_id'], 'required'],
            [['is_lima_exchange', 'status'], 'default', 'value' => self::NO_LIMA_EXCHANGE],
            ['is_show_in_filter', 'default', 'value' => self::SHOW_IN_FILTER],
            ['is_show_in_filter', 'in', 'range' => [self::SHOW_IN_FILTER,
                self::NO_SHOW_IN_FILTER]],
            ['status', 'in', 'range' => [self::STATUS_NEW, self::STATUS_ACCEPT,
                self::STATUS_NO_ACCEPT, self::STATUS_WORKER_CANCEL]],
            ['is_lima_exchange', 'in', 'range' => [self::IS_LIMA_EXCHANGE, self::NO_LIMA_EXCHANGE]],
            [['worker_id', 'order_id', 'order_place_id', 'status',
                'created_at', 'updated_at'], 'integer'],
            [['worker_id', 'order_id'], 'unique', 'targetAttribute' => ['worker_id', 'order_id'], 'message' => 'Заявка для данного пользователя существует'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['order_place_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderPlace::className(), 'targetAttribute' => ['order_place_id' => 'id']],
            [['worker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Worker::className(), 'targetAttribute' => ['worker_id' => 'id']],
            ['order_id', 'validateOrder']
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function validateOrder(){
        $order = Order::find()
            ->where(['id' => $this->order_id])->asArray()->one();
        if ((int)$order['is_complete_order'] == 1 || (int)$order['is_pay'] == Order::NO_PAY){
            throw new HttpException(400, 'Заказ уже завершен или заказ не оплачен');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'worker_id' => 'ID исполнителя',
            'order_id' => 'ID заказа',
            'order_place_id' => 'ID места проведения',
            'is_lima_exchange' => 'Лима биржа',
            'status' => 'Статус',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'is_show_in_filter' => 'Показывать в фильтре'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPlace()
    {
        return $this->hasOne(OrderPlace::className(), ['id' => 'order_place_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerImage()
    {
        return $this->hasOne(ImageWorker::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStartEnd(){
        return $this->hasOne(OrderStartEnd::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProfession(){
        return $this->hasMany(OrderProfession::className(), ['order_id' => 'order_id']);
    }
}
