<?php

namespace api\modules\v1\models\order;

use api\modules\v1\models\worker\CategoryWorker;
use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%order_profession}}".
 *
 * @property int $id
 * @property int $order_id
 * @property int $order_place_id
 * @property int $worker_category_id
 * @property int $count_worker
 * @property int $type_job
 *
 * @property Order $order
 * @property OrderPlace $orderPlace
 * @property CategoryWorker $workerCategory
 */
class OrderProfession extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order_profession}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'order_place_id', 'worker_category_id', 'count_worker',
                'type_job'], 'required'],
            [['order_id', 'order_place_id', 'worker_category_id', 'count_worker',
                'type_job'], 'integer'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['order_place_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderPlace::className(), 'targetAttribute' => ['order_place_id' => 'id']],
            [['worker_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoryWorker::className(), 'targetAttribute' => ['worker_category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'order_place_id' => 'Заказ',
            'worker_category_id' => 'Профессия работника',
            'count_worker' => 'Количество исполнителей',
            'type_job' => 'Тип работника'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPlace()
    {
        return $this->hasOne(OrderPlace::className(), ['id' => 'order_place_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerCategory()
    {
        return $this->hasOne(CategoryWorker::className(), ['id' => 'worker_category_id']);
    }
}
