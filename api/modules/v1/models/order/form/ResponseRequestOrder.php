<?php
namespace api\modules\v1\models\order\form;

use \yii\base\Model;

/**
 * @property integer $id_request
 * @property integer $status
 *
 * Class ResponseRequestOrder
 * @package api\modules\v1\models\order\form
 */
class ResponseRequestOrder extends Model
{
    public $worker_id;
    public $order_id;
    public $status;

    const STATUS_ACCEPT = 1;
    const STATUS_NO_ACCEPT = 2;

    public function rules()
    {
        return [
            [['worker_id', 'order_id', 'status'], 'required'],
            ['status', 'in', 'range' => [self::STATUS_ACCEPT, self::STATUS_NO_ACCEPT]],
        ];
    }
}