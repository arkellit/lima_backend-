<?php

namespace api\modules\v1\models;

use Yii;
use yii\base\Model;

/**
 * Class Upload
 * @package app\modules\v1\models
 */
class Upload extends Model
{
    public $image;

    const SCENARIO_IMAGE_FILE = 1;
    const SCENARIO_IMAGE_STRING = 2;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'], 'string', 'on' => self::SCENARIO_IMAGE_STRING],
            [['image'], 'image', 'skipOnEmpty' => true, 'extensions' => 'png,jpg,jpeg,svg',
                'maxSize' => 1024 * 1024 * 7,
                'tooBig' => 'File is too big', 'on' => self::SCENARIO_IMAGE_FILE],
        ];
    }

    /**
     * Function upload
     *
     * @param $type
     * @param $img_base64
     * @return null|string
     */
    public function upload($type, $img_base64 = null)
    {
        if(!$this->validate()){
            return null;
        }
        // Select dir
        switch ($type){
            case 'customer':
                $file_path = '@image/web';
                $dir = '/img/customer/';
                break;

            case 'worker':
                $file_path = '@image/web';
                $dir = '/img/worker/';
                break;

            case 'temp_worker':
                $file_path = '@image/web';
                $dir = '/img/temp_worker/';
                break;

            case 'secret_passport':
                $file_path = '@image';
                $dir = '/data/passport/';
                break;

            case 'secret_change_passport':
                $file_path = '@image';
                $dir = '/data/passport_change/';
                break;

            case 'secret_photo_doc':
                $file_path = '@image';
                $dir = '/data/photo_doc/';
                break;

            case 'news':
                $file_path = '@image/web';
                $dir = '/img/news/';
                break;

            case 'all-news':
                $file_path = '@image/web';
                $dir = '/img/all-news/';
                break;

            case 'dialog':
                $file_path = '@image/web';
                $dir = '/img/dialog/';
                break;

            case 'feedback':
                $file_path = '@image/web';
                $dir = '/img/feedback/';
                break;

            case 'news_front':
                $file_path = '@image/web';
                $dir = '/img/news_front/';
                break;

            default:
                return null;
        }

        if(!is_null($img_base64)){
            $rand_name = md5(microtime() . rand(0, 9999));
            $file = $dir . $rand_name . '.'. 'jpg';
            $img =   base64_decode($img_base64);
            $create_file = file_put_contents(Yii::getAlias($file_path). $file, $img);
            return $create_file != false ? $file : null;
        }
        else {
            $rand_name = md5(microtime() . rand(0, 9999));
            $file = $dir . $rand_name . '.'. $this->image->extension;
            $this->image->saveAs(Yii::getAlias($file_path . $file));
            return $file;
        }
    }
}