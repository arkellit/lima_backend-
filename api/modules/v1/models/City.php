<?php

namespace api\modules\v1\models;

use api\modules\v1\models\worker\CategoryWorker;
use api\modules\v1\models\worker\CategoryWorkerCity;
use api\modules\v1\models\worker\Worker;
use \yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%city}}".
 *
 * @property int $id
 * @property int $region_id
 * @property string $name
 *
 * @property CategoryWorkerCity[] $categoryWorkerCities
 * @property CategoryWorker[] $categoryWorkers
 * @property Worker[] $workers
 * @property Region[] $region
 */
class City extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%city}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'region_id'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название города',
            'region_id' => 'Регион',
            'lat' => 'Широта',
            'lng' => 'Долгота'
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryWorkerCities()
    {
        return $this->hasMany(CategoryWorkerCity::className(), ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryWorkers()
    {
        return $this->hasMany(CategoryWorker::className(), ['id' => 'category_worker_id'])->viaTable('{{%category_worker_city}}', ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkers()
    {
        return $this->hasMany(Worker::className(), ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }
}
