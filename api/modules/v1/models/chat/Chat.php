<?php

namespace api\modules\v1\models\chat;

use api\modules\v1\models\auth\User;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class Chat
 *
 * @property int $_id
 * @property int $user_from_id
 * @property int $user_to_id
 * @property int $status
 * @property int $created_at
 *
 * @property string $message
 * @property string $attachment
 *
 * @property User $user
 * @package api\modules\v1\models\chat
 */
class Chat extends ActiveRecord
{
    const READ = 1;
    const NO_READ = 0;

    /**
     * @return string the name of the index associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return '{{%chat}}';
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp'  => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['user_from_id', 'user_to_id'], 'required'],
            [['message', 'attachment'], 'validateTextOrImage', 'skipOnEmpty'=> false],
            [['user_from_id', 'user_to_id', 'status', 'created_at'], 'integer'],
            [['attachment'], 'string', 'max' => 255],
            ['message', 'string', 'max' => 1000],
            ['status', 'default', 'value' => self::NO_READ],
            ['status', 'in', 'range' => [self::READ, self::NO_READ]],
            [['user_from_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_from_id' => 'id']],
            [['user_to_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_to_id' => 'id']],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->user_to_id = (int)$this->user_to_id;
            $this->user_from_id = (int)$this->user_from_id;
            return parent::beforeSave($insert);
        } else {
            return false;
        }
    }

    public function validateTextOrImage(){
        if(empty($this->message) && empty($this->attachment))
        {
            $error_message = 'Прикрепите фотографию или напишите сообщение';
            $this->addError('message', $error_message);
            $this->addError('attachment', $error_message);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_from_id' => 'Id отправителя',
            'user_to_id' => 'Id получателя',
            'message' => 'Текст сообщения',
            'attachment' => 'Прикрепленные файлы',
            'status' => 'Статус',
            'created_at' => 'Дата создания',
        ];
    }

    public function getCreatedAt()
    {
        return date('Y-m-d H:i', $this->created_at);
    }

    public static function getDialog($user_from, $user_to){
       return Chat::find()
            ->where(['AND', ['user_from_id' => $user_from], ['user_to_id' => $user_to]])
            ->orWhere(['AND', ['user_to_id' => $user_from], ['user_from_id' => $user_to]])
            ->orderBy(['id' => SORT_ASC])
            ->all();
    }

    public static function getLastMessage($user_from, $user_to){
        return Chat::find()
            ->where(['AND', ['user_from_id' => $user_from], ['user_to_id' => $user_to]])
            ->orWhere(['AND', ['user_to_id' => $user_from], ['user_from_id' => $user_to]])
            ->orderBy(['id' => SORT_DESC])
            ->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser_From()
    {
        return $this->hasOne(User::className(), ['id' => 'user_from_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser_To()
    {
        return $this->hasOne(User::className(), ['id' => 'user_to_id']);
    }
}