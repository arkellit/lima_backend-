<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 25.10.18
 * Time: 14:52
 */

namespace api\modules\v1\models\chat;


use yii\base\Model;

/**
 * @property string $text
 * @property string $image
 *
 * @property integer $user_id
 *
 * Class MessageForm
 * @package api\modules\v1\models\chat
 */
class MessageForm extends Model
{
    public $text;
    public $user_id;
    public $image;

    public function rules()
    {
        return [
            [[/*'text', */'user_id'], 'required'],
            ['user_id', 'integer'],
            ['image', 'string'],
            ['text', 'string', 'max' => 500]
        ];
    }



    public function attributeLabels()
    {
        return [
            'text' => 'Текст сообщения',
            'image' => 'Изображение'
        ];
    }
}