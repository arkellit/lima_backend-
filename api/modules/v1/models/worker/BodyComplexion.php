<?php

namespace api\modules\v1\models\worker;


use yii\db\ActiveRecord;

/**
 * Class BodyComplexion
 *
 * @property integer $id
 * @property string $name
 *
 * @package app\modules\v1\models\worker
 */
class BodyComplexion extends ActiveRecord
{
    public static function tableName()
    {
        return '{{worker_body_complexion}}';
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Телосложение'
        ];
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'max' => 255]
        ];
    }
}