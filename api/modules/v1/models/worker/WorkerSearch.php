<?php

namespace api\modules\v1\models\worker;

use api\modules\v1\models\order\OrderPlace;
use api\modules\v1\models\order\OrderRequest;
use Yii;
use yii\base\Model;

/**
 * @property integer $height_start
 * @property integer $height_end
 * @property integer $type_request
 * @property string $radius_search
 * @property double $lat
 * @property double $lng
 *
 * WorkerSearch represents the model behind the search form of `api\modules\v1\models\worker\Worker`.
 */
class WorkerSearch extends Worker
{
    public $category_worker;
    public $number;
    public $height_start;
    public $height_end;
    public $radius_search;
    public $lat;
    public $lng;
    public $type_request;
    public $age;
    public $age_start;
    public $age_end;
    public $order_id;
    public $offset;
    public $limit;
    public $status_request;

    const SEARCH = 1;
    const LOAD_SEARCH = 2;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sex', 'height_start', 'height_end',
//                'weight_start', 'weight_end',
//                'body_complexion_id', 'hair_color_id', 'hair_length_id',
//                'appearance_id',
// 'eye_color_id',
            'status_request',
                'city_id'], 'integer'],
            [['height_start', 'height_end', 'radius_search'], 'safe'],
            [['lat', 'lng'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param $params
     * @return array|\yii\db\ActiveRecord[]
     */
    public function search($params)
    {
        $radius = (double)(($this->radius_search)/100000);
        $order_place = OrderPlace::find()
            ->where(['order_id' => $this->order_id])
            ->joinWith('orderAddress')
            ->asArray()
            ->one();
        $lat = $order_place['orderAddress']['lat'];
        $lng = $order_place['orderAddress']['lng'];
        $query = Worker::find()->select(['worker.*',
            "date_part('year', age('today'::date, worker.birth_date)) as age"]);
        $this->load($params);
        $sex = $this->sex == 0 ? [] : ['sex' => $this->sex];
        $query->joinWith(['workerProfession', 'orderRequest']);
        $query->andWhere(['is_register_complete' => Worker::IS_REGISTER_COMPLETE_TRUE]);
        $query->andFilterWhere(['AND',
            ['worker_profession.category_worker_id' => json_decode($this->category_worker)]
//            ['worker_profession.is_working' => WorkerProfession::WORKER_WORKING]
        ]);
        $query->andFilterWhere($sex);
        $query->andFilterWhere(['order_request.order_id' => $this->order_id,
//            'order_request.status' => OrderRequest::STATUS_NEW,
            'is_show_in_filter' => OrderRequest::SHOW_IN_FILTER]);
        $query->andWhere(['NOT', ['order_request.status' => OrderRequest::STATUS_WORKER_CANCEL]]);
        $query->andFilterWhere(['eye_color_id' => json_decode($this->eye_color_id)]);
        $query->andFilterWhere(['hair_length_id' => json_decode($this->hair_length_id)]);
        $query->andFilterWhere(['hair_color_id' => json_decode($this->hair_color_id)]);
        $query->andFilterWhere(['body_complexion_id' => json_decode($this->body_complexion_id)]);
        $query->andFilterWhere(['appearance_id' => json_decode($this->appearance_id)]);
        $query->andFilterWhere(['AND',
            ['>=', 'height', $this->height_start],
            ['<=', 'height', $this->height_end]
        ]);

        if (!is_null($this->status_request)) {
            $query->andFilterWhere(['order_request.status' => $this->status_request]);
        }
        else {
            $query->andFilterWhere(['order_request.status' => OrderRequest::STATUS_NEW]);
        }

        if (!is_null($this->age_start) and !is_null($this->age_end)){
            $query->andFilterWhere(['AND', ['>=', "date_part('year', age('today'::date, worker.birth_date))", $this->age_start],
                ['<=', "date_part('year', age('today'::date, worker.birth_date))", $this->age_end]]);
        }
        if (!is_null($this->lat) and !is_null($this->lng)){
            $query->andWhere("ST_DWithin(ST_Buffer(worker.point, worker.radius_search), ST_GeomFromEWKT('SRID=4326;POINT($lat $lng)'), $radius)");
        }
        $query->joinWith('imageWorker');
        $query->offset((integer)$this->offset)->limit((integer)$this->limit);
        $array_query = $query->asArray()->all();
        $total_count = count($array_query);
        return ['array_query' => $array_query, 'totalCount' => $total_count];
    }
}
