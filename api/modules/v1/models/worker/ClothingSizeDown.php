<?php

namespace api\modules\v1\models\worker;

use yii\db\ActiveRecord;

/**
 * Class ClothingSizeDown
 * @package app\modules\v1\models\worker
 */
class ClothingSizeDown extends ActiveRecord
{
    public static function tableName()
    {
        return '{{worker_clothing_size_down}}';
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Низ одежды'
        ];
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'max' => 10]
        ];
    }
}