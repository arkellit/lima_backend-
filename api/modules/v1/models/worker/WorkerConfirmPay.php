<?php

namespace api\modules\v1\models\worker;

use yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%worker_confirm_pay}}".
 *
 * @property int $id
 * @property int $worker_id
 * @property string $payment_id
 * @property string $status
 * @property int $confirm_refund
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Worker $worker
 */
class WorkerConfirmPay extends ActiveRecord
{
    const CONFIRM_REFUND = 1;
    const NO_CONFIRM = 0;

    const SUCCEEDED = 'succeeded';
    const PENDING = 'pending';
    const CANCELED = 'canceled';
    const NO_REFUND = 'no_refund';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%worker_confirm_pay}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['worker_id', 'payment_id'], 'required'],
            [['confirm_refund'], 'default', 'value' => self::NO_CONFIRM],
            [['status'], 'default', 'value' => self::CANCELED],
            ['status', 'in', 'range' => [self::SUCCEEDED, self::PENDING, self::CANCELED]],
            ['confirm_refund', 'in', 'range' => [self::CONFIRM_REFUND, self::NO_CONFIRM]],
            [['worker_id', 'confirm_refund', 'created_at', 'updated_at'], 'integer'],
            [['payment_id'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 50],
            [['worker_id'], 'unique'],
            [['worker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Worker::className(), 'targetAttribute' => ['worker_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'worker_id' => 'Исполнитель',
            'payment_id' => 'ID Якассы',
            'status' => 'Статус платежа',
            'confirm_refund' => 'Был возврат',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['id' => 'worker_id']);
    }
}
