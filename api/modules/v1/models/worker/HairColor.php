<?php

namespace api\modules\v1\models\worker;


use yii\db\ActiveRecord;

/**
 * Class HairColor
 *
 * @property integer $id
 * @property string $name
 *
 * @package app\modules\v1\models\worker
 */
class HairColor extends ActiveRecord
{
    public static function tableName()
    {
        return '{{worker_hair_color}}';
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Цвет волос'
        ];
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'max' => 255]
        ];
    }
}