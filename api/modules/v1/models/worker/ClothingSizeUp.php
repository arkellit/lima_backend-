<?php

namespace api\modules\v1\models\worker;


use yii\db\ActiveRecord;

/**
 * Class ClothingSizeUp
 * @package app\modules\v1\models\worker
 */
class ClothingSizeUp extends ActiveRecord
{
    public static function tableName()
    {
        return '{{worker_clothing_size_up}}';
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Верх одежды'
        ];
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'max' => 10]
        ];
    }
}