<?php

namespace api\modules\v1\models\worker;

use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%worker_profession}}".
 *
 * @property int $id
 * @property int $worker_id
 * @property int $category_worker_id
 * @property int $is_working
 *
 * @property CategoryWorker $typeWorker
 * @property Worker $worker
 */
class WorkerProfession extends ActiveRecord
{
    const WORKER_WORKING = 1;
    const WORKER_NO_WORKING = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%worker_profession}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['worker_id', 'category_worker_id'], 'required'],
            [['worker_id', 'category_worker_id'], 'integer'],
//            [['worker_id', 'category_worker_id'], 'unique'],
            ['is_working', 'default', 'value' => $this::WORKER_NO_WORKING],
            ['is_working', 'in', 'range' => [self::WORKER_NO_WORKING, self::WORKER_WORKING]],
            [['worker_id', 'category_worker_id'], 'unique', 'targetAttribute' => ['worker_id', 'category_worker_id']],
            [['category_worker_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoryWorker::className(), 'targetAttribute' => ['category_worker_id' => 'id']],
            [['worker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Worker::className(), 'targetAttribute' => ['worker_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'worker_id' => 'Worker ID',
            'category_worker_id' => 'Type Worker ID',
            'is_working' => 'Работает'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeWorker()
    {
        return $this->hasOne(CategoryWorker::className(), ['id' => 'category_worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['id' => 'worker_id']);
    }
}
