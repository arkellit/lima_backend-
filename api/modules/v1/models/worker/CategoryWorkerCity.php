<?php

namespace api\modules\v1\models\worker;

use api\modules\v1\models\City;
use Yii;
use  \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%category_worker_city}}".
 *
 * @property int $category_worker_id
 * @property int $city_id
 * @property double $price
 * @property double $advance_price
 *
 * @property CategoryWorker $categoryWorker
 * @property City $city
 */
class CategoryWorkerCity extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%worker_category_city}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_worker_id', 'city_id', 'price'], 'required'],
            [['category_worker_id', 'city_id'], 'integer'],
            [['price', 'advance_price'], 'number'],
            ['advance_price', 'default', 'value' => 0],
            [['category_worker_id', 'city_id'], 'unique', 'targetAttribute' => ['category_worker_id', 'city_id']],
            [['category_worker_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoryWorker::className(), 'targetAttribute' => ['category_worker_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'category_worker_id' => 'Категория работника',
            'city_id' => 'Город',
            'price' => 'Цена за час',
            'advance_price' => 'Расширенная цена',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryWorker()
    {
        return $this->hasOne(CategoryWorker::className(), ['id' => 'category_worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }
}
