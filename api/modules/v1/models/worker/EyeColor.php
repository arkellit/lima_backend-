<?php

namespace api\modules\v1\models\worker;

use yii\db\ActiveRecord;
/**
 * This is the model class for table "eye_color".
 *
 * @property int $id
 * @property string $name
 *
 * @property Worker[] $workers
 */
class EyeColor extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{worker_eye_color}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Цвет глаз',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkers()
    {
        return $this->hasMany(Worker::className(), ['eye_color_id' => 'id']);
    }
}
