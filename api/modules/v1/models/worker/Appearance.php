<?php

namespace api\modules\v1\models\worker;

use yii\db\ActiveRecord;

/**
 * Class Appearance
 *
 * @property integer $id
 * @property string $name
 *
 * @package app\modules\v1\models\worker
 *
 */
class Appearance extends ActiveRecord
{
    public static function tableName()
    {
        return '{{worker_appearance}}';
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Внешность'
        ];
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'max' => 255]
        ];
    }
}