<?php

namespace api\modules\v1\models\worker;

use api\modules\v1\models\auth\User;
use api\modules\v1\models\City;
use api\modules\v1\models\order\OrderRequest;
use nanson\postgis\behaviors\GeometryBehavior;
use Yii;
use yii\db\ActiveRecord;
use yii\web\HttpException;

/**
 * This is the model class for table "worker".
 * Max rating = 100, default = 70.
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $last_name
 * @property string $middle_name
 * @property int $sex
 * @property int $birth_date
 * @property double $lat
 * @property double $lng
 * @property int $height
 * @property int $weight
 * @property string $card_number
 * @property int $body_complexion_id
 * @property int $hair_color_id
 * @property int $hair_length_id
 * @property int $clothing_size_up_id
 * @property int $clothing_size_down_id
 * @property int $appearance_id
 * @property int $eye_color_id
 * @property int $city_id
 *
 * @property int $is_register_complete
 * @property int $is_instruction_complete
 *
 * @property double $last_location_lat
 * @property double $last_location_lng
 * @property double $rating
 * @property double $radius_search
 *
 * @property string $point
 *
 * @property ImageWorker[] $imageWorker
 * @property ImageWorker[] $imageWorkers
 * @property PassportData[] $passportDatas
 * @property Appearance $appearance
 * @property BodyComplexion $bodyComplexion
 * @property ClothingSizeDown $clothingSizeDown
 * @property ClothingSizeUp $clothingSizeUp
 * @property HairColor $hairColor
 * @property EyeColor $eyeColor
 * @property City $city
 * @property HairLength $hairLength
 * @property CategoryWorker $typeWorker
 * @property WorkerConfirmPay $workerConfirmPay
 * @property User $user
 */
class Worker extends ActiveRecord
{
    const MAN = 1;
    const WOMAN = 2;

    const IS_INSTRUCTION_COMPLETE_TRUE = 1;
    const IS_INSTRUCTION_COMPLETE_FALSE = 0;

    const IS_REGISTER_COMPLETE_TRUE = 1;
    const IS_REGISTER_COMPLETE_FALSE = 0;

    const MIN_RATING = 0;
    const DEFAULT_RATING = 70;
    const CONFIRM_ORDER_RATING = 6;
    const REJECT_ORDER_RATING = 2;
    const BLOCKED_RATING = 15;
    const MAX_RATING = 100;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%worker}}';
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            if (!is_null($this->last_location_lat) && !is_null($this->last_location_lng)){
                $this->point = [$this->last_location_lat, $this->last_location_lng];
            }
            else $this->point = null;
            //$this->radius_search = ((double)$this->radius_search / 100000);
            return true;
        }
        return false;
    }

    public function behaviors()
    {
        return [
            [
                'class' => GeometryBehavior::className(),
                'type' => GeometryBehavior::GEOMETRY_POINT,
                'attribute' => 'point',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'last_name', 'sex', 'birth_date', 'height',
                'weight', 'hair_length_id', 'body_complexion_id', 'hair_color_id'/*, 'clothing_size_up_id', 'clothing_size_down_id'*/,
                'appearance_id', 'eye_color_id'/*, 'card_number'*/], 'required'],
            [['user_id', 'sex', 'height', 'weight', 'hair_length_id',
                'body_complexion_id', 'hair_color_id'/*, 'clothing_size_up_id', 'clothing_size_down_id'*/,
                'appearance_id', 'eye_color_id', 'city_id', 'is_register_complete',
                'is_instruction_complete'], 'integer'],
            ['user_id', 'unique', 'message' => 'Работник уже зарегистрирован'],
            [['name', 'last_name', 'middle_name'], 'string', 'max' => 255],
            [['last_location_lat', 'last_location_lng'],'double'],
            [['last_location_lat', 'last_location_lng', 'radius_search'],'default', 'value' => 0],
            ['rating', 'default', 'value' => self::DEFAULT_RATING],
            ['rating', 'number', 'min' => self::MIN_RATING, 'max' => self::MAX_RATING],
            [['birth_date'], 'date', 'format' => 'php:Y-m-d'],
            ['point', 'default', 'value' => null],
            //[['card_number'], 'string', 'max' => 20],
            //['card_number', 'cardNumberValidate'],
            ['sex', 'in', 'range' => [self::MAN, self::WOMAN]],
            ['is_instruction_complete', 'in', 'range' => [self::IS_INSTRUCTION_COMPLETE_TRUE, self::IS_INSTRUCTION_COMPLETE_FALSE]],
            ['is_register_complete', 'in', 'range' => [self::IS_REGISTER_COMPLETE_TRUE, self::IS_REGISTER_COMPLETE_FALSE]],
            [['appearance_id'], 'exist', 'skipOnError' => true, 'targetClass' => Appearance::className(), 'targetAttribute' => ['appearance_id' => 'id']],
            [['body_complexion_id'], 'exist', 'skipOnError' => true, 'targetClass' => BodyComplexion::className(), 'targetAttribute' => ['body_complexion_id' => 'id']],
//            [['clothing_size_down_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClothingSizeDown::className(), 'targetAttribute' => ['clothing_size_down_id' => 'id']],
//            [['clothing_size_up_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClothingSizeUp::className(), 'targetAttribute' => ['clothing_size_up_id' => 'id']],
            [['hair_color_id'], 'exist', 'skipOnError' => true, 'targetClass' => HairColor::className(), 'targetAttribute' => ['hair_color_id' => 'id']],
            [['hair_length_id'], 'exist', 'skipOnError' => true, 'targetClass' => HairColor::className(), 'targetAttribute' => ['hair_length_id' => 'id']],
            [['eye_color_id'], 'exist', 'skipOnError' => true, 'targetClass' => EyeColor::className(), 'targetAttribute' => ['eye_color_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Имя',
            'last_name' => 'Фамилия',
            'middle_name' => 'Отчество',
            'sex' => 'Пол',
            'birth_date' => 'Дата рождения',
            'lat' => 'Широта',
            'lng' => 'Долгота',
            'height' => 'Рост',
            'weight' => 'Вес',
            'hair_length' => 'Длина волос ID',
            //'card_number' => 'Номер карты',
            'body_complexion_id' => 'Комплекция тела ID',
            'hair_color_id' => 'Цвет волос ID',
//            'clothing_size_up_id' => 'Размер верха одежды ID',
//            'clothing_size_down_id' => 'Размер низа одежды ID',
            'appearance_id' => 'Внешность ID',
            'eye_color_id' => 'Цвет глаз ID',
            'is_register_complete' => 'Пользователь зарегистрирован',
            'is_instruction_complete' => 'Пользователь посомотрел интструкцию',
            'last_location_lat' => 'Широта последнего местоположения',
            'last_location_lng' => 'Долгота последнего местоположения',
            'point' => 'Postgis point',
            'rating' => 'Рейтинг'
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImageWorkers()
    {
        return $this->hasMany(ImageWorker::className(), ['worker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImageWorker()
    {
        return $this->hasOne(ImageWorker::className(), ['worker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassportDatas()
    {
        return $this->hasMany(PassportData::className(), ['worker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppearance()
    {
        return $this->hasOne(Appearance::className(), ['id' => 'appearance_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBodyComplexion()
    {
        return $this->hasOne(BodyComplexion::className(), ['id' => 'body_complexion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClothingSizeDown()
    {
        return $this->hasOne(ClothingSizeDown::className(), ['id' => 'clothing_size_down_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClothingSizeUp()
    {
        return $this->hasOne(ClothingSizeUp::className(), ['id' => 'clothing_size_up_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHairColor()
    {
        return $this->hasOne(HairColor::className(), ['id' => 'hair_color_id']);
    }

    public function getHairLength(){
        return $this->hasOne(HairLength::className(), ['id' => 'hair_length_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEyeColor()
    {
        return $this->hasOne(EyeColor::className(), ['id' => 'eye_color_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public function getWorkerProfession()
    {
        return $this->hasMany(WorkerProfession::className(), ['worker_id' => 'id']);
    }

    public function getWorkerConfirmPay(){
        return $this->hasOne(WorkerConfirmPay::className(), ['worker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getOrderRequest(){
        return$this->hasMany(OrderRequest::className(), ['worker_id' => 'id']);
    }

    public static function getAge($birth_date)
    {
        $diff = abs(time()-strtotime($birth_date));
        $age = floor($diff / (365*60*60*24));
        return $age;
    }

    /**
     * Function get rating
     *
     * @param $rating
     * @return float
     */
    public static function getRating($rating){
       return round($rating/20, 1);
    }

    /**
     * Function add rating
     *
     * @param $rating
     * @param $number
     * @return int
     */
    public static function addRating($rating, $number){
        if ($rating == self::MAX_RATING){
            return $rating;
        }
        elseif (($rating + $number) > self::MAX_RATING){
            return self::MAX_RATING;
        }
        return $rating + $number;
    }

    /**
     * Function reduce rating
     *
     * @param $rating
     * @param $number
     * @return int
     */
    public static function reduceRating($rating, $number){
        if (($rating - $number) < self::MIN_RATING){
            return self::MIN_RATING;
        }
        return $rating - $number;
    }


    public function cardNumberValidate($attribute){
        if(!$this->is_valid_credit_card($this->$attribute)){
            throw new HttpException(428, 'Номер карты не валидный');
        }
    }

    /**
     * Function check valid card number
     */
    public function is_valid_credit_card($card_number) {
        $card_number = strrev(preg_replace('/[^\d]/','',$card_number));
        $sum = 0;
        for ($i = 0, $j = strlen($card_number); $i < $j; $i++) {
            if (($i % 2) == 0) {
                $val = $card_number[$i];
            } else {
                $val = $card_number[$i] * 2;
                if ($val > 9)  $val -= 9;
            }
            $sum += $val;
        }
        return (($sum % 10) == 0);
    }

    /**
     * Function create new Worker
     *
     * @param $user_id
     * @param $name
     * @param $last_name
     * @param $middle_name
     * @param $sex
     * @param $birth_date
     * @param $height
     * @param $weight
     * @param $hair_length_id
     * @param $category_worker_list
     * @param $body_complexion_id
     * @param $hair_color_id
     * @param $appearance_id
     * @param $eye_color_id
     * @param $city_id
     * @return array
     * @throws HttpException
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function create($user_id, $name, $last_name, $middle_name, $sex, $birth_date,
                           $height, $weight, $hair_length_id,
                           $category_worker_list, $body_complexion_id, $hair_color_id,
                           /*$clothing_size_up_id, $clothing_size_down_id,*/
                           $appearance_id, $eye_color_id, $city_id/*, $card_number*/){


        $model = new Worker();
        $model->user_id = $user_id;
        $model->name = $name;
        $model->last_name = $last_name;
        $model->middle_name = $middle_name;
        $model->sex = $sex;
        $model->birth_date = Yii::$app->formatter->asDate($birth_date, 'php:Y-m-d');
        $model->height = $height;
        $model->weight = $weight;
        $model->hair_length_id = $hair_length_id;
        $model->body_complexion_id = $body_complexion_id;
        $model->hair_color_id = $hair_color_id;
//        $model->clothing_size_up_id = $clothing_size_up_id;
//        $model->clothing_size_down_id = $clothing_size_down_id;
        $model->appearance_id = $appearance_id;
        $model->eye_color_id = $eye_color_id;
        $model->city_id = $city_id;
        //$model->card_number = preg_replace('/[^\d]/','',$card_number);

        if($model->save()){
            $check_category = false;
            foreach ($category_worker_list as $value){
                $test_cat = CategoryWorker::findOne($value['id']);
                if($test_cat == null){
                    $check_category = false;
                }
                else{
                    $check_category = true;
;               }
            }
            if(!$check_category){
                $model->delete();
                throw new HttpException(400, 'Произошла ошибка при добавлении категории работника');
            }

            foreach ($category_worker_list as $value){
                $model_professional = new WorkerProfession();
                $model_professional->worker_id = $model->id;
                $model_professional->category_worker_id = $value['id'];
                $is_save = $model_professional->save();
                if($is_save == false){
                    WorkerProfession::deleteAll(['worker_id' => $model->id]);
                    Worker::deleteAll(['id' => $model->id]);
                    return array('worker_id' => null, 'error' => $model_professional->getErrors());
                }
            }

            return  array('worker_id' => $model->id);
        }

        return array('worker_id' => null, 'error' => $model->getErrors());
    }
}
