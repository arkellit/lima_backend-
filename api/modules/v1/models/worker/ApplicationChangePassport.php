<?php

namespace api\modules\v1\models\worker;

use Yii;
use yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%worker_change_passport}}".
 *
 * @property int $id
 * @property int $worker_id
 * @property string $passport_photo_one
 * @property string $passport_photo_two
 * @property int $is_changed
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Worker $worker
 */
class ApplicationChangePassport extends ActiveRecord
{
    const CHANGED = 1;
    const NO_CHANGED = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%worker_change_passport}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['worker_id', 'passport_photo_one', 'passport_photo_two'], 'required'],
            [['worker_id', 'is_changed', 'created_at', 'updated_at'], 'integer'],
            ['worker_id', 'unique'],
            [['passport_photo_one', 'passport_photo_two'], 'string'],
            ['is_changed', 'in', 'range' => [self::CHANGED, self::NO_CHANGED]],
            ['is_changed', 'default', 'value' => self::NO_CHANGED],
            [['worker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Worker::className(), 'targetAttribute' => ['worker_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'worker_id' => 'Работник ID',
            'passport_photo_one' => 'Фото первой страницы паспорта',
            'passport_photo_two' => 'Фото второй страницы паспорта',
            'is_changed' => 'Выполнена заявка',
            'created_at' => 'Дата создания',
            'updated_at' => 'Время создания'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['id' => 'worker_id']);
    }
}
