<?php

namespace api\modules\v1\models\worker;

use Yii;
use \yii\db\ActiveRecord;
/**
 * This is the model class for table "{{%hair_length}}".
 *
 * @property int $id
 * @property string $name
 *
 * @property Worker[] $workers
 */
class HairLength extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%worker_hair_length}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Длина волос',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkers()
    {
        return $this->hasMany(Worker::className(), ['hair_length_id' => 'id']);
    }
}
