<?php

namespace api\modules\v1\models\worker;

use api\modules\v1\models\auth\User;
use common\helpers\EncryptionHelpers;
use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%worker_personal_data}}".
 *
 * @property int $id
 * @property int $worker_id
 * @property string $worker_inn
 * @property string $worker_snils
 * @property string $worker_ls_bank
// * @property string $bank_inn
// * @property string $bank_kpp
 * @property string $bank_ks
 * @property string $bank_name
 * @property string $bank_bik
 *
 * @property Worker $worker
 */
class PersonalData extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%worker_personal_data}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['worker_id', 'worker_inn', 'worker_snils', 'worker_ls_bank', 'bank_ks', 'bank_name', 'bank_bik'], 'required'],
            [['worker_id'], 'integer'],
            [['worker_inn'], 'string', 'length' => [10, 12]],
            [['worker_snils'], 'string', 'length' => [11, 11]],
            [['bank_bik'], 'string', 'length' => [9, 9]],
            [['worker_ls_bank', 'bank_ks'], 'string', 'length' => [20, 20]],
            [['bank_name'], 'string', 'max' => 255],
            [['worker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Worker::className(), 'targetAttribute' => ['worker_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'worker_id' => 'Исполнитель',
            'worker_inn' => 'Инн исполнителя',
            'worker_snils' => 'СНИЛС исполнителя',
            'worker_ls_bank' => 'Лицевой счет исполнителя в банке',
            'bank_ks' => 'Корреспондентский счет банка',
            'bank_name' => 'Наименование банка',
            'bank_bik' => 'Бик банка',
        ];
    }

//    public function beforeValidate()
//    {
//        if (parent::beforeValidate()) {
//            $this->worker_snils =  User::normalizePhone($this->worker_snils);
//            return parent::beforeValidate();
//        } else {
//            return false;
//        }
//    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->worker_inn = EncryptionHelpers::dec_enc('encrypt', $this->worker_inn);
            $this->worker_snils = EncryptionHelpers::dec_enc('encrypt', $this->worker_snils);
            $this->worker_ls_bank = EncryptionHelpers::dec_enc('encrypt', $this->worker_ls_bank);
//            $this->bank_inn = EncryptionHelpers::dec_enc('encrypt', $this->bank_inn);
//            $this->bank_kpp = EncryptionHelpers::dec_enc('encrypt', $this->bank_kpp);
            $this->bank_ks = EncryptionHelpers::dec_enc('encrypt', $this->bank_ks);
            $this->bank_name = EncryptionHelpers::dec_enc('encrypt', $this->bank_name);
            $this->bank_bik = EncryptionHelpers::dec_enc('encrypt', $this->bank_bik);
            return parent::beforeSave($insert);
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['id' => 'worker_id']);
    }
}
