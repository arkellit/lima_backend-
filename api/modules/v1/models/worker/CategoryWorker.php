<?php

namespace api\modules\v1\models\worker;

use yii\db\ActiveRecord;

/**
 * Class TypeWorker
 *
 * @property integer $id
 * @property integer $is_active
 * @property string $name
 *
 * @package app\modules\v1\models\worker
 */
class CategoryWorker extends ActiveRecord
{
    const ACTIVE = 1;
    const NO_ACTIVE = 0;

    public static function tableName()
    {
        return '{{worker_category}}';
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Тип работника',
            'is_active' => 'Статус'
        ];
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['is_active', 'default', 'value' => self::ACTIVE],
            ['is_active', 'in', 'range' => [self::ACTIVE, self::NO_ACTIVE]],
            ['name', 'string', 'max' => 255]
        ];
    }
}