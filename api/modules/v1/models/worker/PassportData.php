<?php

namespace api\modules\v1\models\worker;

use api\modules\v1\models\Upload;
use Yii;
use \yii\db\ActiveRecord;
use common\helpers\EncryptionHelpers;

/**
 * This is the model class for table "passport_data".
 *
 * @property int $id
 * @property int $worker_id
 * @property string $passport_name
 * @property string $passport_last_name
 * @property string $passport_middle_name
 * @property string $passport_series
 * @property string $passport_number
 * @property string $passport_whom
 * @property string $passport_when
 * @property string $passport_photo_one
 * @property string $passport_photo_two
 * @property string $passport_birthplace
 * @property string $passport_birth_date
 * @property string $passport_code
 * @property string $passport_residence_address
 *
 * @property Worker $worker
 */
class PassportData extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%worker_passport}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['worker_id', 'passport_name', 'passport_last_name',
                'passport_series',
                'passport_number'/*, 'passport_birthplace'*/, 'passport_birth_date',
                'passport_whom',
                'passport_when', 'passport_code',
                'passport_residence_address',
                'passport_photo_one',
                'passport_photo_two'], 'required'],
            [['worker_id'], 'integer'],
            [['worker_id'], 'unique'],
            [['passport_name', 'passport_last_name', 'passport_middle_name'], 'string', 'max' => 2000],
            [['passport_name', 'passport_last_name', 'passport_middle_name', 'passport_number',
                'passport_series'], 'trim'],
            [['passport_series'], 'string', 'max' => 2000],
            [['passport_number'], 'string', 'max' => 2000],
            [['passport_whom'/*, 'passport_birthplace'*/, 'passport_birth_date'], 'string', 'max' => 2000],
            [['passport_when', 'passport_code'], 'string', 'max' => 2000],
            [['worker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Worker::className(), 'targetAttribute' => ['worker_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'worker_id' => 'Worker ID',
            'passport_name' => 'Имя',
            'passport_last_name' => 'Фамилия',
            'passport_middle_name' => 'Отчество',
            'passport_series' => 'Серия',
            'passport_number' => 'Номер',
//            'passport_birthplace' => 'Место рождения',
            'passport_birth_date' => 'Дата рождения',
            'passport_code' => 'Код подразделения',
            'passport_whom' => 'Кем выдан',
            'passport_when' => 'Когда выдан',
            'passport_residence_address' => 'Адрес прописки',
            'passport_photo_one' => 'Фото первой страницы паспорта',
            'passport_photo_two' => 'Фото второй страницы паспорта',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['id' => 'worker_id']);
    }

    /**
     * Function create passport data worker
     *
     * @param $worker_id
     * @param $passport_name
     * @param $passport_last_name
     * @param $passport_middle_name
     * @param $passport_series
     * @param $passport_number
     * @param $passport_birth_date
     * @param $passport_whom
     * @param $passport_when
     * @param $passport_code
     * @param $passport_residence_address
     * @param $passport_photo_one
     * @param $passport_photo_two
     * @return array
     */
    public function create($worker_id, $passport_name, $passport_last_name, $passport_middle_name,
                           $passport_series, $passport_number,
                           //$passport_birthplace,
                           $passport_birth_date,
                           $passport_whom, $passport_when,
                           $passport_code, $passport_residence_address,
                           $passport_photo_one,$passport_photo_two)
    {
        $model = new PassportData();
        $upload_image = new Upload();
        $model->worker_id = $worker_id;
        $model->passport_name = !is_null($passport_name) ? EncryptionHelpers::dec_enc('encrypt', $passport_name) : null;
        $model->passport_last_name = !is_null($passport_last_name) ? EncryptionHelpers::dec_enc('encrypt', $passport_last_name) : null;
        $model->passport_middle_name = !is_null($passport_middle_name) ? EncryptionHelpers::dec_enc('encrypt', $passport_middle_name) : null;
        $model->passport_series = !is_null($passport_series) ? EncryptionHelpers::dec_enc('encrypt', $passport_series) : null;
        $model->passport_number = !is_null($passport_number) ? EncryptionHelpers::dec_enc('encrypt', $passport_number) : null;
//        $model->passport_birthplace = !is_null($passport_birthplace) ? EncryptionHelpers::dec_enc('encrypt', $passport_birthplace) : null;
        $model->passport_birth_date = !is_null($passport_birth_date) ? EncryptionHelpers::dec_enc('encrypt', Yii::$app->formatter->asDate($passport_birth_date, 'php:Y-m-d')) : null;
        $model->passport_whom = !is_null($passport_whom) ? EncryptionHelpers::dec_enc('encrypt', $passport_whom) : null;
        $model->passport_when = !is_null($passport_when) ? EncryptionHelpers::dec_enc('encrypt', $passport_when) : null;
        $model->passport_code = !is_null($passport_code) ? EncryptionHelpers::dec_enc('encrypt', $passport_code) : null;

        $model->passport_residence_address = !is_null($passport_residence_address) ? EncryptionHelpers::dec_enc('encrypt', $passport_residence_address) : null;


        if (empty($passport_photo_one) || empty($passport_photo_two)){
            return array('is_success' => null, 'error' => ['error' => ['Не удалось сохранить фото']]);
        }
        // Загрузка временных фото
        $photo_one_explode = explode("/img/temp_worker/", $passport_photo_one);
        $photo_two_explode = explode("/img/temp_worker/", $passport_photo_two);

        $link_photo_one = Yii::getAlias('@image/web') . $passport_photo_one;
        $link_photo_two = Yii::getAlias('@image/web') . $passport_photo_two;

        if (!file_exists($link_photo_one) || !file_exists($link_photo_two)){
            return array('is_success' => null, 'error' => ['error' => ['Не удалось сохранить фото']]);
        }

        $photo_one_explode_dir = '/data/passport/' . $photo_one_explode[1];
        $photo_two_explode_dir = '/data/passport/' . $photo_two_explode[1];

        $link_photo_one_copy = Yii::getAlias('@image') . $photo_one_explode_dir;
        $link_photo_two_copy = Yii::getAlias('@image') . $photo_two_explode_dir;

        $copy_file_one = copy($link_photo_one,  $link_photo_one_copy);
        $copy_file_two = copy($link_photo_two, $link_photo_two_copy);

        if (!$copy_file_one || !$copy_file_two){
            return array('is_success' => null, 'error' => ['error' => ['Не удалось сохранить фото']]);
        }
        //


//        $photo_one = $upload_image->upload('secret_passport', $passport_photo_one);
//        $photo_two = $upload_image->upload('secret_passport', $passport_photo_two);

        $model->passport_photo_one = EncryptionHelpers::dec_enc('encrypt', $photo_one_explode_dir);
        $model->passport_photo_two = EncryptionHelpers::dec_enc('encrypt', $photo_two_explode_dir);

        if ($model->save()){
            unlink($link_photo_one);
            unlink($link_photo_two);

            return array('is_success' => true);
        }

        return array('is_success' => null, 'error' => $model->getErrors());
    }

    /**
     * Function encode/decode string
     *
     * @param $action
     * @param $string
     * @return bool|string
     */
    function dec_enc($action, $string) {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_key = Yii::$app->params['secret_key'];
        $secret_iv = Yii::$app->params['secret_iv'];

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        }
        else if( $action == 'decrypt' ){
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

}
