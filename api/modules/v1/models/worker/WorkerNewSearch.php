<?php

namespace api\modules\v1\models\worker;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use api\modules\v1\models\worker\Worker;

/**
 * WorkerNewSearch represents the model behind the search form of `api\modules\v1\models\worker\Worker`.
 */
class WorkerNewSearch extends Worker
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'sex', 'height', 'weight', 'body_complexion_id', 'hair_color_id', 'hair_length_id', 'appearance_id', 'eye_color_id', 'city_id', 'is_register_complete', 'is_instruction_complete'], 'integer'],
            [['name', 'last_name', 'middle_name', 'birth_date', 'point'], 'safe'],
            [['last_location_lat', 'last_location_lng', 'rating', 'radius_search'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Worker::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'is_register_complete' => Worker::IS_REGISTER_COMPLETE_FALSE//$this->is_register_complete,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'last_name', $this->last_name])
            ->andFilterWhere(['ilike', 'middle_name', $this->middle_name])
            ->andFilterWhere(['ilike', 'point', $this->point]);

        return $dataProvider;
    }
}
