<?php

namespace api\modules\v1\models\worker;

use api\modules\v1\models\Upload;
use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "image_worker".
 *
 * @property int $id
 * @property int $worker_id
 * @property string $photo_porter
 * @property string $photo_full_length
 *
 * @property Worker $worker
 */
class ImageWorker extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{worker_photo}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['worker_id', 'photo_porter', 'photo_full_length'], 'required'],
            [['worker_id'], 'integer'],
            [['photo_porter', 'photo_full_length',], 'string', 'max' => 255],
            [['worker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Worker::className(), 'targetAttribute' => ['worker_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'worker_id' => 'Worker ID',
            'photo_porter' => 'Фото лица',
            'photo_full_length' => 'Фото в полный рост'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['id' => 'worker_id']);
    }

    /**
     * Function create new image worker
     *
     * @param $worker_id
     * @param $photo_avatar
     * @param $photo_full
     * @return array
     */
    public function create($worker_id, $photo_avatar, $photo_full){
        $model = new ImageWorker();
        //$upload_image = new Upload();
        $model->worker_id = $worker_id;

        if (empty($photo_avatar) || empty($photo_full)){
            return array('is_success' => null, 'error' => ['error' => ['Не удалось сохранить фото']]);
        }
        // Загрузка временных фото
        $photo_avatar_explode = explode("/img/temp_worker/", $photo_avatar);
        $photo_full_explode = explode("/img/temp_worker/", $photo_full);

        $link_photo_avatar = Yii::getAlias('@image/web') . $photo_avatar;

        $link_photo_full = Yii::getAlias('@image/web') . $photo_full;

        if (!file_exists($link_photo_avatar) || !file_exists($link_photo_full)){
            return array('is_success' => null, 'error' => ['error' => ['Не удалось сохранить фото']]);
        }

        $photo_avatar_explode_dir = '/img/worker/' . $photo_avatar_explode[1];
        $photo_full_explode_dir = '/img/worker/' . $photo_full_explode[1];

        $link_photo_avatar_copy = Yii::getAlias('@image/web') . $photo_avatar_explode_dir;
        $link_photo_full_copy = Yii::getAlias('@image/web') . $photo_full_explode_dir;

        $copy_file_avatar = copy($link_photo_avatar,  $link_photo_avatar_copy);
        $copy_file_full = copy($link_photo_full, $link_photo_full_copy);

        if (!$copy_file_avatar || !$copy_file_full){
            return array('is_success' => null, 'error' => ['error' => ['Не удалось сохранить фото']]);
        }
        //

        $model->photo_porter = $photo_avatar_explode_dir;
        $model->photo_full_length = $photo_full_explode_dir;

        if ($model->save()){
            unlink($link_photo_avatar);
            unlink($link_photo_full);

            return array('is_success' => true);
        }

        return array('is_success' => null, 'error' => $model->getErrors());
    }
}
