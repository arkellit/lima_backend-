<?php


namespace api\modules\v1\models;


use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Logs extends ActiveRecord
{
    public static function tableName()
    {
       return 'logs1c';
    }

    public function rules()
    {
        return [
            ['text', 'required'],
            ['created_at', 'integer']
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp'  => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }
}