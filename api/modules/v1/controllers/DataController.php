<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\City;
use api\modules\v1\models\LimaHelpQuestion;
use api\modules\v1\models\printing\PrintingTemplate;
use api\modules\v1\models\worker\Appearance;
use api\modules\v1\models\worker\BodyComplexion;
use api\modules\v1\models\worker\CategoryWorker;
use api\modules\v1\models\worker\CategoryWorkerCity;
use api\modules\v1\models\worker\ClothingSizeDown;
use api\modules\v1\models\worker\ClothingSizeUp;
use api\modules\v1\models\worker\EyeColor;
use api\modules\v1\models\worker\HairColor;
use api\modules\v1\models\worker\HairLength;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;
use yii\web\Response;
use Yii;

/**
 * Class DataController
 *
 * Controller for obtaining employee data
 *
 * @package app\modules\v1\controllers\worker
 */
class DataController extends Controller
{
    // /**
    //  * @inheritdoc
    //  *
    //  * @return array
    //  */
    // public function behaviors()
    // {
    //     $behaviors = parent::behaviors();
    //     $behaviors['authenticator']['class'] = HttpBearerAuth::className();
    //     $behaviors['access'] = [
    //         'class' => AccessControl::className(),
    //         'rules' => [
    //             [
    //                 'allow' => true,
    //                 'roles' => ['@'],
    //             ],
    //         ],
    //     ];

    //     return $behaviors;
    // }

    public function actionGetParameters()
    {
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $model_category_worker = CategoryWorker::find()->all();
        $model_body = BodyComplexion::find()->all();
        $model_hair_color = HairColor::find()->all();
        $model_hair_length = HairLength::find()->all();
        $model_appearance = Appearance::find()->all();
        $model_eye_color = EyeColor::find()->all();
        $model_city = City::find()->all();
        return array('success' => 1, 'data' => [
            'category_worker' => $model_category_worker,
            'body_complexion' => $model_body,
            'hair_color' => $model_hair_color,
            'hair_length' => $model_hair_length,
            'appearance' => $model_appearance,
            'eye_color' => $model_eye_color,
            'city' => $model_city
        ]);
    }
    /**
     * Function get type worker
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetCategory(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $model = CategoryWorker::find()->all();
        return array('success' => 1, 'data' => $model);
    }

    /**
     * Function get body complexion worker
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetBodyComplexion(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $model = BodyComplexion::find()->all();
        return array('success' => 1, 'data' => $model);
    }

    /**
     * Function get hair color worker
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetHairColor(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $model = HairColor::find()->all();
        return array('success' => 1, 'data' => $model);
    }

    /**
     * Function get hair length worker
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetHairLength(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $model = HairLength::find()->all();
        return array('success' => 1, 'data' => $model);
    }

    /**
     * Function get clothing size up
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetClothingUp(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $model = ClothingSizeUp::find()->all();
        return array('success' => 1, 'data' => $model);
    }

    /**
     * Function get clothing size down
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetClothingDown(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $model = ClothingSizeDown::find()->all();
        return array('success' => 1, 'data' => $model);
    }

    /**
     * Function get appearance worker
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetAppearance(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $model = Appearance::find()->all();
        return array('success' => 1, 'data' => $model);
    }

    /**
     * Function get eye color
     *
     * @return array
     */
    public function actionGetEyeColor(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $model = EyeColor::find()->all();
        return array('success' => 1, 'data' => $model);
    }

    /**
     * Function get city
     *
     * @return array
     */
    public function actionGetCity(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $model = City::find()->select(['id', 'name'])->all();
        return array('success' => 1, 'data' => $model);
    }

    /**
     * Function get price
     *
     * @return array
     */
    public function actionGetPrice(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $model = CategoryWorkerCity::find()->all();
        return array('success' => 1, 'data' => $model);
    }

    /**
     * Function get print data
     *
     * @return array
     */
    public function actionGetPrintData(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;
        $city_id = $request->get('city_id');
        $model_editions = PrintingTemplate::find()
            ->where(['city_id' => $city_id ?? 1,
                'category' => PrintingTemplate::EDITION])
            ->all();
        $model_print_type = PrintingTemplate::find()
            ->where(['city_id' => $city_id ?? 1,
                'category' => PrintingTemplate::PRINT_TYPE])
            ->all();
        $model_print_size = PrintingTemplate::find()
            ->where(['city_id' => $city_id ?? 1,
                'category' => PrintingTemplate::PRINT_SIZE])
            ->all();
        $model_paper = PrintingTemplate::find()
            ->where(['city_id' => $city_id ?? 1,
                'category' => PrintingTemplate::PAPER_DENSITY])
            ->all();
        $model_print_time = PrintingTemplate::find()
            ->where(['city_id' => $city_id ?? 1,
                'category' => PrintingTemplate::PRINT_TIME])
            ->all();
        $model_courier_price = PrintingTemplate::find()
            ->where(['city_id' => $city_id ?? 1,
                'category' => PrintingTemplate::COURIER_PRICE])
            ->one();
        return array('success' => 1, 'data' => [
            PrintingTemplate::EDITION => $model_editions,
            PrintingTemplate::PRINT_TYPE => $model_print_type,
            PrintingTemplate::PRINT_SIZE => $model_print_size,
            PrintingTemplate::PAPER_DENSITY => $model_paper,
            PrintingTemplate::PRINT_TIME => $model_print_time,
            PrintingTemplate::COURIER_PRICE => $model_courier_price
        ]
        );
    }

    /**
     * Function get question answer lima help
     *
     * @return array
     */
    public function actionGetLimaHelp(){
        $request = Yii::$app->request;
        $type = $request->get('type');
        $model = LimaHelpQuestion::find()
            ->select(['id', 'question', 'answer'])
            ->where(['type' => $type])
            ->orderBy(['sort' => SORT_ASC])
            ->all();
        return array('success' => 1, 'data' => $model);
    }
}