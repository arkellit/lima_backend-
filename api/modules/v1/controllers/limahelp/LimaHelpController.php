<?php

namespace api\modules\v1\controllers\limahelp;

use api\modules\v1\models\chat\Chat;
use api\modules\v1\models\Upload;
use Yii;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\web\Controller;
use yii\web\Response;
use yii\web\HttpException;

/**
 * Class LimaHelpController
 * @package api\modules\v1\controllers\limahelp
 */
class LimaHelpController extends Controller
{
    /**
     * @inheritdoc
     *
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'actions' => ['create', 'get-dialog', 'read-message'],
                    'roles' => ['@'],
                ],
            ],
        ];
        return $behaviors;
    }

    /**
     * Method create new message
     *
     * @return array
     * @throws HttpException
     */
    public function actionCreate(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;
        $user_id = Yii::$app->user->getId();
        $message = $request->getBodyParam('message');
        $attachment = $request->getBodyParam('attachment');
        $url_attachment = null;
        if (!is_null($attachment)){
            $upload_image = new Upload();
            $url_attachment = $upload_image->upload('dialog', $attachment);
        }
        $chat = new Chat();
        $chat->message = $message;
        $chat->user_from_id = $user_id;
        $chat->user_to_id = Yii::$app->params['admin_id'];
        $chat->attachment = $url_attachment;
        if ($chat->save()){
            return array('success' => 1, 'data' => ['message' => 'Сообщение отправлено']);
        }
        $message = '';
        foreach ($chat->getErrors() as $key => $value) {
            $message =  $value[0];
        }

        throw new HttpException(400, $message);
    }

    /**
     * Method get all message for one dialog
     *
     * @return array
     */
    public function actionGetDialog(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $user_id = Yii::$app->user->getId();

        $chat = Chat::getDialog(Yii::$app->params['admin_id'], $user_id);

        $new_chat = array();
        foreach ($chat as $item){
            if($item->user_from_id == $user_id){
                $user_id_ = ['id' => $item->id, 'user_id' => $user_id,
                    'message' => $item->message, 'attachment' => $item->attachment,
                    'status' => $item->status, 'created_at' => $item->created_at];
            }
            else{
                $user_id_ = ['id' => $item->id, 'user_id' => $item->user_from_id,
                    'message' => $item->message, 'attachment' => $item->attachment,
                    'status' => $item->status, 'created_at' => $item->created_at];
            }

            array_push($new_chat, $user_id_);
        }
        return array('success' => 1, 'data' => $new_chat);
    }

    /**
     * Method read messages
     *
     * @return array
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionReadMessage(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request->getBodyParams();
        $id_list = $request['id_list'];
        $chat_update = Chat::updateAll(['status' => Chat::READ], [
            'id' => $id_list
        ]);
        if($chat_update){
            return array('success' => 1, 'data' => ['message' => 'Сообщение прочитано']);
        }

        throw new HttpException(400, 'Произошла ошибка');
    }
}