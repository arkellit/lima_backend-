<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\auth\ChangeUser;
use api\modules\v1\models\auth\form\ConfirmPhoneForm;
use api\modules\v1\models\auth\form\ResendCodeForm;
use api\modules\v1\models\chat\Chat;
use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerBankData;
use api\modules\v1\models\customer\CustomerBranch;
use api\modules\v1\models\customer\CustomerCompanyData;
use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\order\Order;
use api\modules\v1\models\order\OrderAddress;
use api\modules\v1\models\order\OrderPlace;
use api\modules\v1\models\order\OrderRequest;
use api\modules\v1\models\worker\ImageWorker;
use api\modules\v1\models\worker\PassportData;
use api\modules\v1\models\worker\PersonalData;
use api\modules\v1\models\worker\Worker;
use api\modules\v1\models\worker\WorkerProfession;
use api\modules\v1\models\worker\ApplicationChangePassport;
use common\helpers\EncryptionHelpers;
use common\helpers\FirebaseHelpers;
use common\helpers\GetErrorsHelpers;
use common\helpers\TokenCodeHelpers;
use common\helpers\DifferenceTimeHelpers;
use api\modules\v1\models\auth\LoginForm;
use api\modules\v1\models\auth\PasswordResetForm;
use api\modules\v1\models\auth\RegistrationForm;
use api\modules\v1\models\auth\SocialAccount;
use api\modules\v1\models\auth\SocialSendForm;
use api\modules\v1\models\auth\Token;
use api\modules\v1\models\auth\User;
use paragraph1\phpFCM\Recipient\Device;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;
use yii\web\HttpException;
use yii\web\Response;

/**
 * User controller
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     *
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        $behaviors['authenticator']['only'] = ['logout', 'update-fcm-token', 'test-send-push'];
        return $behaviors;
    }

    /**
     * Function register new User
     *
     * @return array
     * @throws HttpException
     * @throws \yii\base\Exception
     */
    public function actionRegistration()
    {
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;
        $phone = $request->getBodyParam('phone');
        $password = $request->getBodyParam('password');
        $fcm_token = $request->getBodyParam('fcm_token');
        $model = new RegistrationForm(['phone' => $phone,
            'password' => $password, 'fcm_token' => $fcm_token]);

        $user = $model->create();
        if ($user != null){
            if(isset($user['is_social'])){
                return array('success' => 1, 'data' => ['message' => 'Код подтверждения успешно отправлен',
                    'phone' => $user['phone']], 'status' => 200);
            }
            return array('success' => 1, 'data' => ['message' => 'Код подтверждения успешно отправлен',
                'phone' => $user['phone']], 'status' => 200);
        }
        throw new HttpException(400, 'Неверные данные');
    }

    /**
     * Function confirm phone number
     *
     * @return array
     * @throws HttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionConfirmPhone(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;

        $phone = $request->getBodyParam('phone');
        $code_request = $request->getBodyParam('code');

        $model = new ConfirmPhoneForm(['phone' => $phone, 'code' => $code_request]);
        if (!$model->validate()){
            $message =  GetErrorsHelpers::getError($model->getErrors());
            throw new HttpException(400, $message);
        }

        $user = $model->_user;
        $user->is_confirm = User::CONFIRM_PHONE;
        $user_update = $user->update();

        if($user_update){
            return  $user->isSocial() ? array('success' => 1, 'data' => [
                'message' => 'Регистрация успешно завершена',
                'token' => $user->access_token,
                'social_data' => $user->social,
                'id' => $user->id
            ], 'status' => 200) :
                 array('success' => 1, 'data' => [
                     'token' => $user->access_token,
                     'id' => $user->id
                 ], 'status' => 200);
        }

        throw new HttpException(500, 'Произошла ошибка сервера');
    }

    /**
     * Function login
     *
     * @return array
     * @throws HttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionLogin()
    {
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;

        $phone = $request->getBodyParam('phone');
        $password = $request->getBodyParam('password');
        $fcm_token = $request->getBodyParam('fcm_token');

        $model = new LoginForm(['phone' => $phone, 'password' => $password,
            'fcm_token' => $fcm_token]);

        $model->scenario = LoginForm::SCENARIO_LOGIN;

        $user_login = $model->login();
        return $user_login;
    }

    /**
     * Function login on social network
     *
     * @return array
     * @throws HttpException
     */
    public function actionSocialLogin(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;

        $access_token = $request->getBodyParam('access_token');
        $social_name = $request->getBodyParam('social_name');

        if(!is_null($access_token) && !is_null($social_name)){
            $model = new LoginForm();
            $model->scenario = LoginForm::SCENARIO_SOCIAL_LOGIN;
            $model->social_name = $social_name;
            $model->access_token = $access_token;
            $model->fcm_token = $request->getBodyParam('fcm_token');
            $user_token = $model->socialLogin();
            if($user_token == null){
                throw new HttpException(400, 'Неверные данные');
            }
            elseif (isset($user_token['is_new_create'])){
                if (is_null($user_token['phone'])){
                    throw new HttpException(420, 'В вашем профиле соц сети отсутствует номер телефона');
                }

                $model_create = new RegistrationForm();
                $model_create->phone = User::normalizePhone($user_token['phone']);
                $model_create->password = hash('sha256', $user_token['phone'] . time());
                $model_create->fcm_token = $model->fcm_token;
                $create_new_user = $model_create->create(true, true);
                if($create_new_user == null){
                    $message = '';
                    $key = '';
                    foreach ($model_create->getErrors() as $key => $value) {
                        $message =  $value[0];
                    }

                    if($key == 'phone' && $message == 'Номер телефона уже зарегистрирован'){
                        throw new HttpException(420, 'Номер телефона уже зарегистрирован, введите другой номер');
                    }

                    throw new HttpException(400, $message);
                }

                SocialAccount::updateAll(['user_id' => $create_new_user['id']],
                    ['client_id' => $user_token['client_id'], 'provider' => $social_name]);

                return array('success' => 1, 'data' =>
                    ['token' => $create_new_user['token']], 'status' => 200);
            }
            elseif ($user_token != null){
                return array('success' => 1, 'data' => ['token' => $user_token['token']], 'status' => 200);
            }
        }

        throw new HttpException(400, 'Неверные данные');
    }

    /**
     * The function of entering the phone number, if you have not come from
     * the social network
     *
     * @return array
     * @throws HttpException
     */
    public function actionSocialPhone(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;
        $access_token = $request->getBodyParam('access_token');
        $social_name = $request->getBodyParam('social_name');
        $fcm_token = $request->getBodyParam('fcm_token');

        $phone_request = $request->getBodyParam('phone');
        $phone = User::normalizePhone($phone_request);
        if($access_token && $social_name){
            $social_user = new SocialSendForm();
            $create_social = $social_user->sendSocial($social_name, $access_token);
            if(is_null($create_social['is_success'])){
                throw new HttpException(400, 'Произошла ошибка при создании');
            }

            $model_create = new RegistrationForm();
            $model_create->phone = $phone;
            $model_create->password = hash('sha256', $phone . time());
            $model_create->fcm_token = $fcm_token;
            $create_new_user = $model_create->create(true);

            if($create_new_user == null){
                $message = '';
                $key = '';
                foreach ($model_create->getErrors() as $key => $value) {
                    $message =  $value[0];
                }

                if($key == 'phone' && $message == 'Номер телефона уже зарегистрирован'){
                    throw new HttpException(400, $message);
                }

                throw new HttpException(400, $message);
            }

            SocialAccount::updateAll(['user_id' => $create_new_user['id']],
                ['client_id' => $create_social['client_id'], 'provider' => $social_name]);

            TokenCodeHelpers::createSend($create_new_user['id'], Token::TYPE_CONFIRMATION,
                $phone);

            return array('success' => 1, 'data' => ['message' => 'Код подтверждения успешно отправлен',
                'phone' => $phone], 'status' => 200);
        }

        throw new HttpException(400, 'Неполные данные');
    }

    /**
     * Function resend code
     *
     * @return array
     * @throws HttpException
     */
    public function actionResendCode(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;
        $phone = $request->getBodyParam('phone');
        $model_resend_code = new ResendCodeForm(['phone' => $phone]);
        if (!$model_resend_code->validate()){
            $message =  GetErrorsHelpers::getError($model_resend_code->getErrors());
            throw new HttpException(400, $message);
        }

        $user = $model_resend_code->_user;
        TokenCodeHelpers::updateSend($user->id, Token::TYPE_CONFIRMATION,
            $user->phone);
        return array('success' => 1 , 'data' => ['phone' => $user->phone,
            'message' => 'Код успешно отправлен'], 'status' => 200);
    }

    /**
     * Function update fcm token
     *
     * @return array
     * @throws HttpException
     */
    public function actionUpdateFcmToken(){
        if(Yii::$app->user->isGuest){
            throw new HttpException(401, 'Доступ запрещен');
        }
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;
        $fcm_token = $request->getBodyParam('fcm_token');
        $user = User::findOne(Yii::$app->user->getId());
        $user->fcm_token = $fcm_token;
        if($user->save()){
            return array('success' => 1, 'data' => ['message' => 'Fcm токен успешно обновлен'], 'status' => 200);
        }

        throw new HttpException(400, 'Неверные данные');
    }

    /**
     * Function logout
     *
     * @return array
     * @throws HttpException
     */
    public function actionLogout(){
        if(Yii::$app->user->isGuest){
            throw new HttpException(401, 'Доступ запрещен');
        }
        $user = User::findOne(Yii::$app->user->getId());
        $user->fcm_token = null;
        if($user->save()){
            return array('success' => 1, 'data' => ['message' => 'Выход произведен'], 'status' => 200);
        }
        throw new HttpException(400, 'Неверные данные');
    }

    /**
     * Function password reset
     *
     * @return array
     * @throws HttpException
     */
    public function actionForgotPassword()
    {
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;

        $model = new PasswordResetForm();
        $model->scenario = PasswordResetForm::SCENARIO_GENERATE_TOKEN;
        $phone_request = $request->getBodyParam('phone');
        $phone = User::normalizePhone($phone_request);
        $model->phone = $phone;
        $password_token = $model->generateToken();
        if($password_token != null){
            return array('success' => 1 , 'data' => ['phone' => $phone, 'message' => 'Код успешно отправлен'], 'status' => 200);
        }
        else{
            throw new HttpException(400, 'Неверные данные');
        }
    }

    /**
     * Function reset code for password reset
     *
     * @return array
     * @throws HttpException
     */
    public function actionForgotResendCode(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;
        $phone = $request->getBodyParam('phone');
        $phone_normal = User::normalizePhone($phone);
        $user = User::findOne(['phone' => $phone_normal]);
        $token = Token::findOne(['user_id' => $user->id]);
        if(!is_null($user)){
            if (is_null($user->password_reset_token)){
                throw new HttpException(425, 'Сброс пароля не запрашивался');
            }

            if (DifferenceTimeHelpers::differenceTime($token->updated_at) < 120){
                throw new HttpException(423, 'Отправка cмс разрешена раз в 2 мин');
            }

            if($token->save()){
                TokenCodeHelpers::updateSend($user->id, Token::TYPE_FORGOT_PASSWORD,
                    $user->phone);

                return array('success' => 1 , 'data' => ['phone' => $user->phone, 'message' => 'Код успешно отправлен'], 'status' => 200);
            }
        }

        throw new HttpException(400, 'Номер телефона не найден');
    }

    /**
     * Function confirm for resend code
     *
     * @return array
     * @throws HttpException
     */
    public function actionConfirmForgot(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;

        $phone_request = $request->getBodyParam('phone');
        $phone = User::normalizePhone($phone_request);
        $code_request = $request->getBodyParam('code');

        $user = User::findOne(['phone' => $phone]);

        if(is_null($user)){
            throw new HttpException(400, 'Номер не найден');
        }

        if (is_null($user->password_reset_token)){
            throw new HttpException(425, 'Сброс пароля не запрашивался');
        }

        $code = Token::findOne(['user_id' => $user->id]);

        if($code->code != $code_request){
            throw new HttpException(424, 'Неверный код');
        }

        if(DifferenceTimeHelpers::differenceTime($code->updated_at) > 300){
            throw new HttpException(423, 'Срок действия кода истек');
        }

        $user->is_confirm_forgot_code = User::CONFIRM_CODE;

        if($user->save()){
            return array('success' => 1 , 'data' => ['phone' => $user->phone, 'message' => 'Код успешно подтвержден',
                'reset_token' => $user->password_reset_token], 'status' => 200);
        }

        throw new HttpException(400, 'Не удалось подтвердить код');
    }

    /**
     * Function set new password
     *
     * @return array
     * @throws HttpException
     */
    public function actionResetPassword()
    {
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;

        $model = new PasswordResetForm();
        $model->scenario = PasswordResetForm::SCENARIO_SET_NEW_PASSWORD;
        $token = $request->getBodyParam('reset_token');
        $password = $request->getBodyParam('password');
        $fcm_token = $request->getBodyParam('fcm_token');
        $set_new_password = $model->setNewPassword($token, $password, $fcm_token);

        if($set_new_password == null){
            throw new HttpException(426, 'Не удалось изменить пароль');
        }

        elseif ($set_new_password != null){
            return array('success' => 1, 'data' => ['message' => 'Пароль успешно изменен',
                'token' => $set_new_password['token'],
                'id' => $set_new_password['id']], 'status' => 200);
        }

        else{
            throw new HttpException(400, 'Неверные данные');
        }
    }

    /**
     * Test method send push
     *
     * @return string
     */
    public function actionSendPush(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;

        $fcm_token = $request->getBodyParam('fcm_token');
        $type = $request->getBodyParam('type');

        $note = Yii::$app->fcm->createNotification('Lima title', 'Lima description');

        $message = Yii::$app->fcm->createMessage();
        $note->setIcon('/img/lima-logo.jpg')
            ->setColor('#ffffff')
            ->setClickAction('AppActivity')
//            ->setBadge(1)
            ->setSound("default");
        $message->addRecipient(new Device($fcm_token));
        $message->setNotification($note)
            ->setData([
                'type' => $type,
                'id_request' => 1
            ]);

        $response = Yii::$app->fcm->send($message);
        if($response->getStatusCode() != 200){
           return 'Не удалась отправка';
        }

        return 'Пуш отправлен';
    }

    /**
     * Test method send push
     *
     * @return string
     */
    public function actionTestSendPush(){
        if(Yii::$app->user->isGuest){
            throw new HttpException(401, 'Доступ запрещен');
        }
        $user_token = Yii::$app->user->identity->fcm_token;
        $response = FirebaseHelpers::closeQrPagePush($user_token, FirebaseHelpers::CONFIRM_START_ORDER);

        if(!$response){
            return 'Не удалась отправка';
        }
        return 'Пуш отправлен';
    }

    /**
     * Test method delete user
     *
     * @return string
     */
    public function actionDeleteUser(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;
        $phone = $request->getBodyParam('phone');

        $phone_normal = User::normalizePhone($phone);
        $user = User::findOne(['phone' => $phone_normal]);
        if(empty($phone)){
            return 'Номер не найден';
        }
        $token = Token::findOne(['user_id' => $user->id]);

        $change = ChangeUser::findOne(['user_id' => $user->id]);

        $worker = Worker::findOne(['user_id' => $user->id]);

        $worker_chat = Chat::getLastMessage(Yii::$app->params['admin_id'], $user->id);

        if(!is_null($worker)){
            $worker_personal = PersonalData::findOne(['worker_id' => $worker->id]);
            $passport = PassportData::findOne(['worker_id' => $worker->id]);
            $worker_prof = WorkerProfession::findAll(['worker_id' => $worker->id]);
            $all_image = ImageWorker::findOne(['worker_id' => $worker->id]);
            $all_application_passpot = ApplicationChangePassport::findOne(['worker_id' => $worker->id]);
            if(!is_null($all_image)){
                if($all_image->photo_porter !== null){
                    $file_path1 = Yii::getAlias('@image/web' . $all_image->photo_porter);
                    if(file_exists($file_path1)){
                        unlink(Yii::getAlias('@image/web' . $all_image->photo_porter));
                    }
                }

                if($all_image->photo_full_length !== null){
                     $file_path2 = Yii::getAlias('@image/web' . $all_image->photo_full_length);
                     if(file_exists($file_path2)){
                         unlink(Yii::getAlias('@image/web' . $all_image->photo_full_length));
                     }
                }

                $all_image->delete();
            }

            if(!empty($worker_prof)){
                WorkerProfession::deleteAll(['worker_id' => $worker->id]);
            }

            if(!empty($worker_personal)){
                PersonalData::deleteAll(['worker_id' => $worker->id]);
            }

            if(!is_null($passport)){
                $file_path3 = Yii::getAlias('@image'. EncryptionHelpers::dec_enc('decrypt', $passport->passport_photo_one));
                if(file_exists($file_path3)){
                    unlink(Yii::getAlias('@image'. EncryptionHelpers::dec_enc('decrypt', $passport->passport_photo_one)));
                }
                $file_path4 = Yii::getAlias('@image'. EncryptionHelpers::dec_enc('decrypt', $passport->passport_photo_two));

                if(file_exists($file_path4)){
                    unlink(Yii::getAlias('@image'. EncryptionHelpers::dec_enc('decrypt', $passport->passport_photo_two)));
                }
                $passport->delete();
            }

            if(!is_null($all_application_passpot)){
                $file_path5 = Yii::getAlias('@image'. EncryptionHelpers::dec_enc('decrypt', $all_application_passpot->passport_photo_one));
                if(file_exists($file_path5)){
                    unlink(Yii::getAlias('@image'. EncryptionHelpers::dec_enc('decrypt', $all_application_passpot->passport_photo_one)));
                }
                $file_path6 = Yii::getAlias('@image'. EncryptionHelpers::dec_enc('decrypt', $all_application_passpot->passport_photo_two));

                if(file_exists($file_path6)){
                    unlink(Yii::getAlias('@image'. EncryptionHelpers::dec_enc('decrypt', $all_application_passpot->passport_photo_two)));
                }
                $all_application_passpot->delete();
            }

            OrderRequest::deleteAll(['worker_id' => $worker->id]);

            if(!is_null($worker)){
                $worker->delete();
            }
        }

        if(!is_null($token)){
            $token->delete();
        }

        if(!is_null($change)){
            $change->delete();
        }

        if(!is_null($worker_chat)){
            $worker_chat_all = Chat::getDialog(Yii::$app->params['admin_id'], $user->id);
            foreach ($worker_chat_all as $item){
                if (!is_null($item->attachment)){
                    $file_path_chat = Yii::getAlias('@image/web' . $item->attachment);
                    if(file_exists($file_path_chat)){
                        unlink(Yii::getAlias('@image/web' . $item->attachment));
                    }
                    $item->delete();
                }
                else $item->delete();
            }
        }


        $auth = Yii::$app->authManager ;
        $auth->revokeAll($user->id) ;
        if($user->delete()){
            return 'Удалено';
        }

        return 'НЕ Удалено';
    }

    public function actionDeleteCustomer(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;
        $phone = $request->getBodyParam('phone');

        $phone_normal = User::normalizePhone($phone);
        $user = User::findOne(['phone' => $phone_normal]);
        if(empty($phone)){
            return 'Номер не найден';
        }
        $token = Token::findOne(['user_id' => $user->id]);

        $change = ChangeUser::findOne(['user_id' => $user->id]);

        $customer = Customer::findOne(['user_id' => $user->id]);

//        $customer_personal = CustomerPersonal::findOne(['user_id' => $user->id]);

        $worker_chat = Chat::getLastMessage(Yii::$app->params['admin_id'], $user->id);

        if(!empty($customer)){
            CustomerBranch::deleteAll(['customer_id' => $customer->id]);
            CustomerCompanyData::deleteAll(['customer_id' => $customer->id]);
            CustomerBankData::deleteAll(['customer_id' => $customer->id]);
            CustomerPersonal::deleteAll(['customer_id' => $customer->id]);
            $order = Order::findAll(['customer_id' => $customer->id]);
            if (!empty($order)){
                foreach ($order as $item){
                    $order_place = OrderPlace::findOne(['order_id' => $item->id]);
                    $order_address = OrderAddress::findOne($order_place->order_address_id);
                    $order_request = OrderRequest::findOne(['order_id' => $item->id]);
                    $order_address->delete();
                    $order_place->delete();
                    $order_request->delete();
                    $item->delete();
                }
            }

            $customer->delete();
        }

//        if(!empty($customer_personal)){
//            $customer_personal->delete();
//        }

        if(!is_null($token)){
            $token->delete();
        }

        if(!is_null($change)){
            $change->delete();
        }

        if(!is_null($worker_chat)){
            $worker_chat_all = Chat::getDialog(Yii::$app->params['admin_id'], $user->id);
            foreach ($worker_chat_all as $item){
                if (!is_null($item->attachment)){
                    $file_path_chat = Yii::getAlias('@image/web' . $item->attachment);
                    if(file_exists($file_path_chat)){
                        unlink(Yii::getAlias('@image/web' . $item->attachment));
                    }
                    $item->delete();
                }
                else $item->delete();
            }
        }


        $auth = Yii::$app->authManager ;
        $auth->revokeAll($user->id) ;
        if($user->delete()){
            return 'Удалено';
        }

        return 'НЕ Удалено';
    }
}
