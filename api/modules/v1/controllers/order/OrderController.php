<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 30.11.18
 * Time: 15:51
 */

namespace api\modules\v1\controllers\order;

use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use \yii\rest\ActiveController;

class OrderController extends ActiveController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'actions' => ['create', 'typeSelectWorker'],
                    'roles' => ['customer', 'administrator'],
                ],
                [
                    'allow' => true,
                    'actions' => ['addRequest', 'responseRequest',
                        'getLimaExchange', 'getLoadLimaExchange', 'getOrder', 'startOrder'],
                    'roles' => ['customer', 'administrator', 'worker', 'supervisor'],
                ],
                [
                    'allow' => true,
                    'actions' => ['create'],
                    'roles' => ['@'],
                ],
            ],
        ];

        return $behaviors;
    }

    public $modelClass = 'api\modules\v1\models\order\Order';
    public $modelClassOrderAddress = 'api\modules\v1\models\order\OrderAddress';
    public $modelClassOrderPlace = 'api\modules\v1\models\order\OrderPlace';
    public $modelClassPrice = 'api\modules\v1\models\worker\CategoryWorkerCity';
    public $modelClassRequest = 'api\modules\v1\models\order\OrderRequest';

    public function actions()
    {
        return [
            'create' => [
                'class' => 'api\modules\v1\controllers\order\orderApiClass\create',
                'modelClass' => $this->modelClass
            ],
            'typeSelectWorker'  => [
                'class' => 'api\modules\v1\controllers\order\orderApiClass\typeSelectWorker',
                'modelClass' => $this->modelClass
            ],
            'addRequest'  => [
                'class' => 'api\modules\v1\controllers\order\orderApiClass\addRequest',
                'modelClass' => $this->modelClassRequest
            ],
            'responseRequest'  => [
                'class' => 'api\modules\v1\controllers\order\orderApiClass\responseRequest',
                'modelClass' => $this->modelClassRequest
            ],
            'getLimaExchange'  => [
                'class' => 'api\modules\v1\controllers\order\orderApiClass\GetLimaExchange',
                'modelClass' => $this->modelClass
            ],
            'getLoadLimaExchange'  => [
                'class' => 'api\modules\v1\controllers\order\orderApiClass\GetLoadLimaExchange',
                'modelClass' => $this->modelClass
            ],
            'getOrder'  => [
                'class' => 'api\modules\v1\controllers\order\orderApiClass\GetOrder',
                'modelClass' => $this->modelClass
            ],
            'startOrder'  => [
                'class' => 'api\modules\v1\controllers\order\orderApiClass\StartOrder',
                'modelClass' => $this->modelClass
            ],
        ];
    }
}