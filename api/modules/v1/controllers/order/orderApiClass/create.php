<?php

namespace api\modules\v1\controllers\order\orderApiClass;

use api\modules\v1\models\City;
use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerCompanyData;
use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\order\Order;
use api\modules\v1\models\order\OrderAddress;
use api\modules\v1\models\order\OrderPlace;
use api\modules\v1\models\order\OrderProfession;
use api\modules\v1\models\order\OrderStartEnd;
use api\modules\v1\models\printing\OrderPrintData;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;
use common\helpers\GetErrorsHelpers;

/**
 * Class create
 * @package api\modules\v1\controllers\order\orderApiClass
 */
class create extends Action
{
    public function getCustomerId(){
        if (Yii::$app->user->can('customer')){
            $model_customer = Customer::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = ['customer_id' => $model_customer->id, 'admin_id' => 0];
        }
        else {
            $model_customer = CustomerPersonal::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = ['customer_id' => $model_customer->customer_id, 'admin_id' => $model_customer->id];
        }
        return $customer_id;
    }

    public function getRegionCode($id){
        $city = City::findOne($id);
        return $city->region->region_code;
    }

    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if (Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $request = Yii::$app->request;
        $customer = $this->getCustomerId();
        $customer_id = $customer['customer_id'];
        $admin_id = $customer['admin_id'];
        $personal_id = $request->getBodyParam('personal_id');
        $is_print_flyers = $request->getBodyParam('is_print_flyers');
        $is_courier = $request->getBodyParam('is_courier');
        $professions = $request->getBodyParam('worker_category_ids');

        $model_order = new Order();
        if ($model_order->load($request->bodyParams, '')){
            $model_order_address = new OrderAddress();
            if ($model_order_address->load($request->bodyParams, '')){
                $model_order_address->customer_id = $customer_id;
                if (!$model_order_address->save()){
                    $message = GetErrorsHelpers::getError($model_order_address->getErrors());
                    throw new HttpException(400, $message);
                }
            }

            $model_order->customer_id = $customer_id;
            $model_order->administrator_id = $admin_id;

            if (!$model_order->save()){
                $model_order_address->delete();
                $message = GetErrorsHelpers::getError($model_order->getErrors());
                throw new HttpException(400, $message);
            }


            $order_print_data = new OrderPrintData();
            if ($is_print_flyers == OrderPlace::IS_PRINTING_FLAYER){
                if ($order_print_data->load($request->bodyParams, '')){
                    if (!isset($is_courier) || $is_courier == 0){
                        $order_print_data->courier_price_id = 0;
                        $order_print_data->courier_address = null;
                    }
                    if (!$order_print_data->save()){
                        $model_order_address->delete();
                        $model_order->delete();
                        $message = GetErrorsHelpers::getError($order_print_data->getErrors());
                        throw new HttpException(400, $message);
                    }
                }
            }


//            return 1;



            $model_order_place = new OrderPlace();
            if ($model_order_place->load($request->bodyParams, '')){
                $count_worker = 0;
                foreach ($professions as $value){
                    $count_worker += $value['count'];
                }
                $model_order_place->order_id = $model_order->id;
                $model_order_place->order_address_id = $model_order_address->id;
                $model_order_place->order_print_id = $is_print_flyers == OrderPlace::NO_PRINTING_FLAYER ?
                    OrderPlace::NO_PRINTING_FLAYER : $order_print_data->id;
                $model_order_place->count_worker = $count_worker;
                if (!isset($personal_id) || $personal_id == 0){
                    $model_order_place->personal_id = $admin_id != 0 ? $admin_id : 0;
                }

                $model_order_start_end = new OrderStartEnd();
                $model_order_start_end->order_id = $model_order->id;
                if(!$model_order_place->save() || !$model_order_start_end->save()){
                    $model_order_address->delete();
                    $model_order->delete();
                    $message = GetErrorsHelpers::getError($model_order_place->getErrors());
                    throw new HttpException(400, $message);
                }
                $model_order_place->trigger(OrderPlace::EVENT_SET_NOTICE);
                $model_order_place->trigger(OrderPlace::EVENT_SET_SUPERVISOR_NOTICE);

                $new_profession = [];
                foreach ($professions as $value){
                    $new_profession[] = [
                        $model_order->id,
                        $model_order_place->id,
                        $value['category_id'],
                        $value['count'],
                        $value['type_job']
                    ];
                }

                Yii::$app->db->createCommand()->batchInsert('order_profession', [
                    'order_id', 'order_place_id', 'worker_category_id', 'count_worker',
                    'type_job'
                ], $new_profession)->execute();
            }



            $model_order->number_order =
                $this->getRegionCode($model_order_address->city_id) . '-' . $model_order->id;
            $model_order->total_price = $model_order_place->order_price;
                $model_order->update();

            $company_data = CustomerCompanyData::findOne(['customer_id' => $customer_id]);

            $total_price = $is_print_flyers == OrderPlace::IS_PRINTING_FLAYER ?
                $model_order->total_price + $order_print_data->printing_price : $model_order->total_price;

            if ($company_data->deposit < $total_price){
                $model_order->type_order = Order::TYPE_LIMA_EXCHANGE;
                $model_order->save();
                /**
                 * TODO Сделать формирование и отправку счета
                 */
                return array(
                    'success' => 1,
                    'data' => [
                        'message' => 'Заказ успешно создан',
                        'id' => $model_order->id,
                        'number_order' => $model_order->number_order,
                        'is_pay' => Order::NO_PAY,
                        'deposit' => $company_data->deposit,
                        'order_price' => $total_price,
                        'email' => $company_data->email
                    ],
                    'status' => 200
                );
            }
            else {
                $company_data->deposit = $company_data->deposit - $total_price;
                $model_order->is_pay = Order::IS_PAY;
                $model_order->type_order = Order::TYPE_LIMA_EXCHANGE;
                if ($company_data->save() && $model_order->save()){
                    /**
                     * TODO Отправка данных в 1С
                     */
                    return array(
                        'success' => 1,
                        'data' => [
                            'message' => 'Заказ успешно создан',
                            'id' => $model_order->id,
                            'number_order' => $model_order->number_order,
                            'is_pay' => Order::IS_PAY,
                        ],
                        'status' => 200
                    );
                }
            }
        }

        throw new HttpException(400, 'Неверные данные');
    }
}