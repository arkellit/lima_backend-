<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 13.12.18
 * Time: 12:06
 */

namespace api\modules\v1\controllers\order\orderApiClass;

use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerCompanyData;
use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\order\Order;
use api\modules\v1\models\order\OrderAddress;
use api\modules\v1\models\order\OrderProfession;
use api\modules\v1\models\order\OrderRequest;
use api\modules\v1\models\order\OrderWorker;
use api\modules\v1\models\worker\CategoryWorker;
use common\helpers\DifferenceTimeHelpers;
use common\helpers\OrderHelpers;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class GetOrder
 * @package api\modules\v1\controllers\order\orderApiClass
 */
class GetOrder extends Action
{
    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if (Yii::$app->user->isGuest){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $id = (integer)Yii::$app->request->get("id");
        $order = Order::find()
            ->where(['id' => $id])
            ->asArray()
            ->with(['orderPlace', 'customer'])
            ->one();
        if (!$order) throw new HttpException(400, 'Неверный запрос');

        if ($order['orderPlace']['personal_id'] == 0){
            $supervisor = $order['customer']['last_name'] . ' ' . mb_substr($order['customer']['name'], 0, 1) . '.' . mb_substr($order['customer']['middle_name'], 0, 1) . '.';
        }
        else {
            $customer_personal = CustomerPersonal::findOne($order['orderPlace']['personal_id']);
            if (empty($customer_personal)) {
                $supervisor = $order['customer']['last_name'] . ' ' . mb_substr($order['customer']['name'], 0, 1) . '.' . mb_substr($order['customer']['middle_name'], 0, 1) . '.';
            }
            else $supervisor = $customer_personal->last_name . ' ' . mb_substr($customer_personal->name, 0, 1) . '.' . mb_substr($customer_personal->middle_name, 0, 1) . '.';
        }

        $company_data = CustomerCompanyData::findOne(['customer_id' => $order['customer_id']]);
        $order_address = OrderAddress::findOne($order['orderPlace']['order_address_id']);
        $order_count_worker = OrderRequest::find()->where(['AND', ['order_id' => $order['id']],
            ['status' => OrderRequest::STATUS_ACCEPT]])->count();
//        $order_worker = CategoryWorker::findOne($order['orderPlace']['worker_category_id']);
        $order_professions = OrderProfession::find()
            ->where(['order_id' => $id])
            ->with('workerCategory')
            ->asArray()
            ->all();
        $profession = [];
        foreach ($order_professions as $order_profession) {
            $profession[] = [
                'id' => $order_profession['workerCategory']['id'],
                'name' => $order_profession['workerCategory']['name'],
                'count_worker' => $order_profession['count_worker'],
                'type_job' => $order_profession['type_job']
            ];
        }
        unset($order['customer_id']);
//        unset($order['type_order']);
        $order['price'] = (double)$order['total_price'];
       // $order['price'] = $order['total_price'] / $order['orderPlace']['count_worker'];
        $order['logo_company'] = $company_data->logo_company;
        $order['name_company'] = $company_data->name_company;
        $order['date_time_start'] = $order['orderPlace']['date_time_start'];
        $order['date_time_end'] = $order['orderPlace']['date_time_end'];
        $order['count_hour'] = (double)$order['orderPlace']['count_hour'];
        $order['count_worker'] = $order['orderPlace']['count_worker'];
        $order['required_count_worker'] = $order_count_worker;//$order['orderPlace']['count_worker'] - $order_count_worker;
        $order['category_worker'] = null;//$order_worker->name;
        $order['city'] = $order_address->city->name;
        $order['address_order'] = $order_address->address_order;
        $order['branch'] = $order_address->branch_id != 0 ? $order_address->branch_id : null;
        $order['metro_station'] = $order_address->metro_station;
        $order['place_order'] = $order_address->place_order;
        $order['lat'] = $order_address->lat;
        $order['lng'] = $order_address->lng;
        $order['description'] = $order['orderPlace']['comment'];
        $order['landmark'] = $order_address->landmark;
        $order['supervisor'] = $supervisor;
        $order['type_order'] = $order['is_complete_order'] == Order::NO_COMPLETE_ORDER ?
            OrderHelpers::getType($order['is_pay'], $order['type_order']) : 'archive';
        $order['professions'] = $profession;
        $current_time = time();
        if (Yii::$app->user->can('worker')){
            $worker_id = Yii::$app->user->identity->worker->id;
            $order['price'] = $order['total_price'] / $order['orderPlace']['count_worker'];
            $order_worker_accept = OrderWorker::findOne(['worker_id' => $worker_id,
                'order_id' => $order['id']]);
            $order_request = OrderRequest::findOne(['order_id' => $order['id'],
                'worker_id' => $worker_id]);
            switch (true){
                case empty($order_request):
                    $type = 'send_request';
                    break;
                case $order_request->status == OrderRequest::STATUS_NEW &&
                    OrderRequest::NO_LIMA_EXCHANGE:
                    $type = 'new_request';
                    break;
                case $order_request->status == OrderRequest::STATUS_NEW &&
                    OrderRequest::IS_LIMA_EXCHANGE:
                    $type = 'is_send_request';
                    break;
                case $order_request->status == OrderRequest::STATUS_NO_ACCEPT:
                    $type = 'not_accept_request';
                    break;
                case $order_request->orderStartEnd->is_finished_job == 1 ;
                    $type = 'completed';
                    break;
                case $order_request->orderStartEnd->is_start_job == 0;
                    $type = 'start_job';
                    break;
                case $order_request->orderStartEnd->is_start_job == 1;
                    $type = 'end_job';
                    break;
                default:
                    $type = 1;
                    break;
            }
            if (empty($order_worker_accept) || $order_worker_accept->status !=
                OrderWorker::STATUS_START_JOB){
                if (DifferenceTimeHelpers::differenceTime($order['orderPlace']['date_time_start']) <= 3600){
                    $order['status_work'] = 'less_day';
                }
                elseif (DifferenceTimeHelpers::differenceTime($order['orderPlace']['date_time_start']) >= 3600){
                    $order['status_work'] = 'more_day';
                }
            }
            else{
                $order['status_work'] = 'in_work';
            }
            $order['current_time'] = $current_time;
            $order['worker_id'] = $worker_id;
            $order['type'] = $type;
        }
        unset($order['total_price']);
        unset($order['is_pay']);
        unset($order['is_complete_order']);
        unset($order['orderPlace']);
        unset($order['customer']);

        return array('success' => 1, 'data' => $order, 'status' => 200);
    }


}