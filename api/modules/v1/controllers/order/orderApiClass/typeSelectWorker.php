<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 04.12.18
 * Time: 13:25
 */

namespace api\modules\v1\controllers\order\orderApiClass;

use api\modules\v1\models\order\Order;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class typeSelectWorker
 * @package api\modules\v1\controllers\order\orderApiClass
 */
class typeSelectWorker extends Action
{
    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if (Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $request = Yii::$app->request;
        $order_id = $request->getBodyParam('id');
        $type_select = $request->getBodyParam('type_select');
        $order = Order::findOne($order_id);
        if (!empty($order)){
            $order->type_order = $type_select;
            if ($order->save()){
                return array('success' => 1, 'data' => [
                    'message' => 'Заказ успешно обновлен',
                    'id' => $order->id,
                    'number_order' => $order->number_order],
                    'status' => 200);
            }
        }
        throw new HttpException(400, 'Неверные данные');
    }
}