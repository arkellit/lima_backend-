<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 10.12.18
 * Time: 18:21
 */

namespace api\modules\v1\controllers\order\orderApiClass;

use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\order\form\ResponseRequestOrder;
use api\modules\v1\models\order\Order;
use api\modules\v1\models\order\OrderPlace;
use api\modules\v1\models\order\OrderRequest;
use api\modules\v1\models\order\OrderStartEnd;
use api\modules\v1\models\Sending;
use api\modules\v1\models\order\OrderWorker;
use common\helpers\GetErrorsHelpers;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class responseRequest
 * @package api\modules\v1\controllers\order\orderApiClass
 */
class responseRequest extends Action
{
    /**
     * Function send notice to Customer or Worker
     *
     * @param $status_order
     * @param $is_lima_exchange
     * @param $order_id
     * @param $worker_id
     * @param $number_order
     * @param $order_request_id
     */
    protected function sendNotice($status_order, $is_lima_exchange, $order_id, $worker_id, $number_order, $order_request_id){
        switch ($status_order){
            case OrderRequest::STATUS_ACCEPT:
                if ($is_lima_exchange == Order::TYPE_LIMA_EXCHANGE){
                    Sending::sendNoticeCustomerRequest($order_id, 'Lima', 'Исполнитель согласился на Ваш заказ' . ' '. $number_order);
                }

                elseif ($is_lima_exchange == Order::TYPE_MANUAL_SELECTION) Sending::sendNoticeWorkerRequest($worker_id, 'Lima', 'Заказчик подтвердил Ваше участие в заказе' . ' '. $number_order, $order_request_id, $order_id);
                break;
            case OrderRequest::STATUS_NO_ACCEPT:
                if ($is_lima_exchange == Order::TYPE_LIMA_EXCHANGE){
                    Sending::sendNoticeCustomerRequest($order_id, 'Lima', 'Исполнитель отказался от Вашего заказа' . ' '. $number_order);
                }

                elseif ($is_lima_exchange == Order::TYPE_MANUAL_SELECTION) Sending::sendNoticeWorkerRequest($worker_id, 'Lima', 'Заказчик отклонил Ваше участие в заказе' . ' '. $number_order, $order_request_id, $order_id);
                break;
        }
    }

    /**
     * Function check access
     *
     * @param $order_request
     * @throws HttpException
     */
    protected function CheckAccess($order_request){
        $user_role = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());
        switch ($user_role){
            case array_key_exists('worker', $user_role):
                if ($order_request->worker_id != Yii::$app->user->identity->worker->id){
                    throw new HttpException(401, 'Доступ запрещен');
                }
                break;
            case array_key_exists('customer', $user_role):
                if ($order_request->order->customer_id != Yii::$app->user->identity->customer->id){
                    throw new HttpException(401, 'Доступ запрещен');
                }
                break;
            case array_key_exists('administrator', $user_role):
                $customer_personal = CustomerPersonal::findOne(['customer_id' => $order_request->order->customer_id,
                    'user_id' => Yii::$app->user->getId()]);
                if (empty($customer_personal)){
                    throw new HttpException(401, 'Доступ запрещен');
                }
                break;
            default:
                throw new HttpException(401, 'Доступ запрещен');
                break;
        }
    }

    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if (Yii::$app->user->isGuest){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $request = Yii::$app->request;
//        $id_request = $request->getBodyParam('id_request');
        $order_id = $request->getBodyParam('order_id');
        $worker_id = $request->getBodyParam('worker_id');
        $status = $request->getBodyParam('status');

        $response_request_model = new ResponseRequestOrder(['order_id' => $order_id,
            'worker_id' => $worker_id, 'status' => $status]);
        if (!$response_request_model->validate()){
            throw new HttpException(400, GetErrorsHelpers::getError($response_request_model->getErrors()));
        }

        $order_request = OrderRequest::findOne(['order_id' => $order_id, 'worker_id' => $worker_id]);
        $this->checkAccess($order_request);
        if (!empty($order_request) && isset($status)){
            $order_request->status = $status;
            $order_request->touch('updated_at');
            if (!$order_request->save()){
                $message = GetErrorsHelpers::getError($order_request->getErrors());
                throw new HttpException(400, $message);
            }

            if ($order_request->status == OrderRequest::STATUS_ACCEPT){
                $order_worker = new OrderWorker();
                $order_worker->order_id = $order_request->order_id;
                $order_worker->worker_id = $order_request->worker_id;
                $order_worker->order_place_id = $order_request->order_place_id;
                $order_worker->attention = OrderWorker::ATTENTION_DID_COME_WORK;
                if (!$order_worker->save()){
                    $message = GetErrorsHelpers::getError($order_worker->getErrors());
                    throw new HttpException(400, $message);
                }
                $order_request->trigger(OrderRequest::EVENT_ACCEPT_REQUEST_NOTICE);
            }
//            else {
//                $order_request->trigger(OrderRequest::EVENT_REJECT_REQUEST_NOTICE);
//            }

            $this->sendNotice($order_request->status, $order_request->order->type_order,
                $order_request->order_id, $order_request->worker_id, $order_request->order->number_order, $order_request->id);

            return array('success' => 1, 'data' => [
                'message' => 'Ответ на запрос принят',
                'status' => $order_request->status == OrderRequest::STATUS_ACCEPT ?
                    'Принял приглашение' : 'Отклонил приглашение',
                'number_order' => $order_request->order->number_order
            ], 'status' => 200);
        }

        throw new HttpException(400, 'Неверные данные');
    }
}