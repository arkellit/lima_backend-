<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 10.12.18
 * Time: 14:24
 */

namespace api\modules\v1\controllers\order\orderApiClass;

use api\modules\v1\models\order\Order;
use api\modules\v1\models\order\OrderPlace;
use api\modules\v1\models\order\OrderRequest;
use api\modules\v1\models\Sending;
use common\helpers\GetErrorsHelpers;
use common\helpers\MailerHelpers;
use Yii;
use yii\helpers\Url;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class addRequest
 * @package api\modules\v1\controllers\order\orderApiClass
 */
class addRequest extends Action
{
    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if (Yii::$app->user->isGuest){
            throw new HttpException(401, 'Доступ запрещен');
        }
        $request = Yii::$app->request;
        $order_request = new OrderRequest();
        if ($order_request->load($request->bodyParams, '')){
            $order_place = OrderPlace::findOne(['order_id' => $order_request->order_id]);
            $order_type = $order_place->order->type_order;
            $order_request->status = OrderRequest::STATUS_NEW;
            if (!empty($order_place)){
                $order_request->order_place_id = $order_place->id;
            }

            $order_request->is_lima_exchange = $order_type == OrderRequest::IS_LIMA_EXCHANGE
                ? OrderRequest::IS_LIMA_EXCHANGE : OrderRequest::NO_LIMA_EXCHANGE;

            if (!$order_request->save()){
                $message = GetErrorsHelpers::getError($order_request->getErrors());
                throw new HttpException(400, $message);
            }

            if ($order_type == Order::TYPE_LIMA_EXCHANGE){
                $order_request->trigger(OrderRequest::EVENT_SET_NEW_REQUEST_NOTICE);
                Sending::sendNoticeCustomerRequest($order_request->order_id, 'Lima', 'На Ваш заказал откликнулся исполнитель');
                $mailer = new MailerHelpers(null, 'Lima -  отлик нового исполнителя');
                $worker = $order_request->worker;
                $mailer->sendNewWorkerRequest(
                    $order_place->order->customer->companyData->email,
                    $order_place->order->number_order,
                    sprintf("%s %s", $worker->last_name, $worker->name),
                    Yii::$app->params['baseUrl'] . '/order-offer/offer?order_id='.$order_place->order_id);
//                    Url::to([Yii::$app->params['baseUrl'] . '/order-offer/offer', 'order_id' => $order_place->order_id], true));
            }
            elseif ($order_type == Order::TYPE_MANUAL_SELECTION) Sending::sendNoticeWorkerRequest($order_request->worker_id, 'Lima', 'Вы выбраны кандитатом на заказ', $order_request->id, $order_request->order_id);

            return array('success' => 1, 'data' => [
                'message' => 'Запрос успешно отправлен',
                'number_order' => $order_place->order->number_order],
                'status' => 200);
        }
        throw new HttpException(400, 'Неверные данные');
    }
}