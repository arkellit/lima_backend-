<?php


namespace api\modules\v1\controllers\order\orderApiClass;


use api\modules\v1\models\order\Order;
use api\modules\v1\models\order\OrderRequest;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;
use Yii;

class StartOrder extends Action
{
    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if (Yii::$app->user->isGuest){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $id = (integer)Yii::$app->request->get("id");
        $order = Order::find()
            ->where(['id' => $id])
            ->with(['orderPlace', 'customer'])
            ->one();
        if (!$order) throw new HttpException(400, 'Неверный запрос');

        $count_worker_complete_requests = OrderRequest::find()
            ->where(['order_place_id' => $order->orderPlace->id])
            ->andWhere(['status' => OrderRequest::STATUS_ACCEPT])
            ->all();
        if (count($count_worker_complete_requests) != 0){

        }

        return array('success' => 1, 'data' => $count_worker_complete_requests, 'status' => 200);
    }
}