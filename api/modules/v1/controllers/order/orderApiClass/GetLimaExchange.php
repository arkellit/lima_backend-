<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 12.12.18
 * Time: 13:44
 */

namespace api\modules\v1\controllers\order\orderApiClass;

use api\modules\v1\models\customer\CustomerCompanyData;
use api\modules\v1\models\order\Order;
use api\modules\v1\models\order\OrderAddress;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class GetLimaExchange
 * @package api\modules\v1\controllers\order\orderApiClass
 */
class GetLimaExchange extends Action
{
    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if (Yii::$app->user->isGuest){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $orders = Order::find()
            ->where(['is_complete_order' => Order::NO_COMPLETE_ORDER])
            ->andWhere(['type_order' => Order::TYPE_LIMA_EXCHANGE])
            ->andWhere(['is_pay' => Order::IS_PAY])
            ->asArray()
            ->with('orderPlace')
            ->limit(20)
            ->all();

        $new_orders = array();
        foreach ($orders as $order){
            $company_data = CustomerCompanyData::findOne(['customer_id' => $order['customer_id']]);
            $address = OrderAddress::findOne($order['orderPlace']['order_address_id']);

            unset($order['customer_id']);
            unset($order['type_order']);
            $order['price'] = $order['total_price'] / $order['orderPlace']['count_worker'];
            $order['logo_company'] = $company_data->logo_company;
            $order['name_company'] = $company_data->name_company;
            $order['date_time_start'] = $order['orderPlace']['date_time_start'];
            $order['date_time_end'] = $order['orderPlace']['date_time_end'];
            $order['count_hour'] = $order['orderPlace']['count_hour'];
            $order['city'] = $address->city->name;
            $order['branch'] = $address->branch_id != 0 ? $address->branch_id : null;
            $order['metro_station'] = $address->metro_station;
            $order['place_order'] = $address->place_order;
            $order['lat'] = $address->lat;
            $order['lng'] = $address->lng;
            unset($order['total_price']);
            unset($order['is_pay']);
            unset($order['is_complete_order']);
            unset($order['orderPlace']);
            array_push($new_orders, $order);
        }

        return array('success' => 1, 'data' => $new_orders, 'status' => 200);
    }
}