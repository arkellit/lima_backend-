<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 19.12.18
 * Time: 18:00
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerPersonal;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class GetPersonal
 * @package api\modules\v1\controllers\customer\customerApiClass
 */
class GetPersonal extends Action
{
    public function getCustomerId(){
        if (Yii::$app->user->can('customer')){
            $model_customer = Customer::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->id;
        }
        else {
            $model_customer = CustomerPersonal::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->customer_id;
        }
        return $customer_id;
    }

    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $branch = (integer)Yii::$app->request->get('branch_id');
        $offset = (integer)Yii::$app->request->get('offset');
        $limit = (integer)Yii::$app->request->get('limit');
        $branch_query = isset($branch) ? ['branch_id' => $branch] : [];
        $query = CustomerPersonal::find()
            ->where(['customer_id' => $this->getCustomerId()])
//            ->andWhere(['is_register' => CustomerPersonal::IS_REGISTER_COMPLETE])
            ->andWhere(['is_delete' => CustomerPersonal::NO_DELETE])
            ->andWhere($branch_query)
            ->with(['user'])
            ->asArray();
        $totalCount = $query->count();
        $customers = $query->offset($offset)->limit($limit)->all();
        $personal = array();
        foreach ($customers as $customer){
            if (!empty($customer)){
                $customer['phone'] = $customer['user']['phone'];
                $customer['username'] = $customer['last_name'] . ' ' . mb_substr($customer['name'], 0, 1) . '.' . mb_substr($customer['middle_name'], 0, 1);
                unset($customer['name']);
                unset($customer['last_name']);
                unset($customer['middle_name']);
                unset($customer['email']);
//                unset($customer['branch_id']);
                unset($customer['user']);
                unset($customer['user_id']);
                unset($customer['customer_id']);
                unset($customer['is_invite']);
                unset($customer['is_register']);
                array_push($personal, $customer);
            }
        }
        return array('success' => 1, 'data' => $personal,
            'totalCount' => $totalCount, 'currentOffset' => $offset,
            'status' => 200);
    }

}