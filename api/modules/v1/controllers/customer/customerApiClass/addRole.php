<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 05.12.18
 * Time: 13:23
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerPersonal;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/***
 * Class addRole
 * @package api\modules\v1\controllers\customer\customerApiClass
 */
class addRole extends Action
{
    public function getCustomerId(){
        if (Yii::$app->user->can('customer')){
            $model_customer = Customer::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->id;
        }
        else {
            $model_customer = CustomerPersonal::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->customer_id;
        }
        return $customer_id;
    }

    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }
        $request = Yii::$app->request;
        $id = $request->getBodyParam('id');
        $role = $request->getBodyParam('role');

        if (isset($id) && isset($role)){
            $model_personal = CustomerPersonal::findOne($id);
            $check_role = Yii::$app->authManager->getRolesByUser($model_personal->user_id);
            if (!empty($model_personal) && empty($check_role)){
                $userRole = Yii::$app->authManager->getRole($role);
                if (!$userRole){
                    throw new HttpException(400, 'Неверная роль');
                }
                $model_personal->role = $role;
                $model_personal->customer_id = $this->getCustomerId();
                if ($model_personal->save()){
                    Yii::$app->authManager->assign($userRole, $model_personal->user_id);
                    return array('success' => 1, 'data' => ['message' =>
                        'Роль успешно присвоена',
                        'role' => $userRole->name], 'status' => 200);
                }
            }
        }
        throw new HttpException(400, 'Неверные данные');
    }
}