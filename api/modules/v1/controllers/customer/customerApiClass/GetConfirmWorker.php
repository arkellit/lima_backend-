<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 26.02.19
 * Time: 18:17
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\order\OrderWorker;
use api\modules\v1\models\worker\ImageWorker;
use api\modules\v1\models\worker\Worker;
use common\helpers\CustomerHelpers;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

class GetConfirmWorker extends Action
{
    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $order_id = (double)Yii::$app->request->get('order_id');
        $customer_id = CustomerHelpers::getCustomerId(Yii::$app->user->getId());
        $order_workers = OrderWorker::find()
            ->select(['order_worker.order_id', 'order_worker.status',
                'order_worker.worker_id', 'order.customer_id',
                'worker.id', 'worker.birth_date', 'worker.rating',
                'worker.name', 'worker.last_name', 'worker.middle_name',
                "date_part('year', age('today'::date, worker.birth_date)) as age"])
            ->where(['order_id' => $order_id])
            ->andWhere(['order.customer_id' => $customer_id])
            ->andWhere(['NOT', ['order_worker.status' => OrderWorker::STATUS_WORKER_CANCEL]])
            ->joinWith(['order', 'worker'])
            ->asArray()
            ->all();
        if (!empty($order_workers)){
            $new_order_request = array();
            foreach ($order_workers as $order_worker) {
                $worker_photo = ImageWorker::find()
                    ->where(['worker_id' => $order_worker['worker_id']])
                    ->asArray()
                    ->one();
//                $order_worker['username'] = $order_worker['last_name'] . ' ' . $order_worker['name'];
                $order_worker['rating'] = Worker::getRating($order_worker['rating']);
                $order_worker['photo_porter'] = $worker_photo['photo_porter'];
                $order_worker['age'] = (int)$order_worker['age'];
                unset($order_worker['order_id']);
                unset($order_worker['worker_id']);
                unset($order_worker['customer_id']);
                unset($order_worker['birth_date']);
                unset($order_worker['order']);
                unset($order_worker['worker']);
                array_push($new_order_request, $order_worker);
            }
            return array('success' => 1, 'data' => $new_order_request, 'status' => 200);
        }
        return array('success' => 1, 'data' => [], 'status' => 200);
    }
}