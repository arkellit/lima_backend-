<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 05.12.18
 * Time: 15:48
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\auth\RegistrationForm;
use api\modules\v1\models\auth\Token;
use api\modules\v1\models\auth\User;
use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerBranch;
use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\Sending;
use common\helpers\GetErrorsHelpers;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class CreateInvitePersonal
 * @package api\modules\v1\controllers\customer\customerApiClass
 */
class CreateInvitePersonal extends Action
{
    public function getCustomerId(){
        if (Yii::$app->user->can('customer')){
            $model_customer = Customer::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->id;
        }
        else {
            $model_customer = CustomerPersonal::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->customer_id;
        }
        return $customer_id;
    }

    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }
        $request = Yii::$app->request;
        $phone = $request->getBodyParam('phone');
        $password = Yii::$app->security->generateRandomString(6);
        $role = $request->getBodyParam('role');
        $model = new RegistrationForm(['phone' => $phone,
            'password' => $password, 'role' => $role]);
        $user = $model->createPersonal($is_invite = true);

        if($user == null){
            $message = GetErrorsHelpers::getError($model->getErrors());
            throw new HttpException(400, $message);
        }

        $create_personal = new CustomerPersonal();
        $create_personal->scenario = CustomerPersonal::SCENARIO_INVITE;
        $create_personal->user_id = $user['id'];
        $create_personal->customer_id = $this->getCustomerId();
        $create_personal->role = $role;
        if(!$create_personal->save()){
            Token::deleteAll(['user_id' => $user['id']]);
            User::deleteAll(['id' => $user['id']]);
            $message = GetErrorsHelpers::getError($create_personal->getErrors());
            throw new HttpException(400, $message);
        }

        $create_personal->trigger(CustomerPersonal::EVENT_CONFIRMED_CUSTOMER_PERSONAL_NOTICE);

        $userRole = Yii::$app->authManager->getRole($role);
        if (!$userRole){
            Token::deleteAll(['user_id' => $user['id']]);
            User::deleteAll(['id' => $user['id']]);
            throw new HttpException(400, 'Произошла ошибка при добавлении роли');
        }
        Yii::$app->authManager->assign($userRole, $user['id']);

        Sending::sendLoginToSms($model->phone, $model->password);
        return array('success' => 1, 'data' => ['message' => 'Инвайт успешно отправлен',
                'role' => $role, 'password' => $password, 'phone' => $model->phone], 'status' => 200);
    }
}