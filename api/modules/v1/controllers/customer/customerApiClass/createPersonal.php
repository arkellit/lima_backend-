<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 04.12.18
 * Time: 17:24
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\auth\User;
use api\modules\v1\models\customer\CustomerPersonal;
use common\helpers\GetErrorsHelpers;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

class createPersonal extends Action
{
    public function checkRole(){
        switch (true){
            case Yii::$app->user->isGuest:
                throw new HttpException(401, 'Доступ запрещен');
            case Yii::$app->user->can('worker'):
                throw new HttpException(401, 'Доступ запрещен');
            case Yii::$app->user->can('customer'):
                throw new HttpException(401, 'Доступ запрещен');
        }
    }

    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $this->checkRole();

        $request = Yii::$app->request;
        $new_password = $request->getBodyParam('new_password');
        $user_id = Yii::$app->user->getId();

        $model_customer = CustomerPersonal::findOne(['user_id' => $user_id]);
        $model_customer->scenario = CustomerPersonal::SCENARIO_NO_INVITE;
        if (!empty($model_customer) && $model_customer->load($request->bodyParams, '')){
            $model_customer->user_id = $user_id;
            $model_customer->is_register = CustomerPersonal::IS_REGISTER_COMPLETE;
            if (!$model_customer->save()){
                $message = GetErrorsHelpers::getError($model_customer->getErrors());
                throw new HttpException(400, $message);
            }

            if (!is_null($new_password)){
                $user = User::findOne($user_id);
                $user->setPassword($new_password);
                $user->save();
            }

            $userRole = Yii::$app->authManager->getRolesByUser($user_id);
            $keys = array_keys($userRole);
            $role = $keys[0];

            return array('success' => 1, 'data' => ['message' => 'Данные успешно сохранены',
                'role' => $role], 'status' => 200);
        }
        throw new HttpException(400, 'Неверные данные');
    }
}