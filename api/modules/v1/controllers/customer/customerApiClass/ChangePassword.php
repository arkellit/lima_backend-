<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 19.12.18
 * Time: 16:13
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\auth\ChangeUser;
use api\modules\v1\models\auth\User;
use common\helpers\ChangeUserHelpers;
use common\helpers\DifferenceTimeHelpers;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class ChangePassword
 * @package api\modules\v1\controllers\customer\customerApiClass
 */
class ChangePassword extends Action
{
    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $request = Yii::$app->request;
        $password = $request->getBodyParam('password');
        $new_password = $request->getBodyParam('new_password');

        $user = User::findOne(Yii::$app->user->getId());

        if(is_null($user)){
            throw new HttpException(400, 'Пользователь не найден');
        }

        $change_user = ChangeUser::findOne(['user_id' => $user->id]);
        if (!empty($change_user) && DifferenceTimeHelpers::differenceTime($change_user->updated_at) < 86400){
            throw new HttpException(423, 'Пароль можно изменить один раз в день');
        }

        if (!is_null($new_password) && $user->validatePassword($password)){
            $user->setPassword($new_password);
            if(!$user->save()){
                throw new HttpException(426, 'Не удалось изменить пароль');
            }

            if(empty($change_user)){
                $change = ChangeUserHelpers::create($user->id, ChangeUser::TYPE_CHANGE_PASSWORD);
                if($change){
                    return array('success' => 1, 'data' => ['message' => 'Пароль успешно изменен',
                        'token' => $user->access_token], 'status' => 200);
                }
            }

            $change_user->type = ChangeUser::TYPE_CHANGE_PASSWORD;
            $change_user->touch('updated_at');

            if(!$change_user->save()){
                throw new HttpException(400, 'Неверные данные');
            }

            $user->access_token = User::setAccessToken($user->phone);
            if(!$user->save()){
                throw new HttpException(400, 'Не удалось сохранить новый token');
            }

            return array('success' => 1, 'data' => ['message' => 'Пароль успешно изменен',
                'token' => $user->access_token], 'status' => 200);
        }
        throw new HttpException(400, 'Неверные данные');
    }
}