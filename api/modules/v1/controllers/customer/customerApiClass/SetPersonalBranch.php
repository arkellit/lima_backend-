<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 22.01.19
 * Time: 18:18
 */

namespace api\modules\v1\controllers\customer\customerApiClass;


use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerPersonal;
use common\helpers\GetErrorsHelpers;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

class SetPersonalBranch extends Action
{
    public function getCustomerId(){
        if (Yii::$app->user->can('customer')){
            $model_customer = Customer::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->id;
        }
        else {
            $model_customer = CustomerPersonal::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->customer_id;
        }
        return $customer_id;
    }

    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $request = Yii::$app->request;
        $personal_ids = $request->getBodyParam('personal_ids');
        $branch_id = $request->getBodyParam('branch_id');
        $customer_id = $this->getCustomerId();
        if (!empty($personal_ids)){
            foreach ($personal_ids as $item){
                $customer_personal = CustomerPersonal::findOne(['id' => $item,
                'customer_id' => $customer_id]);
                if (!empty($customer_personal)){
                    $customer_personal->branch_id = $branch_id;
                    if (!$customer_personal->save()){
                        throw new HttpException(400,  GetErrorsHelpers::getError($customer_personal->getErrors()));
                    }
                }
            }
            return array('success' => 1, 'data' => ['message' => 'Данные успешно сохранены'],
                'status' => 200);
        }
        throw new HttpException(400, 'Неверные данные');
    }
}