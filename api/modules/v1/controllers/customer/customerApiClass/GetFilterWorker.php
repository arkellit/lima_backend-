<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 26.12.18
 * Time: 18:44
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\worker\Worker;
use api\modules\v1\models\worker\WorkerSearch;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class GetFilterWorker
 * @package api\modules\v1\controllers\customer\customerApiClass
 */
class GetFilterWorker extends Action
{
    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }
        $request_ids = Yii::$app->request->get();
//        $model_params = array_merge($request_ids, ['type_request' => WorkerSearch::SEARCH]);
        $worker_search = new WorkerSearch($request_ids);
        $model_search = $worker_search->search($request_ids);
        $new_model_search = array();
        foreach ($model_search['array_query'] as $item){
            unset($item['user_id']);
            unset($item['sex']);
            unset($item['birth_date']);
            unset($item['height']);
            unset($item['weight']);
            unset($item['body_complexion_id']);
            unset($item['hair_color_id']);
            unset($item['hair_length_id']);
            unset($item['appearance_id']);
            unset($item['eye_color_id']);
            unset($item['city_id']);
            unset($item['last_location_lat']);
            unset($item['last_location_lng']);
            unset($item['is_register_complete']);
            unset($item['is_instruction_complete']);
            unset($item['point']);
            unset($item['radius_search']);
            $item['photo_porter'] = $item['imageWorker']['photo_porter'];
            $item['rating'] = Worker::getRating($item['rating']);
            $item['age'] = (integer)$item['age'];
            unset($item['imageWorker']);
            unset($item['workerProfession']);
            unset($item['orderRequest']);
            array_push($new_model_search, $item);
        }
        return array('success' => 1, 'data' => $new_model_search,
            'totalCount' => $model_search['totalCount'],
            'currentOffset' => (integer)$request_ids['offset'],
            'status' => 200
        );
    }
}