<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 25.01.19
 * Time: 12:44
 */

namespace api\modules\v1\controllers\customer\customerApiClass;


use api\modules\v1\models\auth\RegistrationForm;
use api\modules\v1\models\auth\Token;
use api\modules\v1\models\auth\User;
use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\Sending;
use common\helpers\GetErrorsHelpers;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class CreateNewPersonal
 * @package api\modules\v1\controllers\customer\customerApiClass
 */
class CreateNewPersonal extends Action
{
    public function getCustomerId(){
        if (Yii::$app->user->can('customer')){
            $model_customer = Customer::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->id;
        }
        else {
            $model_customer = CustomerPersonal::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->customer_id;
        }
        return $customer_id;
    }

    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }
        $request = Yii::$app->request;

        $model = new RegistrationForm();

        $phone_replace = $request->getBodyParam('phone');
        $model->phone = User::normalizePhone($phone_replace);
        $model->password = $request->getBodyParam('password');
        $user = $model->createPersonal($is_invite = null);
        if($user == null){
            $message = GetErrorsHelpers::getError($model->getErrors());
            throw new HttpException(400, $message);
        }

        $model_customer = new CustomerPersonal();
        $model_customer->scenario = CustomerPersonal::SCENARIO_NO_INVITE;
        if ($model_customer->load($request->bodyParams, '')){
            $model_customer->user_id = $user['id'];
            $model_customer->customer_id = $this->getCustomerId();
            $model_customer->is_register = CustomerPersonal::IS_REGISTER_COMPLETE;
            if (!$model_customer->save()){
                $message = GetErrorsHelpers::getError($model_customer->getErrors());
                Token::deleteAll(['user_id' => $user['id']]);
                User::deleteAll(['id' => $user['id']]);
                throw new HttpException(400, $message);
            }
            $userRole = Yii::$app->authManager->getRole($model_customer->role);
            if (!$userRole){
                $model_customer->delete();
                Token::deleteAll(['user_id' => $user['id']]);
                User::deleteAll(['id' => $user['id']]);
                throw new HttpException(400, 'Произошла ошибка при добавлении роли');
            }
            Yii::$app->authManager->assign($userRole, $user['id']);

            if (!is_null($model_customer->email)) Sending::sendLoginToEmail($model_customer->email, $model->phone, $model->password);

            Sending::sendLoginToSms($model->phone, $model->password);

            $model_customer->trigger(CustomerPersonal::EVENT_CONFIRMED_CUSTOMER_PERSONAL_NOTICE);

            return array('success' => 1, 'data' => ['message' => 'Пользователь успешно добавлен',
                'role' => $userRole->name], 'status' => 200);
        }
        throw new HttpException(400, 'Неверные данные');
    }
}