<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 19.12.18
 * Time: 15:52
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\auth\User;
use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerCompanyData;
use api\modules\v1\models\customer\CustomerPersonal;
use common\helpers\GetErrorsHelpers;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class EditProfile
 * @package api\modules\v1\controllers\customer\customerApiClass
 */
class EditProfile extends Action
{
    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $request = Yii::$app->request;
        $phone_request = $request->getBodyParam('phone');
        $phone = User::normalizePhone($phone_request);
        $user_id = Yii::$app->user->getId();
        $model_user = User::findOne($user_id);
        if (array_key_exists('customer', Yii::$app->authManager->getRolesByUser($user_id))){
            $model_customer = Customer::findOne(['user_id' =>$user_id]);
            if ($model_customer->load($request->bodyParams, '')){
                if (!$model_customer->save()){
                    $message = GetErrorsHelpers::getError($model_customer->getErrors());
                    throw new HttpException(400, $message);
                }
            }

            $email_request = $request->getBodyParam('email');
            if (isset($email_request) && !is_null($email_request)){
                $model_company_data = CustomerCompanyData::findOne(['customer_id' =>
                    $model_customer->id]);
                $model_company_data->email = $email_request;
                if (!$model_company_data->save()){
                    $message = GetErrorsHelpers::getError($model_company_data->getErrors());
                    throw new HttpException(400, $message);
                }
            }
        }
        else {
            $model_customer = CustomerPersonal::findOne(['user_id' => $user_id]);
            if ($model_customer->load($request->bodyParams, '')){
                if (!$model_customer->save()){
                    $message = GetErrorsHelpers::getError($model_customer->getErrors());
                    throw new HttpException(400, $message);
                }
            }
        }

        if (isset($phone_request) && !is_null($phone)){
            $model_user->phone = $phone;
            if (!$model_user->save()){
                $message = GetErrorsHelpers::getError($model_user->getErrors());
                throw new HttpException(400, $message);
            }
        }
        return array('success' => 1, 'data' => ['message' => 'Данные успешно обновлены'], 'status' => 200);
    }
}