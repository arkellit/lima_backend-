<?php

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\Api;
use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerBankData;
use api\modules\v1\models\customer\CustomerCompanyData;
use common\helpers\EncryptionHelpers;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use common\helpers\GetErrorsHelpers;

class create extends Action
{
    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if (Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $request = Yii::$app->request;
        $user_id = Yii::$app->user->getId();

        $model_customer = new Customer();

        if ($model_customer->load($request->bodyParams, '')){
            $model_customer->user_id = $user_id;
            if (!$model_customer->save()){
                $message = GetErrorsHelpers::getError($model_customer->getErrors());
                throw new HttpException(400, $message);
            }

            $model_company_customer = new CustomerCompanyData();
            if ($model_company_customer->load($request->bodyParams, '')){
                $model_company_customer->customer_id = $model_customer->id;
                if(!$model_company_customer->save()){
                    $model_customer->delete();
                    $message = GetErrorsHelpers::getError($model_company_customer->getErrors());
                    throw new HttpException(400, $message);
                }
            }

            $model_bank_customer = new CustomerBankData();
            if ($model_bank_customer->load($request->bodyParams, '')){
                $model_bank_customer->customer_id = $model_customer->id;
                if (!$model_bank_customer->save()){
                    $model_company_customer->delete();
                    $model_customer->delete();
                    $message = GetErrorsHelpers::getError($model_bank_customer->getErrors());
                    throw new HttpException(400, $message);
                }
            }

//            Customer::updateAll(['is_register_complete' => Customer::IS_REGISTER_COMPLETE_TRUE]
//                , ['id' => $model_customer->id]);

            $userRole = Yii::$app->authManager->getRole('customer');
            $create_role = Yii::$app->authManager->assign($userRole, $user_id);
            if (!$create_role){
                $model_company_customer->delete();
                $model_bank_customer->delete();
                $model_customer->delete();
                throw new HttpException(400, 'Произошла ошибка при добавлении роли');
            }

            $model_customer->trigger(Customer::EVENT_CONFIRMED_CUSTOMER_NOTICE);

            return array('success' => 1, 'data' => ['message' => 'Заказчик успешно добавлен',
                'role' => $userRole->name], 'status' => 200);
        }

        throw new HttpException(400, 'Неверные данные');
    }
}