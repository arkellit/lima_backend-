<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 29.01.19
 * Time: 16:51
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\customer\form\ReadQrCodeForm;
use api\modules\v1\models\order\Order;
use api\modules\v1\models\order\OrderPlace;
use api\modules\v1\models\order\OrderWorker;
use api\modules\v1\models\Sending;
use api\modules\v1\models\worker\Worker;
use common\helpers\DifferenceTimeHelpers;
use common\helpers\FirebaseHelpers;
use common\helpers\GetErrorsHelpers;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class ReadQrCode
 * @package api\modules\v1\controllers\customer\customerApiClass
 */
class ReadQrCode extends Action
{
    public function getCustomerId(){
        if (array_key_exists('customer', Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId()))){
            $model_customer = Customer::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->id;
        }
        else {
            $model_customer = CustomerPersonal::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->customer_id;
        }
        return $customer_id;
    }

    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }
        $request = Yii::$app->request;
        $worker_id = $request->getBodyParam('worker_id');
        $order_id = $request->getBodyParam('order_id');
        $worker = Worker::find()
            ->select(['id', 'user_id', 'name', 'last_name', 'middle_name', 'rating',
                'birth_date', "date_part('year', age('today'::date, worker.birth_date)) as age"])
            ->where(['id' => $worker_id])
            ->joinWith('imageWorker')
            ->asArray()
            ->one();

        $model_read_qr_code = new ReadQrCodeForm(['worker_id' => $worker_id,
            'order_id' => $order_id, 'customer_id' => $this->getCustomerId()]);
        if (!$model_read_qr_code->validate()){
            $message =  GetErrorsHelpers::getError($model_read_qr_code->getErrors());
            throw new HttpException(400, $message);
        }

        $date_time_start = $model_read_qr_code->_order_accept->date_time_start;

        $order_worker_model = OrderWorker::findOne(['worker_id' => $worker_id,
            'order_id' => $order_id]);
        if ($model_read_qr_code->_type == 0){
            //Если случайно два раза отсканировали, и заказ не стартанул
            if ($model_read_qr_code->_order_start_end == OrderWorker::STATUS_START_JOB){
                Sending::sendNoticeForCloseQr($worker['user_id'], FirebaseHelpers::CONFIRM_START_ORDER);
                return array('success' => 1, 'data' =>[
                    'status' => 'confirm',
                    'id' => $worker_id,
                    'rating' => Worker::getRating($worker['rating']),
                    'age' => (int)$worker['age'],
                    'name' => $worker['name'],
                    'last_name' => $worker['last_name'],
                    'middle_name' => $worker['middle_name'],
                    'photo_porter' => $worker['imageWorker']['photo_porter']
                ],
                    'status' => 200);
            }
            switch (true){
                case (DifferenceTimeHelpers::differenceTime($date_time_start) > 0) &&
                    (DifferenceTimeHelpers::differenceTime($date_time_start) < 600):
                    $attention = OrderWorker::ATTENTION_BEING_LATE_LESS_10;
                    break;
                case DifferenceTimeHelpers::differenceTime($date_time_start) > 600:
                    $attention = OrderWorker::ATTENTION_BEING_LATE_MORE_10;
                    break;
                default:
                    $attention = OrderWorker::ATTENTION_NORM;
                    break;
            }
            $order_worker_model->status = OrderWorker::STATUS_START_JOB;
            $order_worker_model->attention = $attention;
            if ($order_worker_model->save()){
                Sending::sendNoticeForCloseQr($worker['user_id'], FirebaseHelpers::CONFIRM_START_ORDER);
                return array('success' => 1, 'data' =>[
                    'status' => 'confirm',
                    'id' => $worker_id,
                    'rating' => Worker::getRating($worker['rating']),
                    'age' => (int)$worker['age'],
                    'name' => $worker['name'],
                    'last_name' => $worker['last_name'],
                    'middle_name' => $worker['middle_name'],
                    'photo_porter' => $worker['imageWorker']['photo_porter']
                ],
                    'status' => 200);
            }
        }
        elseif ($model_read_qr_code->_type == 1){
            $order_worker_model->status = OrderWorker::STATUS_END_JOB;
            if ($order_worker_model->save()){
                Sending::sendNoticeForCloseQr($worker['user_id'], FirebaseHelpers::CONFIRM_END_ORDER);
                return array('success' => 1, 'data' =>[
                    'status' => 'finished_job',
                    'id' => $worker_id,
                    'rating' => Worker::getRating($worker['rating']),
                    'age' => (int)$worker['age'],
                    'name' => $worker['name'],
                    'last_name' => $worker['last_name'],
                    'middle_name' => $worker['middle_name'],
                    'photo_porter' => $worker['imageWorker']['photo_porter']
                ],
                    'status' => 200);
            }
        }


        throw new HttpException(400, 'Неверные данные');

//        $worker = Worker::findOne($worker_id);
//
//        $model_accept = $model_read_qr_code->_order_accept;
//        $model_type = $model_read_qr_code->_is_type;
//        if ($model_type == ReadQrCodeForm::START_JOB){
//            $model_accept->is_start_job = 1;
//            if ($model_accept->save()){
//                Sending::sendNotice($worker->user_id, 'Lima', 'Заказ успешно стартовал');
//                return array('success' => 1 , 'data' => [
//                    'message' => 'Заказ успешно стартовал',
//                    'type' => 'startOrder'],
//                    'status' => 200);
//            }
//        }
//        elseif ($model_type == ReadQrCodeForm::END_JOB){
//            $model_accept->is_end_job = 1;
//            $model_accept->is_finished_job = 1;
//
//            $model_order = Order::findOne($order_id);
//            $model_order->is_complete_order = Order::COMPLETE_ORDER;
//            if ($model_accept->save() && $model_order->save()){
//                Sending::sendNotice($worker->user_id, 'Lima', 'Заказ № ' . $model_order->number_order . ' успешно завершен');
//                return array('success' => 1 , 'data' => [
//                    'message' => 'Заказ успешно завершен',
//                    'type' => 'endOrder'],
//                    'status' => 200);
//            }
//        }
        //throw new HttpException(400, 'Неверные данные');
    }
}