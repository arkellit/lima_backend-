<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 26.02.19
 * Time: 15:47
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\order\OrderRequest;
use api\modules\v1\models\order\OrderWorker;
use common\helpers\CustomerHelpers;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class RemoveFromRequest
 * @package api\modules\v1\controllers\customer\customerApiClass
 */
class RemoveWorkerFromRequest extends Action
{
    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $request = Yii::$app->request;
        $customer_id = CustomerHelpers::getCustomerId(Yii::$app->user->getId());
        $order_id = $request->getBodyParam('order_id');
        $worker_id = $request->getBodyParam('worker_id');

        $order_request = OrderRequest::find()
            ->joinWith('order')
            ->where(['order_id' => $order_id])
            ->andWhere(['worker_id' => $worker_id])
            ->andWhere(['order.customer_id' => $customer_id])
            ->one();
        if (!empty($order_request)){
            $order_request->is_show_in_filter = OrderRequest::NO_SHOW_IN_FILTER;
            $order_request->status = OrderRequest::STATUS_NO_ACCEPT;
            OrderWorker::deleteAll(['order_id' => $order_id, 'worker_id' => $worker_id]);
            if ($order_request->save()){
                return array('success' => 1, 'data' => ['message' =>
                    'Исполнитель удален'], 'status' => 200);
            }
        }

        throw new HttpException(400, 'Неверные данные');
    }
}