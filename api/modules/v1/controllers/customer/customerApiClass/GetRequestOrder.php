<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 14.12.18
 * Time: 15:07
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\order\Order;
use api\modules\v1\models\order\OrderAddress;
use api\modules\v1\models\order\OrderRequest;
use api\modules\v1\models\worker\Worker;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class GetRequestOrder
 * @package api\modules\v1\controllers\customer\customerApiClass
 */
class GetRequestOrder extends Action
{
    public function getOrdersRequest($order, $order_id, $branch_id){
        if (Yii::$app->user->can('customer')){
            if ($order->customer_id == Yii::$app->user->identity->customer->id){
                $order_request = OrderRequest::find()
                    ->where(['is_lima_exchange' => OrderRequest::IS_LIMA_EXCHANGE])
//                ->andWhere(['customer_id' => $customer_id])
                    ->andWhere(['order_id' => $order_id])
                    ->andWhere(['status' => OrderRequest::STATUS_NEW])
                    ->with(['worker', 'workerImage'])
                    ->asArray()
                    ->all();
                return $order_request;
            }
            throw new HttpException(401, 'Доступ запрещен');
        }
        elseif (Yii::$app->user->can('administrator') && $branch_id != 0) {
            $customer_personal = CustomerPersonal::findOne(['branch_id' => $branch_id,
                'role' => CustomerPersonal::ROLE_ADMINISTRATOR]);
            if (!empty($customer_personal) && $order->customer_id == $customer_personal->customer_id){
                $orders = Order::findAll($order_id);
                $order_ids = [];
                foreach ($orders as $order){
                    if ($customer_personal->branch_id == $branch_id){
                        array_push($order_ids, $order->id);
                    }
                }
                $order_request = OrderRequest::find()
                    ->where(['is_lima_exchange' => OrderRequest::IS_LIMA_EXCHANGE])
//                ->andWhere(['customer_id' => $customer_id])
                    ->andWhere(['order_id' => $order_ids])
                    ->andWhere(['status' => OrderRequest::STATUS_NEW])
                    ->with(['worker', 'workerImage'])
                    ->asArray()
                    ->all();
                return $order_request;
            }
            throw new HttpException(401, 'Доступ запрещен');
        }
        return [];
    }

    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $order_id = (integer)Yii::$app->request->get('order_id');
        $order = Order::findOne($order_id);
        $order_address = OrderAddress::findOne($order->orderPlace->order_address_id);

        $order_requests = $this->getOrdersRequest($order, $order->id,
            $order_address->branch_id);
        $new_order_request = array();
        foreach ($order_requests as $order_request){
            unset($order_request['order_place_id']);
            unset($order_request['is_lima_exchange']);
            unset($order_request['status']);
            unset($order_request['created_at']);
            unset($order_request['updated_at']);
            $order_request['username'] = $order_request['worker']['last_name'] . ' ' . $order_request['worker']['name'];
            $order_request['age'] = Worker::getAge($order_request['worker']['birth_date']);
            $order_request['rating'] = Worker::getRating($order_request['worker']['rating']);
            $order_request['photo_porter'] = $order_request['workerImage']['photo_porter'];
            unset($order_request['worker']);
            unset($order_request['workerImage']);
            array_push($new_order_request, $order_request);
        }
        return array('success' => 1, 'data' => $new_order_request, 'status' => 200);
    }
}