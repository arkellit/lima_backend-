<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 07.12.18
 * Time: 14:21
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerPersonal;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class getProfile
 * @package api\modules\v1\controllers\customer\customerApiClass
 */
class getProfile extends Action
{
    public function getCustomer(){
        if (array_key_exists('customer', Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId()))){
            $model_customer = Customer::getCustomerProfile();
            return $model_customer;
        }
        else {
            $model_customer = CustomerPersonal::getCustomerPersonalProfile();
        }
        return $model_customer;
    }

    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        return array('success' => 1, 'data' => $this->getCustomer());
    }
}