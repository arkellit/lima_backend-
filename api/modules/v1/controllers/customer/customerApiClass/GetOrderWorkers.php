<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 14.02.19
 * Time: 14:12
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\order\OrderWorker;
use api\modules\v1\models\worker\ImageWorker;
use api\modules\v1\models\worker\Worker;
use common\helpers\CustomerHelpers;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class GetOrderWorkers
 * @package api\modules\v1\controllers\customer\customerApiClass
 */
class GetOrderWorkers extends Action
{
    const CONFIRM_WORKER = 'confirm';
    const NO_CONFIRM = 'no_confirm';
    const FINISHED_JOB = 'finished_job';

    public static function getStatus($status)
    {
        switch ($status) {
            case OrderWorker::STATUS_NEW:
                $new_status = self::NO_CONFIRM;
                break;
            case OrderWorker::STATUS_START_JOB:
                $new_status = self::CONFIRM_WORKER;
                break;
            case OrderWorker::STATUS_END_JOB:
                $new_status = self::FINISHED_JOB;
                break;
            default:
                $new_status = self::NO_CONFIRM;
                break;
        }
        return $new_status;
    }

    public function run()
    {
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $user_id = Yii::$app->user->getId();
        if (Yii::$app->user->isGuest || array_key_exists('worker', Yii::$app->authManager->getRolesByUser($user_id))) {
            throw new HttpException(401, 'Доступ запрещен');
        }
        $customer_id = CustomerHelpers::getCustomerId($user_id);
        $order_id = (double)Yii::$app->request->get('order_id');
        $order_workers = OrderWorker::find()
            ->select(['order_worker.order_id', 'order_worker.status',
                'order_worker.worker_id', 'order.customer_id',
                'worker.id', 'worker.birth_date', 'worker.rating',
                'worker.name', 'worker.last_name', 'worker.middle_name',
                "date_part('year', age('today'::date, worker.birth_date)) as age"])
            ->where(['order_id' => $order_id])
            ->andWhere(['order.customer_id' => $customer_id])
            ->joinWith(['order', 'worker'])
            ->asArray()
            ->all();
        if (!empty($order_workers)) {
            $new_order_request = array();
            foreach ($order_workers as $order_worker) {
                $worker_photo = ImageWorker::find()
                    ->where(['worker_id' => $order_worker['worker_id']])
                    ->asArray()
                    ->one();
//                $order_worker['name'] = $order_worker['name'];
//                $order_worker['last_name'] = $order_worker['last_name'];
//                $order_worker['middle_name']
                $order_worker['rating'] = Worker::getRating($order_worker['rating']);

                $order_worker['status'] = $this::getStatus($order_worker['status']);
                $order_worker['photo_porter'] = $worker_photo['photo_porter'];
                $order_worker['age'] = (int)$order_worker['age'];
                unset($order_worker['order_id']);
                unset($order_worker['worker_id']);
                unset($order_worker['customer_id']);
                unset($order_worker['birth_date']);
//                unset($order_worker['name']);
//                unset($order_worker['last_name']);
                unset($order_worker['order']);
                unset($order_worker['worker']);
                array_push($new_order_request, $order_worker);
            }
            return array('success' => 1, 'data' => $new_order_request, 'status' => 200);
        }
        return array('success' => 1, 'data' => [], 'status' => 200);
    }
}