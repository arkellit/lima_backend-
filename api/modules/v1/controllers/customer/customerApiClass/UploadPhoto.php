<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 19.12.18
 * Time: 16:24
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\Upload;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class UploadPhoto
 * @package api\modules\v1\controllers\customer\customerApiClass
 */
class UploadPhoto extends Action
{
    public function getCustomer(){
        if (Yii::$app->user->can('customer')){
            $model_customer = Customer::findOne(['user_id' => Yii::$app->user->getId()]);
        }
        else {
            $model_customer = CustomerPersonal::findOne(['user_id' => Yii::$app->user->getId()]);
        }
        return $model_customer;
    }

    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $customer = $this->getCustomer();
        $customer_photo = $customer->photo;
        $request = Yii::$app->request;
        $photo = $request->getBodyParam('photo');
        if (!is_null($photo)){
            $upload_image = new Upload();
            $upload = $upload_image->upload('customer', $photo);
            if (!is_null($upload)){
                $customer->photo = $upload;
                if ($customer->save()){
                    if (file_exists(Yii::getAlias('@image/web' . $customer_photo))){
                        unlink(Yii::getAlias('@image/web' . $customer_photo));
                    }
                    return array('success' => 1, 'data' => ['message' => 'Фото успешно добавлено'], 'status' => 200);
                }
            }
        }
        throw new HttpException(400, 'Не удалось загрузить фото');
    }
}