<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 16.01.19
 * Time: 17:48
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\order\Order;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class MakeDeposit
 * @package api\modules\v1\controllers\customer\customerApiClass
 */
class MakeDeposit extends Action
{
    public function getCustomerId(){
        if (Yii::$app->user->can('customer')){
            $model_customer = Customer::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->id;
        }
        else {
            $model_customer = CustomerPersonal::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->customer_id;
        }
        return $customer_id;
    }

    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if (Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $amount = Yii::$app->request->getBodyParam('amount');
        if ($amount <= 0) {
            throw new HttpException(400, 'Неверная сумма');
        }

        /** TODO удалить на прод! */
        $customer = Customer::findOne($this->getCustomerId());
        $customer_company = $customer->companyData;
        $customer_company->deposit += $amount;
        $customer_company->save();

        $orders = Order::findAll(['customer_id' => $this->getCustomerId(), 'is_pay' => 0]);
        if (!empty($orders)){
            foreach ($orders as $order) {
                $customer_company->deposit -= $order->total_price;
                $order->is_pay = 1;
                $order->save();
                $customer_company->save();
            }
        }


        //
        /**
         * TODO отправить заявку на E-mail
         */
        return array(
            'success' => 1,
            'data' => [
                'message' => 'Счет на оплату отправлен',
                'email' => $customer->companyData->email
            ],
            'status' => 200
        );
    }
}