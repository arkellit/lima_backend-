<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 26.02.19
 * Time: 16:24
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\order\OrderRequest;
use api\modules\v1\models\order\OrderWorker;
use api\modules\v1\models\Sending;
use common\helpers\CustomerHelpers;
use common\helpers\GetErrorsHelpers;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class ConfirmWorkerRequest
 * @package api\modules\v1\controllers\customer\customerApiClass
 */
class ConfirmWorkerRequest extends Action
{
    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $request = Yii::$app->request;
        $order_id = $request->getBodyParam('order_id');
        $worker_ids = $request->getBodyParam('worker_arrays');
        $customer_id = CustomerHelpers::getCustomerId(Yii::$app->user->getId());
        $order_requests = OrderRequest::find()
            ->joinWith('order')
            ->where(['order_id' => $order_id])
            ->andWhere(['order.customer_id' => $customer_id])
            ->andWhere(['worker_id' => $worker_ids])
            ->andWhere(['status' => OrderRequest::STATUS_NEW])
            ->andWhere(['NOT', ['status' => OrderRequest::STATUS_WORKER_CANCEL]])
            ->andWhere(['is_show_in_filter' => OrderRequest::SHOW_IN_FILTER])
            ->all();
        if (!empty($order_requests)){
            foreach ($order_requests as $order_request){
                $order_request->status = OrderRequest::STATUS_ACCEPT;
                if ($order_request->save()){
                    $order_worker = new OrderWorker();
                    $order_worker->order_id = $order_request->order_id;
                    $order_worker->worker_id = $order_request->worker_id;
                    $order_worker->order_place_id = $order_request->order_place_id;
                    if (!$order_worker->save()){
                        $message = GetErrorsHelpers::getError($order_worker->getErrors());
                        throw new HttpException(400, $message);
                    }
                    Sending::sendNoticeWorkerRequest($order_request->worker_id, 'Lima', 'Вы выбраны кандитатом на заказ', $order_request->id, $order_request->order_id);
                }
            }
            return array('success' => 1, 'data' => ['message' =>
                'Исполнители успешно подтверждены'], 'status' => 200);
        }

        throw new HttpException(400, 'Неверные данные');
    }
}