<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 06.12.18
 * Time: 13:34
 */

namespace api\modules\v1\controllers\customer\customerApiClass;


use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerBranch;
use api\modules\v1\models\customer\CustomerPersonal;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class getBranch
 * @package api\modules\v1\controllers\customer\customerApiClass
 */
class getBranch extends Action
{
    public function getCustomerId(){
        if (Yii::$app->user->can('customer')){
            $model_customer = Customer::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->id;
        }
        else {
            $model_customer = CustomerPersonal::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->customer_id;
        }
        return $customer_id;
    }

    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $model_branch = CustomerBranch::findAll(['customer_id' => $this->getCustomerId()]);
        return array('success' => 1, 'data' => $model_branch);
    }
}