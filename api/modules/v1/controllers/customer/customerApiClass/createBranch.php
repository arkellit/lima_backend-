<?php

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\customer\CustomerBranch;
use api\modules\v1\models\customer\CustomerPersonal;
use common\helpers\GetErrorsHelpers;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

class createBranch extends Action
{
    public function addPersonalBranch($customer_id, $personal_ids, $branch_id){
        $personals = CustomerPersonal::findAll(['customer_id' => $customer_id,
            'id' => $personal_ids]);
        foreach ($personals as $personal){
            $personal->branch_id = $branch_id;
            $personal->save();
        }
        return true;
    }

    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }
        $request = Yii::$app->request;
        $ids_personal = $request->getBodyParam('personal_ids');

        $model_customer_branch = new CustomerBranch();
        if($model_customer_branch->load($request->bodyParams, '')){
            $model_customer_branch->customer_id = Yii::$app->user->identity->customer->id;
            if (!$model_customer_branch->save()){
                throw new HttpException(400,  GetErrorsHelpers::getError($model_customer_branch->getErrors()));
            }

            if (!empty($ids_personal)){
                $this->addPersonalBranch($model_customer_branch->customer_id, $ids_personal, $model_customer_branch->id);
            }

            return array('success' => 1, 'data' => ['message' => 'Филиал успешно создан',
                'id' => $model_customer_branch->id], 'status' => 200);
        }
        throw new HttpException(400, 'Произошла ошибка при создании филиала');
    }
}