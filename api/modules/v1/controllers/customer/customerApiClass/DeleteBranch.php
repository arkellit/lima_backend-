<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 14.01.19
 * Time: 17:51
 */

namespace api\modules\v1\controllers\customer\customerApiClass;


use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerBranch;
use api\modules\v1\models\customer\CustomerPersonal;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

class DeleteBranch extends Action
{
    public function getCustomerId(){
        if (Yii::$app->user->can('customer')){
            $model_customer = Customer::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->id;
        }
        else {
            $model_customer = CustomerPersonal::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->customer_id;
        }
        return $customer_id;
    }

    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $branch_id = (integer)Yii::$app->request->get('branch_id');
        $branch = CustomerBranch::findOne([
            'customer_id' => $this->getCustomerId(),
            'id' => $branch_id
        ]);
        if (!empty($branch)){
            if ($branch->delete()){
                CustomerPersonal::updateAll(['branch_id' => 0],
                    ['branch_id' => $branch_id]);
                return array('success' => 1,
                    'message' => 'Филиал успешно удален',
                    'status' => 200
                );
            }
        }
        throw new HttpException(400, 'Неверные данные');
    }
}