<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 05.12.18
 * Time: 14:06
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\customer\CustomerPersonal;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class getRequestPersonal
 * @package api\modules\v1\controllers\customer\customerApiClass
 */
class getRequestPersonal extends Action
{
    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $query = Yii::$app->request->get('query');
        $offset = (integer)Yii::$app->request->get('offset');
        $limit = (integer)Yii::$app->request->get('limit');

        $query = CustomerPersonal::find()
            ->select(['id', 'user_id', 'name', 'last_name', 'middle_name', 'role',
                'photo'])
            ->where(['customer_id' => 0])
            ->andFilterWhere(['like', 'last_name', '%'.$query.'%',false])
            ->with('user');

        $totalCount = $query->count();
        $model_personal = $query->offset($offset)->limit($limit)->all();
        $new_model_personal = array();
        $model = array();
        foreach ($model_personal as $item){
            $new_model_personal['id'] = $item->id;
            $new_model_personal['phone'] = $item->user->phone;
            $new_model_personal['name'] = $item->last_name . ' ' . mb_substr($item->name, 0, 1) . '.';
            $new_model_personal['photo'] = $item->photo;
            $new_model_personal['role'] = $item->role;
            array_push($model, $new_model_personal);
        }
        return array('success' => 1, 'data' => $model, 'totalCount' => $totalCount,
            'currentOffset' => $offset, 'status' => 200);
    }
}