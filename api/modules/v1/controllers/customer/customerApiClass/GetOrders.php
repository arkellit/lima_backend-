<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 20.12.18
 * Time: 11:50
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\City;
use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerNotice;
use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\order\Order;
use api\modules\v1\models\order\OrderAddress;
use api\modules\v1\models\order\OrderPlace;
use common\helpers\CustomerHelpers;
use Yii;
use yii\data\Pagination;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class GetOrders
 * @package api\modules\v1\controllers\customer\customerApiClass
 */
class GetOrders extends Action
{
    public function getType($is_pay, $type){
        switch (true){
            case $is_pay == Order::NO_PAY:
                $type_order = 'waitPayment';
                break;
            case $type == Order::TYPE_LIMA_EXCHANGE:
                $type_order = 'limaExchange';
                break;
            case $type == Order::TYPE_MANUAL_SELECTION:
                $type_order = 'manualSelection';
                break;
            default:
                $type_order = 'noType';
                break;
        }
        return $type_order;
    }

    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $type_order = Yii::$app->request->get('type');
        $offset = (integer)Yii::$app->request->get('offset');
        $limit = (integer)Yii::$app->request->get('limit');

        switch ($type_order){
            case 'active':
                $is_complete_order = Order::NO_COMPLETE_ORDER;
                break;
            case 'archive':
                $is_complete_order = Order::COMPLETE_ORDER;
                break;
            default:
                throw new HttpException(400, 'Неверный запрос');
        }

        $customer = CustomerHelpers::getCustomerIdAndRole(Yii::$app->user->getId());
        $condition_customer = $customer['role'] == 'customer' ?
            ['order.customer_id' => $customer['id']] : ['order_place.personal_id' =>
                $customer['personal_id']];
        $query = OrderPlace::find()
            ->select(['order.id', 'name_order',
                'number_order', 'type_order', 'total_price',
                'date_time_start', 'date_time_end', 'order_address_id',
                'order_id',
                'branch_id', 'order_address.metro_station',
                'order_address.place_order', 'order_address.address_order',
                'order_address.lat', 'order_address.lng'])
            ->where($condition_customer)
            ->andWhere(['order.is_complete_order' => $is_complete_order])
            ->andWhere(['order.is_published' => Order::IS_PUBLISHED])
            ->asArray()
            ->joinWith(['order', 'orderAddress']);
        $totalCount = $query->count();

        $orders = $query->offset($offset)->limit($limit)->all();

        $new_orders = array();
        if (!empty($orders)){
            foreach ($orders as $order){
                $address = $order['orderAddress'];
                $order['id'] = $order['order']['id'];
                $order['type_order'] = $is_complete_order == Order::NO_COMPLETE_ORDER ?
                    $this->getType($order['order']['is_pay'], $order['order']['type_order']) : 'archive';
                $order['total_price'] = (double)$order['order']['total_price'];
                $order['city'] = $address['city']['name'];
                $order['branch'] = (int)$address['branch_id'] != 0 ? (int)$address['branch_id'] : null;
                $order['metro_station'] = $address['metro_station'];
                $order['place_order'] = $address['place_order'];
                $order['address_order'] = $address['address_order'];
                $order['lat'] = (double)$address['lat'];
                $order['lng'] = (double)$address['lng'];

                unset($order['order_id']);
                unset($order['branch_id']);
                unset($order['is_complete_order']);
                unset($order['order']);
                unset($order['orderAddress']);
                array_push($new_orders, $order);
            }
        }

        return array('success' => 1, 'data' => $new_orders, 'totalCount' => $totalCount,
            'currentOffset' => $offset, 'status' => 200);
    }
}