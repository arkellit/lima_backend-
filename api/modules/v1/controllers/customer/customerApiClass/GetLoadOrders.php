<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 20.12.18
 * Time: 12:55
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\order\Order;
use api\modules\v1\models\order\OrderAddress;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class GetLoadOrders
 * @package api\modules\v1\controllers\customer\customerApiClass
 */
class GetLoadOrders extends Action
{
    public function getCustomerId(){
        if (Yii::$app->user->can('customer')){
            $model_customer = Customer::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->id;
        }
        else {
            $model_customer = CustomerPersonal::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->customer_id;
        }
        return $customer_id;
    }

    public function getType($is_pay, $type){
        switch (true){
            case $is_pay == Order::NO_PAY:
                $type_order = 'waitPayment';
                break;
            case $type == Order::TYPE_LIMA_EXCHANGE:
                $type_order = 'limaExchange';
                break;
            case $type = Order::TYPE_MANUAL_SELECTION:
                $type_order = 'manualSelection';
                break;
            default:
                $type_order = 'noType';
                break;
        }
        return $type_order;
    }
    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $type_order = Yii::$app->request->get('type');
        $number_offset = (integer)Yii::$app->request->get("number");
        switch ($type_order){
            case 'active':
                $is_complete_order = Order::NO_COMPLETE_ORDER;
                break;
            case 'archive':
                $is_complete_order = Order::COMPLETE_ORDER;
                break;
            default:
                throw new HttpException(400, 'Неверный запрос');
        }

        $orders = Order::find()
            ->where(['customer_id' => $this->getCustomerId()])
            ->andWhere(['is_complete_order' => $is_complete_order])
            ->asArray()
            ->with('orderPlace')
            ->offset($number_offset)
            ->limit(20)
            ->all();
        $new_orders = array();
        foreach ($orders as $order){
            $address = OrderAddress::findOne($order['orderPlace']['order_address_id']);

            unset($order['customer_id']);
            $order['type_order'] = $is_complete_order == Order::NO_COMPLETE_ORDER ?
                $this->getType($order['is_pay'], $order['type_order']) : 'archive';            $order['total_price'] = (double)$order['total_price'];
            $order['date_time_start'] = $order['orderPlace']['date_time_start'];
            $order['date_time_end'] = $order['orderPlace']['date_time_end'];
            $order['city'] = $address->city->name;
            $order['branch'] = $address->branch_id != 0 ? $address->branch_id : null;
            $order['metro_station'] = $address->metro_station;
            $order['place_order'] = $address->place_order;
            $order['address_order'] = $address->address_order;
            $order['lat'] = $address->lat;
            $order['lng'] = $address->lng;
            unset($order['is_pay']);
            unset($order['is_complete_order']);
            unset($order['orderPlace']);
            array_push($new_orders, $order);
        }
        return array('success' => 1, 'data' => $new_orders, 'status' => 200);
    }
}