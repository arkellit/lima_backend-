<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 19.12.18
 * Time: 17:02
 */

namespace api\modules\v1\controllers\customer\customerApiClass;

use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerPersonal;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;
use yii\web\Response;

class GetSupervisor extends Action
{
    public function getCustomerId(){
        if (Yii::$app->user->can('customer')){
            $model_customer = Customer::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->id;
        }
        else {
            $model_customer = CustomerPersonal::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->customer_id;
        }
        return $customer_id;
    }

    public function run(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $customers = CustomerPersonal::find()
            ->where(['customer_id' => $this->getCustomerId()])
            ->andWhere(['role' => CustomerPersonal::ROLE_SUPERVISOR])
            ->andWhere(['customer_personal.is_delete' => CustomerPersonal::NO_DELETE])
            ->with(['user'])
            ->asArray()
            ->all();
        $personal = array();
        foreach ($customers as $customer){
            if (!empty($customer)){
                $customer['phone'] = $customer['user']['phone'];
                $customer['username'] = $customer['last_name'] . ' ' . mb_substr($customer['name'], 0, 1) . '.' . mb_substr($customer['middle_name'], 0, 1);
                unset($customer['name']);
                unset($customer['last_name']);
                unset($customer['middle_name']);
                unset($customer['email']);
                unset($customer['branch_id']);
                unset($customer['user']);
                unset($customer['role']);
                unset($customer['user_id']);
                unset($customer['customer_id']);
                array_push($personal, $customer);
            }
        }
        return array('success' => 1, 'data' => $personal, 'status' => 200);
    }
}