<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 22.11.18
 * Time: 15:18
 */

namespace api\modules\v1\controllers\customer;

use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;

/**
 * Class CustomerController
 * @package api\modules\v1\controllers\customer
 */
class CustomerController extends ActiveController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'actions' => ['createBranch', 'createNewPersonal', 'createInvitePersonal', 'addRole',
                        'getRequestPersonal', 'getBranch', 'getRequestOrder',
                        'getSupervisor', 'getPersonal', 'getLoadOrders',
                        'getFilterRequestWorker','setPersonalBranch',
                        'revokePersonalBranch', 'getOrderWorkers',
                        'deleteBranch', 'makeDeposit', 'removeWorkerFromRequest',
                        'confirmWorkerRequest', 'getConfirmWorker'],
                    'roles' => ['customer', 'administrator'],
                ],
                [
                    'allow' => true,
                    'actions' => ['getProfile', 'editProfile', 'changePassword',
                        'uploadPhoto', 'readQrCode', 'getOrders'],
                    'roles' => ['customer', 'administrator', 'supervisor'],
                ],
                [
                    'allow' => true,
                    'actions' => ['create', 'createPersonal'],
                    'roles' => ['@'],
                ],
            ],
        ];

        return $behaviors;
    }

    public $modelClass = 'api\modules\v1\models\customer\Customer';
    public $modelClassCustomerPersonal = 'api\modules\v1\models\customer\CustomerPersonal';
    public $modelClassCustomerBranch = 'api\modules\v1\models\customer\CustomerBranch';
    public $modelClassOrder = 'api\modules\v1\models\order\Order';
    public $modelClassUser = 'api\modules\v1\models\auth\User';
    public $modelClassWorker = 'api\modules\v1\models\worker\Worker';
    public $modelClassOrderWorker = 'api\modules\v1\models\order\OrderWorker';
    public $modelClassOrderRequest = 'api\modules\v1\models\order\OrderRequest';
    public $modelClassCustomerPayment = 'api\modules\v1\models\customer\payment\CustomerReplenishAccount';

    public function actions()
    {
        return [
            'create' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\create',
                'modelClass' => $this->modelClass
            ],
            'createPersonal' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\createPersonal',
                'modelClass' => $this->modelClassCustomerPersonal
            ],
            'createNewPersonal' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\CreateNewPersonal',
                'modelClass' => $this->modelClassCustomerPersonal
            ],
            'createBranch' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\createBranch',
                'modelClass' => $this->modelClassCustomerBranch
            ],
            'getRequestPersonal' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\getRequestPersonal',
                'modelClass' => $this->modelClassCustomerPersonal
            ],
            'addRole' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\addRole',
                'modelClass' => $this->modelClassCustomerPersonal
            ],
            'createInvitePersonal' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\CreateInvitePersonal',
                'modelClass' => $this->modelClassCustomerPersonal
            ],
            'getBranch' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\getBranch',
                'modelClass' => $this->modelClassCustomerBranch
            ],
            'deleteBranch' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\DeleteBranch',
                'modelClass' => $this->modelClassCustomerBranch
            ],
            'getProfile' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\getProfile',
                'modelClass' => $this->modelClass
            ],
            'getSupervisor' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\GetSupervisor',
                'modelClass' => $this->modelClassCustomerPersonal
            ],
            'getPersonal' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\GetPersonal',
                'modelClass' => $this->modelClassCustomerPersonal
            ],
            'setPersonalBranch' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\SetPersonalBranch',
                'modelClass' => $this->modelClassCustomerPersonal
            ],
            'revokePersonalBranch' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\RevokePersonalBranch',
                'modelClass' => $this->modelClassCustomerPersonal
            ],
            'editProfile' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\EditProfile',
                'modelClass' => $this->modelClass
            ],
            'changePassword' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\ChangePassword',
                'modelClass' => $this->modelClassUser
            ],
            'getRequestOrder' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\GetRequestOrder',
                'modelClass' => $this->modelClassOrder
            ],
            'uploadPhoto' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\UploadPhoto',
                'modelClass' => $this->modelClass
            ],
            'makeDeposit' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\MakeDeposit',
                'modelClass' => $this->modelClass
            ],
            'getOrders' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\GetOrders',
                'modelClass' => $this->modelClassOrder
            ],
            'getLoadOrders' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\GetLoadOrders',
                'modelClass' => $this->modelClassOrder
            ],
            'getFilterRequestWorker' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\GetFilterWorker',
                'modelClass' => $this->modelClassWorker
            ],

            'getOrderWorkers' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\GetOrderWorkers',
                'modelClass' => $this->modelClassOrderWorker
            ],
            'readQrCode' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\ReadQrCode',
                'modelClass' => $this->modelClassWorker
            ],
            'removeWorkerFromRequest' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\RemoveWorkerFromRequest',
                'modelClass' => $this->modelClassOrderRequest
            ],
            'confirmWorkerRequest' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\ConfirmWorkerRequest',
                'modelClass' => $this->modelClassOrderRequest
            ],
            'getConfirmWorker' => [
                'class' => 'api\modules\v1\controllers\customer\customerApiClass\GetConfirmWorker',
                'modelClass' => $this->modelClassOrderWorker
            ],
        ];
    }
}