<?php

namespace api\modules\v1\controllers\customer;

use api\modules\v1\models\auth\PersonalRegistrationForm;
use api\modules\v1\models\auth\User;
use api\modules\v1\models\Sending;
use Yii;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class PersonalController
 * @package app\modules\v1\controllers\customer
 */
class PersonalController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['customer'],
                ],
            ],
        ];

        return $behaviors;
    }

    /**
     * Function create administrator or manager
     *
     * @return array
     * @throws HttpException
     */
    public function actionCreate(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;
        $user_id = Yii::$app->user->getId();
        $user_phone = Yii::$app->user->identity->phone;
        $phone = preg_replace('/[^0-9^+]/', '', $request->getBodyParam('phone'));
        $password = $request->getBodyParam('password');
        $type = $request->getBodyParam('personal_type');

        $model_create = new PersonalRegistrationForm(['customer_user_id' => $user_id,
            'customer_phone' => $user_phone,
            'phone' => $phone, 'password' => $password, 'personal_type' => $type]);

        if($model_create->create() == null){
            $message = '';
            foreach ($model_create->getErrors() as $key => $value) {
                $message =  $value[0];
            }
            throw new HttpException(400, $message);
        }

        return array('success' => 1, 'data' => ['phone' => $phone, 'message' =>
            'Код успешно отправлен'], 'status' => 200);
    }

    /**
     * Function confirm phone
     *
     * @return array
     * @throws HttpException
     */
    public function actionConfirmPhone(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;

        $phone = preg_replace('/[^0-9^+]/', '', $request->getBodyParam('phone'));
        $code_request = $request->getBodyParam('code');

        $user_customer = Yii::$app->user->identity;
        $user = User::findOne(['phone' => User::normalizePhone($phone)]);
        $code = $user_customer->code->code;

        if(is_null($user) || $code != $code_request){
            throw new HttpException(400, 'Неверные данные, номер не подтвержден');
        }

        if ($user->is_confirm == User::CONFIRM_PHONE){
            throw new HttpException(422, 'Номер уже подтвержден');
        }

        $user->is_confirm = User::CONFIRM_PHONE;
        $user_update = $user->update();

        if($user_update){
            $role = array_keys(Yii::$app->authManager->getRolesByUser($user->id));
            return  array('success' => 1, 'data' => ['message' => 'Действие подтверждено',
                'role' => $role[0]], 'status' => 200);
        }

        throw new HttpException(500, 'Произошла ошибка сервера');

    }

    /**
     * Function send data login to sms or email
     *
     * @return array
     * @throws HttpException
     */
    public function actionSendLogin(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;

        $recipient_email = $request->getBodyParam('email');
        $phone_number = $request->getBodyParam('phone');
        $user = User::findOne(['phone' => User::normalizePhone($phone_number), 'is_confirm' => User::CONFIRM_PHONE,
            'customer_id' => Yii::$app->user->getId()]);

        $user_password = User::getEncPassword($user->id);
        if (is_null($user)){
            throw new HttpException(400, 'Неверные данные');
        }

        if ($recipient_email){
            $send = Sending::sendLoginToEmail($recipient_email, $user->phone, $user_password);
            if(is_null($send)){
                throw new HttpException(421, 'Произошла ошибка отправки сообщения');
            }
            return array('success' => 1, 'data' => ['phone' => $phone_number, 'message' =>
                'Данные на email успешно отправлены'], 'status' => 200);
        }

        elseif(!$recipient_email) {
            $send = Sending::sendLoginToSms($phone_number, $user->phone, $user_password);
            if(is_null($send)){
                throw new HttpException(421, 'Произошла ошибка отправки сообщения');
            }

            return array('success' => 1, 'data' => ['phone' => $phone_number, 'message' =>
                'Данные на номер телефона успешно отправлены'], 'status' => 200);
        }

        throw new HttpException(500, 'Произошла ошибка сервера');
    }
}