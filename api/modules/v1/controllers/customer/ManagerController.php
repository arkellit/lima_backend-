<?php

namespace api\modules\v1\controllers\customer;

use api\modules\v1\models\customer\CustomerBankData;
use api\modules\v1\models\customer\StoreAddress;
use Yii;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use \yii\rest\Controller;
use yii\web\HttpException;
use \yii\web\Response;
use api\modules\v1\models\customer\Customer;

class ManagerController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'actions' => ['create'],
                    'roles' => ['@'],
                ],
                [
                    'allow' => true,
                    'actions' => ['complete-instruction'],
                    'roles' => ['customer'],
                ],
            ],
        ];

        return $behaviors;
    }

    public function actionCreate()
    {
        Yii::$app->response->format = Response:: FORMAT_JSON;
        if(Yii::$app->user->isGuest || Yii::$app->user->can('worker')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $request = Yii::$app->request;
        $user_id = Yii::$app->user->getId();

        $model_customer = new Customer();
        if($model_customer->load($request->bodyParams, '')){ 
            $model_customer->user_id = $user_id;
            if(!$model_customer->save()){
                $message = $this->getError($model_customer->getErrors());
                throw new HttpException(400, $message);
            }
        }

        $model_bank_customer = new CustomerBankData();
        if($model_bank_customer->load($request->bodyParams, '')){
            $model_bank_customer->customer_id = $model_customer->id;
            if(!$model_bank_customer->save()){
                $model_customer->delete();
                $message = $this->getError($model_bank_customer->getErrors());
                throw new HttpException(400, $message);
            }
        }

        Customer::updateAll(['is_register_complete' => Customer::IS_REGISTER_COMPLETE_TRUE]
            , ['id' => $model_customer->id]);

        $userRole = Yii::$app->authManager->getRole('customer');
        $create_role = Yii::$app->authManager->assign($userRole, $user_id);
        if(!$create_role){
            $model_bank_customer->delete();
            $model_customer->delete();
            throw new HttpException(400, 'Произошла ошибка при добавлении роли');
        }

        return array('success' => 1, 'data' => ['message' => 'Заказчик успешно добавлен',
            'role' => $userRole->name], 'status' => 200);
    }

    protected function getError($errors)
    {
        $message = '';
        foreach ($errors as $key => $value) {
            $message = $value[0];
        }
        return $message;
    }

    protected function deleteImage($legal_info_id)
    {
        $array_image = PhotoDocument::findAll(['legal_information_id' => $legal_info_id]);
        foreach ($array_image as $value){
            if($value->image !== null){
                $file_path = Yii::getAlias('@image' . $value->image);
                if(file_exists($file_path)){
                    unlink(Yii::getAlias('@image'. $value->image));
                }
                $value->delete();
            }
        }
    }

    /**
     * Check valid store address array
     */
    protected function checkArrayStoreAddress($addressList)
    {
        foreach($addressList as $item){
            if(!array_key_exists('lat', $item)) return false;
            if(!array_key_exists('lng', $item)) return false;
            if(!array_key_exists('name_store', $item)) return false;
            if(!array_key_exists('address', $item)) return false;
        }
        return true;
    }

}