<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 18.01.19
 * Time: 13:23
 */

namespace api\modules\v1\controllers\customer;


use yii\rest\Controller;

class PaymentController extends Controller
{
    public $modelClassCustomerPayment = 'api\modules\v1\models\customer\payment\CustomerReplenishAccount';

    public function actions()
    {
        return [
            'replenishAccount' => [
                'class' => 'api\modules\v1\controllers\customer\paymentApiClass\ReplenishAccount',
                'modelClass' => $this->modelClassCustomerPayment
            ],
        ];
    }
}