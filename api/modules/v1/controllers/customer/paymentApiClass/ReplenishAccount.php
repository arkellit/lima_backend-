<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 18.01.19
 * Time: 12:32
 */
namespace api\modules\v1\controllers\customer\paymentApiClass;

use api\modules\v1\models\customer\CustomerCompanyData;
use api\modules\v1\models\customer\payment\CustomerReplenishAccount;
use api\modules\v1\models\Sending;
use common\helpers\GetErrorsHelpers;
use Yii;
use yii\rest\Action;
use yii\web\HttpException;

/**
 * Class ReplenishAccount
 * Method replenish account.
 *
 * @package api\modules\v1\controllers\customer\paymentApiClass
 */
class ReplenishAccount extends Action
{
    public function run(){
        if (Yii::$app->request->isPost){
            $request = Yii::$app->request;
            $customer_inn = $request->getBodyParam('inn_company');
            $customer_company_data = CustomerCompanyData::findOne(['inn_company' => trim($customer_inn)]);
            if (!empty($customer_company_data)){
                $replenish_account = new CustomerReplenishAccount();
                if ($replenish_account->load($request->bodyParams, '')){
                    $replenish_account->customer_id = $customer_company_data->customer_id;
                    if (!$replenish_account->save()){
                        $message = GetErrorsHelpers::getError($replenish_account->getErrors());
                        throw new HttpException(400, $message);
                    }

                    $customer_company_data->deposit += $replenish_account->amount;
                    $customer_company_data->save();

                    Sending::sendNotice($customer_company_data->customer->user_id,
                        'Lima',
                        'Ваш депозит пополнен на ' .  $replenish_account->amount. ' р.');

                    return array('success' => 1, 'data' => [
                        'message' => 'Информация успешно обновлена',
                    ], 'status' => 200);
                }
            }
        }
        throw new HttpException(400, 'Неверные данные');
    }
}