<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 02.10.18
 * Time: 16:54
 */

namespace api\modules\v1\controllers;

use api\modules\v1\models\City;
use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerBranch;
use api\modules\v1\models\customer\CustomerNotice;
use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\news\ImageNews;
use api\modules\v1\models\news\News;
use api\modules\v1\models\order\Order;
use api\modules\v1\models\order\OrderAddress;
use api\modules\v1\models\order\OrderPlace;
use api\modules\v1\models\order\OrderRequest;
use api\modules\v1\models\worker\Worker;
use Yii;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class NotificationController
 * @package api\modules\v1\controllers
 */
class NotificationController extends Controller
{
    /**
     * @inheritdoc
     *
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'actions' => ['get-all'],
                    'roles' => ['worker'],
                ],
                [
                    'allow' => true,
                    'actions' => ['get-all-customer'],
                    'roles' => ['customer', 'administrator', 'supervisor'],
                ],
            ],
        ];

        return $behaviors;
    }

    const TYPE_NEWS = 1;
    const TYPE_NEW_REQUEST = 2;
    const TYPE_ACCEPT_REQUEST = 3;

    /**
     * Function get all notification
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function actionGetAll(){
        $offset_news = (integer)Yii::$app->request->get('offset_news');
        $limit_news = (integer)Yii::$app->request->get('limit_news');

        $offset_order = (integer)Yii::$app->request->get('offset_order');
        $limit_order = (integer)Yii::$app->request->get('limit_order');
        $query_news = News::find()
            ->select(['id', 'title', 'short_description', 'image',
            'full_description', 'created_at'])
            ->where(['is_published' => News::PUBLISH]);
        $totalNewsCount = $query_news->count();
        $model_news = $query_news->offset($offset_news)->asArray()->limit($limit_news)->all();

        $worker_id = Yii::$app->user->identity->worker->id;
        $query_order = OrderRequest::find()
//            ->where(['order_request.worker_id' => $worker_id])
            ->where(['OR', ['AND', ['order_request.is_lima_exchange' =>
                OrderRequest::NO_LIMA_EXCHANGE],
            ['order_request.status' => OrderRequest::STATUS_NEW],
                ['order_request.worker_id' => $worker_id]],
                ['AND', ['order_request.status' => OrderRequest::STATUS_ACCEPT],
                    ['order_request.worker_id' => $worker_id]]])
            ->andWhere(['order_place.is_complete_order' => 0])
            ->joinWith(['order', 'orderPlace']);
        $totalOrderCount = $query_order->count();
        $model_order_requests = $query_order->offset($offset_order)->asArray()->limit($limit_order)->all();

        $new_order_array = array();
        foreach ($model_order_requests as $model_order_request) {
            unset($model_order_request['customer_id']);
            unset($model_order_request['total_price']);
            unset($model_order_request['is_pay']);
            unset($model_order_request['is_complete_order']);
            unset($model_order_request['type_order']);
            $model_order_request['type'] = $model_order_request['status'] == OrderRequest::STATUS_NEW ?
                $this::TYPE_NEW_REQUEST : $this::TYPE_ACCEPT_REQUEST;
            $model_order_request['id'] = $model_order_request['order_id'];
//            $model_order_request['id_request'] = $model_order_request['id'];
            $model_order_request['worker_id'] = $worker_id;
            $model_order_request['name_order'] = $model_order_request['order']['name_order'];
            $model_order_request['number_order'] = $model_order_request['order']['number_order'];

            $model_order_request['date_time_start'] = $model_order_request['orderPlace']['date_time_start'];
            $model_order_request['date_time_end'] = $model_order_request['orderPlace']['date_time_end'];
            $model_order_request['date_sort'] = $model_order_request['updated_at'];
            $model_order_request['created_at'] = Yii::$app->formatter->asDate($model_order_request['updated_at'], 'php:Y-m-d');
            unset($model_order_request['order']);
            unset($model_order_request['is_lima_exchange']);
            unset($model_order_request['updated_at']);
            unset($model_order_request['status']);
            unset($model_order_request['order_place_id']);
            unset($model_order_request['orderPlace']);
            unset($model_order_request['order_id']);
//            unset($model_order_request['id']);
            array_push($new_order_array, $model_order_request);
        }

        $new_array = array();
        foreach ($model_news as $item){
            $created_at = $item['created_at'];
            $item['date_sort'] = $created_at;

            unset($item['created_at']);
            $item['created_at'] =  Yii::$app->formatter->asDate($created_at, 'php:Y-m-d');
            if (empty($item['full_description'])){
                unset($item['full_description']);
                $item['full_description'] = null;
            }
            $image_news = ImageNews::findAll(['news_id' => $item['id']]);

            if(!empty($image_news)){
                $news = array();
                foreach ($image_news as $image_new){
                    array_push($news, $image_new->image);
                }

                $item['array_image'] = $news;
            }
            else{
                $item['array_image'] = null;
            }
            $item['type'] = $this::TYPE_NEWS;
            array_push($new_array, $item);
        }

        $array_sort = array_merge($new_order_array, $new_array);
        $keys = array_column($array_sort, 'date_sort');
        array_multisort($keys, SORT_DESC, $array_sort);

        return array('success' => 1,
            'data' => $array_sort,
            'totalCountNews' => $totalNewsCount,
            'totalCountOrder' => $totalOrderCount,
            'currentOffsetNews' => $offset_news,
            'currentOffsetOrder' => $offset_order,
            'status' => 200);
    }


    public function getCustomerNotice($customer_id, $role, $offset, $limit,
                                      $administrator_id = null, $branch_admin = null){
        $administrator_id = is_null($administrator_id) ? ['customer_personal_id' => 0] :
            ['customer_notice.customer_personal_id' => $administrator_id,
                'customer_notice.personal_role' => 'administrator'];
        $query_order = CustomerNotice::find()
            ->where(['customer_notice.customer_id' => $customer_id])
            ->andWhere($administrator_id)
            ->andWhere(['is_delete' => 0])
            ->orderBy(['created_at' => SORT_DESC])
            ->joinWith(['order', 'orderPlace']);
        $totalCount = $query_order->count();
        $orders = $query_order->offset($offset)->asArray()->limit($limit)->all();

//        $totalCount = count($query_order->all());

        $new_orders_array = array();

        foreach ($orders as $order){
            if ($order['worker_id'] == 0){
                if ($order['category_notice'] == CustomerNotice::ADMINISTRATOR_CONFIRMED ||
                $order['category_notice'] == CustomerNotice::CUSTOMER_CONFIRMED){
                    $order['role'] = $role;
                    $order['type'] = $order['category_notice'];
                    unset($order['order_id']);
                    unset($order['order_place_id']);
                }
                else {
                    $order_address = OrderAddress::find()
                        ->where(['id' => $order['orderPlace']['order_address_id']])
                        ->asArray()->one();
                    $city = City::findOne($order_address['city_id']);
                    $branch = CustomerBranch::find()
                        ->where(['id' => $order_address['branch_id']])
                        ->asArray()->one();
                    $order['date_time_start'] =  $order['orderPlace']['date_time_start'];
                    $order['date_time_end'] =  $order['orderPlace']['date_time_end'];
                    $order['count_hour'] =  (double)$order['orderPlace']['count_hour'];
                    $order['count_worker'] =  $order['orderPlace']['count_worker'];
                    $order['number_order'] =  $order['order']['number_order'];
                    $order['total_price'] =  (double)$order['order']['total_price'];
                    $order['place_order'] =  $order_address['place_order'];
                    $order['metro_station'] =  $order_address['metro_station'];
                    $order['landmark'] =  $order_address['landmark'];
                    $order['branch'] =  $branch['branch'];
                    $order['city'] = $city->name;
                    $order['type'] =  $order['category_notice'];
                    $order['name_order'] = $order['order']['name_order'];
                    $order['address_order'] = $order_address['address_order'];
                }

                unset($order['id']);
                unset($order['worker_id']);
                unset($order['category_notice']);
                unset($order['updated_at']);
                unset($order['is_delete']);
                unset($order['customer_id']);
                unset($order['customer_personal_id']);
                unset($order['personal_role']);
                unset($order['order']);
                unset($order['orderPlace']);
            }
            else{
                $worker = Worker::find()
                    ->where(['id' => $order['worker_id']])
                    ->asArray()->one();
                $order['name'] = $worker['name'];
                $order['last_name'] = $worker['last_name'];
                $order['middle_name'] = $worker['middle_name'];
                $order['name_order'] = $order['order']['name_order'];
                $order['number_order'] = $order['order']['number_order'];
                $order['type'] =  $order['category_notice'];
                unset($order['id']);
                unset($order['category_notice']);
                unset($order['order_place_id']);
                unset($order['updated_at']);
                unset($order['is_delete']);
                unset($order['customer_id']);
                unset($order['customer_personal_id']);
                unset($order['personal_role']);
                unset($order['order']);
                unset($order['orderPlace']);
            }
            array_push($new_orders_array, $order);
        }
//        if ($offset == 0){
//            $role == 'customer' ? array_push($new_orders_array, [
//                'role' => $role,
//                'created_at' => 1552984201,
//                'type' => 'customer_confirmed'
//            ]) :
//                array_push($new_orders_array, [
//                    'role' => $role,
//                    'type' => 'administrator_confirmed',
//                    'created_at' => 1552984201,
//                    'branch_administrator' =>
//                        CustomerBranch::find()->select(['id', 'branch'])
//                            ->where(['id' => $branch_admin])->asArray()->one()
//                ]);
//        }
        return array(
            'success' => 1,
            'data' => $new_orders_array,
            'totalCount' => $totalCount,
            'currentOffset' => $offset,
            'status' => 200
        );
    }

    public function getSupervisorNotice($customer_id, $role, $supervisor_id,
                                        $offset, $limit, $branch){
        $query_order = CustomerNotice::find()
            ->where(['customer_notice.customer_id' => $customer_id])
            ->andWhere(['personal_role' => $role])
            ->andWhere(['customer_personal_id' => $supervisor_id])
            ->andWhere(['is_delete' => 0])
            ->orderBy(['created_at' => SORT_DESC])
            ->joinWith(['order', 'orderPlace']);
        $totalCount = $query_order->count();
        $orders = $query_order->offset($offset)->asArray()->limit($limit)->all();

//        $totalCount = count($query_order->all());
        $new_orders_array = [];
        foreach ($orders as $order){
            if ($order['category_notice'] == CustomerNotice::SUPERVISOR_CONFIRMED){
                $order['role'] = $role;
                $order['type'] = $order['category_notice'];
                unset($order['worker_id']);
                unset($order['order_id']);
                unset($order['order_place_id']);
            }
            else{
                $order_address = OrderAddress::find()
                    ->where(['id' => $order['orderPlace']['order_address_id']])
                    ->asArray()->one();
                $city = City::findOne($order_address['city_id']);
                $branch = CustomerBranch::find()
                    ->where(['id' => $order_address['branch_id']])
                    ->asArray()->one();
                $order['date_time_start'] =  $order['orderPlace']['date_time_start'];
                $order['date_time_end'] =  $order['orderPlace']['date_time_end'];
                $order['count_hour'] =  (double)$order['orderPlace']['count_hour'];
                $order['count_worker'] =  $order['orderPlace']['count_worker'];
                $order['number_order'] =  $order['order']['number_order'];
                $order['total_price'] =  (double)$order['order']['total_price'];
                $order['place_order'] =  $order_address['place_order'];
                $order['metro_station'] =  $order_address['metro_station'];
                $order['landmark'] =  $order_address['landmark'];
                $order['branch'] =  $branch['branch'];
                $order['city'] = $city->name;
                $order['type'] =  $order['category_notice'];
                $order['name_order'] = $order['order']['name_order'];
                $order['address_order'] = $order_address['address_order'];
            }

            unset($order['id']);
            unset($order['category_notice']);
            unset($order['updated_at']);
            unset($order['is_delete']);
            unset($order['customer_id']);
            unset($order['customer_personal_id']);
            unset($order['personal_role']);
            unset($order['order']);
            unset($order['orderPlace']);
            array_push($new_orders_array, $order);
        }
//        return $new_orders_array;
//        if ($offset == 0){
//            array_push($new_orders_array, [
//                'role' => $role,
//                'type' => 'supervisor_confirmed',
//                'created_at' => 1552984201,
//                'branch_supervisor' =>
//                    CustomerBranch::find()->select(['id', 'branch'])
//                        ->where(['id' => $branch])->asArray()->one()
//            ]);
//        }

        return array(
            'success' => 1,
            'data' => $new_orders_array,
            'totalCount' => $totalCount,
            'currentOffset' => $offset,
            'status' => 200
        );
    }

    public function actionGetAllCustomer(){
        $user_id = Yii::$app->user->getId();
        $offset = (integer)Yii::$app->request->get('offset');
        $limit = (integer)Yii::$app->request->get('limit');

        if (array_key_exists('customer',
            Yii::$app->authManager->getRolesByUser($user_id))){
            $customer = Customer::findOne(['user_id' => $user_id, 'is_register_complete' =>
            Customer::IS_REGISTER_COMPLETE_TRUE]);
            return $this->getCustomerNotice($customer->id, 'customer', $offset, $limit);
        }
        elseif (array_key_exists('administrator',
            Yii::$app->authManager->getRolesByUser($user_id))){
            $customer_personal = CustomerPersonal::findOne(['user_id' => $user_id]);
            return $this->getCustomerNotice($customer_personal->customer_id, 'administrator',
                $offset, $limit, $customer_personal->id,
                $customer_personal->branch_id);
        }
        elseif (array_key_exists('supervisor',
            Yii::$app->authManager->getRolesByUser($user_id))){
            $customer_personal = CustomerPersonal::findOne(['user_id' => $user_id]);
            return $this->getSupervisorNotice($customer_personal->customer_id,
                'supervisor',
                $customer_personal->id, $offset, $limit, $customer_personal->branch_id);
        }
        throw new HttpException(400, 'Неверные данные');
    }
    /**
     * @param $user_id
     * @return \yii\rbac\Role[]
     * @throws HttpException
     */
    public function getRoleCustomer($user_id) {
        switch ($user_id){
            case array_key_exists('customer',
                $role = Yii::$app->authManager->getRolesByUser($user_id)):
                break;
            case array_key_exists('administrator',
                $role = Yii::$app->authManager->getRolesByUser($user_id)):
                break;
            case array_key_exists('supervisor',
                $role = Yii::$app->authManager->getRolesByUser($user_id)):
                break;
            default:
                throw new HttpException(401, 'Доступ запрещен');
        }
        return $role;
    }

}