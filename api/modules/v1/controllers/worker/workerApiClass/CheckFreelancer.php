<?php

namespace api\modules\v1\controllers\worker\workerApiClass;

use api\modules\v1\models\Sending;
use yii\rest\Action;
use Yii;

/**
 * Class CheckFreelancer
 * @package api\modules\v1\controllers\worker\workerApiClass
 */
class CheckFreelancer extends Action
{
    public function run() {
        $request = Yii::$app->request;
        $result_check = Sending::checkFreelancer($request->getBodyParam('worker_inn'));
        if (isset($result_check->status)) {
            return array('success' => 1, 'data' =>
                $result_check);
        }
        return array('success' => 1, 'data' => [
            'status' => false, 'message' => $result_check->message]);
    }
}