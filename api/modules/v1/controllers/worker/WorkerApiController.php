<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 08.08.19
 * Time: 11:31
 */

namespace api\modules\v1\controllers\worker;


use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;

class WorkerApiController extends Controller
{
    /**
     * @inheritdoc
     *
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'actions' => ['checkFreelancer'],
                    'roles' => ['@'],
                ],
            ],
        ];
        return $behaviors;
    }

    public $modelClass = 'api\modules\v1\models\worker\Worker';

    public function actions()
    {
        return [
            'checkFreelancer' => [
                'class' => 'api\modules\v1\controllers\worker\workerApiClass\CheckFreelancer',
                'modelClass' => $this->modelClass
            ]
        ];
    }
}