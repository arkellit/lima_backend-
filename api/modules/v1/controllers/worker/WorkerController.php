<?php

namespace api\modules\v1\controllers\worker;

use api\modules\v1\models\auth\ChangeUser;
use api\modules\v1\models\auth\User;
use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerCompanyData;
use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\order\Order;
use api\modules\v1\models\order\OrderAddress;
use api\modules\v1\models\order\OrderPlace;
use api\modules\v1\models\order\OrderRequest;
use api\modules\v1\models\order\OrderStartEnd;
use api\modules\v1\models\order\OrderWorker;
use api\modules\v1\models\Sending;
use api\modules\v1\models\Upload;
use api\modules\v1\models\worker\ApplicationChangePassport;
use api\modules\v1\models\worker\CategoryWorker;
use api\modules\v1\models\worker\PersonalData;
use api\modules\v1\models\worker\WorkerConfirmPay;
use common\helpers\ChangeUserHelpers;
use common\helpers\DifferenceTimeHelpers;
use common\helpers\EncryptionHelpers;
use api\modules\v1\models\worker\ImageWorker;
use api\modules\v1\models\worker\PassportData;
use api\modules\v1\models\worker\Worker;
use api\modules\v1\models\worker\WorkerProfession;
use common\helpers\WorkerPayHelpers;
use YandexCheckout\Client;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;
use yii\web\HttpException;
use yii\web\Response;
use Yii;

/**
 * Class WorkerController
 * @package api\modules\v1\controllers\worker
 */
class WorkerController extends Controller
{
    /**
     * @inheritdoc
     *
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'actions' => ['create', 'get-info', 'temp-photo',
                        'get-profile', 'pay', 'refund-payment'],
                    'roles' => ['@'],
                ],
                [
                    'allow' => true,
                    'actions' => ['complete-instruction',
                        'get-edit-profile', 'change-password', 'change-passport',
                        'update', 'get-my-orders', 'set-location',
                        'get-lima-exchange', 'get-lima-exchange',
                        'get-sort-lima-exchange', 'get-load-sort-lima-exchange',
                        'reject-order', 'repeat-payment'],
                    'roles' => ['worker'],
                ],
            ],
        ];
        return $behaviors;
    }

    /**
     * Function create new worker
     *
     * @return array
     * @throws HttpException
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function actionCreate(){
        Yii::$app->response->format = Response:: FORMAT_JSON;

        if(Yii::$app->user->isGuest || Yii::$app->user->can('customer')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $request = Yii::$app->request;

        //To table worker
        $user_id = Yii::$app->user->getId();
        $name = $request->getBodyParam('name');
        $last_name = $request->getBodyParam('last_name');
        $middle_name = $request->getBodyParam('middle_name');
        $sex = $request->getBodyParam('sex');
        $birth_date = $request->getBodyParam('birth_date');
        $height = $request->getBodyParam('height');
        $weight = $request->getBodyParam('weight');
        $category_worker_list = $request->getBodyParam('category_list');
        $body_complexion_id = $request->getBodyParam('body_id');
        $hair_color_id = $request->getBodyParam('hair_color_id');
        $hair_length_id = $request->getBodyParam('hair_length_id');
//        $clothing_size_up_id = $request->getBodyParam('clothing_up_id');
//        $clothing_size_down_id = $request->getBodyParam('clothing_down_id');
        $appearance_id = $request->getBodyParam('appearance_id');
        $eye_color_id = $request->getBodyParam('eye_color_id');
        $city_id = $request->getBodyParam('city_id');

        //To table image worker
        $photo_porter = $request->getBodyParam('photo_porter');
        $photo_full_length = $request->getBodyParam('photo_full');

        //To table passport worker
        $passport_name = $name;
        $passport_last_name = $last_name;
        $passport_middle_name = $middle_name;
        $passport_series = $request->getBodyParam('passport_series');
        $passport_number = $request->getBodyParam('passport_number');
//        $passport_birthplace = $request->getBodyParam('passport_birth_place');
        $passport_birth_date = $birth_date;
        $passport_whom = $request->getBodyParam('passport_whom');
        $passport_when = $request->getBodyParam('passport_when');
        $passport_code = $request->getBodyParam('passport_code');
        $passport_residence_address = $request->getBodyParam('passport_address');
        $passport_photo_one = $request->getBodyParam('passport_photo_one');
        $passport_photo_two = $request->getBodyParam('passport_photo_two');

        //To personal data table
        $worker_inn = $request->getBodyParam('worker_inn');
        $worker_snils = $request->getBodyParam('worker_snils');
        $worker_ls_bank = $request->getBodyParam('worker_ls_bank');
        $bank_inn = $request->getBodyParam('bank_inn');
        $bank_kpp = $request->getBodyParam('bank_kpp');
        $bank_ks = $request->getBodyParam('bank_ks');
        $bank_name = $request->getBodyParam('bank_name');
        $bank_bik = $request->getBodyParam('bank_bik');
        $payment_token = $request->getBodyParam('payment_token');
        if (!isset($payment_token) || is_null($payment_token)){
            throw new HttpException(400, 'Отсутствует токен Я.кассы');
        }

        $model_worker = new Worker();
        $worker_id = $model_worker->create($user_id, $name, $last_name, $middle_name,
            $sex, $birth_date, $height, $weight, $hair_length_id,
            $category_worker_list, $body_complexion_id, $hair_color_id,
            //$clothing_size_up_id, $clothing_size_down_id,
            $appearance_id, $eye_color_id, $city_id);

        if($worker_id['worker_id'] == null){
            $message = '';
            foreach ($worker_id['error'] as $key => $value) {
                $message =  $value[0];
            }

            throw new HttpException(400, $message);
        }

        $worker_id = $worker_id['worker_id'];

        $worker_personal_data = new PersonalData(['worker_id' => $worker_id,
            'worker_inn' => $worker_inn, 'worker_snils' => $worker_snils,
            'worker_ls_bank' => $worker_ls_bank,
            'bank_inn' => $bank_inn, 'bank_kpp' => $bank_kpp,
            'bank_ks' => $bank_ks, 'bank_name' => $bank_name, 'bank_bik' => $bank_bik]);

        if(!$worker_personal_data->save()){
            $message = '';
            foreach ($worker_personal_data->getErrors() as $key => $value) {
                $message =  $value[0];
            }
            WorkerProfession::deleteAll(['worker_id' => $worker_id]);
            Worker::deleteAll(['id' => $worker_id]);
            throw new HttpException(400, $message);
        }

        $model_image = new ImageWorker();
        $create_image = $model_image->create($worker_id, $photo_porter, $photo_full_length);

        if($create_image['is_success'] == null){
            $message = '';
            foreach ($create_image['error'] as $key => $value) {
                $message =  $value[0];
            }

            PersonalData::deleteAll(['worker_id' => $worker_id]);
            WorkerProfession::deleteAll(['worker_id' => $worker_id]);
            Worker::deleteAll(['id' => $worker_id]);

            throw new HttpException(400, $message);
        }

        $model_passport = new PassportData();
        $create_passport = $model_passport->create($worker_id, $passport_name,
            $passport_last_name, $passport_middle_name,
            $passport_series, $passport_number, $passport_birth_date,
            $passport_whom, $passport_when, $passport_code, $passport_residence_address,
            $passport_photo_one, $passport_photo_two);

        if($create_passport['is_success'] == null){
            $message = '';
            foreach ($create_passport['error'] as $key => $value) {
                $message =  $value[0];
            }
            $array_image = ImageWorker::findOne(['worker_id' => $worker_id]);//findOne(['worker_id' => $worker_id]);
            if($array_image->photo_porter !== null && $array_image->photo_full_length !== null){
                unlink(Yii::getAlias('@image/web'. $array_image->photo_porter));
                unlink(Yii::getAlias('@image/web'. $array_image->photo_full_length));
                $array_image->delete();
            }
            PersonalData::deleteAll(['worker_id' => $worker_id]);
//            ImageWorker::deleteAll(['worker_id' => $worker_id]);//findOne(['worker_id' => $worker_id]);
            WorkerProfession::deleteAll(['worker_id' => $worker_id]);
            Worker::deleteAll(['id' => $worker_id]);

            throw new HttpException(400, $message);
        }

//        Worker::updateAll(['is_register_complete' => Worker::IS_REGISTER_COMPLETE_TRUE], ['id' => $worker_id]);

        $userRole = Yii::$app->authManager->getRole('worker');
        $create_role = Yii::$app->authManager->assign($userRole, $user_id);
        if(!$create_role){
            $array_image = ImageWorker::findOne(['worker_id' => $worker_id]);//findOne(['worker_id' => $worker_id]);
            if($array_image->photo_porter !== null && $array_image->photo_full_length !== null){
                unlink(Yii::getAlias('@image/web'. $array_image->photo_porter));
                unlink(Yii::getAlias('@image/web'. $array_image->photo_full_length));
                $array_image->delete();
            }
            WorkerProfession::deleteAll(['worker_id' => $worker_id]);
            $array_passport = PassportData::findOne(['worker_id' => $worker_id]);
            unlink(Yii::getAlias('@image'. EncryptionHelpers::dec_enc('decrypt', $array_passport->passport_photo_one)));
            unlink(Yii::getAlias('@image'. EncryptionHelpers::dec_enc('decrypt', $array_passport->passport_photo_two)));
            $array_passport->delete();
            PersonalData::deleteAll(['worker_id' => $worker_id]);
            Worker::deleteAll(['id' => $worker_id]);

            throw new HttpException(400, 'Произошла ошибка при добавлении роли');
        }

        $worker_pay = $this->RequestPay($payment_token, $worker_id, $name, $last_name);
        switch ($worker_pay['status']){
            case WorkerConfirmPay::PENDING:
                $url = $worker_pay['confirm_url'];
                $payment_id = $worker_pay['payment_id'];
                break;
            case WorkerConfirmPay::SUCCEEDED:
                $url = null;
                $payment_id = null;
                break;
            case WorkerConfirmPay::CANCELED:
                $url = null;
                $payment_id = null;
                break;
            default:
                $url = null;
                $payment_id = null;
                break;
        }

        return array('success' => 1, 'data' => [
            'message' => 'Исполнитель успешно добавлен',
            'role' => $userRole->name,
            'status_pay' => $worker_pay['status'],
            'confirm_url' => $url,
            'payment_id' => $payment_id
        ], 'status' => 200);
    }

    public function actionTempPhoto(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;
        $photo = $request->getBodyParam('photo');
        if (!is_null($photo)){
            $upload_image = new Upload();
            $upload = $upload_image->upload('temp_worker', $photo);
            if (!is_null($upload)){
                return array('success' => 1, 'data' => ['url_photo' => $upload], 'status' => 200);
            }
        }

        throw new HttpException(400, 'Произошла ошибка при добавлении фото');
    }

    /**
     * Password change function
     *
     * @return array
     * @throws HttpException
     */
    public function actionChangePassword(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;

        if (!Yii::$app->user->can("worker")) {
            throw new HttpException(401, 'Доступ запрещен');
        }

        $password = $request->getBodyParam('password');
        $new_password = $request->getBodyParam('new_password');

        $user = User::findOne(Yii::$app->user->getId());

        if(is_null($user)){
            throw new HttpException(400, 'Пользователь не найден');
        }

        $change_user = ChangeUser::findOne(['user_id' => $user->id]);
        if (!empty($change_user) && DifferenceTimeHelpers::differenceTime($change_user->updated_at) < 86400){
            throw new HttpException(423, 'Пароль можно изменить один раз в день');
        }

        if (!is_null($new_password) && $user->validatePassword($password)){
            $user->setPassword($new_password);
            if(!$user->save()){
                throw new HttpException(426, 'Не удалось изменить пароль');
            }

            if(empty($change_user)){
                $change = ChangeUserHelpers::create($user->id, ChangeUser::TYPE_CHANGE_PASSWORD);
                if($change){
                    return array('success' => 1, 'data' => ['message' => 'Пароль успешно изменен',
                        'token' => $user->access_token], 'status' => 200);
                }
            }

            $change_user->type = ChangeUser::TYPE_CHANGE_PASSWORD;
            $change_user->touch('updated_at');

            if(!$change_user->save()){
                throw new HttpException(400, 'Неверные данные');
            }

            $user->access_token = User::setAccessToken($user->phone);
            if(!$user->save()){
                throw new HttpException(400, 'Не удалось сохранить новый token');
            }

            return array('success' => 1, 'data' => ['message' => 'Пароль успешно изменен',
                'token' => $user->access_token], 'status' => 200);
        }
        throw new HttpException(400, 'Неверные данные');
    }

    /**
     * Function get info worker
     *
     * @return array
     * @throws HttpException
     */
    public function actionGetInfo(){
        if (Yii::$app->user->isGuest) {
            throw new HttpException(401, 'Доступ запрещен');
        }

        if(!Yii::$app->user->can("worker")){
            $array_user = array();
            $array_user += ['message' => 'Вы не завершили полную регистрацию'];
            $array_user += ['is_full_register' => 0];
            $array_user += ['phone' => Yii::$app->user->identity->phone];
            return array('success' => 1, 'data' => $array_user, 'status' => 200);
        }

        $worker_id = Yii::$app->user->identity->worker->id;

        $sql = "SELECT worker.id, worker.name, last_name, middle_name, birth_date, height, weight,
    worker.is_register_complete,
    date_part('year',age('today'::date, birth_date)) as age,  
    worker_hair_length.name as hair_length, 
    city.name as city, rating,
    worker_body_complexion.name as complexion, 
    worker_hair_color.name as hair_color,
    worker_appearance.name as appearance, 
    worker_eye_color.name as eye_color,
    photo_porter, photo_full_length,  
    body_complexion_id, hair_color_id,appearance_id, eye_color_id,
    hair_length_id, city_id
    FROM worker
 LEFT JOIN city ON  city.id = worker.city_id   
 LEFT JOIN worker_photo ON  worker_photo.worker_id = worker.id
 LEFT JOIN worker_body_complexion ON  worker_body_complexion.id = body_complexion_id
 LEFT JOIN worker_hair_color ON  worker_hair_color.id = hair_color_id
 LEFT JOIN worker_appearance ON  worker_appearance.id = appearance_id
 LEFT JOIN worker_hair_length ON  worker_hair_length.id = hair_length_id
 LEFT JOIN worker_eye_color ON  worker_eye_color.id = eye_color_id
 WHERE worker.id = $worker_id;";

        $worker = Yii::$app->db->createCommand($sql)->queryAll();

        $worker_confirm_pay = WorkerConfirmPay::findOne(['worker_id' => $worker_id]);
        if (empty($worker_confirm_pay) || ($worker_confirm_pay->status != WorkerConfirmPay::SUCCEEDED &&
                $worker_confirm_pay->status != WorkerConfirmPay::NO_REFUND)){
            throw new HttpException(430,
                'Вы не подтвердили условия сервиса');
        }

        if ($worker[0]['is_register_complete'] == Worker::IS_REGISTER_COMPLETE_FALSE){
            throw new HttpException(429, 'Ваш профиль еще не прошел модерацию');
        }


//        $card_number = $worker[0]['card_number'];
//        unset($worker[0]['card_number']);
//        //$card_number = preg_replace('/[^\d]/','',$card_number);
//        $worker[0] += ['card_number' =>  substr_replace($card_number, '************', 0,12)];

        $worker_rating = $worker[0]['rating'];
        unset($worker[0]['rating']);
        $worker[0] += ['rating' => Worker::getRating($worker_rating)];
        $worker[0] += ['phone' =>  Yii::$app->user->identity->phone];
        $worker[0] += ['is_full_register' => 1];

        $professions = WorkerProfession::findAll(['worker_id' => $worker_id]);
        $new_proffession = [];
        foreach ($professions as $profession) {
            $new_proffession[] = ['id' => $profession->category_worker_id,
                'name' => $profession->typeWorker->name];
        }

//        $prof = array();
//        foreach ($profession as $item){

//        if(count($profession) == 2){
//            $worker[0] += $profession[0]->category_worker_id == 1 ? ['is_monic' => 1] : ['is_monic' => null];
//            $worker[0] += $profession[1]->category_worker_id == 2 ? ['is_promoter' => 1] : ['is_promoter' => null];
//        }
//
//        else{
//            $worker[0] += $profession[0]->category_worker_id == 1 ? ['is_monic' => 1] : ['is_monic' => null];
//            $worker[0] += $profession[0]->category_worker_id == 2 ? ['is_promoter' => 1] : ['is_promoter' => null];
//        }

        $order_offer = $this->getRequestOrder(OrderRequest::STATUS_NEW, $worker_id,
            [1,2], ['is_lima_exchange' => OrderRequest::NO_LIMA_EXCHANGE], 0, 1000);
        $count_order_offer = $order_offer['totalCount'];
        $order_active = $this->getRequestOrder(OrderRequest::STATUS_ACCEPT, $worker_id,
            [1,2], null, 0, 1000);
        $count_order_active = $order_active['totalCount'];
        $order_finished = OrderStartEnd::find()
            ->where(['is_finished_job' => 1])
            ->andWhere(['order_request.worker_id' => $worker_id])
            ->joinWith(['orderRequest', 'orderPlace', 'order']);
        $count_order_finished = count($order_finished->all());

        $passport = PassportData::findOne(['worker_id' => $worker_id]);
        $passport_series = EncryptionHelpers::dec_enc('decrypt', $passport->passport_series);
        $passport_number = EncryptionHelpers::dec_enc('decrypt', $passport->passport_number);
        $passport_register_address = EncryptionHelpers::dec_enc('decrypt', $passport->passport_residence_address);
        $worker[0] += ['passport_series' =>  substr_replace($passport_series, '**', 1,2)];
        $worker[0] += ['passport_number' =>  substr_replace($passport_number, '**', 2,2)];
        $worker[0] += ['passport_register_address' =>  $passport_register_address];
        $worker[0] += ['count_order_offer' => $count_order_offer];
        $worker[0] += ['count_order_active' => $count_order_active];
        $worker[0] += ['count_order_finished' => $count_order_finished];
        $worker[0] += ['professions' => $new_proffession];
        return array('success' => 1, 'data' => $worker[0], 'status' => 200);
    }

    public function actionGetEditProfile(){
        if (!Yii::$app->user->can("worker")) {
            throw new HttpException(401, 'Доступ запрещен');
        }

        $worker_id = Yii::$app->user->identity->worker->id;

        $sql = "SELECT worker.id, worker.name, last_name, middle_name, birth_date, height, weight,
    hair_length.name as hair_length, city.name as city,
    body_complexion.name as complexion, hair_color.name as hair_color,
    appearance.name as appearance, 
    eye_color.name as eye_color,photo_porter, photo_full_length, card_number,
    body_complexion_id,hair_color_id,appearance_id, eye_color_id,
    hair_length_id, city_id
    FROM worker
 LEFT JOIN city ON  city.id = worker.city_id   
 LEFT JOIN image_worker ON  image_worker.worker_id = worker.id
 LEFT JOIN body_complexion ON  body_complexion.id = body_complexion_id
 LEFT JOIN hair_color ON  hair_color.id = hair_color_id
 LEFT JOIN appearance ON  appearance.id = appearance_id
 LEFT JOIN hair_length ON  hair_length.id = hair_length_id
 LEFT JOIN eye_color ON  eye_color.id = eye_color_id
 WHERE worker.id = $worker_id;";

        $worker = Yii::$app->db->createCommand($sql)->queryAll();
        $worker[0] += ['phone' =>  Yii::$app->user->identity->phone];
        $profession = WorkerProfession::findAll(['worker_id' => $worker_id]);

        $worker[0] += $profession[0]->category_worker_id == 1 ? ['is_monic' => 1] : ['is_monic' => null];
        $worker[0] += $profession[1]->category_worker_id == 2 ? ['is_promoter' => 1] : ['is_promoter' => null];

        return array('success' => 1, 'data' => [$worker[0]], 'status' => 200);
    }

    /**
     * Function update worker
     *
     * @return array
     * @throws HttpException
     */
    public function actionUpdate(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;
        if (!Yii::$app->user->can("worker")) {
            throw new HttpException(401, 'Доступ запрещен');
        }

        $worker = Worker::findOne(['id' =>  Yii::$app->user->identity->worker->id]);

        $phone_request = $request->getBodyParam('phone');
        if (!is_null($phone_request)){
            $phone = User::normalizePhone($phone_request);
            $user = User::findOne(['phone' => $phone]);
            $user->phone = $phone;
            if (!$user->save()){
                $message = '';
                foreach ($user->getErrors() as $key => $value) {
                    $message =  $value[0];
                }
                throw new HttpException(400, $message);
            }
        }
        $name = $request->getBodyParam('name');
        $last_name = $request->getBodyParam('last_name');
        $middle_name = $request->getBodyParam('middle_name');
        $birth_date = $request->getBodyParam('birth_date');
        $height = $request->getBodyParam('height');
        $weight = $request->getBodyParam('weight');
        $body_complexion_id = $request->getBodyParam('body_id');
        $hair_color_id = $request->getBodyParam('hair_color_id');
        $hair_length_id = $request->getBodyParam('hair_length_id');
        $appearance_id = $request->getBodyParam('appearance_id');
        $eye_color_id = $request->getBodyParam('eye_color_id');
        $city_id = $request->getBodyParam('city_id');
        $professions = $request->getBodyParam('professions');

        $photo_porter = $request->getBodyParam('photo_porter');
        $photo_full_length = $request->getBodyParam('photo_full');

        $worker->name = is_null($name) ? $worker->name : $name;
        $worker->last_name = is_null($last_name) ? $worker->last_name : $last_name;
        $worker->middle_name = is_null($middle_name) ? $worker->middle_name : $middle_name;
        $worker->birth_date = is_null($birth_date)  ? $worker->birth_date :
            Yii::$app->formatter->asDate($birth_date, 'php:Y-m-d');
        $worker->height = is_null($height) ? $worker->height : $height;
        $worker->weight = is_null($weight) ? $worker->weight : $weight;
        $worker->body_complexion_id = is_null($body_complexion_id)
            ? $worker->body_complexion_id : $body_complexion_id;
        $worker->hair_color_id = is_null($hair_color_id) ?
            $worker->hair_color_id : $hair_color_id;
        $worker->hair_length_id = is_null($hair_length_id) ?
            $worker->hair_length_id : $hair_length_id;
        $worker->appearance_id = is_null($appearance_id) ?
            $worker->appearance_id : $appearance_id;
        $worker->eye_color_id = is_null($eye_color_id) ?
            $worker->eye_color_id : $eye_color_id;
        $worker->city_id = is_null($city_id) ?
            $worker->city_id : $city_id;

        if (!$worker->save()){
            $message = '';
            foreach ($worker->getErrors() as $key => $value) {
                $message =  $value[0];
            }
            throw new HttpException(400, $message);
        }

        if (isset($professions) && !empty($professions)) {
            WorkerProfession::deleteAll(['worker_id' => $worker->id]);
            $new_profession = [];
            foreach ($professions as $profession) {
                $new_profession[] = [$worker->id, $profession, 1];
            }
            Yii::$app->db->createCommand()->batchInsert('worker_profession',
                ['worker_id', 'category_worker_id', 'is_working'],
                    $new_profession
             )->execute();
        }

        if(!is_null($photo_porter) || !is_null($photo_full_length)){
            $photo_worker = ImageWorker::findOne(['worker_id' => $worker->id]);
            $tmp_photo_porter = $photo_worker->photo_porter;
            $tmp_photo_full = $photo_worker->photo_full_length;
            if(!is_null($photo_porter)){
                unlink(Yii::getAlias('@image/web' . $photo_worker->photo_porter));
                $upload_image = new Upload();
                $image_name_one = $upload_image->upload('worker', $photo_porter);
            }

            if(!is_null($photo_full_length)){
                unlink(Yii::getAlias('@image/web' . $photo_worker->photo_full_length));
                $upload_image = new Upload();
                $image_name_two = $upload_image->upload('worker', $photo_full_length);
            }

            $photo_worker->photo_porter = $image_name_one ?? $tmp_photo_porter;
            $photo_worker->photo_full_length = $image_name_two ?? $tmp_photo_full;

            if (!$photo_worker->save()){
                $message = '';
                foreach ($photo_worker->getErrors() as $key => $value) {
                    $message =  $value[0];
                }
                throw new HttpException(400, $message);
            }
        }

        return array('success' => 1, 'data' =>
            ['message' => 'Информация успешно обновлена'], 'status' => 200);
    }

    /**
     * Function create application for change passport
     *
     * @return array
     * @throws HttpException
     */
    public function actionChangePassport(){
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;
        if (!Yii::$app->user->can("worker")) {
            throw new HttpException(401, 'Доступ запрещен');
        }

        $passport_photo_one = $request->getBodyParam('passport_photo_one');
        $passport_photo_two = $request->getBodyParam('passport_photo_two');

        if(empty($passport_photo_one) || empty($passport_photo_two)){
            throw new HttpException(400, 'Неверные данные');
        }

        $worker = Worker::findOne(['id' =>  Yii::$app->user->identity->worker->id]);

        $model_change = ApplicationChangePassport::findOne(['worker_id' => $worker->id]);
        if(empty($model_change)){
            $model_change = new ApplicationChangePassport();
            $upload_image = new Upload();
            $model_change->worker_id = $worker->id;
            $photo_one = $upload_image->upload('secret_change_passport', $passport_photo_one);
            $photo_two = $upload_image->upload('secret_change_passport', $passport_photo_two);
            $model_change->passport_photo_one = !is_null($photo_one) ? EncryptionHelpers::dec_enc('encrypt', $photo_one) : null;
            $model_change->passport_photo_two = !is_null($photo_two) ? EncryptionHelpers::dec_enc('encrypt', $photo_two) : null;
            if(!$model_change->save()){
                throw new HttpException(400, 'Не удалось создать');
            }
            return array('success' => 1, 'data' =>
                ['message' => 'Заявка успешно создана'], 'status' => 200);
        }

        if (DifferenceTimeHelpers::differenceTime($model_change->updated_at) < 86400){
            throw new HttpException(423, 'Запрос на смену паспортных разрешен раз в сутки');
        }

        $tmp_file_one = Yii::getAlias('@image'. EncryptionHelpers::dec_enc('decrypt', $model_change->passport_photo_one));
        $tmp_file_two = Yii::getAlias('@image'. EncryptionHelpers::dec_enc('decrypt', $model_change->passport_photo_two));
        $upload_image = new Upload();
        $model_change->touch('updated_at');
        $model_change->is_changed = ApplicationChangePassport::NO_CHANGED;
        $photo_one = $upload_image->upload('secret_change_passport', $passport_photo_one);
        $photo_two = $upload_image->upload('secret_change_passport', $passport_photo_two);

        $model_change->passport_photo_one = !is_null($photo_one) ? EncryptionHelpers::dec_enc('encrypt', $photo_one) : null;
        $model_change->passport_photo_two = !is_null($photo_two) ? EncryptionHelpers::dec_enc('encrypt', $photo_two) : null;

        if(!$model_change->save()){
            throw new HttpException(400, 'Не удалось создать');
        }

        if(file_exists($tmp_file_one)){
            unlink($tmp_file_one);
        }

        if(file_exists($tmp_file_two)){
            unlink($tmp_file_two);
        }

        return array('success' => 1, 'data' =>
            ['message' => 'Заявка успешно создана'], 'status' => 200);
    }


    public function actionUpdatePassword(){
        if (!Yii::$app->user->can("worker")) {
            throw new HttpException(401, 'Доступ запрещен');
        }
    }


    /**
     * Function change is_instruction_complete
     *
     * @return array
     * @throws HttpException
     */
    public function actionCompleteInstruction(){
        if (!Yii::$app->user->can("worker")) {
            throw new HttpException(401, 'Доступ запрещен');
        }
        $update_worker = Worker::updateAll(['is_instruction_complete' => Worker::IS_REGISTER_COMPLETE_TRUE]
            ,['id' =>  Yii::$app->user->identity->worker->id]);
        if($update_worker){
            return array('success' => 1, 'data' =>
                ['message' => 'Информация успешно обновлена'], 'status' => 200);
        }

        throw new HttpException(400, 'Произошла ошибка при обновлении записи');
    }

    public function actionGetMyOrders(){
        if (!Yii::$app->user->can("worker")) {
            throw new HttpException(401, 'Доступ запрещен');
        }

        $worker_id = Yii::$app->user->identity->worker->id;
        $type_request = Yii::$app->request->get('type');
        $category_worker = Yii::$app->request->get('category_worker');

        $offset = (integer)Yii::$app->request->get('offset');
        $limit = (integer)Yii::$app->request->get('limit');
        switch ($type_request){
            case 'offer':
                $order_requests = $this->getRequestOrder(OrderRequest::STATUS_NEW, $worker_id, $category_worker, ['is_lima_exchange' => OrderRequest::NO_LIMA_EXCHANGE], $offset, $limit);
                break;
            case 'accept':
                $order_requests = $this->getRequestOrder(OrderRequest::STATUS_ACCEPT, $worker_id, $category_worker, null, $offset, $limit);
                break;
            case 'not_accept':
                $order_requests = $this->getRequestOrder(OrderRequest::STATUS_NO_ACCEPT, $worker_id, $category_worker, null, $offset, $limit);
                break;
            case 'completed':
                $order_requests = $this->getOrderComplete($worker_id, $category_worker, $offset, $limit);
                return array('success' => 1,
                    'data' => $order_requests['model_request'],
                    'totalCount' => $order_requests['totalCount'],
                    'currentOffset' => $offset,
                    'status' => 200
                );
            default:
                throw new HttpException(401, 'Неверный параметр');
        }
        $new_order = array();
        $current_time = time();
        foreach ($order_requests['model_request'] as $order_request){
            $company_data = CustomerCompanyData::findOne(['customer_id' => $order_request['order']['customer_id']]);
            $order_address = OrderAddress::findOne($order_request['orderPlace']['order_address_id']);
            $order_worker = CategoryWorker::findOne($order_request['orderPlace']['worker_category_id']);
            $order_worker_accept = OrderWorker::findOne(['worker_id' => $worker_id,
                'order_id' => $order_request['order_id']]);
            //$order_request['id_request'] = $order_request['id'];
//            unset($order_request['id']);
            $order_request['id'] = $order_request['order_id'];
            unset($order_request['order']['customer_id']);
            unset($order_request['order']['type_order']);
            $order_request['price'] = $order_request['order']['total_price'] / $order_request['orderPlace']['count_worker'];
            $order_request['logo_company'] = $company_data->logo_company;
            $order_request['name_company'] = $company_data->name_company;
            $order_request['date_time_start'] = $order_request['orderPlace']['date_time_start'];
            $order_request['date_time_end'] = $order_request['orderPlace']['date_time_end'];
            $order_request['category_worker'] = $order_worker->name;
            $order_request['city'] = $order_address->city->name;
            $order_request['branch'] = $order_address->branch_id != 0 ? $order_address->branch_id : null;
            $order_request['metro_station'] = $order_address->metro_station;
            $order_request['place_order'] = $order_address->place_order;
            $order_request['lat'] = $order_address->lat;
            $order_request['lng'] = $order_address->lng;
            $order_request['landmark'] = $order_address->landmark;
            if (empty($order_worker_accept) || $order_worker_accept->status !=
                OrderWorker::STATUS_START_JOB){
                if (DifferenceTimeHelpers::differenceTime($order_request['orderPlace']['date_time_start']) <= 3600){
                    $order_request['status_work'] = 'less_day';
                }
                elseif (DifferenceTimeHelpers::differenceTime($order_request['orderPlace']['date_time_start']) >= 3600){
                    $order_request['status_work'] = 'more_day';
                }
            }
            else{
                $order_request['status_work'] = 'in_work';
            }
            $order_request['current_time'] = $current_time;
            unset($order_request['order_id']);
            unset($order_request['order_place_id']);
            unset($order_request['is_lima_exchange']);
            unset($order_request['created_at']);
            unset($order_request['updated_at']);
            unset($order_request['order']);
            unset($order_request['status']);
            unset($order_request['orderPlace']);
            array_push($new_order, $order_request);
        }

        return array('success' => 1,
            'data' => $new_order,
            'totalCount' => $order_requests['totalCount'],
            'currentOffset' => $offset,
            'status' => 200
        );
    }

    public function getRequestOrder($status, $worker_id, $category_worker, $is_lima_exchange, $offset, $limit){
        $is_lima_exchange_new = $is_lima_exchange == null ? [] : $is_lima_exchange;
        $query = OrderRequest::find()
            ->where($is_lima_exchange_new)
            ->andWhere([
                'status' => $status == OrderRequest::STATUS_NO_ACCEPT ? [2, 3] : $status,
                'worker_id' => $worker_id])
            ->andWhere(['NOT', ['order_start_end.is_finished_job' => 1]])
            ->leftJoin('order_place', 'order_place.order_id = order_request.order_id')
            ->leftJoin('order_profession', 'order_profession.order_id = order_request.order_id')
            ->leftJoin('order_start_end', 'order_start_end.order_id = order_request.order_id')

            //            ->andWhere(['order_place.worker_category_id' => $category_worker])
//            ->andWhere(['order_profession.worker_category_id' => $category_worker])
            ->with(['order', 'orderPlace'])
            ->orderBy(['order_request.updated_at' => SORT_DESC])
//            ->joinWith('orderProfession')
        ;
        $totalCount = $query->count();
        $order_requests = $query->offset($offset)->asArray()->limit($limit)->all();

        return ['model_request' => $order_requests, 'totalCount' => $totalCount];
    }

    public function getOrderComplete($worker_id, $category_worker, $offset, $limit){
        $query = OrderStartEnd::find()
            ->where(['is_finished_job' => 1])
            ->andWhere(['order_request.worker_id' => $worker_id])
//            ->andWhere(['order_place.worker_category_id' => $category_worker])
            ->joinWith(['orderRequest', 'orderPlace', 'order'])
            ->orderBy(['order_start_end.id' => SORT_DESC]);
        $totalCount = count($query->all());
        $order_accepts = $query->offset($offset)->asArray()->limit($limit)->all();
        $new_order = array();
        foreach ($order_accepts as $order_accept){
            $order_accept['id'] = $order_accept['order_id'];
            unset($order_accept['order_id']);
            unset($order_accept['order_request_id']);
            unset($order_accept['is_start_job']);
            unset($order_accept['is_finished_job']);
            unset($order_accept['is_end_job']);
            unset($order_accept['start_order_type']);
            $company_data = CustomerCompanyData::findOne(['customer_id' => $order_accept['order']['customer_id']]);
            $order_address = OrderAddress::findOne($order_accept['orderPlace']['order_address_id']);
            $order_worker = CategoryWorker::findOne($order_accept['orderPlace']['worker_category_id']);
            $order_accept['id'] = $order_accept['order']['id'];
            $order_accept['price'] = (double)$order_accept['order']['total_price'] / $order_accept['orderPlace']['count_worker'];
            $order_accept['logo_company'] = $company_data->logo_company;
            $order_accept['name_company'] = $company_data->name_company;
            $order_accept['date_time_start'] = $order_accept['orderPlace']['date_time_start'];
            $order_accept['date_time_end'] = $order_accept['orderPlace']['date_time_end'];
            $order_accept['category_worker'] = $order_worker->name;
            $order_accept['city'] = $order_address->city->name;
            $order_accept['branch'] = $order_address->branch_id != 0 ? $order_address->branch_id : null;
            $order_accept['metro_station'] = $order_address->metro_station;
            $order_accept['place_order'] = $order_address->place_order;
            $order_accept['lat'] = $order_address->lat;
            $order_accept['lng'] = $order_address->lng;
            $order_accept['landmark'] = $order_address->landmark;
            unset($order_accept['orderRequest']);
            unset($order_accept['orderPlace']);
            unset($order_accept['order']);
            array_push($new_order, $order_accept);
        }
        return ['model_request' => $new_order, 'totalCount' => $totalCount];
    }

    public function actionSetLocation(){
        if (!Yii::$app->user->can("worker")) {
            throw new HttpException(401, 'Доступ запрещен');
        }
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = Yii::$app->request;

        $worker = Yii::$app->user->identity->worker;

        $category_worker = $request->getBodyParam('category_worker');

        $lat = $request->getBodyParam('lat');
        $lng = $request->getBodyParam('lng');

        $radius_search = $request->getBodyParam('radius_search');

        //$worker = Worker::findOne($worker_id);
        $worker->last_location_lat = $lat;
        $worker->last_location_lng = $lng;
        if (isset($radius_search) && !is_null($radius_search)) $worker->radius_search = ((double)$radius_search / 100000);

        if ($worker->save()){
            if (is_null($worker->point)){
                WorkerProfession::updateAll(['is_working' => WorkerProfession::WORKER_NO_WORKING],
                    ['worker_id' => $worker->id]);
            }
            elseif (!is_null($category_worker)){
                if ($category_worker == 0) {
                    WorkerProfession::updateAll(['is_working' => WorkerProfession::WORKER_WORKING],
                        ['worker_id' => $worker->id]);
                }
                else{
                    $worker_proffessions = WorkerProfession::findAll(['worker_id' => $worker->id]);
                    foreach ($worker_proffessions as $worker_proffession){
                        if ($worker_proffession->category_worker_id == $category_worker){
                            $worker_proffession->is_working = WorkerProfession::WORKER_WORKING;
                        }
                        else $worker_proffession->is_working = WorkerProfession::WORKER_NO_WORKING;
                        $worker_proffession->save();
                    }
                }
            }

            return array('success' => 1, 'data' => [
                'message' => 'Координаты обновлены'
            ], 'status' => 200);
        }
        throw new HttpException(400, 'Не удалось сохранить координаты');
    }

    public function actionGetSortLimaExchange(){
        if (!Yii::$app->user->can("worker")) {
            throw new HttpException(401, 'Доступ запрещен');
        }

        $worker_id = Yii::$app->user->identity->worker->id;

        $category_worker =  Yii::$app->request->get('category_worker');
        $radius_search_request = (double)Yii::$app->request->get('radius_search');
        $radius_search = $radius_search_request / 100000;
        $sort_type = Yii::$app->request->get('sort_type');

        $offset = (integer)Yii::$app->request->get('offset');
        $limit = (integer)Yii::$app->request->get('limit');

        $lat = Yii::$app->user->identity->worker->last_location_lat;
        $lng = Yii::$app->user->identity->worker->last_location_lng;
        switch ($sort_type){
            case 'date':
                $sort_sql = ['date_time_start' => SORT_DESC];
                break;
            case 'amount':
                $sort_sql = ['price' => SORT_DESC];
                break;
            case 'place':
                $sort_sql = [];
                break;
            default:
                $sort_sql = [];
                break;
        }

//        return explode(',', $category_worker);
        $category_worker_sql = $category_worker == 0 ? [] :
            ['in', 'order_profession.worker_category_id', explode(',', $category_worker)];
//        return $category_worker_sql;
        $condition_place = $lat == 0 || $lng == 0 ? [] : 'ST_DWithin(order_address.point,ST_GeomFromEWKT(\'SRID=4326;POINT('.$lat.' '.$lng.')\'), '.$radius_search.')';
        $query = OrderPlace::find()
            ->select(['order_place.*',
                '(
                    CASE 
                        WHEN order_profession.count_worker = 0 THEN 0 
                        ELSE order_price/order_profession.count_worker
                    END
                ) AS price'])
            ->with(['orderAddress'])
            ->joinWith(['order', 'orderRequest', 'orderProfession'])
            ->leftJoin('order_address','order_address.id = order_place.order_address_id')
            ->where(['AND', ['order.type_order' => Order::TYPE_LIMA_EXCHANGE],
                ['order.is_pay' => Order::IS_PAY]])
            ->andWhere(['order.is_complete_order' => Order::NO_COMPLETE_ORDER])
            ->andWhere($condition_place)
            ->andWhere($category_worker_sql)
            ->andWhere(['or', ['order_request.worker_id' => null],
                ['not', ['order_request.worker_id' => $worker_id]]])
            ->orderBy($sort_sql);

        $orders = $query->offset($offset)->asArray()->limit($limit)->all();
        $totalCount = count($orders);
        if (!empty($orders)){
            $new_orders = array();
            $current_time = time();
            foreach ($orders as $order){
                $company_data = CustomerCompanyData::findOne(['customer_id' => $order['order']['customer_id']]);
                $address = OrderAddress::findOne($order['order_address_id']);

                $order['price'] = (double)$order['price'];
                $order['id'] = $order['order']['id'];
                $order['worker_id'] = $worker_id;
                $order['name_order'] = $order['order']['name_order'];
                $order['number_order'] = $order['order']['number_order'];
                //          $order['price'] = $order['total_price'] / $order['orderPlace']['count_worker'];
                $order['logo_company'] = $company_data->logo_company;
                $order['name_company'] = $company_data->name_company;
                $order['city'] = $address->city->name;
                $order['branch'] = $address->branch_id != 0 ? $address->branch_id : null;
                $order['metro_station'] = $address->metro_station;
                $order['place_order'] = $address->place_order;
                $order['lat'] = $address->lat;
                $order['lng'] = $address->lng;
                $order['current_time'] = $current_time;
                unset($order['order_address_id']);
                unset($order['orderProfession']);
                unset($order['worker_category_id']);
                unset($order['type_job']);
                unset($order['personal_id']);
                unset($order['order_price']);
                unset($order['count_worker']);
                unset($order['is_complete_worker']);
                unset($order['order_id']);
                unset($order['total_price']);
                unset($order['is_pay']);
                unset($order['is_complete_order']);
                unset($order['order']);
                unset($order['orderAddress']);
                unset($order['orderRequest']);
                array_push($new_orders, $order);
            }

            return array('success' => 1,
                'data' => $new_orders,
                'totalCount' => $totalCount,
                'currentOffset' => $offset,
                'status' => 200
            );
        }
        return array('success' => 1,
            'data' => [],
            'totalCount' => $totalCount,
            'currentOffset' => $offset,
            'status' => 200
        );
    }


    public function actionRejectOrder(){
        if (!Yii::$app->user->can("worker")) {
            throw new HttpException(401, 'Доступ запрещен');
        }

        $worker = Yii::$app->user->identity->worker;
        $request = Yii::$app->request;
        $order_id = $request->getBodyParam('order_id');
        if (!$order_id) throw new HttpException(400, 'Неверные данные');
        $model_request = OrderRequest::findOne(['worker_id' => $worker->id,
            'order_id' => $order_id, 'status' => OrderRequest::STATUS_ACCEPT]);
        if (!$model_request){
            throw new HttpException(400, 'Неверные данные');
        }
        /** @var TODO   Надо изменить $model_request_accept */

        $model_request_accept = OrderStartEnd::findOne(['order_id' =>
            $model_request->order_id, 'is_start_job' => 0]);
        $model_order_worker = OrderWorker::findOne(['order_id' =>
            $model_request->order_id]);
        if (!$model_request_accept){
            throw new HttpException(400, 'Неверные данные');
        }

        $model_request->status = OrderRequest::STATUS_WORKER_CANCEL;
        $model_order_worker->status = OrderWorker::STATUS_WORKER_CANCEL;
        $model_request_accept->is_cancel_order = OrderStartEnd::IS_CANCEL_WORKER;
        if ($model_request_accept->save() && $model_request->save()){
            $model_order_worker->save();
            $worker->rating = Worker::reduceRating($worker->rating, Worker::BLOCKED_RATING);
            $worker->save();

            $customer = Customer::findOne($model_request->order->customer_id);
            Sending::sendNotice($customer->user->id, 'Lima',
                'Пользователь ' . $worker->last_name . ' ' .
                $worker->name . '.' . 'отказался от выполнения заказа');
            if ($model_request->order->administrator_id != 0){
                $customer_personal = CustomerPersonal::findOne($model_request->order->administrator_id);
                Sending::sendNotice($customer_personal->user->id, 'Lima',
                    'Пользователь ' . $worker->last_name . ' ' .
                    $worker->name . '. ' . 'отказался от выполнения заказа');
            }
            return array('success' => 1, 'data' => [
                'message' => 'Вы успешно отказались от заказа'
            ], 'status' => 200);
        }
        throw new HttpException(400, 'Неверные данные');
    }

    /**
     * Get public profile
     *
     * @return array
     * @throws HttpException
     */
    public function actionGetProfile(){
        if (Yii::$app->user->isGuest) {
            throw new HttpException(401, 'Доступ запрещен');
        }
        $worker_id = (int)Yii::$app->request->get('worker_id');
        $worker = Worker::find()
            ->select(['worker.id','worker.name', 'last_name', 'rating', 'weight',
                'height', 'hair_length_id', 'hair_color_id', 'city_id',
                'eye_color_id', 'appearance_id', 'birth_date',
                "date_part('year', age('today'::date, worker.birth_date)) as age"])
            ->where(['worker.id' => $worker_id])
            ->joinWith(['hairColor', 'hairLength', 'eyeColor', 'appearance',
                'imageWorker', 'city'])
            ->asArray()
            ->one();
        if (!empty($worker)){
            $order_offer = $this->getRequestOrder(OrderRequest::STATUS_NEW, $worker_id,
                [1,2], ['is_lima_exchange' => OrderRequest::NO_LIMA_EXCHANGE], 0, 1000);
            $count_order_offer = $order_offer['totalCount'];
            $order_active = $this->getRequestOrder(OrderRequest::STATUS_ACCEPT, $worker_id,
                [1,2], null, 0, 1000);
            $count_order_active = $order_active['totalCount'];

            $order_finished = OrderStartEnd::find()
                ->where(['is_finished_job' => 1])
                ->andWhere(['order_request.worker_id' => $worker_id])
                ->joinWith(['orderRequest']);
            $count_order_finished = count($order_finished->all());

            $worker += ['worker_id' =>  $worker['id']];
            $worker['rating'] = Worker::getRating((double)$worker['rating']);
            $worker['hair_color'] = $worker['hairColor']['name'];
            $worker['hair_length'] = $worker['hairLength']['name'];
            $worker['eye_color'] = $worker['eyeColor']['name'];
            $worker['appearance'] = $worker['appearance']['name'];
            $worker['photo_porter'] = $worker['imageWorker']['photo_porter'];
            $worker['photo_full_length'] = $worker['imageWorker']['photo_full_length'];
            $worker['city'] = $worker['city']['name'];
            $worker['age'] = (int)$worker['age'];
            $worker += ['count_order_offer' => $count_order_offer];
            $worker += ['count_order_active' => $count_order_active];
            $worker += ['count_order_finished' => $count_order_finished];
            unset($worker['hair_length_id']);
            unset($worker['hair_color_id']);
            unset($worker['city_id']);
            unset($worker['eye_color_id']);
            unset($worker['appearance_id']);
            unset($worker['birth_date']);
            unset($worker['hairColor']);
            unset($worker['hairLength']);
            unset($worker['eyeColor']);
            unset($worker['imageWorker']);
            unset($worker['id']);
            return array('success' => 1, 'data' => $worker, 'status' => 200);
        }
        throw new HttpException(400, 'Профиль не найден');
    }

    public function RequestPay($payment_token, $worker_id, $name, $last_name){
//        $client = new Client();
//        $client->setAuth(Yii::$app->params['shop_id_yandex'], Yii::$app->params['token_password_yandex']);
//        $idempotenceKey = uniqid('', true);
//        $response = $client->createPayment(
//            array(
//                'payment_token' => $payment_token,
//                'amount' => array(
//                    'value' => '1.00',
//                    'currency' => 'RUB',
//                ),
//                'confirmation' => array(
//                    'type' => 'redirect',
//                    'locale' => 'ru_RU',
//                    'return_url' => 'http://iamlima.com/return_url',
//                ),
//                'capture' => true,
//                'description' => 'Снятие одного рубля: ' . $last_name .' ' . $name,
//            ),
//            $idempotenceKey
//        );
//
        $worker_pay = WorkerConfirmPay::findOne(['worker_id' => $worker_id]);
        if (empty($worker_pay)){
            $worker_confirm_pay = new WorkerConfirmPay();
        }
        else $worker_confirm_pay = $worker_pay;
//        $worker_confirm_pay = new WorkerConfirmPay();
        $worker_confirm_pay->worker_id = $worker_id;
        $worker_confirm_pay->payment_id = Yii::$app->security->generateRandomString();//$response->id;
        $worker_confirm_pay->status = WorkerConfirmPay::SUCCEEDED;
        $worker_confirm_pay->confirm_refund = WorkerConfirmPay::CONFIRM_REFUND;
        $worker_confirm_pay->save();
        return ['status' => 'succeeded'];
//        if ($response->status == 'succeeded'){
//            if (WorkerPayHelpers::RefundPay($client, $response->id)->status == 'succeeded'){
//                $worker_confirm_pay->status = WorkerConfirmPay::SUCCEEDED;
//                $worker_confirm_pay->confirm_refund = WorkerConfirmPay::CONFIRM_REFUND;
//                $worker_confirm_pay->save();
//                return ['status' => 'succeeded'];
//            }
//            else {
//                $worker_confirm_pay->status = WorkerConfirmPay::NO_REFUND;
//                $worker_confirm_pay->confirm_refund = WorkerConfirmPay::NO_CONFIRM;
//                $worker_confirm_pay->save();
//                return ['status' => 'error_refund'];
//            }
//        }
//        elseif ($response->status == 'pending'){
//            $worker_confirm_pay->status = WorkerConfirmPay::PENDING;
//            $worker_confirm_pay->confirm_refund = WorkerConfirmPay::NO_CONFIRM;
//            $worker_confirm_pay->save();
//            return ['status' => 'pending', 'confirm_url' =>
//                $response->confirmation->confirmationUrl,
//                'payment_id' => $response->id];
//        }
//        else {
//            $worker_confirm_pay->status = WorkerConfirmPay::CANCELED;
//            $worker_confirm_pay->confirm_refund = WorkerConfirmPay::NO_CONFIRM;
//            $worker_confirm_pay->save();
//            return ['status' => 'canceled'];
//        }
    }

    public function actionRepeatPayment(){
        if(Yii::$app->user->isGuest || Yii::$app->user->can('customer')){
            throw new HttpException(401, 'Доступ запрещен');
        }
        $request = Yii::$app->request;
        $payment_token = $request->getBodyParam('payment_token');
        if (is_null($payment_token)){
            throw new HttpException(400, 'Отсутствует токен');
        }
        $worker = Yii::$app->user->identity->worker;
        $model_worker_pay = WorkerConfirmPay::findOne([
            'worker_id' => $worker->id,
            'confirm_refund' => WorkerConfirmPay::NO_CONFIRM]);
        if (!empty($model_worker_pay) &&
            ($model_worker_pay->status != WorkerConfirmPay::NO_REFUND &&
                $model_worker_pay->status != WorkerConfirmPay::SUCCEEDED)){
            $worker_pay = $this->RequestPay($payment_token, $worker->id, $worker->name, $worker->last_name);
            switch ($worker_pay['status']){
                case WorkerConfirmPay::PENDING:
                    $url = $worker_pay['confirm_url'];
                    $payment_id = $worker_pay['payment_id'];
                    break;
                case WorkerConfirmPay::SUCCEEDED:
                    $url = null;
                    $payment_id = null;
                    break;
                case WorkerConfirmPay::CANCELED:
                    $url = null;
                    $payment_id = null;
                    break;
                default:
                    $url = null;
                    $payment_id = null;
                    break;
            }
            return array('success' => 1, 'data' => [
                'status_pay' => $worker_pay['status'],
                'confirm_url' => $url,
                'payment_id' => $payment_id
            ], 'status' => 200);
        }
        throw new HttpException(400, 'Не удалось выполнить');
    }

    public function actionRefundPayment(){
        if(Yii::$app->user->isGuest || Yii::$app->user->can('customer')){
            throw new HttpException(401, 'Доступ запрещен');
        }

        $request = Yii::$app->request;
        $payment_id = $request->getBodyParam('payment_id');
        if (is_null($payment_id)){
            throw new HttpException(400, 'Отсутствует токен');
        }
        $model_worker_pay = WorkerConfirmPay::findOne([
            'status' => WorkerConfirmPay::PENDING,
            'confirm_refund' => WorkerConfirmPay::NO_CONFIRM]);
        if (!empty($model_worker_pay)){
            $client = new Client();
            $client->setAuth(Yii::$app->params['shop_id_yandex'],
                Yii::$app->params['token_password_yandex']);

            $payment = $client->getPaymentInfo($payment_id);
            if($payment->status == WorkerConfirmPay::SUCCEEDED){
                $refund_pay = WorkerPayHelpers::RefundPay($client, $payment_id);
                if ($refund_pay->status == WorkerConfirmPay::SUCCEEDED){
                    $model_worker_pay->status = WorkerConfirmPay::SUCCEEDED;
                    $model_worker_pay->confirm_refund = WorkerConfirmPay::CONFIRM_REFUND;
                    if ($model_worker_pay->save()){
                        return array('success' => 1, 'data' => [
                            'message' => 'Оплата и возрат прошли успешно'
                        ], 'status' => 200);
                    }
                }
            }
        }
        throw new HttpException(400, 'Не удалось подтвердить');
    }
}