<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\v1\models\worker\CategoryWorkerCity */

$this->title = $model->categoryWorker->name .' - '. $model->city->name;
$this->params['breadcrumbs'][] = ['label' => 'Категория работника - город', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-worker-city-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'category_worker_id' => $model->category_worker_id, 'city_id' => $model->city_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'category_worker_id' => $model->category_worker_id, 'city_id' => $model->city_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'category_worker_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->categoryWorker->name;
                },
            ],
            [
                'attribute' => 'city_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->city->name;
                },
            ],
            'price',
            'advance_price',
        ],
    ]) ?>

</div>
