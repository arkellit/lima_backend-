<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\v1\models\worker\CategoryWorkerCity */

$this->title = 'Create Category Worker City';
$this->params['breadcrumbs'][] = ['label' => 'Category Worker Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-worker-city-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'city' => $city,
        'category_worker' => $category_worker
    ]) ?>

</div>
