<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\v1\models\worker\CategoryWorkerCity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-worker-city-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category_worker_id')
        ->dropDownList(ArrayHelper::map($category_worker, 'id', 'name'),
            ['prompt' => 'Выберите категорию работника']) ?>

    <?= $form->field($model, 'city_id')
        ->dropDownList(ArrayHelper::map($city, 'id', 'name'),
            ['prompt' => 'Выберите город']) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'advance_price')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
