<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категория работника - город';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-worker-city-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'category_worker_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->categoryWorker->name;
                },
            ],
            [
                'attribute' => 'city_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->city->name;
                },
            ],
            'price',
            'advance_price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
