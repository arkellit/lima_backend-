<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\v1\models\worker\CategoryWorkerCity */

$this->title = 'Обновить: ' . $model->categoryWorker->name .' - '. $model->city->name;
$this->params['breadcrumbs'][] = ['label' => 'Category Worker Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->categoryWorker->name .' - '. $model->city->name, 'url' => ['view', 'category_worker_id' => $model->category_worker_id, 'city_id' => $model->city_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="category-worker-city-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'city' => $city,
        'category_worker' => $category_worker
    ]) ?>

</div>
