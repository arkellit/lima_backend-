<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

/* @var $chat api\modules\v1\models\chat\Chat */

$this->title = 'Диалог';
$this->params['breadcrumbs'][] = ['label' => 'Все чаты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->registerCssFile('/control-panel/css/main.css') ?>
<div id="wrapper">
  <div id="menu" style="display: none">
        <p class="welcome">Dialog<b></b></p>
        <div style="clear:both"></div>
    </div>

    <?php Pjax::begin(); ?>
        <div id="chatbox">
            <?php
            foreach ($chat as $item):?>
                <div  class="one_message">
                <?= $item->createdAt ?>
                <b class="phone_check" data-status="<?= $item->status ?>" data-id="<?= $item->id ?>"><?= $item->user_From->phone ?>: </b>
                <?= $item->message ?>
                <?php if (!is_null($item->attachment)):?>
                    <br><?= Html::img(Yii::$app->params['urlImage']  .$item->attachment, ['style' => 'width:300px']) ?>
                <?php endif;?>
                <?php if(!next($chat) && $item->user_from_id != Yii::$app->params['admin_id']):?>
                    <div class="status_message"><?= $item->status == 0 ? '(Не прочитано)' : '(Прочитано)' ?></div>
                <?php endif;?>
                <br>
                </div>
            <?php endforeach;?>
        </div>
        <?= Html::button("Обновить", [
                'id' => 'refreshButton',
                'class' => 'btn btn-lg btn-primary hidden']) ?>

    <?php Pjax::end(); ?>

    <?php $form = ActiveForm::begin([
            'action' => ['/lima-help/send-message']
    ]); ?>
    <?= $form->field($model, 'text')->textarea(['maxlength' => true]) ?>
    <?= $form->field($model, 'image')->fileInput() ?>

    <div>
        <img id="img-preview" src=""/>
        <a href="javascript:void(0)" style="display:none;" title="Удалить" id="reset-img-preview" class="btn btn-warning btn-remove-img btn-remove-img_new">X</a>

    </div>
    <?= $form->field($model, 'user_id')->hiddenInput(['value' => $user_id])->label(false);?>
    <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>

    <?php ActiveForm::end(); ?>
</div>

<?php
$count_message = count($chat);
$user_id_mes = Yii::$app->request->get('user_id');
$script = <<<JS
$('#messageform-text').click(function () {  
    var ids_messages  = [];
    var id;
    $('#chatbox b.phone_check').each(function(index) { 
        if($(this).attr('data-status') == 0){
            id = $(this).attr('data-id');
            ids_messages.push(id);
        }
    });  
    
    if(ids_messages.length != 0){
        $('.status_message').text('(Прочитано)');
        var ids_messages = JSON.stringify(ids_messages);
        var csrf_token = yii.getCsrfToken(); 
                
        $.post('/control-panel/lima-help/update-read', {ids_messages:ids_messages, '_csrf-backend':csrf_token}, function(response) {
            console.log('Прочитано');
        });
    }
});

$(document).ready(function() {
    var scrollinDiv = document.getElementById('chatbox'); 
    scrollinDiv.scrollTop = 9999; 
    
    var count = '$count_message'; 
    var user_id = '$user_id_mes';  
    setInterval(function(){ 
        $.post('/control-panel/lima-help/check-new-message', {user_id:user_id, count:count}, function(response) {
            if(response ==1){
                location.reload();
            }
        });
    }, 3000);
});

$('#messageform-image').change(function () {
        var input = $(this)[0];
        if (input.files && input.files[0]) {
            if (input.files[0].type.match('image.*')) {
                var reader = new FileReader();
                reader.onload = function (e) {
					// $('#img-preview').css("background-image", 'url('+e.target.result+')');
					$('#reset-img-preview').css("display",'');
                    $('#img-preview').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            } else {
                console.log('ошибка, не изображение');
            }
        } else {
            console.log('что-то не то...');
        }
    });

    $('#reset-img-preview').click(function() {
        $('#messageform-image').val('');
        $('#img-preview').attr('src', '');
        $('#img-preview').css("background-image",'');//.attr('src', 'default-preview.jpg');
		$('#reset-img-preview').css("display","none");
    });

JS;
$this->registerJs($script);

?>

