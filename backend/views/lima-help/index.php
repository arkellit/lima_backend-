<?php

use api\modules\v1\models\chat\Chat;
use \yii\helpers\Url;
use yii\widgets\LinkPager;

/* @var $chat api\modules\v1\models\chat\Chat */

$this->title = 'Все чаты';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
<table class="table table-inverse">
    <thead>
    <tr>
        <th>Статус</th>
        <th>Пользователь</th>
        <th>Текст сообщения</th>
        <th>Дата отправки</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($chats as $item):?>
    <tr>
        <td>
            <?php if ($item->user_from_id != Yii::$app->params['admin_id']):?>
            <?= $item->status == 0 ? '<span title="Не прочитано" style="color: red" class="glyphicon glyphicon-remove"></span>' :
                '<span title="Прочитано" style="color:green;" class="glyphicon glyphicon-ok"></span>' ?>
            <?php endif;?>
        </td>
        <td><b><?= $item->user_From['phone'] ?> </b></td>\
        <td><?= $item->user_from_id == Yii::$app->params['admin_id'] ? 'Я: ' : '' ?> <?=  $item['message']  ?></td>
        <td><?=  Yii::$app->formatter->asDatetime($item['created_at'], 'php:d-m-Y H:i')  ?></td>
        <td><a href="<?= Url::to(['/lima-help/chat', 'user_id' =>  $item->user_from_id == Yii::$app->params['admin_id'] ? $item->user_to_id : $item->user_from_id]) ?>">
                Перейти к чату</a></td>
    </tr>
    <?php endforeach;?>

    </tbody>
</table>
    <div class="row" style="text-align: center;">
        <?=  LinkPager::widget([
            'pagination' => $pages,
            'options' => [
                'class' => 'pagination justify-content-center',
            ],
            'linkOptions' => [
                'class' => 'page-link',
            ],
            'pageCssClass' => 'page-item',
            'prevPageCssClass' => 'page-item',
            'nextPageCssClass' => 'page-item',
        ]) ?>
    </div>
</div>
