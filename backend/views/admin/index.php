<?php

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
?>
<div class="container">
    <div class="col-md-12">
        <div class="alert alert-success" role="alert">
            Баланс smsc.ru  <?= Yii::$app->sms->get_balance();?>
        </div>
        <div class="row">
            <div class="col-md-4">
                <a href="/control-panel/news" class="btn btn-primary btn-lg">Создать новость</a>
            </div>
            <div class="col-md-4">
                <a href="/control-panel/notification" class="btn btn-primary btn-lg">Отправка пушей</a>
            </div>
            <div class="col-md-4">
                <a href="/control-panel/region" class="btn btn-primary btn-lg">Города</a>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top:15px;">
        <div class="row">
            <div class="col-md-4">
                <a href="/control-panel/user" class="btn btn-primary btn-lg">Пользователи</a>
            </div>
            <div class="col-md-4">
                <a href="/control-panel/category-worker-city" class="btn btn-primary btn-lg">Цена работника</a>
            </div>
            <div class="col-md-4">
                <a href="/control-panel/application-passport" class="btn btn-primary btn-lg">Заявка смены паспортных данных</a>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top:15px;">
        <div class="row">
            <div class="col-md-4">
                <a href="/control-panel/user-statistics" class="btn btn-primary btn-lg">Статистика пользователей</a>
            </div>
            <div class="col-md-4">
                <a href="/control-panel/lima-help" class="btn btn-primary btn-lg">Lima-Help</a>
            </div>
            <div class="col-md-4">
                <a href="/control-panel/worker-parameter" class="btn btn-primary btn-lg">Параметры исполнителя</a>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top:15px;">
        <div class="row">
            <div class="col-md-4">
                <a href="/control-panel/customer-new" class="btn btn-primary btn-lg">Новые заказчики(<?= $customer_count ?>)</a>
            </div>
            <div class="col-md-4">
                <a href="/control-panel/worker/worker" class="btn btn-primary btn-lg">Новые исполнители(<?= $worker_count ?>)</a>
            </div>
            <div class="col-md-4">
                <a href="/control-panel/customer" class="btn btn-primary btn-lg">Заказчики</a>
            </div>
        </div>
    </div>

    <div class="col-md-12" style="margin-top:15px;">
        <div class="row">
            <div class="col-md-4">
                <a href="/control-panel/printing-data" class="btn btn-primary btn-lg">Данные для печати</a>
            </div>
            <div class="col-md-4">
                <a href="/control-panel/lima-question" class="btn btn-primary btn-lg">LimaHelp - вопрос-ответ</a>
            </div>
        </div>
    </div>

    <div class="col-md-12" style="margin-top:15px;">
        <div class="row">
            <div class="col-md-4">
                <a href="/control-panel/feedback" class="btn btn-warning btn-lg">Обратная связь(<?= $feedback_count ?>)</a>
            </div>
            <div class="col-md-4">
                <a href="/control-panel/news-front" class="btn btn-warning btn-lg">Новости на сайт Лима</a>
            </div>
        </div>
    </div>
</div>
