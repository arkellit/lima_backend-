<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\printing\PrintingTemplate */

$this->title = 'Update Printing Template: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Printing Templates', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="printing-template-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_new_form', [
        'model' => $model,
    ]) ?>

</div>
