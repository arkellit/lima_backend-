<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\printing\PrintingTemplate */

$this->title = 'Create Printing Template';
$this->params['breadcrumbs'][] = ['label' => 'Printing Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printing-template-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
