<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\printing\PrintingTemplate */
/* @var $form yii\widgets\ActiveForm */
$city = \api\modules\v1\models\City::find()->all();
$items = ArrayHelper::map($city,'id','name');
?>

<div class="printing-template-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'city_id')->dropDownList($items, ['prompt' => 'Выберите город']) ?>



    <?= $form->field($model, 'name')->textInput() ?>

    <?= $form->field($model, 'price')->input('number', [
        'min' => 0
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
