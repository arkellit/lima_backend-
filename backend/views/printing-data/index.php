<?php

use yii\helpers\Html;
use yii\grid\GridView;
use api\modules\v1\models\printing\PrintingTemplate;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printing Templates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printing-template-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Printing Template', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'city_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->city->name;
                },
            ],
            [
                'attribute' => 'category',
                'format' => 'raw',
                'value' => function ($model) {
                    switch ($model->category){
                        case PrintingTemplate::EDITION:
                            $name = 'Тираж';
                            break;
                        case PrintingTemplate::PRINT_TYPE:
                            $name = 'Тип печати';
                            break;
                        case PrintingTemplate::PRINT_SIZE:
                            $name = 'Размер листовок';
                            break;
                        case PrintingTemplate::PAPER_DENSITY:
                            $name = 'Плотность бумаги';
                            break;
                        case PrintingTemplate::PRINT_TIME:
                            $name = 'Время печати';
                            break;
                        case PrintingTemplate::COURIER_PRICE:
                            $name = 'Цена курьера';
                            break;
                    }
                    return $name;
                },
            ],
            'name',
            'price',
            //'paper_density',
            //'print_time',
            //'courier_price',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
            ],
        ],
    ]); ?>
</div>
