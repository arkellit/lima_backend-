<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\NewsFront */

$this->title = 'Create News Front';
$this->params['breadcrumbs'][] = ['label' => 'News Fronts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-front-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
