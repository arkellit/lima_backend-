<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use common\models\NewsFront;
use  kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\NewsFront */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-front-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'previewFileType' => 'image',
            'showUpload' => false,
            'initialPreview'=> [
                $model->image != null ? '<img style="max-width:100%;max-height: 100%;" src="'.Yii::$app->params['urlImage'].$model->image.'" class="file-preview-image">' : '',
            ],
            'initialCaption'=> $model->image,
        ],

    ]) ?>

    <?= $form->field($model, 'short_description')->textarea() ?>

    <?= $form->field($model, 'description')->widget(TinyMce::className(), [
        'options' => ['rows' => 18],
        'language' => 'ru',
        'clientOptions' => [
            'plugins' => [
                'advlist autolink lists link charmap hr preview pagebreak',
                'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                'save insertdatetime media table contextmenu template paste image responsivefilemanager filemanager',
            ],
            'toolbar' => 'forecolor backcolor | undo redo | styleselect | 
            fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media',
            'external_filemanager_path' => '/control-panel/plugins/responsive_filemanager/filemanager/',
            'filemanager_title' => 'Responsive Filemanager',
            'external_plugins' => [
                //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                'filemanager' => '/control-panel/plugins/responsive_filemanager/filemanager/plugin.min.js',
                //Иконка/кнопка загрузки файла в панеле иснструментов.
                'responsivefilemanager' => '/control-panel/plugins/responsive_filemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
            ],
            'relative_urls' => false,
        ]
    ]); ?>

    <?= $form->field($model, 'is_published')->radioList([
            NewsFront::NO_PUBLISH => 'Не опубликовать',
            NewsFront::PUBLISH => 'Опубликовать'
    ]) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
