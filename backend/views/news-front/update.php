<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\NewsFront */

$this->title = 'Update News Front: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News Fronts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="news-front-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
