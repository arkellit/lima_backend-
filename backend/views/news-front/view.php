<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\NewsFront */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News Fronts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-front-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            [
                'attribute' => 'short_description',
                'contentOptions' => ['style' => 'width:600px; white-space: normal;'],
            ],
            'description:html',
            [
                'label' => 'Изображение',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img(Yii::$app->params['urlImage']  . $data->image,[
                        'style' => 'width:250px;',
                        'class' => 'img-responsive'
                    ]);
                },
            ],
            [
                'attribute' => 'is_published',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->is_published == 0 ? 'Не опубликован'
                        : 'Опубликован';
                },
            ],
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDatetime($model->created_at, 'php:Y-m-d H:i');
                },
            ],
        ],
    ]) ?>

</div>
