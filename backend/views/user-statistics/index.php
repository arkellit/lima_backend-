<?php
?>

<div class="alert alert-success" role="alert">
    <h3>Количество пользователей зарегистрированных за сутки: <b style="color: red"><?= $users_today;?></b></h3>
</div>
<div class="alert alert-success" role="alert">
    <h3>Количество пользователей зарегистрированных за 3 дня: <b style="color: red"><?= $users_three_date;?></b></h3>
</div>
<div class="alert alert-success" role="alert">
    <h3>Количество пользователей зарегистрированных за неделю: <b style="color: red"><?= $users_week_date;?></b></h3>
</div>
