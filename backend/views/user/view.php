<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\v1\models\auth\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
<!--        --><?//= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<!--        --><?//= Html::a('Delete', ['delete', 'id' => $model->id], [
//            'class' => 'btn btn-danger',
//            'data' => [
//                'confirm' => 'Are you sure you want to delete this item?',
//                'method' => 'post',
//            ],
//        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'phone',
            [
                'attribute' => 'is_blocked',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->is_blocked == 0 ? 'Нет' : 'Да' ;
                },
            ],
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model->created_at, 'php:Y-m-d');
                },
            ],
            [
                'attribute' => 'is_confirm',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->is_confirm == 0 ? 'Нет' : 'Да' ;
                },
            ],
            [
                'attribute' => 'is_social',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->is_social == 0 ? 'Нет' : 'Да' ;
                },
            ],

        ],
    ]) ?>

</div>
