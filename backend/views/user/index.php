<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\v1\models\auth\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'phone',

            [
                'attribute' => 'is_blocked',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->is_blocked == 0 ? 'Нет' : 'Да' ;
                },
            ],
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model->created_at, 'php:Y-m-d');
                },
            ],
            'registration_ip',
            [
                'attribute' => 'is_confirm',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->is_confirm == 0 ? 'Нет' : 'Да' ;
                },
            ],
            [
                'attribute' => 'is_social',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->is_social == 0 ? 'Нет' : 'Да' ;
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>
</div>
