<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel api\modules\v1\models\customer\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Customer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'phone',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->user->phone;
                },
            ],
            'name',
            'last_name',
            'middle_name',
            //'is_register_complete',
            //'photo',

            [
                'class' => \yii\grid\ActionColumn::className(),
                'template' => '{view}',
            ]
        ],
    ]); ?>
</div>
