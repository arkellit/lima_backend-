<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\customer\Customer */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-view">

    <h1><?= Html::encode($this->title) ?></h1>



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'last_name',
            'middle_name',
            [
                'attribute' => 'phone',
                'label' => 'Номер телефона',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->user->phone;
                },
            ],
            [
                'attribute' => 'name_company',
                'label' => 'Наименовании компании',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->companyData->name_company;
                },
            ],
            [
                'label' => 'ИНН компании',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->companyData->inn_company;
                },
            ],
            [
                'label' => 'КПП компании',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->companyData->kpp_company;
                },
            ],
            [
                'label' => 'Фактический адрес',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->companyData->actual_address;
                },
            ],
            [
                'label' => 'E-mail',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->companyData->email;
                },
            ],
            [
                'label' => 'Контактный телефон',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->companyData->contact_phone;
                },
            ],
        ],
    ]) ?>
    <p>
        <?= Html::a('Подтвердить профиль?', ['confirm', 'id' => $model->id], [
                    'class' => 'btn btn-success',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
    </p>
</div>
