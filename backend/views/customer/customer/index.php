<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Customer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'label' => 'Номер телефона',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->user->phone;
                },
            ],
            [
                'attribute' => 'is_register_complete',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->is_register_complete == 0 ? 'Нет' : 'Да';
                },
            ],
            [
                'label' => 'Управляющий',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->last_name . ' '. $model->name . ' '. $model->middle_name;
                },
            ],
            [
                'label' => 'Дата регистрации',
                'format' => 'raw',
                'value' => function ($model) {
                    $date_created = $model->dateRegister;
                    return !empty($date_created) ?
                        Yii::$app->formatter->asDate($date_created->created_at, 'php:d.m.Y') : '';
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
