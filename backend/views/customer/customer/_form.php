<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\Url;

$customer = Yii::$app->session->get('customer');
$company_data = Yii::$app->session->get('customer_company_data');
$bank_data = Yii::$app->session->get('customer_bank_data');

/* @var $this yii\web\View */
/* @var $model backend\models\forms\CustomerForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>

<!--    --><?//= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model_company_data, 'inn_company')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model_company_data, 'name_company')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model_company_data, 'kpp_company')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model_company_data, 'actual_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model_company_data, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model_company_data, 'contact_phone')->textInput(['maxlength' => true]) ?>

    <?php $model_bank_data->customer_rs = \common\helpers\EncryptionHelpers::dec_enc('decrypt', $model_bank_data->customer_rs) ?>

    <?= $form->field($model_bank_data, 'customer_rs')->textInput() ?>

    <?= $form->field($model_bank_data, 'bank_bik')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model_bank_data, 'bank_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model_bank_data, 'bank_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model_bank_data, 'bank_ks')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model_bank_data, 'bank_kpp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model_bank_data, 'bank_inn')->textInput(['maxlength' => true]) ?>

    <!--    --><?//= $form->field($model_customer_form, 'is_register_complete')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

