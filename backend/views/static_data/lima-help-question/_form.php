<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \api\modules\v1\models\LimaHelpQuestion;
/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\LimaHelpQuestion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lima-help-question-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')
        ->radioList([
            LimaHelpQuestion::TYPE_WORKER => 'Лима исполнитель',
            LimaHelpQuestion::TYPE_CUSTOMER => 'Лима заказчик',
        ]);?>

    <?= $form->field($model, 'question')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'answer')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'sort')->textInput()->hint('Введите порядковый номер вопрос. По возрастанию будут отсортированы') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
