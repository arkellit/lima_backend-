<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\LimaHelpQuestion */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Lima Help Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lima-help-question-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'type',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->type == \api\modules\v1\models\LimaHelpQuestion::TYPE_WORKER ?
                        'Лима исполнитель' : 'Лима заказчик';
                },
            ],
            'question',
            'answer',
            'sort',
        ],
    ]) ?>

</div>
