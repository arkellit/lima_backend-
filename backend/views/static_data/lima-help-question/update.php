<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\LimaHelpQuestion */

$this->title = 'Update Lima Help Question: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Lima Help Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lima-help-question-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
