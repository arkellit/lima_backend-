<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\LimaHelpQuestion */

$this->title = 'Create Lima Help Question';
$this->params['breadcrumbs'][] = ['label' => 'Lima Help Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lima-help-question-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
