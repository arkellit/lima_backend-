<?php

use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\news\News */
/* @var $form yii\widgets\ActiveForm */
/* @var $all_images api\modules\v1\models\news\ImageNews */
/* @var $view_all_images api\modules\v1\models\news\ImageNews */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'image')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'previewFileType' => 'image',
            'showUpload' => false,
            'initialPreview'=> [
                $model->image != null ? '<img style="max-width:100%;max-height: 100%;" src="'.Yii::$app->params['urlImage'].$model->image.'" class="file-preview-image">' : '',
            ],
            'initialCaption'=> $model->image,
        ],

    ]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'full_description')->textarea(['rows' => 6]) ?>

    <?php if(!empty($view_all_images)):?>
        <?php foreach ($view_all_images as $value){
            echo '<img style="max-width:200px%;max-height: 200px; margin-left:20px;" src="'. Yii::$app->params['urlImage'] . $value->image.'" class="file-preview-image">' . '<span class="delete-image" data-id="'.$value->id.'" title="Удалить" style="font-size: 25px;color: red;">X</span>';

        }?>
    <?php endif;?>
    <?= $form->field($all_images, 'image[]')->widget(FileInput::classname(), [
        'options' => [ 'multiple' => true,'accept' => 'image/*'],
        'pluginOptions' => [
            'previewFileType' => 'image',
            'showUpload' => false,

        ],

    ]) ?>

    <?= $form->field($model, 'is_send_push')
        ->radioList([
            0 => 'Не делать рассылку',
            1 => 'Сделать рассылку',
        ]);?>

    <?= $form->field($model, 'is_published')
        ->radioList([
            1 => 'Опубликовать',
            0 => 'Не опубликовывать',
        ]);?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$js = <<<JS
    $('.delete-image').on('click', function() {
        var id = $(this).attr('data-id');
        $.post('/control-panel/news/delete-image', {id:id}, function(response) {
            if(response == 1){
                location.reload();
            }
            else {
                alert('Произошла ошибка при удалении');
            }
        });
    });
JS;
$this->registerJs($js);

?>
