<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\news\News */
/* @var $all_images api\modules\v1\models\news\ImageNews */

$this->title = 'Create News';
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'all_images' => $all_images
    ]) ?>

</div>
