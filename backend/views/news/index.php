<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel api\modules\v1\models\news\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create News', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
                'label' => 'Изображение',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img(Yii::$app->params['urlImage']  . $data->image,[
                        'style' => 'width:250px;',
                        'class' => 'img-responsive'
                    ]);
                },
            ], 
            [
                'attribute' => 'short_description',
                'format' => 'raw', 
                'contentOptions'=>['style'=>'white-space: normal;'],
            ],
             //'full_description:ntext',
            [
                'attribute' => 'is_send_push',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->is_send_push == 0 ? 'Не отправлен' : 'Отправлен';
                },
            ],

            [
                'attribute' => 'is_published',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->is_published == 0 ? 'Нет' : 'Да';
                },
            ],

            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model->created_at,
                        'php:Y-m-d');
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
