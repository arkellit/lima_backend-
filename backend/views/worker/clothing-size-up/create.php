<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\worker\ClothingSizeUp */

$this->title = 'Create Clothing Size Up';
$this->params['breadcrumbs'][] = ['label' => 'Все параметры', 'url' => ['/worker-parameter']];
$this->params['breadcrumbs'][] = ['label' => 'Clothing Size Ups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clothing-size-up-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
