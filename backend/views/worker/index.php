<?php

/* @var $this yii\web\View */

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="container">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <a href="/control-panel/worker/appearance" class="btn btn-primary btn-lg">Внешность</a>
            </div>
            <div class="col-md-4">
                <a href="/control-panel/worker/body-complexion" class="btn btn-primary btn-lg">Комплекция тела</a>
            </div>
            <div class="col-md-4">
                <a href="/control-panel/worker/category-worker" class="btn btn-primary btn-lg">Категория работника</a>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top:15px;">
        <div class="row">
            <div class="col-md-4">
                <a href="/control-panel/worker/clothing-size-down" class="btn btn-primary btn-lg">Низ одежды</a>
            </div>
            <div class="col-md-4">
                <a href="/control-panel/worker/clothing-size-up" class="btn btn-primary btn-lg">Верх одежды</a>
            </div>
            <div class="col-md-4">
                <a href="/control-panel/worker/eye-color" class="btn btn-primary btn-lg">Цвет глаз</a>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top:15px;">
        <div class="row">
            <div class="col-md-4">
                <a href="/control-panel/worker/hair-color" class="btn btn-primary btn-lg">Цвет волос</a>
            </div>
            <div class="col-md-4">
                <a href="/control-panel/worker/hair-length" class="btn btn-primary btn-lg">Длина волос</a>
            </div>
        </div>
    </div>

</div>
