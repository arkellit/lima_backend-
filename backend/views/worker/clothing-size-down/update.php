<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\worker\ClothingSizeDown */

$this->title = 'Update Clothing Size Down: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Все параметры', 'url' => ['/worker-parameter']];
$this->params['breadcrumbs'][] = ['label' => 'Clothing Size Downs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="clothing-size-down-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
