<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\worker\HairColor */

$this->title = 'Create Hair Color';
$this->params['breadcrumbs'][] = ['label' => 'Все параметры', 'url' => ['/worker-parameter']];
$this->params['breadcrumbs'][] = ['label' => 'Hair Colors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hair-color-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
