<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\worker\Appearance */

$this->title = 'Create Appearance';
$this->params['breadcrumbs'][] = ['label' => 'Все параметры', 'url' => ['/worker-parameter']];
$this->params['breadcrumbs'][] = ['label' => 'Appearances', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appearance-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
