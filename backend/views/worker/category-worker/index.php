<?php

use yii\helpers\Html;
use yii\grid\GridView;
use api\modules\v1\models\worker\CategoryWorker;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Category Workers';
$this->params['breadcrumbs'][] = ['label' => 'Все параметры', 'url' => ['/worker-parameter']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-worker-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Category Worker', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'is_active',
                'value' => function ($model) {
                    if ($model->is_active) {
                        return Html::a('Заблокировать', ['change-status', 'id' => $model->id], [
                            'class' => 'btn btn-xs btn-success btn-block',
                            'data-method' => 'post',
                            'data-confirm' => 'Действительно хотите сделать неактивным?',
                        ]);
                    } else {
                        return Html::a('Разблокировать', ['change-status', 'id' => $model->id], [
                            'class' => 'btn btn-xs btn-danger btn-block',
                            'data-method' => 'post',
                            'data-confirm' => 'Действительно хотите разблокировать?',
                        ]);
                    }
                },
                'format' => 'raw',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
