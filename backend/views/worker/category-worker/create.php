<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\worker\CategoryWorker */

$this->title = 'Create Category Worker';
$this->params['breadcrumbs'][] = ['label' => 'Все параметры', 'url' => ['/worker-parameter']];
$this->params['breadcrumbs'][] = ['label' => 'Category Workers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-worker-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
