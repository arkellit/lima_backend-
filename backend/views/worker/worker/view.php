<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\worker\Worker */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Worker', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-view">

    <h1><?= Html::encode($this->title) ?></h1>



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'last_name',
            'middle_name',
            [
                'label' => 'Статус снятие 1 рубля',
                'format' => 'raw',
                'value' => function ($model) {
                    if (empty($model->workerConfirmPay)){
                        return 'Не было верификации аккаунт(снятие 1 рубля)';
                    }
                    $status = $model->workerConfirmPay->status;
                    if ($status == \api\modules\v1\models\worker\WorkerConfirmPay::SUCCEEDED){
                        return 'Оплата прошла успешно';
                    }

                    return '<div style="color: red;">Оплата не успешная!</div>';
                },
            ],
            [
                'attribute' => 'phone',
                'label' => 'Номер телефона',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->user->phone;
                },
            ],
        ],
    ]) ?>
    <p>
        <?= Html::a('Подтвердить профиль?', ['confirm', 'id' => $model->id], [
                    'class' => 'btn btn-success',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
    </p>
</div>
