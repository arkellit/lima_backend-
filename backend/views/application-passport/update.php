<?php

use yii\helpers\Html;
use common\helpers\EncryptionHelpers;

/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\worker\PassportData */
$name = EncryptionHelpers::dec_enc('decrypt', $model->passport_name);
$last_name = EncryptionHelpers::dec_enc('decrypt', $model->passport_last_name);
$this->title = 'Обновить паспортные данные: ' . $name . ' ' . $last_name;
$this->params['breadcrumbs'][] = ['label' => 'passport', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="news-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
