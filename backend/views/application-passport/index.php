<?php

use common\helpers\EncryptionHelpers;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки на смену паспортных данных';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-change-passport-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'label' => 'Имя Фамилия работника',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->worker->name . ' '. $model->worker->last_name;
                },
            ],
            [
                'attribute' => 'passport_photo_one',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<a  target="_blank" href="/control-panel/img.php?img='.EncryptionHelpers::dec_enc('decrypt',$model->passport_photo_one).'">Просмотреть фото</a>';
                },
            ],
            [
                'attribute' => 'passport_photo_two',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<a target="_blank" href="/control-panel/img.php?img='.EncryptionHelpers::dec_enc('decrypt',$model->passport_photo_two).'">Просмотреть фото</a>';
                },
            ],
            [
                'attribute' => 'is_changed',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->is_changed == 0 ? 'Нет' : 'Да';
                },
            ],
            [
                'attribute' => 'updated_at',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model->updated_at,
                        'php:Y-m-d H:i');
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function ($url, $model) {
                        if($model->is_changed != 1){

                            $url = '/control-panel/application-passport/update?id=' . $model->worker_id;
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => 'Изменить',
                            ]);

                        }
                    }
                ]
            ],
        ],
    ]); ?>
</div>
