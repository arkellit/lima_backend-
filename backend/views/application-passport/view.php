<?php

use common\helpers\EncryptionHelpers;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\worker\ApplicationChangePassport */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заявка на смену паспортных данных', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-change-passport-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'worker_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->worker->name . ' '. $model->worker->last_name;
                },
            ],
            [
                'attribute' => 'passport_photo_one',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<a  target="_blank" href="/control-panel/img.php?img='.EncryptionHelpers::dec_enc('decrypt',$model->passport_photo_one).'">Просмотреть фото</a>';
                },
            ],
            [
                'attribute' => 'passport_photo_two',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<a target="_blank" href="/control-panel/img.php?img='.EncryptionHelpers::dec_enc('decrypt',$model->passport_photo_two).'">Просмотреть фото</a>';
                },
            ],
            [
                'attribute' => 'is_read',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->is_read == 0 ? 'Нет' : 'Да';
                },
            ],
            [
                'attribute' => 'updated_at',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model->updated_at,
                        'php:Y-m-d H:i');
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]) ?>

</div>
