<?php

use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\worker\PassportData */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'passport_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'passport_last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'passport_middle_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'passport_series')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'passport_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'passport_birth_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'passport_whom')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'passport_when')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'passport_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'passport_residence_address')->textarea(['rows' => 6]) ?>




    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$js = <<<JS
    $('.delete-image').on('click', function() {
        var id = $(this).attr('data-id');
        $.post('/control-panel/news/delete-image', {id:id}, function(response) {
            if(response == 1){
                location.reload();
            }
            else {
                alert('Произошла ошибка при удалении');
            }
        });
    });
JS;
$this->registerJs($js);

?>
