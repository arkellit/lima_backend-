<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Feedbacks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedback-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'email:email',
            'message:ntext',
            [
                'label' => 'Изображение',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img(Yii::$app->params['urlImage']  . $data->image,[
                        'style' => 'width:250px;',
                        'class' => 'img-responsive'
                    ]);
                },
            ],
            [
                'attribute' => 'is_view',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->is_view == 0 ? 'Не просмотрен' : 'Просмотрен';
                },
            ],
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDatetime($model->created_at, 'php:Y-m-d H:i');
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}',],
        ],
    ]); ?>
</div>
