<?php
use yii\helpers\Html;
use api\modules\v1\models\NotificationForm;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model api\modules\v1\models\NotificationForm */
/* @var $form ActiveForm */
?>

<?php
$items_send=[
    NotificationForm::SEND_ALL_USERS => 'Разослать всем пользователям',
    NotificationForm::SEND_ALL_WORKER => 'Разослать исполнителям'
];

$items_type=[
    NotificationForm::TYPE_INFO => 'Информационная рассылка'
];

$items_sound = [
    NotificationForm::NO_SOUND => 'Выключить звуковое оповещение',
    NotificationForm::IS_SOUND=> 'Использовать звуковое оповещение'
];


$this->title = 'Отправка пушей';
$this->params['breadcrumbs'][] = $this->title;
//echo Html::dropDownList('cat', 'null', $items);
?>

<div class="admin-notification-index">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'send_users')->dropDownList($items_send); ?>
    <?= $form->field($model, 'type')->dropDownList($items_type);  ?>
    <?= $form->field($model, 'title') ?>
    <?= $form->field($model, 'description')->textarea() ?>

    <?php $model->sound = 0;?>
    <?= $form->field($model, 'sound')->radioList($items_sound);  ?>
    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
