<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'name' => 'Lima admin',
    'language' => 'ru',
    'homeUrl'=> '/control-panel',
    'modules' => [],
    'components' => [
        'request' => [
            'baseUrl' => '/control-panel',
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'frontend\models\User',
//            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => '/admin/index',
                'login' => '/site/login',
                'worker-parameter' => '/worker/worker-parameter',
                'customer-new' => '/customer/customer-new',
                'customer' => '/customer/customer',
                'lima-question' => '/static_data/lima-help-question'
            ],
        ],
    ],
    'params' => $params,
];
