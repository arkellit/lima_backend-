<?php

namespace backend\controllers;

use api\modules\v1\models\Upload;
use Yii;
use common\models\NewsFront;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * NewsFrontController implements the CRUD actions for NewsFront model.
 */
class NewsFrontController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all NewsFront models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => NewsFront::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NewsFront model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NewsFront model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NewsFront();
        $model->scenario = NewsFront::SCENARIO_CREATE;

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            if ($image !== null) {
                $upload_form = new Upload();
                $upload_form->scenario = Upload::SCENARIO_IMAGE_FILE;
                $upload_form->image = $image;
                $model->image = $upload_form->upload('news_front');
            }
            if ($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing NewsFront model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $tmp_image_name = $model->image;
        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            if ($image !== null) {
                $upload_form = new Upload();
                $upload_form->scenario = Upload::SCENARIO_IMAGE_FILE;
                $upload_form->image = $image;
                $image_name = $upload_form->upload('news_front');

                if($tmp_image_name != null || '') {
                    if (file_exists(Yii::getAlias('@image/web' . $tmp_image_name))){
                        unlink(Yii::getAlias('@image/web' . $tmp_image_name));
                    }
                }
            }

            $model->image = $image_name ?? $tmp_image_name;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing NewsFront model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = NewsFront::findOne($id);
        $this->findModel($id)->delete();
        if($model->image !== null){
            unlink(Yii::getAlias('@image/web'. $model->image));
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the NewsFront model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NewsFront the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NewsFront::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
