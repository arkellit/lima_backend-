<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 25.09.18
 * Time: 12:14
 */

namespace backend\controllers;

use api\modules\v1\models\NotificationForm;
use api\modules\v1\models\Sending;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class NotificationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }
    public function actionIndex(){
        $model = new NotificationForm();
        if($model->load(Yii::$app->request->post())){
            switch ($model->send_users){
                case NotificationForm::SEND_ALL_USERS:
                    $result_sending = Sending::sendNotification(null, $model->title,
                        $model->description, NotificationForm::TYPE_INFO,
                        $model->sound);
                    break;
                case NotificationForm::SEND_ALL_WORKER:
                    $result_sending = Sending::sendNotification('worker',
                        $model->title, $model->description, NotificationForm::TYPE_INFO,
                        $model->sound);
                    break;
//                case NotificationForm::SEND_ALL_CUSTOMER:
//                    $result_sending = Sending::sendNotification('customer');
//                    break;
                default:
                    return null;
            }

            if ($result_sending == true){
                Yii::$app->session->setFlash('success', 'Уведомление успешно отправлено');
                return $this->refresh();
            }

            Yii::$app->session->setFlash('error', 'Произошла ошибка');
            var_dump($result_sending);
        }
        return $this->render('index', [
            'model' => $model
        ]);
    }
}