<?php

namespace backend\controllers;

use api\modules\v1\models\worker\PassportData;
use common\helpers\EncryptionHelpers;
use Yii;
use api\modules\v1\models\worker\ApplicationChangePassport;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ApplicationPassportController implements the CRUD actions for ApplicationChangePassport model.
 */
class ApplicationPassportController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all ApplicationChangePassport models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ApplicationChangePassport::find(),
            'sort'=> ['defaultOrder' => ['updated_at'=>SORT_DESC]]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ApplicationChangePassport model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ApplicationChangePassport model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ApplicationChangePassport();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ApplicationChangePassport model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = PassportData::findOne(['worker_id' => $id]);
        $model->passport_name = EncryptionHelpers::dec_enc('decrypt', $model->passport_name);
        $model->passport_last_name = EncryptionHelpers::dec_enc('decrypt', $model->passport_last_name);
        $model->passport_middle_name = EncryptionHelpers::dec_enc('decrypt', $model->passport_middle_name);
        $model->passport_series = EncryptionHelpers::dec_enc('decrypt', $model->passport_series);
        $model->passport_number = EncryptionHelpers::dec_enc('decrypt', $model->passport_number);
        $model->passport_birth_date = EncryptionHelpers::dec_enc('decrypt', $model->passport_birth_date);
        $model->passport_whom = EncryptionHelpers::dec_enc('decrypt', $model->passport_whom);
        $model->passport_code = EncryptionHelpers::dec_enc('decrypt', $model->passport_code);
        $model->passport_when = EncryptionHelpers::dec_enc('decrypt', $model->passport_when);
        $model->passport_residence_address = EncryptionHelpers::dec_enc('decrypt', $model->passport_residence_address);

        $tmp_photo_one = $model->passport_photo_one;
        $tmp_photo_two = $model->passport_photo_two;

        $model->passport_photo_one = EncryptionHelpers::dec_enc('decrypt', $model->passport_photo_one);
        $model->passport_photo_two = EncryptionHelpers::dec_enc('decrypt', $model->passport_photo_two);

        if ($model->load(Yii::$app->request->post())) {
            $model->passport_name = EncryptionHelpers::dec_enc('encrypt', $model->passport_name);
            $model->passport_last_name = EncryptionHelpers::dec_enc('encrypt', $model->passport_last_name);
            $model->passport_middle_name = EncryptionHelpers::dec_enc('encrypt', $model->passport_middle_name);
            $model->passport_series = EncryptionHelpers::dec_enc('encrypt', $model->passport_series);
            $model->passport_number = EncryptionHelpers::dec_enc('encrypt', $model->passport_number);
            $model->passport_birth_date = EncryptionHelpers::dec_enc('encrypt', $model->passport_birth_date);
            $model->passport_whom = EncryptionHelpers::dec_enc('encrypt', $model->passport_whom);
            $model->passport_code = EncryptionHelpers::dec_enc('encrypt', $model->passport_code);
            $model->passport_when = EncryptionHelpers::dec_enc('encrypt', $model->passport_when);
            $model->passport_residence_address = EncryptionHelpers::dec_enc('encrypt', $model->passport_residence_address);

            $model->passport_photo_one = $tmp_photo_one;
            $model->passport_photo_two = $tmp_photo_two;
            if($model->save()){
                ApplicationChangePassport::updateAll(['is_changed' => ApplicationChangePassport::CHANGED],
                    ['worker_id' => $id]);
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ApplicationChangePassport model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ApplicationChangePassport model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApplicationChangePassport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ApplicationChangePassport::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
