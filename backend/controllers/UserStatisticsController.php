<?php

namespace backend\controllers;

use api\modules\v1\models\auth\User;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * Class UserStatistics
 * @package backend\controllers
 */
class UserStatisticsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }
    /**
     * Displays users reg for one day, three day, week
     *
     * @return string
     */
    public function actionIndex(){
        $now_date = strtotime('now');
        $yesterday_date = strtotime('yesterday');
        $three_date = strtotime("-3 day", $now_date);
        $week_date = strtotime("-7 day", $now_date);
        $users_today = User::find()->select('created_at')
            ->where(['>', 'created_at', $yesterday_date])
            ->andWhere(['<', 'created_at', $now_date])
            ->count();
        $users_three_date = User::find()->select('created_at')
            ->where(['>', 'created_at', $three_date])
            ->andWhere(['<', 'created_at', $now_date])
            ->count();
        $users_week_date = User::find()->select('created_at')
            ->where(['>', 'created_at', $week_date])
            ->andWhere(['<', 'created_at', $now_date])
            ->count();
        return $this->render('index', [
            'users_today' => $users_today,
            'users_three_date' => $users_three_date,
            'users_week_date' => $users_week_date
        ]);
    }
}