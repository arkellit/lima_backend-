<?php

namespace backend\controllers;

use api\modules\v1\models\news\ImageNews;
use api\modules\v1\models\NotificationForm;
use api\modules\v1\models\Sending;
use api\modules\v1\models\Upload;
use Yii;
use api\modules\v1\models\news\News;
use api\modules\v1\models\news\NewsSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();
        $model->scenario = News::SCENARIO_CREATE;
        $all_images = new ImageNews();
        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            if ($image !== null) {
                $upload_form = new Upload();
                $upload_form->scenario = Upload::SCENARIO_IMAGE_FILE;
                $upload_form->image = $image;
                $model->image = $upload_form->upload('news');
            }

            if ($model->save()) {
                if($all_images->load(Yii::$app->request->post())){
                    $image_all_product = UploadedFile::getInstances($all_images, 'image');
                    $id = $model->id;
                    foreach ($image_all_product as $file){
                        $all_image = new ImageNews();
                        $upload_all = new Upload();
                        $upload_all->scenario = Upload::SCENARIO_IMAGE_FILE;
                        $upload_all->image = $file;
                        $all_image->image = $upload_all->upload('all-news');
                        $all_image->news_id = $id;
                        if(!$all_image->save()){
                            var_dump($all_image->getErrors());die();
                        }
                    }
                }
                if($model->is_send_push == News::IS_SEND_PUSH_YES){
                    $result_sending = Sending::sendNotification(null, $model->title, $model->short_description, NotificationForm::TYPE_NEWS,
                        NotificationForm::NO_SOUND, $model->id);
                    if ($result_sending == true){
                        Yii::$app->session->setFlash('success', 'Уведомление успешно отправлено');
                        return $this->redirect(['view', 'id' => $model->id]);                    }

                    Yii::$app->session->setFlash('error', 'Произошла ошибка при рассылке');
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }

            var_dump($model->getErrors());die();
        }

        return $this->render('create', [
            'model' => $model,
            'all_images' => $all_images

        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = News::SCENARIO_UPDATE;
        $all_images = new ImageNews();
        $view_all_images = ImageNews::findAll(['news_id' => $id]);
        $tmp_image_name = $model->image;
        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            if ($image !== null) {
                $upload_form = new Upload();
                $upload_form->image = $image;
                $image_name = $upload_form->upload('news');

                if ($tmp_image_name != null || '') {
                    unlink(Yii::getAlias('@image/web' . $tmp_image_name));
                }

            }
            $model->image = $image_name ?? $tmp_image_name;

            if ($model->save()) {
                if ($all_images->load(Yii::$app->request->post())) {
                    $image_all_product = UploadedFile::getInstances($all_images, 'image');
                    $id = $model->id;
                    foreach ($image_all_product as $file) {
                        $all_image = new ImageNews();
                        $upload_all = new Upload();
                        $upload_all->scenario = Upload::SCENARIO_IMAGE_FILE;
                        $upload_all->image = $file;
                        $all_image->image = $upload_all->upload('all-news');
                        $all_image->news_id = $id;
                        if (!$all_image->save()) {
                            var_dump($all_image->getErrors());
                            die();
                        }
                    }
                }
                if($model->is_send_push == News::IS_SEND_PUSH_YES){
                    $result_sending = Sending::sendNotification(null, $model->title, $model->short_description, NotificationForm::TYPE_NEWS,
                        NotificationForm::NO_SOUND, $model->id);
                    if ($result_sending == true){
                        Yii::$app->session->setFlash('success', 'Уведомление успешно отправлено');
                        return $this->redirect(['view', 'id' => $model->id]);
                    }

                    Yii::$app->session->setFlash('error', 'Произошла ошибка при рассылке');
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
            'all_images' => $all_images,
            'view_all_images' => $view_all_images
        ]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = News::findOne($id);
        $all_image = ImageNews::findAll(['news_id' => $id]);
        foreach ($all_image as $value){
            if($value->image !== null){
                unlink(Yii::getAlias('@image/web' . $value->image));
            }
            $value->delete();
        }

        $this->findModel($id)->delete();
        if($model->image !== null){
            unlink(Yii::getAlias('@image/web'. $model->image));
        }
        return $this->redirect(['index']);
    }

    public function actionDeleteImage(){
        if(Yii::$app->request->post()){
            $model = ImageNews::findOne(Yii::$app->request->post('id'));
            $model->delete();
            if($model->image !== null){
                unlink(Yii::getAlias('@image/web'. $model->image));
            }
            return true;
        }
        return false;
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
