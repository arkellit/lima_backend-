<?php

namespace backend\controllers;

use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\worker\Worker;
use common\models\Feedback;
use yii\filters\AccessControl;
use \yii\web\Controller;

/**
 * Admin controller
 *
 * Class AdminController
 */
class AdminController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex(){
        $customer_count = Customer::find()
            ->where(['is_register_complete' => Customer::IS_REGISTER_COMPLETE_FALSE])
            ->count();
        $worker_count = Worker::find()
            ->where(['is_register_complete' => Worker::IS_REGISTER_COMPLETE_FALSE])
            ->count();
        $feedback_count = Feedback::find()
            ->where(['is_view' => Feedback::NO_VIEW])
            ->count();

        return $this->render('/admin/index', [
            'customer_count' => $customer_count,
            'worker_count' => $worker_count,
            'feedback_count' => $feedback_count
        ]);
    }
}