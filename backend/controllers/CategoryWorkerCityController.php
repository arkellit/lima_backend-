<?php

namespace backend\controllers;

use api\modules\v1\models\City;
use api\modules\v1\models\worker\CategoryWorker;
use Yii;
use api\modules\v1\models\worker\CategoryWorkerCity;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryWorkerCityController implements the CRUD actions for CategoryWorkerCity model.
 */
class CategoryWorkerCityController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all CategoryWorkerCity models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => CategoryWorkerCity::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CategoryWorkerCity model.
     * @param integer $category_worker_id
     * @param integer $city_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($category_worker_id, $city_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($category_worker_id, $city_id),
        ]);
    }

    /**
     * Creates a new CategoryWorkerCity model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CategoryWorkerCity();

        $city = City::find()->all();
        $category_worker = CategoryWorker::find()->all();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'category_worker_id' => $model->category_worker_id, 'city_id' => $model->city_id]);
        }

        return $this->render('create', [
            'model' => $model,
            'city' => $city,
            'category_worker' => $category_worker
        ]);
    }

    /**
     * Updates an existing CategoryWorkerCity model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $category_worker_id
     * @param integer $city_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($category_worker_id, $city_id)
    {
        $model = $this->findModel($category_worker_id, $city_id);
        $city = City::find()->all();
        $category_worker = CategoryWorker::find()->all();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'category_worker_id' => $model->category_worker_id, 'city_id' => $model->city_id]);
        }

        return $this->render('update', [
            'model' => $model,
            'city' => $city,
            'category_worker' => $category_worker
        ]);
    }

    /**
     * Deletes an existing CategoryWorkerCity model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $category_worker_id
     * @param integer $city_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($category_worker_id, $city_id)
    {
        $this->findModel($category_worker_id, $city_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CategoryWorkerCity model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $category_worker_id
     * @param integer $city_id
     * @return CategoryWorkerCity the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($category_worker_id, $city_id)
    {
        if (($model = CategoryWorkerCity::findOne(['category_worker_id' => $category_worker_id, 'city_id' => $city_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
