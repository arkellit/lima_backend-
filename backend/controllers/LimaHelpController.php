<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 23.10.18
 * Time: 15:20
 */

namespace backend\controllers;

use api\modules\v1\models\chat\Chat;
use api\modules\v1\models\chat\MessageForm;
use api\modules\v1\models\Sending;
use api\modules\v1\models\Upload;
use common\helpers\GetErrorsHelpers;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\db\Query;
/**
 * Class LimaHelpController
 * @package backend\controllers
 */
class LimaHelpController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }
    /**
     * Display all message
     *
     * @return string
     */
    public function actionIndex(){
        //Получаем последнее сообщение и сортируем в порядке убывания
        $query = Chat::find()
            ->select(['user_from_id', 'MAX(chat.id) id',
//                'split_part(string_agg("message", \';\' order by chat.id desc), \';\', 1) message',
                '(array_agg(status order by chat.id desc))[1] status', 'MAX(chat.created_at) created_at'])
            ->where(['user_to_id' => Yii::$app->params['admin_id']])
            ->joinWith('user_From')
            ->orderBy(['id' => SORT_DESC])
            ->groupBy(['user_from_id']);
        $pages = new Pagination(['totalCount' => $query->count(),
            'pageSize' => 15, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $chat = $query->offset($pages->offset)->limit($pages->limit)->all();
//        var_dump($chat[0]);die();
        $chats = [];
        foreach ($chat as $item){
            $chat_info =  Chat::getLastMessage($item['user_from_id'], Yii::$app->params['admin_id']);
            array_push($chats, $chat_info);
        }
//        $keys = array_column($chats, 'id');
//        array_multisort($keys, SORT_DESC, $chats);
        return $this->render('index', [
            'chats' => $chats,
            'pages' => $pages
        ]);
    }

    /**
     * Display one chat
     *
     * @param $user_id
     * @return string
     */
    public function actionChat($user_id)
    {
        $model = new MessageForm();

        $user_id = (int)$user_id;
        $chat = Chat::getDialog(Yii::$app->params['admin_id'], $user_id);

        return $this->render('chat', [
            'chat' => $chat,
            'model' => $model,
            'user_id' => $user_id
        ]);
    }

    /**
     * Function read message
     */
    public function actionUpdateRead(){
        if(Yii::$app->request->post()){
            $message_id = Yii::$app->request->post('ids_messages');

            Chat::updateAll(['status' => Chat::READ],
                ['id' => json_decode($message_id)]);
        }
    }

    /**
     * Function send new message
     *
     * @return bool|\yii\web\Response
     */
    public function actionSendMessage(){
        $model = new MessageForm();
        if ($model->load(Yii::$app->request->post())){
            $image = UploadedFile::getInstance($model, 'image');
            $url_image = null;
            if ($image !== null) {
                $upload_form = new Upload();
                $upload_form->scenario = Upload::SCENARIO_IMAGE_FILE;
                $upload_form->image = $image;
                $url_image = $upload_form->upload('dialog');
            }
            if (empty($model->text) and empty($url_image)) {
                Yii::$app->session->setFlash('error', 'Прикрепите фотографию или напишите сообщение');
                return $this->redirect(['/lima-help/chat', 'user_id' => $model->user_id]);
            }
            $chat = new Chat();
            $chat->message = $model->text;
            $chat->user_from_id = Yii::$app->params['admin_id'];
            $chat->user_to_id = $model->user_id;
            $chat->attachment = $url_image;
            if($chat->save()){
                Sending::sendNotice($chat->user_to_id, 'Новое сообщение!',
                    $chat->message);
                return $this->redirect(['/lima-help/chat', 'user_id' =>  $model->user_id]);
            }
            Yii::$app->session->setFlash('error', 'Произошла ошибка при отправке сообщения');
            return $this->redirect(['/lima-help/chat', 'user_id' =>  $model->user_id]);
        }
       return false;
    }

    /**
     * Check count new message
     *
     * @return bool
     */
    public function actionCheckNewMessage(){
        if(Yii::$app->request->post()){
            $user_id = Yii::$app->request->post('user_id');
            $count = Yii::$app->request->post('count');
            $chat = Chat::getDialog(Yii::$app->params['admin_id'], $user_id);
            if(count($chat) != $count){
                return true;
            }
        }
        return false;
    }
}