<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 31.10.18
 * Time: 18:14
 */

namespace backend\controllers\worker;

use yii\filters\AccessControl;
use yii\web\Controller;

class WorkerParameterController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex(){
        $this->view->title = 'Параметры исполнителя';
        return $this->render('/worker/index');
    }
}