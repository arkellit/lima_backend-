<?php

namespace backend\controllers\worker;

use api\modules\v1\models\Sending;
use api\modules\v1\models\worker\Worker;
use api\modules\v1\models\worker\WorkerConfirmPay;
use api\modules\v1\models\worker\WorkerNewSearch;
use api\modules\v1\models\worker\WorkerSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CustomerController implements the CRUD actions for Customer model.
 */
class WorkerController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Customer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WorkerNewSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Customer model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
//        $customer_company = CustomerCompanyData::findOne(['customer_id' => $id]);
        return $this->render('view', [
            'model' => $this->findModel($id),
//            'customer_company' => $customer_company
        ]);
    }

    public function actionConfirm($id){
        $worker = $this->findModel($id);
        $worker->is_register_complete = Worker::IS_REGISTER_COMPLETE_TRUE;
        $status = $worker->workerConfirmPay->status;
        if ($status != WorkerConfirmPay::SUCCEEDED){
            Yii::$app->session->setFlash('error',
                'Вы не можете подтвердить пользователя, т.к. он не принял Подтверждение условий сервиса');
            return $this->redirect(['index']);
        }
        if ($worker->save()){
            Sending::sendNotice($worker->user_id, 'Lima', 'Ваш профиль успешно подтвержден');
            Yii::$app->session->setFlash('success','Профиль обновлен');
            return $this->redirect(['index']);
        }
        Yii::$app->session->setFlash('error', 'Произошла ошибка');
        return $this->redirect(['index']);
    }

    /**
     * Creates a new Customer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Customer();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Customer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Customer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Customer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Worker the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Worker::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
