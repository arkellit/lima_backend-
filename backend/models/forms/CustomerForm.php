<?php

namespace backend\models\forms;

use \yii\base\Model;

class CustomerForm extends Model
{
    public $id;
    public $name;
    public $last_name;
    public $middle_name;
    public $phone;
    public $inn_company;
    public $name_company;
    public $kpp_company;
    public $actual_address;
    public $email;
    public $contact_phone;
    public $logo_company;
    public $deposit;

    public $customer_rs;
    public $bank_bik;
    public $bank_city;
    public $bank_name;
    public $bank_ks;
    public $bank_kpp;
    public $bank_inn;

    public function rules()
    {
        return [
            [['id', 'name', 'last_name', 'middle_name', 'phone', 'inn_company',
                'name_company', 'kpp_company', 'actual_address', 'email',
                'contact_phone', 'logo_company', 'deposit', 'customer_rs',
                'bank_bik', 'bank_city', 'bank_name', 'bank_ks',
                'bank_kpp', 'bank_inn'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'last_name' => 'Фамилия',
            'middle_name' => 'Отчество',
            'phone' => 'Телефон регистрации',
            'inn_company' => 'ИНН компании',
            'name_company' => 'Название компании',
            'kpp_company' => 'КПП компании',
            'actual_address' => 'Фактический адрес',
            'email' => 'E-mail',
            'contact_phone' => 'Контактный телефон с кодом города',
            'logo_company' => 'Логотип компании',
            'deposit' => 'Депозит',
            'customer_rs' => 'Р/С компании',
            'bank_bik' => 'БИК банка',
            'bank_city' => 'Город банка',
            'bank_name' => 'Наименование банка',
            'bank_ks' => 'К/С банка',
            'bank_inn' => 'ИНН банка',
        ];
    }
}