<?php


namespace backend\models\forms;


use frontend\models\User;
use yii\base\Model;

class LoginForm extends Model
{
    public $phone;
    public $password;

    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'password'], 'required'],
            ['phone', 'string', 'length' => [11, 12]],
            [['password'], 'string', 'max' => 64, 'tooLong' => 'Неверное значение пароля'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->phone = User::normalizePhone($this->phone);
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'phone' => 'Номер телефона',
            'password' => 'Пароль'
        ];
    }

    /**
     * Function return login token
     *
     * @return array|null
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function login()
    {
        if (!$this->validate()) {
            return null;
        }
        $user = $this->getUser();
        return \Yii::$app->user->login($user, 60*60*2) ?? null;
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неверный пароль');
            }
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByPhone($this->phone);
        }

        return $this->_user;
    }
}

