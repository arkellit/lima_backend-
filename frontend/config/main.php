<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'language' => 'ru',
    //'catchAll' => ['site/page'],
    'components' => [
//        'assetManager' => [
//            'appendTimestamp' => true,
//            'assetMap' => [
//                'vendor.min.js' => '@web/js/vendor.min.js',
//                'app.js' => '@web/js/app.js',
//            ],
//            'bundles' => [
//                'yii\web\JqueryAsset' => [
//                    'sourcePath' => null,
//                    'basePath' => '@webroot',
//                    'baseUrl' => '@web',
//                    'js' => [
//                        'js/vendor.min.js',
//                    ]
//                ],
//            ],
//        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,
                    'basePath' => '@webroot',
                    'baseUrl' => '@web',
                    'js' => [
                        'js/jquery-1.12.js',
                    ]
                ],
            ],
        ],
        'request' => [
            'baseUrl' => '',
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'frontend\models\User',
                'enableAutoLogin' => false,
                'authTimeout' => 3600,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
            'class' => 'yii\web\Session',
            'cookieParams' => ['httponly' => true, 'lifetime' => 3600 * 4],
            'timeout' => 3600*4, //session expire
            'useCookies' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => '/site/index',
                'login' => '/site/login',
                'signup' => '/site/signup',
                'logout' => '/site/logout',
                'monic' => '/page/monic',
                'promoter' => '/page/promoter',
                'customer' => '/page/customer',
                'single-news-1' => '/news/single-news-1',
                'single-news-2' => '/news/single-news-2',
                'single-news-3' => '/news/single-news-3',
                'single-news-4' => '/news/single-news-4',
                'conditions' => '/page/conditions',
                'more' => '/page/more',
                'contacts' => '/page/contacts',
                'feedback' => '/page/feedback',
                'faq-customer' => '/page/faq-customer',
                'faq-worker' => '/page/faq-worker',
                'return_url' => '/site/return-url',
                'offer' => '/page/offer',
                'become-worker' => '/page/become-worker',
                'freelancer-lima' => '/page/freelancer-lima',
                'mass-media' => '/page/mass-media',
                'faq-freelancer' => '/page/faq-freelancer'
            ],
        ],

    ],
    'params' => $params,
];
