jQuery.fn.exists = function() {
   return $(this).length;
}

jQuery(document).ready(function($) {
  $('.header-burger').click(function() {
    $(this).toggleClass("close");
    $('.header-mobile').slideToggle();
  });

  $('.open-popup').magnificPopup({
    type: 'inline',
    removalDelay: 300,
    mainClass: 'my-mfp-zoom-in'
  });

  $('.close-popup').click(function(e) {
    e.preventDefault();
    $.magnificPopup.close();
  });

  $('.select').niceSelect();

  // var tabContainers = $('.tab');
  // tabContainers.hide().filter(':first').show();

  // $('.tabs-btn').click(function () {
  //   tabContainers.hide();
  //   tabContainers.filter(this.hash).show();
  //   $('.tabs-btn').removeClass('active');
  //   $(this).addClass('active');
  //   return false;
  // }).filter(':first').click();

  var tabContainers = $('.tab');
  tabContainers.hide().filter(':first').show();

  $('.tabs-btn').click(function () {
    tabContainers.fadeOut(300);
    tabContainers.filter(this.hash).delay(300).fadeIn(300);
    $('.tabs-btn').removeClass('active');
    $(this).addClass('active');
    return false;
  }).filter(':first').click();

  $('#materialCheckbox').change(function() {
    $('.order_hide').slideToggle();
  });

  $( "#slider-range" ).slider({
      range: 'min',
      min: 1,
      max: 20,
      slide: function( event, ui ) {
        $( "#range-search-radius span" ).text(ui.value);
      }
    });

    $( "#slider-range2" ).slider({
      range: true,
      min: 140,
      max: 210,
      values: [ 160, 180 ],
      slide: function( event, ui ) {
        $( "#range-height span" ).text(ui.values[ 0 ] + "-" + ui.values[ 1 ] );
      }
    });

    $( "#slider-range3" ).slider({
      range: true,
      min: 14,
      max: 50,
      values: [ 18, 25 ],
      slide: function( event, ui ) {
        $( "#range-age span" ).text(ui.values[ 0 ] + "-" + ui.values[ 1 ] );
      }
    });

    if ($('.order__check').length) {
      var topPos = $('.order__check').offset().top; //topPos - это значение от верха блока до окна браузера
    $(window).scroll(function() { 
      var top = $(document).scrollTop();
      if (top > topPos) $('.order__check').addClass('order__check-fixed'); 
      else $('.order__check').removeClass('order__check-fixed');
    });
    }

  /*selected elements in table*/
  $('input[name=selectedCheck]').change(function() {
    if ($('input[name=selectedCheck]:checked').length > 0) {
      $('.checked-block').addClass('open');
      $('.checked-block__count span').text($('input[name=selectedCheck]:checked').length);
    } else {
      $('.checked-block').removeClass('open');
    }
  });

  var owl = $('.benefits_block');
  owl.owlCarousel({
    loop: true,
    center: true,
    responsive:{
      0:{
          items:1
      },
      600:{
          items:2
      },
      1000:{
          items:3
      }
    }
  });
  $('.benefits__prev').click(function(e) {
    e.preventDefault();
    owl.trigger('prev.owl.carousel');
  });
  $('.benefits__next').click(function(e) {
    e.preventDefault();
    owl.trigger('next.owl.carousel');
  });

  $('.password-input a').click(function(e) {
    e.preventDefault();
    if ($('.password-input .form-control').attr('type') === 'password') {
      $('.password-input .form-control').prop('type', 'text');
    } else {
      $('.password-input .form-control').prop('type', 'password');
    }
  });

  // press more
  $('.press__single-more').click(function(e) {
    e.preventDefault();
    $(this).parent().find('.press__single-hide').slideToggle();
  });
});

$(document).ready(function() {
  setInterval(function(){
    var customer_id = $('.active_notice_icon').attr('data-customer_id');
    $.get('/customer/check-worker-request', {customer_id:customer_id}, function(response) {
      if(response != 0){
        if ($('.append_style_active_icon style').length == 0){
          $('.append_style_active_icon').append('<style>\n' +
              '                        .active_notice_icon::after{\n' +
              '                            content: \'\';\n' +
              '                            display: inline-block;\n' +
              '                            width: 7px;\n' +
              '                            height: 7px;\n' +
              '                            -moz-border-radius: 7.5px;\n' +
              '                            -webkit-border-radius: 7.5px;\n' +
              '                            border-radius: 7.5px;\n' +
              '                            background-color: red;\n' +
              '                        }\n' +
              '                    </style>');
        }
        $('.active_notice_icon').attr('href', '/order-offer/offer?order_id=' + response);
      }
      else {
        $('.append_style_active_icon').empty();
        $('.active_notice_icon').attr('href', '#');
      }
    });
  }, 10000);
});