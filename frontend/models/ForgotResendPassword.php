<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 25.02.19
 * Time: 15:30
 */

namespace frontend\models;

use api\modules\v1\models\auth\Token;
use common\helpers\DifferenceTimeHelpers;
use common\helpers\TokenCodeHelpers;
use yii\base\Model;

/**
 * @property string $phone
 *
 * Class ForgotResendPassword
 * @package frontend\models
 */
class ForgotResendPassword extends Model
{
    public $phone;

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->phone = User::normalizePhone($this->phone);
            return true;
        }
        return false;
    }

    public function rules()
    {
        return [
            [['phone'], 'required'],
            ['phone', 'validateForm']
        ];
    }

    public function validateForm(){
        $user = User::findOne(['phone' => $this->phone]);
        $token = Token::findOne(['user_id' => $user->id]);
        if(!is_null($user)){
            if (is_null($user->password_reset_token)){
                return $this->addError('phone', 'Сброс пароля не запрашивался');
            }

            if (DifferenceTimeHelpers::differenceTime($token->updated_at) < 120){
                return $this->addError('phone', 'Отправка cмс разрешена раз в 2 мин');
            }

            if($token->save()){
                TokenCodeHelpers::updateSend($user->id, Token::TYPE_FORGOT_PASSWORD,
                    $user->phone);
                return true;
            }
        }
        return false;
    }

    public function attributeLabels()
    {
        return [
            'phone' => 'Номер телефона'
        ];
    }
}