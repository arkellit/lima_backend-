<?php

namespace frontend\models;

//use api\modules\v1\models\auth\form\ConfirmPhoneForm;
use common\helpers\DifferenceTimeHelpers;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\web\HttpException;

/**
 * @property string $phone
 * @property int $code
 *
 * @property object $_user
 *
 * Class ConfirmPhoneForm
 * @package frontend\models
 */
class ConfirmPhoneForm extends Model
{
    public $phone;
    public $code;
    public $_user;

    public function rules()
    {
        return [
            [['phone', 'code'], 'required'],
            [['code'], 'validateUserData'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->phone = User::normalizePhone($this->phone);
            return true;
        }
        return false;
    }

    public function validateUserData()
    {
        $user = User::findOne(['phone' => $this->phone]);
        if (empty($user)){
             return $this->addError('code',  'Пользователь не найден');
        }
        switch ($user){
            case $user->is_confirm == User::CONFIRM_PHONE:
                return $this->addError('code',  'Номер уже подтвержден');
                break;
            case $user->code->code != $this->code:
                return $this->addError('code', 'Неверный код подтверждения');
                break;
            case DifferenceTimeHelpers::differenceTime($user->code->updated_at) > 300:
                return $this->addError('code',  'Срок действия кода истек');
                break;
        }
        $this->_user = $user;
        return true;
    }

    public function attributeLabels()
    {
        return [
            'code' => 'Код подтверждения'
        ];
    }
}