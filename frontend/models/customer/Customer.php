<?php

namespace frontend\models\customer;

use \api\modules\v1\models\customer\Customer as BaseCustomer;

/**
 * @property string $email
 *
 * Class Customer
 * @package frontend\models\customer
 */
class Customer extends BaseCustomer
{
    public $email;
    public function rules()
    {
        $rules = parent::rules();
        array_push($rules, ['email', 'required']);
        array_push($rules, ['email', 'email']);
        return $rules;
    }

    public function attributeLabels()
    {
        $attribute = parent::attributeLabels();
        $attribute['email'] = 'E-mail';
        return $attribute;
    }
}