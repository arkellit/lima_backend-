<?php

namespace frontend\models\customer;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\customer\CustomerPersonal;
use Yii;
/**
 * CustomerPersonalSearch represents the model behind the search form of
 * `frontend\models\customer\CustomerPersonal`.
 */
class CustomerPersonalSearch extends CustomerPersonal
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'customer_id', 'branch_id', 'is_invite', 'is_register'], 'integer'],
            [['name', 'last_name', 'middle_name', 'email', 'photo', 'role'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerPersonal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'customer_id' => $this->customer_id,
            'branch_id' => $this->branch_id,
            'is_invite' => $this->is_invite,
            'is_register' => $this->is_register,
        ]);

        if (!array_key_exists('customer', Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId()))){
            $query->andWhere(['branch_id' => Yii::$app->user->identity->customerPersonal->branch_id]);
            $query->andWhere(['role' => 'supervisor']);
        }

        $query->andWhere(['is_delete' => CustomerPersonal::NO_DELETE]);

        $query->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'last_name', $this->last_name])
            ->andFilterWhere(['ilike', 'middle_name', $this->middle_name])
            ->andFilterWhere(['ilike', 'email', $this->email])
            ->andFilterWhere(['ilike', 'photo', $this->photo])
            ->andFilterWhere(['ilike', 'role', $this->role]);

        return $dataProvider;
    }
}
