<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 05.03.19
 * Time: 18:15
 */

namespace frontend\models\customer;

use yii\base\Model;

/**
 * @property string $inn
 *
 * Class CustomerInnForm
 * @package frontend\models\customer
 */
class CustomerInnForm extends Model
{
    public $inn;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['inn', 'required'],
            [['inn'], 'string', 'length' => [9, 15]],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'inn' => 'ИНН'
        ];
    }
}