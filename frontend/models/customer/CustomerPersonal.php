<?php

namespace frontend\models\customer;

use api\modules\v1\models\auth\RegistrationForm;
use api\modules\v1\models\auth\User;
use api\modules\v1\models\customer\CustomerPersonal as BaseCustomerPersonal;
use api\modules\v1\models\Sending;
use common\helpers\CustomerHelpers;
use Yii;

/**
 * @property string $phone
 * @property string $password
 *
 * Class CustomerPersonal
 * @package frontend\models\customer
 */
class CustomerPersonal extends BaseCustomerPersonal
{
    public $phone;
    public $password;
    public $is_edit;

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->phone = User::normalizePhone($this->phone);
            return true;
        }
        return false;
    }

    public function rules()
    {
        $rules = parent::rules();
        if (!isset($this->is_edit) && !$this->is_edit) {
            array_push($rules, [['phone', 'branch_id', 'role'], 'required']);
        }
        else {
            array_push($rules, [['branch_id', 'role'], 'required']);
        }

        array_push($rules, ['phone', 'unique', 'targetClass' =>
            '\api\modules\v1\models\auth\User', 'message' => 'Номер телефона уже зарегистрирован']);
        array_push($rules, ['password', 'string', 'min' => 8, 'max' => 72]);
        return $rules;
    }

    public function create($is_edit = null){
        if ($this->validate()){
            $phone = $this->phone;
            if ($is_edit == null){
                $personal = new \api\modules\v1\models\customer\CustomerPersonal();
                $password = $this->password == null ?
                    Yii::$app->security->generateRandomString(6) : $this->password;
                $role = $this->role;
                $model = new RegistrationForm(['phone' => $phone,
                    'password' => $password, 'role' => $role]);
                if (($this->password != null && $this->name != null) &&
                    ($this->last_name != null)){
                    $user = $model->createPersonal($is_invite = null);
                }
                else {
                    $user = $model->createPersonal($is_invite = true);
                }

                if($user == null){
                    return null;
                }

                $personal->user_id = $user['id'];
            }
            else{
                $personal = \api\modules\v1\models\customer\CustomerPersonal::findOne($is_edit);
            }

            $personal->role = $this->role;
            $personal->name = $this->name;
            $personal->last_name = $this->last_name;
            $personal->middle_name = $this->middle_name;
            $personal->email = $this->email;
            $personal->branch_id = $this->branch_id;
            $personal->customer_id = CustomerHelpers::getCustomerId(Yii::$app->user->getId());
            if ($personal->save()){
                if ($is_edit == null){
                    $personal->trigger(\api\modules\v1\models\customer\CustomerPersonal::EVENT_CONFIRMED_CUSTOMER_PERSONAL_NOTICE);
                    $userRole = Yii::$app->authManager->getRole($role);
                    Yii::$app->authManager->assign($userRole, $user['id']);
                }
                if ($is_edit == null){
                    Sending::sendLoginToSms($phone, $password);
                }
                return true;
            }
            var_dump($personal->getErrors());die();
            return null;
        }
        return null;
    }
    public function attributeLabels()
    {
        $rules =  parent::attributeLabels();
        $rules['phone'] = 'Телефон';
        return $rules;
    }
}