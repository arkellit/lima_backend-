<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 06.03.19
 * Time: 16:08
 */

namespace frontend\models\customer;

use yii\base\Model;

/**
 * @property string $bik
 *
 * Class CustomerBikForm
 * @package frontend\models\customer
 */
class CustomerBikForm extends Model
{
    public $bik;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['bik', 'required'],
            [['bik'], 'string', 'length' => [9, 9]],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'bik' => 'БИК Банка'
        ];
    }
}