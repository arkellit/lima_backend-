<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 06.03.19
 * Time: 13:28
 */

namespace frontend\models\customer;

use yii\httpclient\Client;

class GetDataCompany
{
    public static function getData($inn){
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setFormat(Client::FORMAT_JSON)
            ->setHeaders(['Authorization' => 'Token e1ba91cd48347a8777a0035a8d3e8ce800a7cfb6'])
            ->setUrl('https://suggestions.dadata.ru/suggestions/api/4_1/rs/findById/party')
            ->setData(['query' => $inn])
            ->send();
        if ($response->isOk) {
           return $response->data;
        }
        return null;
    }

    public static function getBank($bik){
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setFormat(Client::FORMAT_JSON)
            ->setHeaders(['Authorization' => 'Token e1ba91cd48347a8777a0035a8d3e8ce800a7cfb6'])
            ->setUrl('https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/bank')
            ->setData(['query' => $bik])
            ->send();
        if ($response->isOk) {
            return $response->data;
        }
        return null;
    }
}