<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 06.03.19
 * Time: 16:54
 */

namespace frontend\models\customer;

use yii\base\Model;

/**
 * Class CustomerAllForm
 * @package frontend\models\customer
 */
class CustomerAllForm extends Model
{
    public $name;
    public $last_name;
    public $middle_name;
    public $email;
    public $contact_phone;
    public $inn_company;
    public $kpp_company;
    public $name_company;
    public $actual_address;
    public $customer_rs;
    public $bank_bik;
    public $bank_city;
    public $bank_name;
    public $bank_ks;
    public $bank_inn;
    public $bank_kpp;

    public $logo_company;

    public function rules()
    {
        return [
            [['name', 'last_name', 'email', 'contact_phone', 'inn_company',
                'name_company', 'actual_address', 'customer_rs',
                /*'bank_inn', 'bank_kpp'*/], 'required'],
            [['bank_city', 'bank_ks', 'bank_name', 'middle_name'], 'string'],
            [['bank_bik'], 'required',
                'message' => 'Необходимо заполнить «БИК банка»'],
            [['bank_kpp'], 'string', 'length' => [9, 9]],
            [['customer_rs'], 'string', 'length' => [20, 20]],
            [['bank_inn'], 'string', 'length' => [9, 12]],
        ];
    }

    public function attributeLabels()
    {
        return [
            'bank_inn' => 'ИНН банка',
            'bank_kpp' => 'КПП Банка',
            'customer_rs' => 'Р/c организации',
            'contact_phone' => 'Телефон организации',
            'bank_bik' => 'БИК банка',
            'bank_city' => 'Город банка',
            'bank_name' => 'Название банка',
            'bank_ks' => 'Корреспондентский счет банка',
            'actual_address' => 'Фактический адрес',
            'logo_company' => 'Логотип'
        ];
    }
}