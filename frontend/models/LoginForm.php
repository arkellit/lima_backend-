<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $phone;
    public $password;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['phone', 'password'], 'required'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $password_hash = hash('sha256', $this->password);
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($password_hash)) {
                $this->addError($attribute, 'Неверный номер телефона или пароль');
            }

            elseif (!$user->validatePassword($password_hash)){
                $this->addError($attribute, 'Неверный номер телефона или пароль');
            }

            elseif ($user->is_blocked == User::STATUS_BLOCKED){
                $this->addError($attribute, 'Номер телефона заблокирован');
            }
            elseif($user->is_confirm == User::NO_CONFIRM_PHONE){
                $this->addError($attribute, 'Номер телефона не подтвержден');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), 60*60*2);
        }
        return false;
    }

    /**
     * Finds user by [[phone]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $user = \frontend\models\User::findByPhone($this->phone);
            $this->_user = $user;
        }

        return $this->_user;
    }

    public function attributeLabels()
    {
        return [
            'phone' => 'Номер телефона',
            'password' => 'Пароль'
        ];
    }
}
