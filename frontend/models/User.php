<?php

namespace frontend\models;

use yii\behaviors\TimestampBehavior;
use \api\modules\v1\models\auth\User as BaseUser;


class User extends BaseUser
{
    public $username;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function findByPhone($phone)
    {
        return static::findOne(['phone' => User::normalizePhone($phone), 'is_delete' => User::NO_DELETE]);
    }
}
