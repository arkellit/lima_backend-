<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 22.02.19
 * Time: 14:39
 */

namespace frontend\models;

use common\helpers\DifferenceTimeHelpers;
use yii\base\Model;

/**
 * @property string $phone
 * @property object $_user
 *
 * Class ResendCodeForm
 * @package frontend\models
 */
class ResendCodeForm extends Model
{
    public $phone;
    public $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['phone', 'required'],
            [['phone'], 'validateSendNewToken']
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->phone = User::normalizePhone($this->phone);
            return true;
        }
        return false;
    }

    /**
     * Validate function
     *
     * @inheritdoc
     */
    public function validateSendNewToken(){
        $user = User::findOne(['phone' => $this->phone]);
        if (empty($user)){
            return $this->addError('phone', 'Пользователь не найден');
        }
        switch ($user){
            case $user->is_confirm == User::CONFIRM_PHONE:
                return $this->addError('phone', 'Номер уже подтвержден');
                break;
            case DifferenceTimeHelpers::differenceTime($user->code->updated_at) < 120:
                return $this->addError('phone', 'Отправка cмс разрешена раз в 2 мин');
                break;
        }
        $this->_user = $user;
        return true;
    }
}