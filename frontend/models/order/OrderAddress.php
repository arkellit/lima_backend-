<?php


namespace frontend\models\order;

use api\modules\v1\models\order\OrderAddress as BaseOrderAddress;

/**
 * Class OrderAddress
 * @package frontend\models\order
 *
 * @property string $address;
 */
class OrderAddress extends BaseOrderAddress
{
    public $address;

    public function rules()
    {
        $rules = parent::rules();
//        array_push($rules, ['address', 'required']);
        return $rules;
    }
}