<?php

namespace frontend\models\order;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\order\Order;

/**
 * OrderSearch represents the model behind the search form of `frontend\models\order\Order`.
 */
class OrderSearch extends Order
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type_order', 'is_pay', 'administrator_id', 'created_at'], 'integer'],
            [['name_order', 'number_order'], 'safe'],
            [['total_price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'type_order' => $this->type_order,
            'total_price' => $this->total_price,
            'is_pay' => $this->is_pay,
            'is_complete_order' => $this->is_complete_order,
            'administrator_id' => $this->administrator_id,
            'created_at' => $this->created_at,
            'is_published' => $this->is_published
        ]);

        $query->andFilterWhere(['ilike', 'name_order', $this->name_order])
            ->andFilterWhere(['ilike', 'number_order', $this->number_order]);

        return $dataProvider;
    }
}
