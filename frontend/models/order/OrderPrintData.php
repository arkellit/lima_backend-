<?php


namespace frontend\models\order;

use api\modules\v1\models\printing\OrderPrintData as BaseOrder;
use api\modules\v1\models\printing\PrintingTemplate;

/**
 * Class OrderPrintData
 * @package frontend\models\order
 */
class OrderPrintData extends BaseOrder
{
    public function rules()
    {
        $rules = parent::rules();
        unset($rules);
        $rules = [
            [[//'order_id', 'order_place_id',
                'edition_id',
                'print_type_id', 'print_size_id', 'paper_density_id',
                'print_time_id', 'flyer_title', 'flyer_description',
                'printing_price'], 'required', 'whenClient' => "function (attribute, value) {
                    return $('#materialCheckbox').prop('checked') == true;
                }"],
//            [
//                ['courier_address', 'courier_price_id'],
//                'required',
////                'when' => function($model) {
////                    return  $model->is_courier != 0;
////                },
//                'whenClient' => "function (attribute, value) {
//                    return $('#orderprintdata-courier_address').val() != '';
//                }"
//            ],
            [['is_courier'], 'default', 'value' => 0],
            [[ 'edition_id',
                'print_type_id', 'print_size_id', 'paper_density_id', 'print_time_id',
                'courier_price_id', 'is_courier'], 'integer'],
            [['urls'], 'safe'],
            ['printing_price', 'number'],
            ['printing_price', 'default', 'value' => 0],
            [['flyer_description'], 'string'],
            [['flyer_title', 'courier_address', 'comment_order_print'], 'string', 'max' => 1000],
//            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
//            [['order_place_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderPlace::className(), 'targetAttribute' => ['order_place_id' => 'id']],
            [['edition_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrintingTemplate::className(), 'targetAttribute' => ['edition_id' => 'id']],
            [['print_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrintingTemplate::className(), 'targetAttribute' => ['print_type_id' => 'id']],
            [['print_size_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrintingTemplate::className(), 'targetAttribute' => ['print_size_id' => 'id']],
            [['paper_density_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrintingTemplate::className(), 'targetAttribute' => ['paper_density_id' => 'id']],
            [['print_time_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrintingTemplate::className(), 'targetAttribute' => ['print_time_id' => 'id']],
        ];
        return $rules;
    }
}