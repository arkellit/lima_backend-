<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 25.02.19
 * Time: 14:52
 */

namespace frontend\models;

use api\modules\v1\models\auth\Token;
use common\helpers\DifferenceTimeHelpers;
use yii\base\Model;

/**
 * @property string $phone
 * @property int $code
 *
 * @property object $_user
 *
 * Class ConfirmPasswordReset
 * @package frontend\models
 */
class ConfirmPasswordReset extends Model
{
    public $phone;
    public $code;

    public $_user;

    public function rules()
    {
        return [
            [['phone', 'code'], 'required'],
            ['code', 'validateForm']
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->phone = User::normalizePhone($this->phone);
            return true;
        }
        return false;
    }

    public function validateForm(){
        $user = User::findOne(['phone' => $this->phone]);

        if(is_null($user)){
            return $this->addError('code', 'Номер не найден');
        }

        if (is_null($user->password_reset_token)){
            return $this->addError('code', 'Сброс пароля не запрашивался');
        }

        $code = Token::findOne(['user_id' => $user->id]);

        if($code->code != $this->code){
            return $this->addError('code', 'Неверный код');
        }

        if(DifferenceTimeHelpers::differenceTime($code->updated_at) > 300){
            return $this->addError('code', 'Срок действия кода истек');
        }

        $user->is_confirm_forgot_code = User::CONFIRM_CODE;
        $this->_user = $user;
        return true;
    }

    public function attributeLabels()
    {
        return [
            'phone' => 'Номер телефона',
            'code' => 'Код подтверждения'
        ];
    }
}