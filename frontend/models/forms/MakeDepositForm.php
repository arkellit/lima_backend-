<?php


namespace frontend\models\forms;

use yii\base\Model;

/**
 * Class MakeDepositForm
 * @package frontend\models\forms
 *
 * @property double $sum
 */
class MakeDepositForm extends Model
{
    public $sum;

    public function rules()
    {
        return [
            ['sum', 'required'],
            [['sum'], 'number', 'min' => 0, 'max' => 1000000],
        ];
    }

    public function attributeLabels()
    {
        return [
            'sum' => 'Сумма'
        ];
    }
}