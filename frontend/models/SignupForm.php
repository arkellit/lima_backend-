<?php

namespace frontend\models;

use api\modules\v1\models\auth\RegistrationForm as BaseSignup;
use api\modules\v1\models\auth\Token;
use common\helpers\TokenCodeHelpers;

/**
 * Signup form
 */
class SignupForm extends BaseSignup
{
    public $phone;
    public $password;
    public $repeat_password;
    public $is_confirm;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = parent::rules();

        array_push($rules, ['repeat_password', 'required']);
        array_push($rules, ['repeat_password', 'compare', 'compareAttribute' => 'password',
            'message' => 'Пароли не совпадают']);
        array_push($rules, ['is_confirm', 'compare', 'compareValue' => 1,
            'message' => 'Вы не согласились с условиями пользования']);
        array_push($rules, ['password', 'string', 'min' => 8, 'max' => 72]);
//        array_push($rules, ['password', 'match', 'pattern' => '/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*/',
//            'message' => 'Пароль должен содержать минимум: 8 символов, одну цифру, одну букву в верхнем регистре, одну в нижнем и буквы латинского алфавита']);
        return $rules;
    }

    public function validatePhone($attribute)
    {
        $phone_check = User::findOne(['phone' => $this->$attribute]);

        if($phone_check != null){
            if ($phone_check->is_confirm == User::NO_CONFIRM_PHONE) {
                $this->addError($attribute,  'Номер телефона не подтвержден');
            }
            elseif ($phone_check->is_confirm == User::CONFIRM_PHONE){
                $this->addError($attribute,  'Номер телефона уже зарегистирован и подтвержден');
            }
        }
    }


    /**
     * Signs user up.
     *
     * @return User|null
     * @throws \yii\base\Exception
     * @throws \yii\web\HttpException
     */
    public function signup()
    {
        if (!$this->validate()) {
           return null;
        }

        $user = new User();
        $user->phone = $this->phone;
        $password_hash = hash('sha256', $this->password);
        $user->setPassword($password_hash);
        $user->generateAuthKey();
        $user->access_token = User::setAccessToken($user->phone);
//        $user->fcm_token = $this->fcm_token;
        $user->is_social =  User::NO_SOCIAL_ACCOUNT;
        $user->is_confirm = User::NO_CONFIRM_PHONE;

        if($user->save()){
            TokenCodeHelpers::createSend($user->id, Token::TYPE_CONFIRMATION,
                    $user->phone);
        }
        
        return $user->save() ? $user : null;
    }

    public function attributeLabels()
    {
        $attribute = parent::attributeLabels();
        $attribute['repeat_password'] = 'Повторите пароль';
        return $attribute;
    }
}
