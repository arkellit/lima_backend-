<?php

/* @var $this \yii\web\View */

/* @var $content string */
use frontend\assets\IndexAsset;
use yii\helpers\Html;
use \yii\helpers\Url;

IndexAsset::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode(strip_tags($this->title)) ?></title>

        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap&subset=cyrillic-ext" rel="stylesheet">
        <?php $this->registerJsFile('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js') ?>
        <?php $this->registerJsFile('https://oss.maxcdn.com/respond/1.4.2/respond.min.js') ?>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <header class="header">
            <div class="header__left">
                <button type="button" class="header-burger">
                    <span></span>
                </button>
            </div>
            <div class="header__logo">
                <a href="/"><img src="/img/logo-white.png" alt="logo"></a>
            </div>
            <ul class="header__list">
                <li>
                    <a href="<?= Url::to(['/promoter']) ?>">Исполнителям</a>
                </li>
                <li><a href="<?= Url::to(['/customer']) ?>">Заказчикам</a></li>
            </ul>
            <ul class="header__list mr-2">
                <?php if (Yii::$app->user->isGuest) :?>
                    <li><a href="<?= Url::to(['/login']) ?>">Вход</a></li>
                    <li><a href="<?= Url::to(['/signup']) ?>">Регистрация</a></li>
                <?php else:?>
                    <li><a href="<?= Url::to(['/customer/index']) ?>">Профиль</a></li>
                    <li><a href="<?= Url::to(['/logout']) ?>" data-method="post">Выход(<?= Yii::$app->user->identity->phone ?>)</a></li>
                <?php endif;?>
                <li><a href="<?= Url::to(['/contacts']) ?>">Контакты</a></li>
            </ul>

        <div class="header-mobile">
            <ul>
                <li><a href="<?= Url::to(['/promoter']) ?>">Исполнителям</a></li>
                <li><a href="<?= Url::to(['/customer']) ?>">Заказчикам</a></li>
                <li><a href="<?= Url::to(['/contacts']) ?>">Контакты</a></li>
                <?php if (Yii::$app->user->isGuest) :?>
                    <li><a href="<?= Url::to(['/login']) ?>">Вход</a></li>
                    <li><a href="<?= Url::to(['/signup']) ?>">Регистрация</a></li>
                <?php else:?>
                    <li><a href="<?= Url::to(['/customer/index']) ?>">Профиль</a></li>
                    <li><a href="<?= Url::to(['/logout']) ?>" data-method="post">Выход(<?= Yii::$app->user->identity->phone ?>)</a></li>
                <?php endif;?>
            </ul>
        </div>
<!--        </div>-->
<!--        </div>-->
    </header>
    <?= \common\widgets\Alert::widget() ?>

    <?= $content ?>
    <!-- ***** Footer Start ***** -->
    <section class="footer__partners">
        <div class="container">
            <div class="footer__partners-inner">
                <div class="footer__partners-single">
                    <img src="/img/rbk-logo.png" alt="">
                </div>
                <div class="footer__partners-single">
                    <img src="/img/it-world.png" alt="">
                </div>
                <div class="footer__partners-single">
                    <img src="/img/retailru.png" alt="">
                </div>
                <div class="footer__partners-single">
                    <img src="/img/expert-logo.png" alt="">
                </div>
                <div class="footer__partners-single">
                    <img src="/img/rus-planet-logo.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="footer_logo">
                        <a href="/"><img src="/img/logo-white.png" alt=""></a>
                    </div>
                    <p class="footer_copyright">© 2018 LiMA <br>All rights reserved</p>
                </div>
                <div class="col-lg-9">
                    <div class="footer_right">
                        <div class="footer_nav">
                            <h2><a href="#">Исполнителю</a></h2>
                            <ul class="footer_list">
                                <li><a href="<?= Url::to(['/promoter']) ?>">Промоутер</a></li>
                                <!--                        <li><a href="#">Модель</a></li>-->
                                <li><a href="<?= Url::to(['/monic']) ?>">Моник</a></li>
                                <li><a href="<?= Url::to(['/become-worker']) ?>">Стать исполнителем</a></li>

                            </ul>
                        </div>
                        <div class="footer_nav">
                            <h2><a href="<?= Url::to(['/customer']) ?>">Заказчику</a></h2>
                            <ul class="footer_list">
                                <li><a href="<?= Url::to(['/signup']) ?>">Регистрация</a></li>
                            </ul>
                        </div>
                        <div class="footer_nav">
                            <h2><a href="#">FAQ</a></h2>
                            <ul class="footer_list">
                                <li><a href="<?= Url::to(['/faq-customer']) ?>">Заказчику</a></li>
                                <li><a href="<?= Url::to(['/faq-worker']) ?>">Исполнителю</a></li>
                                <li><a href="<?= Url::to(['/faq-freelancer']) ?>">Про самозанятых</a></li>
                                <li><a href="<?= Url::to(['/freelancer-lima']) ?>">Самозанятые в LIMA</a></li>
                            </ul>
                        </div>
                        <div class="footer_nav">
                            <h2><a href="#">О НАС</a></h2>
                            <ul class="footer_list">
<!--                                <li><a href="#">О компании</a></li>-->
<!--                                <li><a href="#">Мы в соцсетях</a></li>-->
                                <li><a href="<?= Url::to(['/mass-media']) ?>">СМИ</a></li>
<!--                                <li><a href="--><?//= Url::to(['/news']) ?><!--">Блог</a></li>-->
<!--                                <li><a href="#">Мероприятия</a></li>-->
                            </ul>
                        </div>
                        <div class="footer_nav">
                            <h2><a href="#">КОНТАКТЫ</a></h2>
                            <ul class="footer_list">
                                <li><a href="<?= Url::to(['/contacts']) ?>">Обратная связь</a></li>
                                <li><a href="<?= Url::to(['/contacts']) ?>">Контакты</a></li>
                            </ul>
                            <ul class="footer__social">
                                <li><a href="https://vk.com/public175797702" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 23 13"><path d="M10.619.007c.562 0 1.126.019 1.688-.005.86-.037 1.114.425 1.123 1.122.016 1.331-.003 2.663.012 3.994.003.246.065.505.162.732.185.437.569.515.88.157.473-.546.968-1.092 1.329-1.711.568-.975 1.032-2.01 1.54-3.02.076-.153.134-.316.22-.464a.71.71 0 0 1 .651-.37c1.344 0 2.687-.005 4.03.002.59.003.853.334.706.9-.208.803-.653 1.488-1.134 2.149-.71.973-1.434 1.936-2.148 2.905-.6.812-.56 1.146.15 1.846a77.509 77.509 0 0 1 2.27 2.305c.286.307.53.671.72 1.046.12.233.15.538.132.805-.023.358-.333.592-.695.592-1.252.003-2.505.017-3.757-.006-.638-.011-1.164-.333-1.599-.777-.526-.538-1.019-1.107-1.54-1.65a2.754 2.754 0 0 0-.591-.47c-.34-.197-.652-.104-.871.224-.297.446-.359.958-.406 1.472-.009.1.002.2-.005.3-.043.602-.358.897-.96.909-1.09.02-2.175.012-3.234-.3-1.546-.456-2.84-1.311-3.913-2.498-1.613-1.78-2.815-3.834-3.908-5.955C1.003 3.333.583 2.4.142 1.479a1.807 1.807 0 0 1-.085-.2C-.113.776.104.45.637.446a167.65 167.65 0 0 1 3.43.001c.438.006.7.285.877.666.354.763.68 1.544 1.092 2.275.44.778.956 1.513 1.458 2.255.124.183.308.332.485.471.336.265.675.195.793-.21.133-.455.235-.935.243-1.405a24.432 24.432 0 0 0-.101-2.435c-.048-.574-.369-.963-.962-1.102-.398-.093-.418-.216-.14-.525a1.242 1.242 0 0 1 1.01-.434c.599.015 1.198.004 1.797.004"/></svg></a></li>
                                <li><a href="https://www.instagram.com/lima_official_/" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 0c2.716 0 3.056.012 4.123.06 1.064.049 1.791.218 2.427.465a4.902 4.902 0 0 1 1.772 1.153 4.902 4.902 0 0 1 1.153 1.772c.247.636.416 1.363.465 2.427.048 1.067.06 1.407.06 4.123s-.012 3.056-.06 4.123c-.049 1.064-.218 1.791-.465 2.427a4.902 4.902 0 0 1-1.153 1.772 4.902 4.902 0 0 1-1.772 1.153c-.636.247-1.363.416-2.427.465-1.067.048-1.407.06-4.123.06s-3.056-.012-4.123-.06c-1.064-.049-1.791-.218-2.427-.465a4.902 4.902 0 0 1-1.772-1.153A4.902 4.902 0 0 1 .525 16.55c-.247-.636-.416-1.363-.465-2.427C.012 13.056 0 12.716 0 10s.012-3.056.06-4.123C.11 4.813.278 4.086.525 3.45a4.902 4.902 0 0 1 1.153-1.772A4.902 4.902 0 0 1 3.45.525C4.086.278 4.813.109 5.877.06 6.944.012 7.284 0 10 0zm0 1.802c-2.67 0-2.986.01-4.04.058-.976.045-1.505.207-1.858.344-.466.182-.8.399-1.15.748-.35.35-.566.684-.748 1.15-.137.353-.3.882-.344 1.857-.048 1.055-.058 1.37-.058 4.041 0 2.67.01 2.986.058 4.04.045.976.207 1.505.344 1.858.182.466.399.8.748 1.15.35.35.684.566 1.15.748.353.137.882.3 1.857.344 1.054.048 1.37.058 4.041.058 2.67 0 2.987-.01 4.04-.058.976-.045 1.505-.207 1.858-.344.466-.182.8-.399 1.15-.748.35-.35.566-.684.748-1.15.137-.353.3-.882.344-1.857.048-1.055.058-1.37.058-4.041 0-2.67-.01-2.986-.058-4.04-.045-.976-.207-1.505-.344-1.858a3.098 3.098 0 0 0-.748-1.15 3.098 3.098 0 0 0-1.15-.748c-.353-.137-.882-.3-1.857-.344-1.055-.048-1.37-.058-4.041-.058zm0 3.063a5.135 5.135 0 1 1 0 10.27 5.135 5.135 0 0 1 0-10.27zm0 8.468a3.333 3.333 0 1 0 0-6.666 3.333 3.333 0 0 0 0 6.666zm6.538-8.671a1.2 1.2 0 1 1-2.4 0 1.2 1.2 0 0 1 2.4 0z"/></svg></a></li>
                                <li><a href="https://www.facebook.com/Llimaofficial/" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 23"><path d="M7.79 23V12.509h3.537l.53-4.09H7.789V5.81c0-1.184.33-1.99 2.036-1.99L12 3.818V.16C11.624.11 10.332 0 8.83 0 5.694 0 3.547 1.905 3.547 5.405V8.42H0v4.089h3.547V23h4.242z"/></svg></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <?php $this->endBody() ?>
    </body>
</html>
 <?php $this->endPage() ?>