<?php

/* @var $this \yii\web\View */

/* @var $content string */

use common\helpers\CustomerHelpers;
use frontend\assets\IndexAsset;
use yii\helpers\Html;
use \yii\helpers\Url;

IndexAsset::register($this);

?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode(strip_tags($this->title)) ?></title>

<!--        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap&subset=cyrillic-ext" rel="stylesheet">-->
<!--        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>-->
<!--        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <!--    <header class="header">-->
    <div class="header">
        <div class="header__left">
            <button type="button" class="header-burger">
                <span></span>
            </button>
        </div>
        <div class="header__logo">
            <a href="/"><img src="/img/logo-white.png" alt="logo"></a>
        </div>
        <ul class="header__list">
            <li>
                <a href="<?= Url::to(['/promoter']) ?>">Исполнителям</a>
            </li>
            <li><a href="<?= Url::to(['/customer']) ?>">Заказчикам</a></li>
        </ul>
        <ul class="header__list mr-2">
            <?php if (Yii::$app->user->isGuest) :?>
                <li><a href="<?= Url::to(['/login']) ?>">Вход</a></li>
                <li><a href="<?= Url::to(['/signup']) ?>">Регистрация</a></li>
            <?php else:?>
                <li><a href="<?= Url::to(['/customer/index']) ?>">Профиль</a></li>
                <li><a href="<?= Url::to(['/logout']) ?>" data-method="post">Выход(<?= Yii::$app->user->identity->phone ?>)</a></li>
            <?php endif;?>
            <li><a href="<?= Url::to(['/contacts']) ?>">Контакты</a></li>
            <?php $order_id_request_worker = CustomerHelpers::checkNewWorkerRequest();?>
            <li><a href="<?= $order_id_request_worker['order_id'] == null ? '#' :
                Url::to(['order-offer/offer', 'order_id' => $order_id_request_worker['order_id']])?>"
                   class="active_notice_icon"
                   data-customer_id="<?= $order_id_request_worker['customer_id'] ?>" title="У вас новый отклик на заказ">
                    <?= Html::img('/img/active_notice.png') ?></a></li>
            <div class="append_style_active_icon"></div>

            <?php
                if (!empty($order_id_request_worker['order_id'])):?>
                    <style>
                        .active_notice_icon::after{
                            content: '';
                            display: inline-block;
                            width: 7px;
                            height: 7px;
                            -moz-border-radius: 7.5px;
                            -webkit-border-radius: 7.5px;
                            border-radius: 7.5px;
                            background-color: red;
                        }
                    </style>
                <?php endif;?>
        </ul>

    </div>
    <div class="header-mobile">
        <ul>
            <li><a href="<?= Url::to(['/promoter']) ?>">Исполнителям</a></li>
            <li><a href="<?= Url::to(['/customer']) ?>">Заказчикам</a></li>
            <li><a href="<?= Url::to(['/contacts']) ?>">Контакты</a></li>
            <?php if (Yii::$app->user->isGuest) :?>
                <li><a href="<?= Url::to(['/login']) ?>">Вход</a></li>
                <li><a href="<?= Url::to(['/signup']) ?>">Регистрация</a></li>
            <?php else:?>
                <li><a href="<?= Url::to(['/customer/index']) ?>">Профиль</a></li>
                <li><a href="<?= Url::to(['/logout']) ?>" data-method="post">Выход(<?= Yii::$app->user->identity->phone ?>)</a></li>
            <?php endif;?>
        </ul>
    </div>
    <!--    </header>-->
    <div class="header-mobile">
        <ul>
            <li><a href="<?= Url::to(['/promoter']) ?>">Исполнителям</a></li>
            <li><a href="<?= Url::to(['/customer']) ?>">Заказчикам</a></li>
            <li><a href="<?= Url::to(['/contacts']) ?>">Контакты</a></li>
            <?php if (Yii::$app->user->isGuest) :?>
                <li><a href="<?= Url::to(['/login']) ?>">Вход</a></li>
                <li><a href="<?= Url::to(['/signup']) ?>">Регистрация</a></li>
            <?php else:?>
                <li><a href="<?= Url::to(['/customer/index']) ?>">Профиль</a></li>
                <li><a href="<?= Url::to(['/logout']) ?>" data-method="post">Выход(<?= Yii::$app->user->identity->phone ?>)</a></li>
            <?php endif;?>
        </ul>
    </div>


    <?= \common\widgets\Alert::widget() ?>
    <div class="box_link_header">
        <div class="container link_header_container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="link_header text-center">
                        <li class="link_header_li"><a class="<?= Yii::$app->controller->id == 'order' ? 'active' : '' ?>" href="<?= Url::to(['/order']) ?>">Заказы</a></li>
                        <li class="link_header_li"><a class="<?= Yii::$app->controller->id == 'customer-personal' ? 'active' : '' ?>" href="<?= Url::to(['/customer-personal/staff']) ?>">Персонал</a></li>
                        <li class="link_header_li"><a class="<?= Yii::$app->controller->id == 'customer' ? 'active' : '' ?>" href="<?= Url::to(['/customer/index']) ?>">Профиль</a></li>
                        <li class="link_header_li"><a href="<?= Url::to(['/site/logout']) ?>" data-method="post">Выход</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <?= $content ?>
    <!-- ***** Footer Start ***** -->
    <section class="footer__partners">
        <div class="container">
            <div class="footer__partners-inner">
                <div class="footer__partners-single">
                    <img src="/img/rbk-logo.png" alt="">
                </div>
                <div class="footer__partners-single">
                    <img src="/img/it-world.png" alt="">
                </div>
                <div class="footer__partners-single">
                    <img src="/img/retailru.png" alt="">
                </div>
                <div class="footer__partners-single">
                    <img src="/img/expert-logo.png" alt="">
                </div>
                <div class="footer__partners-single">
                    <img src="/img/rus-planet-logo.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="footer_logo">
                        <a href="/"><img src="/img/logo-white.png" alt=""></a>
                    </div>
                    <p class="footer_copyright">© 2018 LiMA <br>All rights reserved</p>
                </div>
                <div class="col-lg-9">
                    <div class="footer_right">
                        <div class="footer_nav">
                            <h2><a href="#">Исполнителю</a></h2>
                            <ul class="footer_list">
                                <li><a href="<?= Url::to(['/promoter']) ?>">Промоутер</a></li>
                                <!--                        <li><a href="#">Модель</a></li>-->
                                <li><a href="<?= Url::to(['/monic']) ?>">Моник</a></li>
                                <li><a href="<?= Url::to(['/become-worker']) ?>">Стать исполнителем</a></li>

                            </ul>
                        </div>
                        <div class="footer_nav">
                            <h2><a href="<?= Url::to(['/customer']) ?>">Заказчику</a></h2>
                            <ul class="footer_list">
                                <li><a href="<?= Url::to(['/signup']) ?>">Регистрация</a></li>
                            </ul>
                        </div>
                        <div class="footer_nav">
                            <h2><a href="#">FAQ</a></h2>
                            <ul class="footer_list">
                                <li><a href="<?= Url::to(['/faq-customer']) ?>">Заказчику</a></li>
                                <li><a href="<?= Url::to(['/faq-worker']) ?>">Исполнителю</a></li>
                                <li><a href="<?= Url::to(['/faq-freelancer']) ?>">Про самозанятых</a></li>
                                <li><a href="<?= Url::to(['/freelancer-lima']) ?>">Самозанятые в LIMA</a></li>
                            </ul>
                        </div>
                        <div class="footer_nav">
                            <h2><a href="#">О НАС</a></h2>
                            <ul class="footer_list">
                                <!--                                <li><a href="#">О компании</a></li>-->
                                <!--                                <li><a href="#">Мы в соцсетях</a></li>-->
                                <li><a href="<?= Url::to(['/mass-media']) ?>">СМИ</a></li>
                                <li><a href="<?= Url::to(['/news']) ?>">Блог</a></li>
                                <!--                                <li><a href="#">Мероприятия</a></li>-->
                            </ul>
                        </div>
                        <div class="footer_nav">
                            <h2><a href="#">КОНТАКТЫ</a></h2>
                            <ul class="footer_list">
                                <li><a href="<?= Url::to(['/contacts']) ?>">Обратная связь</a></li>
                                <li><a href="<?= Url::to(['/contacts']) ?>">Контакты</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>