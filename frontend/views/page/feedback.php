<?php
use yii\helpers\Url;
$this->title = 'Обратная связь';
?>
<section class="feedback">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="<?= Url::to('/more') ?>">Еще</a></li>
                        <li><a href="#">Обратная связь</a></li>
                    </ul>
                </div>
                <h2 class="contacts_title">Обратная связь</h2>
                <p class="feedback_info">По всем интересующим вопросом писать на <a href="mailto:support@lima.com">support@lima.com</a> <br>для удобства можете воспользоваться следующей формой: </p>
                <form action="/" class="feedback_form">
                    <div class="form-group">
                        <label for="f1">Ваше имя</label>
                        <input id="f1" type="text" class="form-control" placeholder="Как к вам обращаться?">
                    </div>
                    <div class="form-group">
                        <label for="f2">Ваш e-mail</label>
                        <input id="f2" type="email" class="form-control" placeholder="Введите реальный e-mail">
                    </div>
                    <div class="form-group">
                        <label for="f3">Опишите свою проблему или вопрос</label>
                        <textarea id="f3" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="f3">Прикрепите файл</label>
                        <input type="file">
                        <p class="form-group-help">Загрузите файл в формате JPG, размером не более 500Кб</p>
                    </div>
                    <button class="orange-btn">Отправить</button>
                </form>
            </div>
        </div>
    </div>
</section>