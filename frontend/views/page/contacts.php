<?php
$this->title = 'Контакты';

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use \yii\helpers\Html;
/* @var $model_feedback common\models\Feedback */

?>
<section class="feedback">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-1">
                <h2 class="contacts_title">Обратная связь</h2>
                <p class="feedback_info">По всем интересующим вопросом писать на <a href="mailto:support@lamlima.com">support@lamlima.com</a> <br>для удобства можете воспользоваться следующей формой: </p>
                <?php $form = ActiveForm::begin([
                        'options' => [
                                'class' => 'feedback_form'
                        ]
                ]);?>
                <?= $form->field($model_feedback, 'name')
                    ->textInput(['placeholder' => 'Как к вам обращаться?'])
                    ->label('Ваше имя') ?>
                <?= $form->field($model_feedback, 'email')
                    ->textInput(['placeholder' => 'Введите реальный e-mail'])
                    ->label('Ваш e-mail') ?>
                <?= $form->field($model_feedback, 'message')
                    ->textarea()->label('Опишите свою проблему или вопрос') ?>
                <?= $form->field($model_feedback, 'image')
                    ->fileInput()->hint('Загрузите файл в формате JPG, размером не более 500Кб', [
                            'class' => 'form-group-help'
                    ])
                    ->label('Прикрепите файл') ?>
                <?= Html::submitButton('Отправить', [
                        'class' => 'orange-btn'
                ]) ?>
                <?php ActiveForm::end();?>


            </div>
            <div class="col-lg-4">
                <h2 class="contacts_title">Контакты</h2>
                <img src="img/logo.png" alt="" class="contacts_logo">
                <p class="contacts_phone">+7 495 740 6584</p>
                <p class="contacts_social">mail: support@lamlima.com <!--<br>skype: lima_co<--></p>
                <p class="contacts_address">188640, Россия <br>Ленинграская область,
                    г. Всевосложск, Всеволожский проспект, дом 113 оф. 77</p>
                <div class="contacts_map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2247.319184347914!2d37.6080433158179!3d55.718205002127476!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54b6cc9f91ca3%3A0xecf9c473509987a6!2sShabolovka+St%2C+37%2C+Moskva%2C+Russia%2C+115162!5e0!3m2!1sen!2sua!4v1560862785347!5m2!1sen!2sua" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</section>