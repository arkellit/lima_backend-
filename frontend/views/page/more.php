<?php
$this->title = 'Еще';

use yii\helpers\Url;
?>
<section class="more-sect">
    <div class="container">
        <div class="row">
            <div class="col-md-5 offset-md-2">
                <ul class="more-sect-list">
                    <li><a href="#">Новости</a></li>
                    <li><a href="#">Мероприятия</a></li>
                    <li><a href="#">Блог</a></li>
                </ul>
            </div>
            <div class="col-md-5">
                <ul class="more-sect-list">
                    <li><a href="#">О компании</a></li>
                    <li><a href="<?= Url::to('/feedback') ?>">Обратная связь</a></li>
                    <li><a href="<?= Url::to('/contacts') ?>">Контакты</a></li>
                    <li><a href="#">Мы в соцсетях</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 offset-md-2">
                <ul class="tab-list">
                    <li><a href="#tab1" class="active tabs-btn">ЗАКАЗЧИК</a></li>
                    <li><a href="#tab2" class="tabs-btn">ИСПОЛНИТЕЛЬ</a></li>
                </ul>
                <ul id="tab1" class="more-sect-list tab">
                    <li><a href="#">Обучение</a></li>
                    <li><a href="<?= Url::to('/faq-customer') ?>">FAQ</a></li>
                    <li><a href="#">Документация</a></li>
                </ul>
                <ul id="tab2" class="more-sect-list tab">
                    <li><a href="#">Обучение</a></li>
                    <li><a href="<?= Url::to('/faq-worker') ?>">FAQ</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>