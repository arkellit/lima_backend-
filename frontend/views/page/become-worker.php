<?php
$this->title = 'Как стать исполнителем услуг LIMA?';
?>
<section class="become-performer">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <h2 class="become-performer__title">Как стать исполнителем услуг LIMA?</h2>
                <p class="become-performer__subtitle">Скачайте приложение LIMA! и начните работать с заказами</p>
                <div class="apps_info-links">
                    <div class="apps_info-link">
                        <img src="img/qr.png" alt="">
                        <a href="#"><img src="img/apple.png" alt=""></a>
                    </div>
                    <div class="apps_info-link">
                        <img src="img/qr2.png" alt="">
                        <a href="#"><img src="img/google.png" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <img src="img/bitmap.png" alt="" class="become-performer__image">
            </div>
        </div>
    </div>
</section>
<section class="become-performer__info">
    <div class="container">
        <p>Выполнять заказы могут ИП и физические лица с налоговым статусом «Самозанятый» достигшие возраста старше 18 лет.</p>
    </div>
</section>
<section class="become-performer__about">
    <div class="container">
        <h2 class="become-performer__about-title">От заполнения данных в приложении до первого заказа — несколько часов</h2>
        <ul class="become-performer__about-list">
            <li>Не тратьте время на поиски клиентов, заказы появятся автоматически</li>
            <li>После выполнения заказа, формируются закрывающие документы с Вашим заказчиком в автоматическом режиме, после согласования (1-2 дня) вы получите деньги на карту за выполненную работу.</li>
            <li>LIMA работает только с проверенными Юридическими лицами и Индивидуальными Предпринимателями, и только по 100% предоплате.</li>
            <li>Мы передаем вам только оплаченные заказы.</li>
        </ul>
    </div>
</section>
<section class="become-performer__about" style="background: #f1f1f1;">
    <div class="container">
        <h2 class="become-performer__about-title">От заполнения данных в приложении до первого заказа — несколько часов</h2>
        <ul class="become-performer__about-list">
            <li>Работайте с заказчиками напрямую как <a href="#">Самозанятый</a></li>
            <li>Деньги за заказы будут поступать на вашу банковскую карту после каждого заказа в рабочий банковский день, начиная с первого выполненного  заказа.</li>
            <li>Вы работаете с LIMA напрямую, а значит, не платите лишних комиссий.</li>
            <li>Вы можете официально подтвердить доходы, например, чтобы взять кредит.</li>
            <li>Статус самозанятого партнёра даст вам приоритет при распределении заказов на целый месяц работы.</li>
        </ul>
        <a href="#" class="more">Подробнее о самозанятости</a>
    </div>
</section>