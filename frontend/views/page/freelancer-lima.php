<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Cамозанятые в Lima';
?>
<section class="self-employed">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h2 class="self-employed__title">Станьте <br>самозанятым <br>с LIMA</h2>
                <div class="self-employed__text">
                    <p>Вы зарабатываете деньги за выполненные заказы официально, которые будут перечисляться на вашу карту.</p>
                    <p>Для этого не нужно ездить в налоговую и заниматься бухгалтерией. Всё будет в вашем смартфоне.</p>
                </div>
                <a href="https://lknpd.nalog.ru/auth/register/about" target="_blank" class="black-btn">Стать самозанятым</a>
            </div>
        </div>
    </div>
</section>
<section class="self-employed-benefits">
    <div class="container">
        <h2 class="main-title">Преимущества самозанятого</h2>
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="self-employed-benefits__single">
                    <img src="img/benefit1.png" alt="">
                    <h2>Работайте <br>без лишних комиссий</h2>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="self-employed-benefits__single">
                    <img src="img/benefit2.png" alt="">
                    <h2>Быстрая оплата денег <br>на вашу карту</h2>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="self-employed-benefits__single">
                    <img src="img/benefit3.png" alt="">
                    <h2>Официальный <br>доход</h2>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="self-employed-benefits__single">
                    <img src="img/benefit4.png" alt="">
                    <h2>Легко открыть и <br>закрыть — всё онлайн</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="self-employed-info">
    <div class="container">
        <h2 class="main-title">КАК СТАТЬ САМОЗАНЯТЫМ?</h2>
        <p class="self-employed-info__subtitle">Чтобы стать самозанятым партнером LIMA нужен только смартфон и паспорт.</p>
        <div class="row">
            <div class="col-lg-4">
                <div class="self-employed-info__single">
                    <img src="img/self-employed-info2.png" alt="">
                    <h2>Установите приложение «Мой налог» и зарегистрируйтесь как самозанятый</h2>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="self-employed-info__single">
                    <img src="img/self-employed-info1.png" alt="">
                    <h2>При регистрации в приложении выберете «Стать самозанятым»</h2>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="self-employed-info__single">
                    <img src="img/self-employed-info3_1.png" style="max-width: 241px;" alt="">
                    <h2>Вернитесь в LIMA и закончите регистрацию</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="self-employed-contact">
    <h2 class="self-employed-contact__title">Центр контроля качества LIMA</h2>
    <div class="container">
        <p class="self-employed-contact__subtitle">Если у вас возникли вопросы при регистрации, вы можете связаться нами, написав в службу поддержки.</p>

        <?php $form = ActiveForm::begin([
            'options' => [
                'class' => 'self-employed-contact_form'
            ]
        ]);?>
        <div class="row">
            <div class="offset-lg-1 col-lg-4">
                <?= $form->field($model_feedback, 'name')
                    ->textInput(['placeholder' => 'Как к вам обращаться?'])
                    ->label('Ваше имя') ?>
                <?= $form->field($model_feedback, 'email')
                    ->textInput(['placeholder' => 'Введите реальный e-mail'])
                    ->label('Ваш e-mail') ?>
            </div>
            <div class="col-lg-4">
                <?= $form->field($model_feedback, 'message')
                    ->textarea()->label('Опишите свою проблему или вопрос') ?>
            </div>
            <div class="col-lg-2">
                <?= Html::submitButton('Отправить', [
                    'class' => 'orange-btn'
                ]) ?>
            </div>
        </div>
        <?php ActiveForm::end();?>
    </div>
</section>