<?php
use yii\widgets\ActiveForm;
?>
<section class="login">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h2 class="login_title">Регистрация для заказчиков</h2>
                <?php $form = ActiveForm::begin([
                        'options' => [
                          'class' => 'signup_form'
                        ]
                ]); ?>
                    <div class="signup_form-name">
                        <?= $form->field($model, 'last_name')
                            ->textInput(['placeholder' => 'Фамилия'])->label(false)?>
                        <?= $form->field($model, 'name')
                            ->textInput(['placeholder' => 'Имя'])->label(false)?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'middle_name')
                            ->textInput(['placeholder' => 'Отчество'])->label(false) ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'email')
                            ->textInput(['placeholder' => 'Действительный е-mail']) ?>
                    </div>
                    <button type="submit" class="orange-btn">Далее</button>
                    <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>