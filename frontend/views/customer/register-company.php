<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;

$customer = Yii::$app->session->get('customer');
$company_data = Yii::$app->session->get('customer_company_data');
$bank_data = Yii::$app->session->get('customer_bank_data');
?>
<section class="login">
    <div class="container">
<!--        <form action="/" class="signup_form">-->
            <div class="row">
                <div class="col-lg-6">
                    <h2 class="login_title">Регистрация для заказчиков</h2>
                    <div class="signup_text">
                        <p><?= $company_data['name_company'] ?></p>
                        <p>ИНН:  <?= $company_data['inn'] ?></p>
                        <p>КПП:  <?= $company_data['kpp'] ?></p>
                        <p>ОКПО: <?= $company_data['okpo'] ?></p>
                        <p>ОГРН: <?= $company_data['ogrn'] ?></p>
                        <p>Юридический адрес: <?= $company_data['address'] ?></p>
                    </div>
                    <div class="radio radio-address is_jurystic">
                        <input id="r1" type="radio" checked name="address" value="Фактический адрес совпадает с юридическим">
                        <label for="r1">Фактический адрес совпадает с юридическим</label>
                    </div>
                    <div class="radio radio-address is_fact">
                        <input id="r2" type="radio" name="address" value="Фактический адрес отличается от юридического">
                        <label for="r2">Фактический адрес отличается от юридического</label>
                    </div>
                    <div class="signup_form-block">
                        <div class="form-group">
                            <textarea class="form-control input_address" style="display: none;" placeholder="Индекс, город, улица, дом"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="s1">Телефон организации</label>
                            <input id="s1" type="tel" class="form-control phone_company"
                                   value="<?= $model_customer->contact_phone ?>"
                                   placeholder="Контактный телефон с кодом города">
                        </div>

                    </div>
                </div>
                <div class="col-lg-6">
                    <?php $form = ActiveForm::begin([
                        'id' => 'form-bik',
                        'action' => Url::to(['/customer/search-bik']),
                        'options' => [
                            'class' => 'bik_form'
                        ]
                    ]); ?>
                    <div class="form-group">
                        <?= $form->field($model_bik_form, 'bik')->textInput()?>

                        <button type="submit">Найти</button>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <div class="signup_text bik_bank_text">
                        <?php if(!empty($bank_data)):?>
                            <p>БИК: <span class="bik_bank_data"><?= $bank_data['bik'] ?></span></p>
                            <p>Город: <span class="city_bank_data"><?= $bank_data['city_bank'] ?></span></p>
                            <p>Банк: <span class="name_bank_data"><?= $bank_data['name_bank'] ?></span></p>
                            <p>Кор.счет.: <span class="ks_bank_data"><?= $bank_data['ks_bank'] ?></span></p>
                        <?php endif;?>

                    </div>

                    <?php $form = ActiveForm::begin();?>
                    
                    <?= $form->field($model_customer, 'customer_rs')
                        ->textInput() ?>
<!--                    <div class="form-group">-->
<!--                        <label for="s3">ИНН Банка</label>-->
<!--                        <input id="s3" type="text" class="form-control">-->
<!--                    </div>-->
<!--                    <div class="form-group">-->
<!--                        <label for="s4">КПП Банка</label>-->
<!--                        <input id="s4" type="text" class="form-control">-->
<!--                    </div>-->
<!--                    <div class="form-group">-->
<!--                        <label for="s5">Р/с вашей организации</label>-->
<!--                        <input id="s5" type="text" class="form-control">-->
<!--                    </div>-->
                </div>
            </div>
            <?= $form->field($model_customer, 'name')
                ->hiddenInput(['value' => $customer['name']])->label(false) ?>
            <?= $form->field($model_customer, 'last_name')
                ->hiddenInput(['value' => $customer['last_name']])->label(false) ?>
            <?= $form->field($model_customer, 'middle_name')
                ->hiddenInput(['value' => $customer['middle_name']])->label(false) ?>
            <?= $form->field($model_customer, 'email')
                ->hiddenInput(['value' => $customer['email']])->label(false) ?>
            <?= $form->field($model_customer, 'contact_phone')
                ->hiddenInput()->label(false) ?>
            <?= $form->field($model_customer, 'inn_company')
                ->hiddenInput(['value' =>  $company_data['inn']])->label(false) ?>
            <?= $form->field($model_customer, 'kpp_company')
                ->hiddenInput(['value' => $company_data['kpp']])->label(false) ?>
            <?= $form->field($model_customer, 'name_company')
                ->hiddenInput(['value' => $company_data['name_company']])->label(false) ?>
            <?= $form->field($model_customer, 'actual_address')
                ->hiddenInput()->label(false) ?>
            <?= $form->field($model_customer, 'bank_bik')
                ->hiddenInput()->label(false) ?>
            <?= $form->field($model_customer, 'bank_city')
                ->hiddenInput()->label(false) ?>
            <?= $form->field($model_customer, 'bank_name')
                ->hiddenInput()->label(false) ?>
            <?= $form->field($model_customer, 'bank_ks')
                ->hiddenInput()->label(false) ?>

        <div class="form-group">
            <p>Добавьте логотип предприятия</p>
            <div class="file">
                <?= $form->field($model_customer, 'logo_company')
                    ->fileInput()->label(false) ?>
            </div>
        </div>
            <button class="orange-btn small" type="submit">Далее</button>
            <?php ActiveForm::end();?>
<!--            <a href="#info-popup" class="orange-btn small open-popup">Далее</a>-->
<!--        </form>-->
    </div>
</section>


<?php
$address = $company_data['address'];
$script = <<<JS
    $('#customerallform-actual_address').val('$address');
    $('#customerallform-bank_bik').val($('.bik_bank_data').text());
    $('#customerallform-bank_city').val($('.city_bank_data').text());
    $('#customerallform-bank_name').val($('.name_bank_data').text());
    $('#customerallform-bank_ks').val($('.ks_bank_data').text());

    $('.is_jurystic').on('click', function() {
        $('.input_address').css('display', 'none');
        $('#customerallform-actual_address').val('$address');
    });
    
    $('.is_fact').on('click', function() {
        $('.input_address').css('display', '');
    });
    
    $('.input_address').on('mouseout blur', function() {
        $('#customerallform-actual_address').val($('.input_address').val()); 
    });
    
    $('.phone_company').on('mouseout blur', function() {
        $('#customerallform-contact_phone').val($('.phone_company').val()); 
    });

    $('#form-bik').on('beforeSubmit', function() { 
        var form = $('#form-bik');
        $.ajax({
            method: 'POST',
            url: form.attr('action'),
            data: form.serialize(),
            success: function(response) {
                console.log(response);
                if (Object.keys(response).length !== 0) {
                    viewData(response); 
                } else {
                    // If everything is ok ask to choose a role
                    console.log('ok');
                }
            }
        });
        function viewData(data) {
            var bik = data['bik'];
            var name_bank = data['name_bank'];
            var city_bank = data['city_bank'];
            var ks_bank = data['ks_bank']; 
            $('#customerallform-bank_bik').val(bik);
            $('#customerallform-bank_city').val(city_bank);
            $('#customerallform-bank_name').val(name_bank);
            $('#customerallform-bank_ks').val(ks_bank);
            
            var elementClassic =
            '<p>БИК: ' + '<span class="bik_bank_data">' + bik + '</span>' +'</p>' + 
            '<p>Город:  ' + '<span class="city_bank_data">' + city_bank + '</span>' +'</p>' +
            '<p>Банк:  ' + '<span class="name_bank_data">' + name_bank + '</span>' + '</p>' +
            '<p>Кор.счет.:  ' + '<span class="ks_bank_data">' + ks_bank + '</span>' + '</p>';
            $(".bik_bank_text").html("");
            $('.bik_bank_text').append(elementClassic)
        } 
        return false;
    });
JS;
$this->registerJs($script);
?>