<?php
use common\helpers\EncryptionHelpers;
use frontend\widgets\MakeDepositWidget;

/* @var $customer frontend\models\customer\Customer */
?>
<section class="profile">

    
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="profile_head">
                    <?php if(!is_null($customer->companyData->logo_company)):?>
                        <img style="width: 50px;" src="<?= Yii::$app->params['urlImage'] .
                        $customer->companyData->logo_company ?>"
                             alt="<?= $customer->companyData->name_company ?>">
                    <?php else:?>
                        <img style="width: 50px;" src="/img/default-logo.png"
                             alt="<?= $customer->companyData->name_company ?>">
                    <?php endif;?>

                    <h2><?= $customer->companyData->name_company ?></h2>
                </div>
                <div class="profile_info">
                    <p>ИНН:  <?= $customer->companyData->inn_company ?></p>
                    <p>КПП:  <?= $customer->companyData->kpp_company ?></p>
                    <p>Фактический адрес: <?= $customer->companyData->actual_address ?></p>
                    <p>Телефон: <?= $customer->companyData->contact_phone ?></p>
                    <p class="profile_info-director">Управляющий: <?= $customer->last_name . ' ' . $customer->name .
                        ' ' . $customer->middle_name ?></p>
                </div>
            </div>
            <div class="col-lg-5 offset-lg-2">
                <div class="profile_deposit">
                    <span>На депозите</span>
                    <div class="profile_deposit-info">
                        <p><?= $customer->companyData->deposit ?>₽</p>
<!--                        <a href="#add-deposit-popup3" class="open-popup">-->
<!--                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 334.877 334.877"><path d="M333.196 155.999h-16.067V82.09c0-17.719-14.415-32.134-32.134-32.134h-21.761L240.965 9.917a19.28 19.28 0 0 0-26.222-7.488l-85.464 47.526H33.815c-17.719 0-32.134 14.415-32.134 32.134v220.653c0 17.719 14.415 32.134 32.134 32.134h251.18c17.719 0 32.134-14.415 32.134-32.134V237.94h16.067v-81.941zm-48.201-93.19c9.897 0 17.982 7.519 19.068 17.14h-24.152l-9.525-17.14h14.609zm-63.999-49.146c3.014-1.69 7.07-.508 8.734 2.494l35.476 63.786H101.798l119.198-66.28zm83.279 289.079c0 10.63-8.651 19.281-19.281 19.281H33.815c-10.63 0-19.281-8.651-19.281-19.281V82.09c0-10.63 8.651-19.281 19.281-19.281h72.353L75.345 79.95H37.832c-3.554 0-6.427 2.879-6.427 6.427s2.873 6.427 6.427 6.427h266.443v63.201h-46.999c-21.826 0-39.589 17.764-39.589 39.589v2.764c0 21.826 17.764 39.589 39.589 39.589h46.999v64.795zm16.067-77.655h-63.066c-14.743 0-26.736-11.992-26.736-26.736v-2.764c0-14.743 11.992-26.736 26.736-26.736h63.066v56.236zm-43.381-27.59c0 7.841-6.35 14.19-14.19 14.19-7.841 0-14.19-6.35-14.19-14.19s6.35-14.19 14.19-14.19c7.841-.001 14.19 6.355 14.19 14.19z"/></svg>-->
<!--                        </a>-->
                        <a href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 334.877 334.877"><path d="M333.196 155.999h-16.067V82.09c0-17.719-14.415-32.134-32.134-32.134h-21.761L240.965 9.917a19.28 19.28 0 0 0-26.222-7.488l-85.464 47.526H33.815c-17.719 0-32.134 14.415-32.134 32.134v220.653c0 17.719 14.415 32.134 32.134 32.134h251.18c17.719 0 32.134-14.415 32.134-32.134V237.94h16.067v-81.941zm-48.201-93.19c9.897 0 17.982 7.519 19.068 17.14h-24.152l-9.525-17.14h14.609zm-63.999-49.146c3.014-1.69 7.07-.508 8.734 2.494l35.476 63.786H101.798l119.198-66.28zm83.279 289.079c0 10.63-8.651 19.281-19.281 19.281H33.815c-10.63 0-19.281-8.651-19.281-19.281V82.09c0-10.63 8.651-19.281 19.281-19.281h72.353L75.345 79.95H37.832c-3.554 0-6.427 2.879-6.427 6.427s2.873 6.427 6.427 6.427h266.443v63.201h-46.999c-21.826 0-39.589 17.764-39.589 39.589v2.764c0 21.826 17.764 39.589 39.589 39.589h46.999v64.795zm16.067-77.655h-63.066c-14.743 0-26.736-11.992-26.736-26.736v-2.764c0-14.743 11.992-26.736 26.736-26.736h63.066v56.236zm-43.381-27.59c0 7.841-6.35 14.19-14.19 14.19-7.841 0-14.19-6.35-14.19-14.19s6.35-14.19 14.19-14.19c7.841-.001 14.19 6.355 14.19 14.19z"/></svg>
                        </a>
                    </div>
                </div>
                <div class="profile_info">
                    <p>БИК: <?= $customer->bankData->bank_bik ?></p>
                    <p>Город: <?= $customer->bankData->bank_city ?></p>
                    <p>Банк: <?= $customer->bankData->bank_name ?></p>
                    <p>ИНН: <?= $customer->bankData->bank_inn ?></p>
                    <p>Кор.счет.: <?= $customer->bankData->bank_ks ?></p>
                    <p>Р/с <?= EncryptionHelpers::dec_enc('decrypt', $customer->bankData->customer_rs) ?></p>
                </div>
            </div>
        </div>
        <a href="#"  style="display: none;" class="orange-btn profile-orange-btn">Изменить профиль</a>
    </div>
</section>
    <?= MakeDepositWidget::widget(); ?>
<!--    <div id="add-deposit-popup" class="zoom-anim-dialog mfp-hide popup add-deposit-popup">-->
<!--        <h2>Пополнить депозит</h2>-->
<!--        <form action="/">-->
<!--            <div class="form-group">-->
<!--                <label for="ad1">Пополнить депозит на сумму</label>-->
<!--                <input id="ad1" type="text" class="form-control">-->
<!--                <p class="form-group-help">Будет сформирован счет на оплату указанной суммы</p>-->
<!--            </div>-->
<!--            <div class="add-deposit-popup-buttons">-->
<!--                <button class="grey-btn">Отмена</button>-->
<!--                <button type="submit" class="orange-btn">Пополнить</button>-->
<!--            </div>-->
<!--        </form>-->
<!--    </div>-->
<?php if (Yii::$app->session->has('success_make_deposit')):?>
    <div id="info-popup1" class="zoom-anim-dialog mfp-hide popup info-popup">
        <h2>Счет на оплату отправлен</h2>
        <div class="info-popup-text">Счет на оплату
            <?= Yii::$app->session->get('success_make_deposit') ?>₽ выслан на
            e-mail: <?= $customer->companyData->email ?>
            После поступления денежных средств депозит будет пополнен</div>
    </div>
    <?php
    $script_open_popup = <<<JS
$.magnificPopup.open({
              items: {
                src: '#info-popup1'
              },
              type: 'inline'
});
JS;
    $this->registerJs($script_open_popup);
    Yii::$app->session->remove('success_make_deposit') ?>
<?php endif;?>
<?php if (Yii::$app->session->has('success_register')):?>
    <div id="info-popup" class="zoom-anim-dialog mfp-hide popup info-popup">
        <h2>Спасибо!</h2>
        <div class="info-popup-text" style="margin-bottom: 0">
            <p>Ваши данные проходят модерацию. Это займет около 30 мин.
                Уведомление о прохождении модерации будет выслано на e-mail:
                <?= Yii::$app->session->get('success_register') ?></p>
        </div>
    </div>
    <?php
    $script_open_popup = <<<JS
$.magnificPopup.open({
              items: {
                src: '#info-popup'
              },
              type: 'inline'
              
}); 
$('.close-popup').click(function(e) {
            e.preventDefault();
            $.magnificPopup.close();
        });
JS;
    $this->registerJs($script_open_popup);
    Yii::$app->session->remove('success_register') ?>
<?php endif;?>