<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>
<section class="login">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h2 class="login_title">Регистрация для заказчиков</h2>
                <?php $form = ActiveForm::begin([
                        'id' => 'form-inn',
                        'action' => Url::to(['/customer/check-inn']),
                        'options' => [
                            'class' => 'signup_form'
                        ]
                ]); ?>
                    <div class="form-group">
                        <?= $form->field($model_inn, 'inn')
                            ->textInput(['placeholder' => 'Введите ИНН вашей организации'])->label(false)?>
                    </div>
                <button  type="submit" class="orange-btn submit_button">Далее</button>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>

<div id="info-popup" class="zoom-anim-dialog mfp-hide popup info-popup">
    <h2>Ваша организация</h2>
    <div class="info-popup-text">
<!--        <p>ООО "АРКЕЛЛ ИТ-СОЛЮШН"</p>-->
<!--        <p>ИНН / КПП:  5027261200 / 502701001</p>-->
<!--        <p>ОКПО: 24882157</p>-->
<!--        <p>ОГРН: 1185027002654</p>-->
<!--        <p>Юридический адрес: 140090, МОСКОВСКАЯ ОБЛ, ДЗЕРЖИНСКИЙ Г, АКАДЕМИКА ЖУКОВА УЛ, ДОМ 38, ПОМЕЩЕНИЕ 101</p>-->
<!--        <p>Телефон: 8 (961) 883-22-32</p>-->
    </div>
    <div class="add-brahch-popup-buttons">
        <a href="#" class="grey-btn close-popup">Нет</a>
        <a href="<?= Url::to(['/customer/register-company']) ?>" class="orange-btn">Верно</a>
    </div>
</div>

<?php
$script = <<<JS
$('#form-inn').on('beforeSubmit', function() {
        $('.submit_button').attr('disabled', true);
        var form = $('#form-inn');
        $.ajax({
            method: 'POST',
            url: form.attr('action'),
            data: form.serialize(),
            success: function(response) {
                
                // console.log(response);return  false;
                response_error = response; //JSON.parse(response);
                $('.submit_button').attr('disabled', false);

                // console.log(response['error']);
                if (response['error']){
                    alert(response['error']);
                    return false;
                }
                if (response['error']){
                    alert(response_error['error']);
                } 
                // if (Object.keys(response).length !== 0) {
                if (!response_error['error']){
                    $('.info-popup-text').empty();
                    viewData(response);
                    openPopup();
                } else {
                    // If everything is ok ask to choose a role
                    console.log('ok');
                }
            },
        });
        function viewData(data) {
            var inn = $('#customerinnform-inn').val();
            var company_data = data['name_company'];
            var kpp = data['kpp'] == null ? '' : data['kpp'];
            var address = data['address'];
            var phone = data['phone'] == null ? '' : data['phone'];
            var okpo = data['okpo'] == null ? '' : data['okpo'];
            var ogrn = data['ogrn'];
            
            var elementClassic = '<p>' + company_data + '</p>' + 
            '<p>ИНН / КПП: ' + inn + ' / '+ kpp +'</p>' + 
            '<p>ОКПО:  ' + okpo + '</p>' +
            '<p>ОГРН:  ' + ogrn + '</p>' +
            '<p>Юридический адрес:  ' + address + '</p>' +
            '<p>Телефон:  ' + phone + '</p>';
           
            $('.info-popup-text').append(elementClassic)
        }
        function openPopup(){
           $.magnificPopup.open({
              items: {
                src: '#info-popup'
              },
              type: 'inline'
            });
        }
        $('.close-popup').click(function(e) {
            e.preventDefault();
            $.magnificPopup.close();
        });
        return false;
    });
JS;
$this->registerJs($script);
?>