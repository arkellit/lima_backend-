<?php
$this->title = 'Новости';

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $news \common\models\NewsFront */
?>
<section class="news">
    <div class="container">
        <div class="row">
            <?php foreach ($news as $item) :?>
                <div class="col-lg-6">
                    <article class="news__single">
                        <?= Html::img(Yii::$app->params['urlImage']  . $item->image,
                            ['class' => 'news__image', 'alt' => $item->title,
                                 ]) ?>
                         <h2 class="news__title">
                             <a href="<?= Url::to(['news/single-news', 'id' => $item->id]) ?>">
                                 <?= $item->title ?></a>
                         </h2>
                        <p class="news__excerpt">
                            <?= $item->short_description ?>
                        </p>
                        <a href="<?= Url::to(['news/single-news', 'id' => $item->id]) ?>" class="news__more">Подробнее</a>
                    </article>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</section>