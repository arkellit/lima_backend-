<?php
/* @var $news \common\models\NewsFront */
$this->title = $news->title;

?>
<section class="politics">
    <div class="container">
        <h2 class="main-title"><?= $news->title ?></h2>
        <div class="politics__block">
            <?= $news->description ?>
        </div>
    </div>
</section>