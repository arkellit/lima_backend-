<?php

use api\modules\v1\models\order\OrderRequest;
use api\modules\v1\models\worker\Worker;
use common\helpers\DeclensionWordHelpers;
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = 'Исполнители';
/* @var $order frontend\models\order\Order */
/* @var $order frontend\models\order\Order */
/* @var $order_worker_count */
/* @var $models OrderRequest */
?>
<section class="offer">
    <div class="container">
        <div class="offer__head">
            <h2>Заказ № <?= $order->number_order ?></h2>
            <div class="archive-btns">
                <a href="<?= Url::to(['/order-offer/offer', 'order_id' => $order->id]) ?>"
                   class="<?= Yii::$app->controller->action->id == 'offer' ? 'active' : ''?>">ПРЕДЛОЖЕНИЯ</a>
                <a href="<?= Url::to(['/order-offer/offer-worker', 'order_id' => $order->id]) ?>"
                   class="<?= Yii::$app->controller->action->id == 'offer-worker' ? 'active' : ''?>">ИСПОЛНИТЕЛИ</a>
            </div>
            <div class="offer__head-filter">
<!--                <a href="#filter-popup" class="open-popup">Фильтр</a>-->
            </div>
        </div>
        <div class="offer__inner">
            <div class="offer__inner-head">
                <p>Исполнителей набрано: <?= $order_worker_count ?> из <?= $order->orderPlace->count_worker ?></p>
            </div>
            <?php foreach ($models as $model):?>
                <div class="offer__single">
<!--                    <div class="offer__single-checkbox">-->
<!--                        <input id="--><?//= $model['id'] ?><!--" type="checkbox" class="worker_id"-->
<!--                               data-id="--><?//= $model['id'] ?><!--" name="selectedCheck">-->
<!--                        <label for="--><?//= $model['id'] ?><!--"></label>-->
<!--                    </div>-->
                    <div class="offer__single-img">
                        <!--                        --><?php //var_dump($model['imageWorker']['photo_porter'])?>
                        <?= Html::img(Yii::$app->params['urlImage'] . $model['imageWorker']['photo_porter']) ?>
                    </div>
                    <div class="offer__single-info">
                        <h3 class="offer__single-name"><?= $model['last_name'] ?> <?= $model['name'] ?></h3>
                        <div class="offer__single-meta">
                            <p>Возраст: <span><?= $model['age'] ?> <?= DeclensionWordHelpers::declension_word($model['age'],
                                        ['год', 'года', 'лет']) ?></span></p>
                            <p>Рейтинг: <span><?= Worker::getRating($model['rating']) ?></span></p>
                        </div>
                    </div>
                    <div class="offer__single-btns">
                        <a href="#profile-popup"  data-id="<?= $model['id'] ?>" class="white-btn open-popup open_popup_worker_profile">Профиль</a>
<!--                        <a href="#" data-id="--><?//= $model['id'] ?><!--" class="orange-btn send_worker_array">В исполнители</a>-->
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</section>
<!--<div class="checked-block">-->
<!--    <div class="container">-->
<!--        <div class="checked-block__inner">-->
<!--            <div class="checked-block__count">-->
<!--                <p><span>0</span> выбрано</p>-->
<!--            </div>-->
<!--            <a href="#" class="orange-btn">Удалить</a>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<!--<div id="filter-popup" class="zoom-anim-dialog mfp-hide popup filter-popup">-->
<!--    <h2>Фильтр</h2>-->
<!--    <div class="row">-->
<!--        <div class="col-lg-6">-->
<!--            <div class="filter-popup__radio">-->
<!--                <div class="filter-popup__radio-single">-->
<!--                    <input id="male" type="radio" name="gnender" value="Парни">-->
<!--                    <label for="male">Парни</label>-->
<!--                </div>-->
<!--                <div class="filter-popup__radio-single">-->
<!--                    <input id="female" type="radio" name="gnender" value="Девушки">-->
<!--                    <label for="female">Девушки</label>-->
<!--                </div>-->
<!--                <div class="filter-popup__radio-single">-->
<!--                    <input id="gender-all" type="radio" name="gnender" value="Все">-->
<!--                    <label for="gender-all">Все</label>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="form-group">-->
<!--                <label>Город</label>-->
<!--                <select class="select">-->
<!--                    <option value="Москва">Москва</option>-->
<!--                    <option value="Москва">Москва</option>-->
<!--                </select>-->
<!--            </div>-->
<!--            <div class="filter-popup__range">-->
<!--                <p class="filter-popup__range-label">Поиск в радиусе</p>-->
<!--                <div id="slider-range" class="filter-popup__range-slider"></div>-->
<!--                <p id="range-search-radius" class="filter-popup__range-value"><span>1</span> км</p>-->
<!--            </div>-->
<!--            <div class="filter-popup__range">-->
<!--                <p class="filter-popup__range-label">Рост</p>-->
<!--                <div id="slider-range2" class="filter-popup__range-slider"></div>-->
<!--                <p id="range-height" class="filter-popup__range-value"><span>160-180</span></p>-->
<!--            </div>-->
<!--            <div class="filter-popup__range">-->
<!--                <p class="filter-popup__range-label">Возраст</p>-->
<!--                <div id="slider-range3" class="filter-popup__range-slider"></div>-->
<!--                <p id="range-age" class="filter-popup__range-value"><span>18-25</span></p>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="col-lg-6">-->
<!--            <div class="form-group">-->
<!--                <label>Цвет глаз</label>-->
<!--                <select class="select">-->
<!--                    <option value="Любой">Любой</option>-->
<!--                    <option value="Любой">Любой</option>-->
<!--                </select>-->
<!--            </div>-->
<!--            <div class="form-group">-->
<!--                <label>Длина волос</label>-->
<!--                <select class="select">-->
<!--                    <option value="Длинные">Длинные</option>-->
<!--                    <option value="Длинные">Длинные</option>-->
<!--                </select>-->
<!--            </div>-->
<!--            <div class="form-group">-->
<!--                <label>Цвет волос</label>-->
<!--                <select class="select">-->
<!--                    <option value="Шатен">Шатен</option>-->
<!--                    <option value="Шатен">Шатен</option>-->
<!--                </select>-->
<!--            </div>-->
<!--            <div class="form-group">-->
<!--                <label>Телосложение</label>-->
<!--                <select class="select">-->
<!--                    <option value="Стройное">Стройное</option>-->
<!--                    <option value="Стройное">Стройное</option>-->
<!--                </select>-->
<!--            </div>-->
<!--            <div class="form-group">-->
<!--                <label>Внешность</label>-->
<!--                <select class="select">-->
<!--                    <option value="Европейская">Европейская</option>-->
<!--                    <option value="Европейская">Европейская</option>-->
<!--                </select>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="profile-popup__btns">-->
<!--        <a href="#" class="grey-btn close-popup">Отмена</a>-->
<!--        <a href="#" class="orange-btn">Применить</a>-->
<!--    </div>-->
<!--</div>-->
    <div class="open_profile_worker"></div>
<?php
$script_open_popup = <<<JS
var order_id = "$order->id";
$('.open_popup_worker_profile').click(function() {
    var id = $(this).attr('data-id'); 
    $.get('/order-offer/get-worker-profile', {"id":id, "order_id": order_id, "is_offer_worker":true}, function(response) {
        if (response != false) {
            $('.open_profile_worker').empty();
            $('.open_profile_worker').append(response);
        }
        else alert('Неверный запрос');
    });
});
JS;
$this->registerJs($script_open_popup);
?>