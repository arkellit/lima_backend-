<?php

/* @var $model_worker Worker */
/* @var $count_order_active */
/* @var $count_order_finished */
/* @var $order_id */
/* @var $is_offer_worker */

use api\modules\v1\models\worker\Worker;
use common\helpers\DeclensionWordHelpers;
use yii\helpers\Html; ?>
<script>
    $.magnificPopup.open({
        items: {
            src: '#profile-popup'
        },
        type: 'inline'
    });
    $('.close-popup').click(function(e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
</script>
<div id="profile-popup" class="zoom-anim-dialog mfp-hide popup profile-popup">
    <h2>Карточка исполнителя</h2>
    <div class="profile-popup__info">
        <div class="profile-popup__image">
            <?= Html::img(Yii::$app->params['urlImage'] . $model_worker['imageWorker']['photo_porter']) ?>
        </div>
        <div class="profile-popup__bio">
            <p class="profile-popup__name"><?= $model_worker['last_name'] ?> <?= $model_worker['name'] ?></p>
            <p class="profile-popup__age"><?= $model_worker['age'] ?> <?= DeclensionWordHelpers::declension_word($model_worker['age'],
                    ['год', 'года', 'лет']) ?>, <?= $model_worker['city']['name'] ?></p>
            <div class="profile-popup__rate">
                <p class="profile-popup__rate-value"><?= Worker::getRating($model_worker['rating']) ?></p>
                <p class="profile-popup__rate-label">Рейтинг</p>
            </div>
            <div class="profile-popup__orders">
                <div class="profile-popup__order">
                    <p class="profile-popup__order-value"><?= $count_order_finished ?></p>
                    <p class="profile-popup__order-title">Выполнено</p>
                </div>
                <div class="profile-popup__order">
                    <p class="profile-popup__order-value"><?= $count_order_active ?></p>
                    <p class="profile-popup__order-title">Активных</p>
                </div>
<!--                <div class="profile-popup__order">-->
<!--                    <p class="profile-popup__order-value">--><?//= $count_order_offer ?><!--</p>-->
<!--                    <p class="profile-popup__order-title">Отмененных</p>-->
<!--                </div>-->
            </div>
        </div>
    </div>
    <div class="profile-popup__characteristics">
        <div class="profile-popup__characteristics-single">
            <table class="profile-popup__table">
                <tr>
                    <td>Рост</td>
                    <td><?= $model_worker['height'] ?> см</td>
                </tr>
                <tr>
                    <td>Вес</td>
                    <td><?= $model_worker['weight'] ?> кг</td>
                </tr>
                <tr>
                    <td>Цвет глаз</td>
                    <td><?= $model_worker['eyeColor']['name'] ?></td>
                </tr>
            </table>
        </div>
        <div class="profile-popup__characteristics-single">
            <table class="profile-popup__table">
                <tr>
                    <td>Длина волос</td>
                    <td><?= $model_worker['hairLength']['name'] ?></td>
                </tr>
                <tr>
                    <td>Цвет волос</td>
                    <td><?= $model_worker['hairColor']['name'] ?></td>
                </tr>
                <tr>
                    <td>Внешность</td>
                    <td><?= $model_worker['appearance']['name'] ?></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="profile-popup__btns">
        <a href="#" class="grey-btn close-popup">Отмена</a>
        <?php if (!$is_offer_worker) : ?>
            <a href="#" data-id="<?= $model_worker['id'] ?>" class="orange-btn send_worker_array_from_profile">Назначить исполнителем</a>
        <?php endif;?>
    </div>
</div>

<?php
$script_open_popup_profile = <<<JS
var worker_arrays = [];
var order_id = "$order_id";
$('.send_worker_array_from_profile').on('click', function() {
    worker_arrays = $(this).attr('data-id');  
    $.post('/order-offer/confirm-worker', {"order_id": order_id, "worker_arrays": worker_arrays}, function(response) {
        if (response == false){
            alert('Не удалось добавить пользователя');
        }
  });
});

JS;
$this->registerJs($script_open_popup_profile);
?>
