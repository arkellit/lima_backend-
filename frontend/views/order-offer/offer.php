<?php

use api\modules\v1\models\order\OrderRequest;
use api\modules\v1\models\worker\Worker;
use common\helpers\DeclensionWordHelpers;
use frontend\widgets\WorkerSearchWidget;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Предложения исполнителей';

/* @var $order frontend\models\order\Order */
/* @var $order_worker_count */
/* @var $models OrderRequest */

?>

<section class="offer">
    <div class="container">
        <div class="row">
            <?= Html::a('<span class="glyphicon glyphicon-arrow-left"></span> Назад',
                Url::to(Yii::$app->request->referrer), ['class' => 'btn btn-default btn-lg']) ?>
        </div>
        <div class="offer__head">
            <h2>Заказ № <?= $order->number_order ?></h2>
            <div class="archive-btns">
                <a href="<?= Url::to(['/order-offer/offer', 'order_id' => $order->id]) ?>"
                   class="<?= Yii::$app->controller->action->id == 'offer' ? 'active' : '' ?>">ПРЕДЛОЖЕНИЯ</a>
                <a href="<?= Url::to(['/order-offer/offer-worker', 'order_id' => $order->id]) ?>"
                   class="<?= Yii::$app->controller->action->id == 'offer-worker' ? 'active' : '' ?>">ИСПОЛНИТЕЛИ</a>
            </div>
            <div class="offer__head-filter">
                <a href="#filter-popup" class="open-popup">Фильтр</a>
            </div>
        </div>
        <div class="offer__inner">
            <div class="offer__inner-head">
                <p>Исполнителей набрано: <?= $order_worker_count ?> из <?= $order->orderPlace->count_worker ?></p>
            </div>
            <?php foreach ($models as $model):?>
                <div class="offer__single">
                    <div class="offer__single-checkbox">
                        <input id="<?= $model['id'] ?>" type="checkbox" class="worker_id"
                               data-id="<?= $model['id'] ?>" name="selectedCheck">
                        <label for="<?= $model['id'] ?>"></label>
                    </div>
                    <div class="offer__single-img">
<!--                        --><?php //var_dump($model['imageWorker']['photo_porter'])?>
                        <?= Html::img(Yii::$app->params['urlImage'] . $model['imageWorker']['photo_porter']) ?>
                    </div>
                    <div class="offer__single-info">
                        <h3 class="offer__single-name"><?= $model['last_name'] ?> <?= $model['name'] ?></h3>
                        <div class="offer__single-meta">
                            <p>Возраст: <span><?= $model['age'] ?> <?= DeclensionWordHelpers::declension_word($model['age'],
                                        ['год', 'года', 'лет']) ?></span></p>
                            <p>Рейтинг: <span><?= Worker::getRating($model['rating']) ?></span></p>
                        </div>
                    </div>
                    <div class="offer__single-btns">
                        <a href="#profile-popup"  data-id="<?= $model['id'] ?>" class="white-btn open-popup open_popup_worker_profile">Профиль</a>
                        <a href="#" data-id="<?= $model['id'] ?>" class="orange-btn send_worker_array">В исполнители</a>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</section>
<div class="checked-block">
    <div class="container">
        <div class="checked-block__inner">
            <div class="checked-block__count">
                <p><span>0</span> выбрано</p>
            </div>
            <a href="#" class="orange-btn send_worker_array_all">В исполнители</a>
        </div>
    </div>
</div>
<?= WorkerSearchWidget::widget() ?>
<div class="open_profile_worker"></div>
<?php
$script_open_popup = <<<JS
var order_id = "$order->id";
$('.open_popup_worker_profile').click(function() {
    var id = $(this).attr('data-id'); 
    $.get('/order-offer/get-worker-profile', {"id":id, "order_id": order_id}, function(response) {
        if (response != false) {
            $('.open_profile_worker').empty();
            $('.open_profile_worker').append(response);
        }
        else alert('Неверный запрос');
    });
});

var worker_arrays = [];
$('.worker_id').on('click', function() {
  var id = $(this).attr('data-id');
  if($.inArray(id, worker_arrays) === -1){
      worker_arrays.push(id);
  }
  else {
       worker_arrays = $.grep(worker_arrays, function(value) {
          return value !== id;
      });
  }
  console.log(worker_arrays);
});

$('.send_worker_array_all').on('click', function() {
    $.post('/order-offer/confirm-worker', {"order_id": order_id, "worker_arrays": worker_arrays}, function(response) {
        if (response == false){
            alert('Не удалось добавить пользователя');
        }
  });
});

$('.send_worker_array').on('click', function() {
    worker_arrays = $(this).attr('data-id');
    $.post('/order-offer/confirm-worker', {"order_id": order_id, "worker_arrays": worker_arrays}, function(response) {
        if (response == false){
            alert('Не удалось добавить пользователя');
        }
  });
});

JS;
$this->registerJs($script_open_popup);
?>