<?php

$this->title = 'Исполнители';
?>

<section class="offer">
    <div class="container">
        <div class="offer__head">
            <h2>Заказ № 77-1234</h2>
            <div class="archive-btns">
                <a href="#">ПРЕДЛОЖЕНИЯ</a>
                <a href="#" class="active">ИСПОЛНИТЕЛИ</a>
            </div>
            <div class="offer__head-filter">
                <a href="#filter-popup" class="open-popup">Фильтр</a>
            </div>
        </div>
        <div class="offer__inner">
            <div class="offer__inner-head">
                <p>Исполнителей набрано: 8 из 10</p>
            </div>
            <div class="offer__single">
                <div class="offer__single-checkbox">
                    <input id="c1" type="checkbox" name="selectedCheck">
                    <label for="c1"></label>
                </div>
                <div class="offer__single-img">
                    <img src="img/offer.jpg" alt="">
                </div>
                <div class="offer__single-info">
                    <h3 class="offer__single-name">Астафьева Дарья</h3>
                    <div class="offer__single-meta">
                        <p>Возраст: <span>21 год</span></p>
                        <p>Рейтинг: <span>4,9</span></p>
                    </div>
                </div>
                <div class="offer__single-btns">
                    <a href="#profile-popup" class="white-btn open-popup">Профиль</a>
                    <a href="#" class="orange-btn">Удалить</a>
                </div>
            </div>
            <div class="offer__single">
                <div class="offer__single-checkbox">
                    <input id="c2" type="checkbox" name="selectedCheck">
                    <label for="c2"></label>
                </div>
                <div class="offer__single-img">
                    <img src="img/offer.jpg" alt="">
                </div>
                <div class="offer__single-info">
                    <h3 class="offer__single-name">Астафьева Дарья</h3>
                    <div class="offer__single-meta">
                        <p>Возраст: <span>21 год</span></p>
                        <p>Рейтинг: <span>4,9</span></p>
                    </div>
                </div>
                <div class="offer__single-btns">
                    <a href="#profile-popup" class="white-btn open-popup">Профиль</a>
                    <a href="#" class="orange-btn">Удалить</a>
                </div>
            </div>
            <div class="offer__single">
                <div class="offer__single-checkbox">
                    <input id="c3" type="checkbox" name="selectedCheck">
                    <label for="c3"></label>
                </div>
                <div class="offer__single-img">
                    <img src="img/offer.jpg" alt="">
                </div>
                <div class="offer__single-info">
                    <h3 class="offer__single-name">Астафьева Дарья</h3>
                    <div class="offer__single-meta">
                        <p>Возраст: <span>21 год</span></p>
                        <p>Рейтинг: <span>4,9</span></p>
                    </div>
                </div>
                <div class="offer__single-btns">
                    <a href="#profile-popup" class="white-btn open-popup">Профиль</a>
                    <a href="#" class="orange-btn">Удалить</a>
                </div>
            </div>
            <div class="offer__single">
                <div class="offer__single-checkbox">
                    <input id="c4" type="checkbox" name="selectedCheck">
                    <label for="c4"></label>
                </div>
                <div class="offer__single-img">
                    <img src="img/offer.jpg" alt="">
                </div>
                <div class="offer__single-info">
                    <h3 class="offer__single-name">Астафьева Дарья</h3>
                    <div class="offer__single-meta">
                        <p>Возраст: <span>21 год</span></p>
                        <p>Рейтинг: <span>4,9</span></p>
                    </div>
                </div>
                <div class="offer__single-btns">
                    <a href="#profile-popup" class="white-btn open-popup">Профиль</a>
                    <a href="#" class="orange-btn">Удалить</a>
                </div>
            </div>
            <div class="offer__single">
                <div class="offer__single-checkbox">
                    <input id="c5" type="checkbox" name="selectedCheck">
                    <label for="c5"></label>
                </div>
                <div class="offer__single-img">
                    <img src="img/offer.jpg" alt="">
                </div>
                <div class="offer__single-info">
                    <h3 class="offer__single-name">Астафьева Дарья</h3>
                    <div class="offer__single-meta">
                        <p>Возраст: <span>21 год</span></p>
                        <p>Рейтинг: <span>4,9</span></p>
                    </div>
                </div>
                <div class="offer__single-btns">
                    <a href="#profile-popup" class="white-btn open-popup">Профиль</a>
                    <a href="#" class="orange-btn">Удалить</a>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="checked-block">
    <div class="container">
        <div class="checked-block__inner">
            <div class="checked-block__count">
                <p><span>0</span> выбрано</p>
            </div>
            <a href="#" class="orange-btn">Удалить</a>
        </div>
    </div>
</div>
<div id="profile-popup" class="zoom-anim-dialog mfp-hide popup profile-popup">
    <h2>Карточка исполнителя</h2>
    <div class="profile-popup__info">
        <div class="profile-popup__image">
            <img src="/img/profile.jpg" alt="">
        </div>
        <div class="profile-popup__bio">
            <p class="profile-popup__name">Милославский Вениамин</p>
            <p class="profile-popup__age">21 год, Москва</p>
            <div class="profile-popup__rate">
                <p class="profile-popup__rate-value">4,9</p>
                <p class="profile-popup__rate-label">Рейтинг</p>
            </div>
            <div class="profile-popup__orders">
                <div class="profile-popup__order">
                    <p class="profile-popup__order-value">15</p>
                    <p class="profile-popup__order-title">Выполнено</p>
                </div>
                <div class="profile-popup__order">
                    <p class="profile-popup__order-value">0</p>
                    <p class="profile-popup__order-title">Активных</p>
                </div>
                <div class="profile-popup__order">
                    <p class="profile-popup__order-value">2</p>
                    <p class="profile-popup__order-title">Отмененных</p>
                </div>
            </div>
        </div>
    </div>
    <div class="profile-popup__characteristics">
        <div class="profile-popup__characteristics-single">
            <table class="profile-popup__table">
                <tr>
                    <td>Рост</td>
                    <td>180 см</td>
                </tr>
                <tr>
                    <td>Вес</td>
                    <td>75 кг</td>
                </tr>
                <tr>
                    <td>Цвет глаз</td>
                    <td>Серый</td>
                </tr>
            </table>
        </div>
        <div class="profile-popup__characteristics-single">
            <table class="profile-popup__table">
                <tr>
                    <td>Длина волос</td>
                    <td>Короткие</td>
                </tr>
                <tr>
                    <td>Цвет волос</td>
                    <td>Шатен</td>
                </tr>
                <tr>
                    <td>Внешность</td>
                    <td>Европейская</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="profile-popup__btns">
        <a href="#" class="grey-btn close-popup">Отмена</a>
        <a href="#" class="orange-btn">Назначить исполнителем</a>
    </div>
</div>
<div id="filter-popup" class="zoom-anim-dialog mfp-hide popup filter-popup">
    <h2>Фильтр</h2>
    <div class="row">
        <div class="col-lg-6">
            <div class="filter-popup__radio">
                <div class="filter-popup__radio-single">
                    <input id="male" type="radio" name="gnender" value="Парни">
                    <label for="male">Парни</label>
                </div>
                <div class="filter-popup__radio-single">
                    <input id="female" type="radio" name="gnender" value="Девушки">
                    <label for="female">Девушки</label>
                </div>
                <div class="filter-popup__radio-single">
                    <input id="gender-all" type="radio" name="gnender" value="Все">
                    <label for="gender-all">Все</label>
                </div>
            </div>
            <div class="form-group">
                <label>Город</label>
                <select class="select">
                    <option value="Москва">Москва</option>
                    <option value="Москва">Москва</option>
                </select>
            </div>
            <div class="filter-popup__range">
                <p class="filter-popup__range-label">Поиск в радиусе</p>
                <div id="slider-range" class="filter-popup__range-slider"></div>
                <p id="range-search-radius" class="filter-popup__range-value"><span>1</span> км</p>
            </div>
            <div class="filter-popup__range">
                <p class="filter-popup__range-label">Рост</p>
                <div id="slider-range2" class="filter-popup__range-slider"></div>
                <p id="range-height" class="filter-popup__range-value"><span>160-180</span></p>
            </div>
            <div class="filter-popup__range">
                <p class="filter-popup__range-label">Возраст</p>
                <div id="slider-range3" class="filter-popup__range-slider"></div>
                <p id="range-age" class="filter-popup__range-value"><span>18-25</span></p>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label>Цвет глаз</label>
                <select class="select">
                    <option value="Любой">Любой</option>
                    <option value="Любой">Любой</option>
                </select>
            </div>
            <div class="form-group">
                <label>Длина волос</label>
                <select class="select">
                    <option value="Длинные">Длинные</option>
                    <option value="Длинные">Длинные</option>
                </select>
            </div>
            <div class="form-group">
                <label>Цвет волос</label>
                <select class="select">
                    <option value="Шатен">Шатен</option>
                    <option value="Шатен">Шатен</option>
                </select>
            </div>
            <div class="form-group">
                <label>Телосложение</label>
                <select class="select">
                    <option value="Стройное">Стройное</option>
                    <option value="Стройное">Стройное</option>
                </select>
            </div>
            <div class="form-group">
                <label>Внешность</label>
                <select class="select">
                    <option value="Европейская">Европейская</option>
                    <option value="Европейская">Европейская</option>
                </select>
            </div>
        </div>
    </div>
    <div class="profile-popup__btns">
        <a href="#" class="grey-btn close-popup">Отмена</a>
        <a href="#" class="orange-btn">Применить</a>
    </div>
</div>
