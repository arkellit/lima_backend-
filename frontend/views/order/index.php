<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 28.05.19
 * Time: 12:50
 */

use frontend\models\order\OrderAddress;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\web\View;
use yii\widgets\LinkPager;

?>
<section class="order">
    <div class="container">
        <div class="order_header">
            <div class="order_header-btns">
                <a href="<?= Url::to(['/order/index', 'is_complete_order' => 0]) ?>"
                   class="<?= Yii::$app->request->get('is_complete_order') == '0' || !Yii::$app->request->get() ? 'active' : '' ?>">АКТИВНЫЕ</a>
                <a href="<?= Url::to(['/order/index', 'is_complete_order' => 1]) ?>"
                   class="<?= Yii::$app->request->get('is_complete_order') == '1' ? 'active' : '' ?>"
                >АРХИВНЫЕ</a>
                <a href="<?= Url::to(['/order/index', 'is_published' => 0]) ?>"
                   class="<?= Yii::$app->request->get('is_published') == '0' ? 'active' : '' ?>"
                   style="width: 200px">НЕОПУБЛИКОВАННЫЕ</a>
            </div>
            <div class="order_header-btns">
                <a href="<?= Url::to(['/order', 'is_complete_order' => Yii::$app->request->get('is_complete_order')]) ?>" class="active">СПИСКОМ</a>
                <a href="<?= Url::to(['/order/maps', 'is_complete_order' => Yii::$app->request->get('is_complete_order')]) ?>">НА КАРТЕ</a>
            </div>
        </div>

        <div class="overflow-table">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
//                'showHeader'=> false,
                'id' => false,
                'tableOptions' => [
                    'class' => 'order_list'
                ],
                'layout' => '{items}<br>{pager}',
                'pager' => [
                    'options' => ['class' => 'pagination__list'],
                    'nextPageCssClass' => 'pagination__next',
                    'prevPageCssClass' => 'pagination__prev',
                    'activePageCssClass' => 'active',
                    'maxButtonCount' => 3,
                    'prevPageLabel' => '',
                    'nextPageLabel' => '',
                ],
                'columns' => [
                    [
                        'attribute' => 'number_order',
                        'label' => 'Номер заказа'
                    ],
                    [
                        'label' => 'Дата заказа',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDate($model->orderPlace->date_time_start,
                                'php: d.m.Y');
                        },
                    ],
                    [
                        'label' => 'Время заказа',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return '<span class="order_date_'.$model->id.'" >' . Yii::$app->formatter->asTime($model->orderPlace->date_time_start, 'php: H:i')
                                 . ' - ' . Yii::$app->formatter->asTime($model->orderPlace->date_time_end, 'php: H:i')
                                . '</span>';
                        },
                    ],
                    [
                        'attribute' => 'name_order',
                        'label' => 'Описание работы / задачи'
                    ],
                    [
                        'label' => 'Старт заказа',
                        'format' => 'raw',
                        'value' => function ($model) {
                            if ($model->is_pay == 1) {
                                $date_time_start = $model->orderPlace->date_time_start;
                                $date_time_end = $model->orderPlace->date_time_end;
                                $result = "
                                    <div class=\"order_time order_time_".$model->id."\">
                                        <svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 612 612\"><path d=\"M587.572 186.881c-32.266-75.225-87.096-129.934-162.949-162.285C386.711 8.427 346.992.168 305.497.168c-41.488 0-80.914 8.181-118.784 24.428-75.225 32.265-130.298 86.939-162.621 162.285C7.895 224.629 0 264.176 0 305.664c0 41.496 7.895 81.371 24.092 119.127 32.323 75.346 87.403 130.348 162.621 162.621 37.877 16.247 77.295 24.42 118.784 24.42 41.489 0 81.214-8.259 119.12-24.42C500.47 555.06 555.3 500.009 587.573 424.791 603.819 386.914 612 347.16 612 305.664c0-41.488-8.174-80.907-24.428-118.783zm-48.848 253.972c-24.021 41.195-56.929 73.876-98.375 98.039-41.195 24.021-86.332 36.135-134.845 36.135-36.47 0-71.27-7.024-104.4-21.415-33.129-14.384-61.733-33.294-85.661-57.215-23.928-23.928-42.973-52.811-57.214-85.997-14.199-33.065-21.08-68.258-21.08-104.735 0-48.52 11.921-93.428 35.807-134.509 23.971-41.231 56.886-73.947 98.039-98.04 41.146-24.092 85.99-36.142 134.502-36.142 48.52 0 93.649 12.121 134.845 36.142 41.446 24.164 74.283 56.879 98.375 98.039 24.092 41.153 36.135 85.99 36.135 134.509 0 48.521-11.964 93.735-36.128 135.189z\"/><path d=\"M324.906 302.988V129.659c0-10.372-9.037-18.738-19.41-18.738-9.701 0-18.403 8.366-18.403 18.738v176.005c0 .336.671 1.678.671 2.678-.671 6.024 1.007 11.043 5.019 15.062L392.836 423.45c6.695 6.695 19.073 6.695 25.763 0 7.694-7.695 7.188-18.86 0-26.099l-93.693-94.363z\"/></svg>
                                        <div class='date_order_start' data-order-time='$date_time_start' data-order_id='$model->id'></div>
                                        <div class='date_order_end' data-order-time='$date_time_end'></div> 
                                        <span class='order_to_start_order_".$model->id."'></span>
                                    </div>
                                   
                                ";
//                                $date1 = new DateTime(Yii::$app->formatter->asDatetime(time(), 'php:Y-m-d H:i:s'));
//                                if (!empty($model->orderPlace->date_time_start)) {
//                                    $date2 = new DateTime(Yii::$app->formatter->asDatetime($model->orderPlace->date_time_start,
//                                        'php:Y-m-d H:i:s'));
//                                    $diff = $date2->diff($date1);
//                                    $hours = $diff->h;
//                                    $hours = $hours + ($diff->days * 24);
//                                    $result = round($hours);
//                                }
//                                else {
//                                    $result = '<span style="color: #f6931d;">Ошибка вывода даты</span>';
//                                }
//
//                                if ($model->orderPlace->date_time_start < time()) {
//                                    $result = '';
//                                }
//                                else {
//                                    $result = $result <= 24 ? '<div class="order_time"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612"><path d="M587.572 186.881c-32.266-75.225-87.096-129.934-162.949-162.285C386.711 8.427 346.992.168 305.497.168c-41.488 0-80.914 8.181-118.784 24.428-75.225 32.265-130.298 86.939-162.621 162.285C7.895 224.629 0 264.176 0 305.664c0 41.496 7.895 81.371 24.092 119.127 32.323 75.346 87.403 130.348 162.621 162.621 37.877 16.247 77.295 24.42 118.784 24.42 41.489 0 81.214-8.259 119.12-24.42C500.47 555.06 555.3 500.009 587.573 424.791 603.819 386.914 612 347.16 612 305.664c0-41.488-8.174-80.907-24.428-118.783zm-48.848 253.972c-24.021 41.195-56.929 73.876-98.375 98.039-41.195 24.021-86.332 36.135-134.845 36.135-36.47 0-71.27-7.024-104.4-21.415-33.129-14.384-61.733-33.294-85.661-57.215-23.928-23.928-42.973-52.811-57.214-85.997-14.199-33.065-21.08-68.258-21.08-104.735 0-48.52 11.921-93.428 35.807-134.509 23.971-41.231 56.886-73.947 98.039-98.04 41.146-24.092 85.99-36.142 134.502-36.142 48.52 0 93.649 12.121 134.845 36.142 41.446 24.164 74.283 56.879 98.375 98.039 24.092 41.153 36.135 85.99 36.135 134.509 0 48.521-11.964 93.735-36.128 135.189z"/><path d="M324.906 302.988V129.659c0-10.372-9.037-18.738-19.41-18.738-9.701 0-18.403 8.366-18.403 18.738v176.005c0 .336.671 1.678.671 2.678-.671 6.024 1.007 11.043 5.019 15.062L392.836 423.45c6.695 6.695 19.073 6.695 25.763 0 7.694-7.695 7.188-18.86 0-26.099l-93.693-94.363z"/></svg>' .
//                                        '<span class="order_time_start order_id_' . $model->id . '" data-hour="'.$result.'" data-order_id="'.$model->id.'"> ' . $result  . ' ч' . '</span></div>' : '';
//                                }
                                return $result;
                            }
                            else {
                               return $result = '<span style="color: #f6931d;">Ожидает оплаты</span>';
                            }
                        },
                    ],
                    [
                        'label' => 'Действие',
                        'format' => 'raw',
                        'value' => function ($model) {
                           $is_published = Yii::$app->request->get('is_published') == '0' ? '' : 'none';
                          return '<div class="staff_table-more-btn">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><circle cx="256" cy="256" r="64"/><circle cx="256" cy="448" r="64"/><circle cx="256" cy="64" r="64"/></svg>
                            <ul>
                              <li><a style="display: none" href="'.Url::to(['/order/confirm-order', 'order_id' => $model->id]).'" class="open-popup">Редактировать</a></li>
                              <li><a style="display: '. $is_published.'" href="'.Url::to(['/order/confirm-order', 'id' => $model->id]).'">Публикация</a></li>
                              <li><a style="display: none" href="#">Удалить</a></li>
                              <li><a  href="'.Url::to(['/order-offer/offer', 'order_id' => $model->id]).'">Исполнители</a></li>
                            </ul>
                          </div>';
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <div class="order">
        <div class="container">
            <div class="order_list-button">
                <a href="<?= Url::to(['/order/create']) ?>" class="orange-btn"><span>+</span>Новый заказ</a>
            </div>
        </div>
    </div>
</section>

<?php

$script = <<< JS
time_zone = new Date().getTimezoneOffset() / 60;
$('.order_time').each(function(index) {
    obj_date_order_start = $(this).find('.date_order_start');
    obj_date_order_end = $(this).find('.date_order_end');
    order_id = $(this).find('.date_order_start').attr('data-order_id');
    if (obj_date_order_start && obj_date_order_end){
        date_start = obj_date_order_start.attr('data-order-time') - time_zone;
        date_end = obj_date_order_end.attr('data-order-time')  - time_zone;
        date_start_finish = (new Date(date_start * 1000).getHours() < 10 ? '0' + 
            new Date(date_start * 1000).getHours() : new Date(date_start * 1000).getHours())
            + ':' + (new Date(date_start * 1000).getMinutes()
            < 10 ? '0' + new Date(date_start * 1000).getMinutes() : new Date(date_start * 1000).getMinutes())
            + ' - ' + (new Date(date_end * 1000).getHours() < 10 ? '0'+ new Date(date_end * 1000).getHours() :
            new Date(date_end * 1000).getHours())
            + ':' + (new Date(date_end * 1000).getMinutes() < 10 ?
            '0' + new Date(date_end * 1000).getMinutes() : new Date(date_end * 1000).getMinutes());
        $('.order_date_' + order_id).text(date_start_finish);
        current_date = Math.round($.now() / 1000); 
        if (current_date > date_start) {
            $('.order_time_' + order_id).remove();
        }
        else if ((Math.round((date_start - current_date ) / 3600)) <= 24) {
            $('.order_to_start_order_' + order_id).text((Math.round((date_start - current_date ) / 3600)) + ' ч');
        }
        else {
            $('.order_time_' + order_id).remove();
        } 
    } 
});
JS;
$this->registerJs($script, yii\web\View::POS_READY)

?>


