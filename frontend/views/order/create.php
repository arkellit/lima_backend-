<?php

use unclead\multipleinput\MultipleInput;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use janisto\timepicker\TimePicker;
use yii\widgets\MaskedInput;

/*  @var $cities api\modules\v1\models\City  */
/*  @var $customer_branches api\modules\v1\models\customer\CustomerBranch */
/*  @var $model_order frontend\models\order\Order */
/*  @var $model_order_address api\modules\v1\models\order\OrderAddress */
/*  @var $order_print_data api\modules\v1\models\printing\OrderPrintData */
/*  @var $model_order_place api\modules\v1\models\order\OrderPlace */
/*  @var $model_order_start_end api\modules\v1\models\order\OrderStartEnd */
/*  @var $worker_price api\modules\v1\models\worker\CategoryWorkerCity */

$this->title = 'Создание заказа';

?>
<section class="order">
    <div class="container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="<?= Url::to(['/customer/index']) ?>">ЛИЧНЫЙ КАБИНЕТ</a></li>
                <li><a href="<?= Url::to(['/order']) ?>">Заказы</a></li>
                <li><a href="<?= Url::to(['/order/create']) ?>">Новый заказ</a></li>
            </ul>
        </div>
        <div class="row">
            <div class="col-xl-6 col-lg-7 offset-xl-3">
                <div class="order_new">
                    <h2 class="order_new-title">Новый заказ</h2>
                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'class' => 'order_new-form'
                        ]
                    ]);?>
<!--                    <form action="/" class="order_new-form">-->
                        <div class="order_new-block">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <?= $form->field($model_order_address, 'city_id')
                                            ->dropDownList(ArrayHelper::map($cities,'id','name'),
                                                ['class' => 'select']) ?>
<!--                                        <label for="on1">Город</label>-->
<!--                                          <input id="on1" type="text" class="form-control"> -->
<!--                                        <select class="select">-->
<!--                                            <option value="Москва">Москва</option>-->
<!--                                            <option value="Москва">Москва</option>-->
<!--                                        </select>-->
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <?= $form->field($model_order_address, 'branch_id')
                                            ->dropDownList(ArrayHelper::map($customer_branches,'id','branch'),
                                                ['prompt' => 'Нет'],
                                                ['class' => 'select']) ?>
<!--                                        <label for="on2">Филиал</label>-->
<!--                                         <input id="on2" type="text" class="form-control"> -->
<!--                                        <select class="select">-->
<!--                                            <option value="Нет">Нет</option>-->
<!--                                            <option value="Нет">Нет</option>-->
<!--                                        </select>-->
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom: 0;">
                                <label for="on3">Название заказа</label>
                                <?= $form->field($model_order, 'name_order')
                                    ->textInput()->label(false); ?>
<!--                                <input id="on3" type="text" class="form-control">-->
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="on5">Дата</label>
                                        <span class="input-has-icon calendar">
                                            <?php $date = Yii::$app->formatter->asDate(time(), 'php:d/m/Y'); ?>
                                            <?=

                                            $form->field($model_order_place,
                                                'begin_date')->widget(DatePicker::className(), [
                                                'name'  => 'begin_date',
                                                'id' => 'begin_date',
                                                'language' => 'ru',
                                                'value' => $date,
                                                'dateFormat' => 'dd/MM/yyyy',
                                                'options' => ['class' => 'form-control', 'autocomplete' => 'off']
                                            ], ['options' => ['value' => $date]])->label(false);
                                            ?>
<!--											<input id="on5" type="text" class="form-control" placeholder="12/03/2018">-->
										</span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="on6">Время начала</label>
                                        <span class="input-has-icon time">
                                           <?= $form->field($model_order_place,
                                               'begin_time')->widget(MaskedInput::className(),
                                               [
                                                   'mask' => '99:99',
                                                   'id' => 'begin_time',
                                                   'name' => 'begin_time',
                                                   'options' => [
                                                       'class' => 'form-control',
                                                   ],
                                                   'clientOptions' => [
                                                       'clearIncomplete' => true
                                                   ]
                                               ]
                                           )->label(false); ?>
<!--											<input id="on6" type="text" class="form-control" placeholder="12:00">-->
										</span>
                                        <?= $form->field($model_order_place,
                                            'order_price', [
                                                'template' => "{input}\n{label}"])->hiddenInput()->label(false)?>
                                        <?= $form->field($model_order_place,
                                            'count_hour', [
                                                'template' => "{input}\n{label}"])->hiddenInput()->label(false)?>
                                        <?= $form->field($model_order,
                                            'total_price', [
                                                'template' => "{input}\n{label}"])->hiddenInput()->label(false)?>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="on7">Время окончания</label>
                                        <span class="input-has-icon time">
                                            <?= $form->field($model_order_place,
                                                'end_time')->widget(MaskedInput::className(),
                                                [
                                                    'mask' => '99:99',
                                                    'id' => 'end_time',
                                                    'name' => 'end_time',
                                                    'options' => [
                                                        'class' => 'form-control',
                                                    ],
                                                    'clientOptions' => [
                                                        'clearIncomplete' => true
                                                    ]
                                                ]
                                            )->label(false); ?>
                                            <?= $form->field($model_order_place, 'time_zone')
                                            ->hiddenInput()->label(false);?>
<!--											<input id="on7" type="text" class="form-control" placeholder="12:00">-->
										</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="order_new-block">
                            <div class="row">
                                <?php
                                    $price = $worker_price['price'];
                                    $price_advanced = $worker_price['advance_price'];
                                    $price_worker_professions = [];
                                    foreach ($model_worker_professions as $item){
                                        $price_worker_professions[$item['category_worker_id'] . "||" .
                                        $item['price'] . "||" . $item['advance_price']] =
                                            $item->categoryWorker->name;
                                    }
                                ?>
                                <?= $form->field($model_order_place, 'worker_profession')->widget(MultipleInput::className(), [
                                    'max' => 10,
                                    'columns' => [
                                        [
                                            'name'  => 'type_worker',
                                            'type'  => 'dropDownList',
                                            'title' => 'Тип работника',
                                            'items' =>
                                                $price_worker_professions,
                                            'options' => [
                                                'id' => 'type_worker',
//                                                'prompt' => 'Выберите тип работника'
                                            ]
                                        ],
                                        [
                                            'name'  => 'count_worker',
                                            'type'  => 'textinput',
                                            'title' => 'Количество исполнителей',
                                            'defaultValue' => 1,
                                            'options' => [
                                                'id' => 'orderplace-count_worker',
                                                'class' => 'count_worker',
                                                'onblur' => 'checkCountWorker(this, this.value);',
                                                'onmouseout' => 'checkCountWorker(this, this.value);',
                                                'onchange' => 'checkCountWorker(this, this.value);',
                                            ],
                                        ],
                                        [
                                            'name'  => 'type_job',
                                            'type'  => 'dropDownList',
                                            'title' => 'Тип работы',
                                            'defaultValue' => 1,
                                            'items' => [
                                                1 => 'Работа в помещении',
                                                2 => 'Работа на улице'
                                            ],
                                            'options' => [
                                                'id' => 'orderplace-type_job',
//                                                'prompt' => 'Выберите тип работы'
                                            ]
                                        ],
                                    ]

                                ])->label(false) ?>

                            </div>
                        </div>
                        <div class="order_new-block">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="on8">Место выполнения заказа</label>
                                        <?= $form->field($model_order_address, 'place_order')
                                            ->textInput()->label(false); ?>
<!--                                        <input id="on8" type="text" class="form-control">-->
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="on9">Ближайшая станция метро</label>
                                        <!-- <input id="on9" type="text" class="form-control"> -->
                                        <?= $form->field($model_order_address, 'metro_station')
                                            ->textInput()->label(false); ?>
<!--                                        <select class="select">-->
<!--                                            <option value="Проспект Вернадского">Проспект Вернадского</option>-->
<!--                                            <option value="Проспект Вернадского">Проспект Вернадского</option>-->
<!--                                        </select>-->
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="on10">Адрес</label>
                                <?= $form->field($model_order_address, 'address_order')
                                    ->textInput(['onkeydown' => 'if(event.keyCode==13){return false;}'])
                                    ->label(false); ?>

                                <?= $form->field($model_order_address, 'lat', [
                                    'template' => "{input}\n{label}"])
                                    ->hiddenInput()
                                    ->label(false); ?>

                                <?= $form->field($model_order_address, 'lng', [
                                    'template' => "{input}\n{label}"])
                                    ->hiddenInput()
                                    ->label(false); ?>
<!--                                <input id="mapsearch_region" onkeydown="if(event.keyCode==13){return false;}" type="text" class="form-control">-->

                                <!--                                <input id="on10" type="text" class="form-control">-->
                            </div>
<!--                            <div id="orderMap" class="new-order__map"></div>-->
<!--                            <div id="orderMap" class="new-order__map">-->
<!--                                <span id="place-name"  class="title"></span>-->
<!---->
<!--                                <span id="place-address"></span>-->
<!--                            </div>-->
                            <div id="infowindow-content">
                                <span id="place-name"  class="title"></span>

                                <span id="place-address"></span>
                            </div>
                            <div id="maps" style="width: 100%; height: 235px;"></div>


                            <div class="form-group" style="margin-bottom: 0;">
                                <label for="on11">Ориентир на местности</label>
                                <?= $form->field($model_order_address, 'landmark')
                                    ->textarea(['class' => 'form-control small'])->label(false); ?>
<!--                                <textarea id="on11" class="form-control small"></textarea>-->
                            </div>
                        </div>
                        <div class="order_new-block">
                            <div class="form-group">
                                <!-- <input id="on12" type="text" class="form-control"> -->
                                <?= $form->field($model_order_place, 'personal_id')
                                    ->dropDownList(ArrayHelper::map($customer_supervisor,'id','last_name'),
                                        ['prompt' => 'Выберите супервайзера'],
                                        ['class' => 'select']) ?>
<!--                                <select class="select">-->
<!--                                    <option value="Петровский Н.Н.">Петровский Н.Н.</option>-->
<!--                                    <option value="Петровский Н.Н.">Петровский Н.Н.</option>-->
<!--                                </select>-->
                            </div>
                            <div class="form-group" style="margin-bottom: 0;">
                                <label for="on13">Комментарий к заказу</label>
                                <?= $form->field($model_order_place, 'comment')
                                    ->textarea(['class' => 'form-control small'])->label(false); ?>
                            </div>
                        </div>
                        <div class="checkbox">
                           
                            <input id="materialCheckbox" type="checkbox" name="is_print_flyers">
                            <label for="materialCheckbox">Напечатать раздаточный материал в LiMA</label>
                        </div>
                        <div class="order_hide">
                            <h2>Заказ на печать листовок</h2>
                            <div class="order_new-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php
//                                                var_dump(array_column($printing_template, 'category'));
                                            ?>


                                            <?php

                                                $price_print_edition = [];
                                                foreach ($print_edition as $item){
                                                    $price_print_edition[$item['id']] =  ['price' => $item['price']];
                                                }


                                                $price_print_data = [];
                                                foreach ($print_type as $item){
                                                    $price_print_data[$item['id']] =  ['price' => $item['price']];
                                                }

                                                $price_print_site = [];
                                                foreach ($print_size as $item){
                                                    $price_print_site[$item['id']] =  ['price' => $item['price']];
                                                }

                                                $price_print_paper = [];
                                                foreach ($print_paper_density as $item){
                                                    $price_print_paper[$item['id']] =  ['price' => $item['price']];
                                                }

                                                $price_print_time = [];
                                                foreach ($print_time as $item){
                                                    $price_print_time[$item['id']] =  ['price' => $item['price']];
                                                }


                                            ?>

                                            <?= $form->field($order_print_data, 'edition_id')
                                                ->dropDownList(ArrayHelper::map($print_edition,'id','name'),
                                                    [
                                                        'prompt' => 'Выберите тираж',
                                                        'options' =>
                                                            $price_print_edition
                                                    ],
                                                    ['class' => 'select'])
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?= $form->field($order_print_data, 'print_type_id')
                                                ->dropDownList(ArrayHelper::map($print_type,'id','name'),
                                                    [
                                                        'prompt' => 'Выберите тип печати',
                                                        'options' =>
                                                            $price_print_data
                                                    ],
                                                    ['class' => 'select'])
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?= $form->field($order_print_data, 'print_size_id')
                                                ->dropDownList(ArrayHelper::map($print_size,'id','name'),
                                                    [
                                                        'prompt' => 'Выберите размер листовок',
                                                        'options' =>
                                                            $price_print_site
                                                    ],
                                                    ['class' => 'select'])
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?= $form->field($order_print_data, 'paper_density_id')
                                                ->dropDownList(ArrayHelper::map($print_paper_density,'id','name'),
                                                    [
                                                        'prompt' => 'Выберите плотность бумаги',
                                                        'options' =>
                                                            $price_print_paper
                                                    ],
                                                    ['class' => 'select'])
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= $form->field($order_print_data, 'print_time_id')
                                        ->dropDownList(ArrayHelper::map($print_time,'id','name'),
                                            [
                                                'prompt' => 'Выберите время печати',
                                                'options' =>
                                                    $price_print_time
                                            ],
                                            ['class' => 'select'])
                                    ?>
                                </div>
<!--                                <div class="form-group">-->
<!--                                    <label>Название заказа</label>-->
<!--                                    <input type="text" class="form-control">-->
<!--                                </div>-->
                                <div class="form-group">
                                    <label>Логотип или макет</label>
                                    <?= $form->field($order_print_data, 'urls')->textarea([
                                            'placeholder' => 'Ссылка на файл для скачивания'])->label(false) ?>
<!--                                    <input type="text" class="form-control" placeholder="Ссылка на файл для скачивания">-->
                                </div>
                                <div class="form-group">
                                    <label>Заголовок листовки</label>
                                    <?= $form->field($order_print_data, 'flyer_title')->textInput()->label(false) ?>
<!--                                    <input type="text" class="form-control">-->
                                </div>
                                <div class="form-group">
                                    <label>Текст листовки</label>
                                    <?= $form->field($order_print_data, 'flyer_description')
                                        ->textInput()->label(false) ?>

                                    <!--                                    <input type="text" class="form-control">-->
                                </div>
                                <div class="form-group">
                                    <label>Комментарий</label>
                                    <?= $form->field($order_print_data, 'comment_order_print')
                                        ->textInput()->label(false) ?>
<!--                                    <textarea class="form-control small"></textarea>-->
                                </div>
                                <div class="form-group" style="margin-bottom: 0;">
                                    <label>Если нужен курьер укажите адрес</label>
                                    <?= $form->field($order_print_data, 'courier_address')
                                        ->textInput()->label(false) ?>

                                    <input type="hidden" id="courier_price"
                                           value="<?= $print_courier_price[0]['price'] ?>"
                                    data-id="<?= $print_courier_price[0]['id'] ?>">
                                    <?= $form->field($order_print_data, 'courier_price_id')
                                        ->hiddenInput()->label(false) ?>
                                    <?= $form->field($order_print_data, 'printing_price')
                                        ->hiddenInput()->label(false) ?>
                                </div>
                            </div>
                        </div>
                        <button class="orange-btn">Разместить заказ</button>
                    <?php ActiveForm::end();?>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="order__check">
                    <div class="order__check-single">
                        <p class="order__check-title">Стоимость заказа</p>
                        <p class="order__check-price order_new-price">0<span>₽</span></p>
                    </div>
                    <div class="order__check-single">
                        <p class="order__check-title">Стоимость печати</p>
                        <p class="order__check-price count_print">0₽</p>
                    </div>
                    <div class="order__check-single">
                        <p class="order__check-title">Курьер</p>
                        <p class="order__check-price sum_courier">0₽</p>
                    </div>
                    <div class="order__check-full">
                        <div class="order__check-single">
                            <p class="order__check-title">Итого:</p>
                            <p class="order__check-price order total_sum_order">0<span>₽</span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    /**
     * View error
     *
     * @param e
     * @param val
     */
    function checkCountWorker(e, val){
        if (Number(val) !== val && val % 1 !== 0){
            if (!$(e).next().hasClass('help-block')){
                $(e).after('<div style="color:#a94442" class="help-block">Неверное значение</div>');
                $(e).css('border-color', '#a94442');
            }
        }
        else if (val > 1000){
            if (!$(e).next().hasClass('help-block')){
                $(e).after('<div style="color:#a94442" class="help-block">Количество исполнителей слишком большое</div>');
                $(e).css('border-color', '#a94442');
            }
        }
        else {
            $(e).next().remove();
            $(e).css('border-color', '');
        }
    }
</script>
<?php
$script = <<<JS
    totalSum();
    $('#orderplace-count_worker').on('change mouseout blur', function() {
        totalSum();
    });
    
    $('#orderplace-type_job').on('change mouseout blur', function() {
        totalSum();
    });
    
    $('#orderplace-begin_time').on('change mouseout blur', function() {
        totalSum();
    });
    
    $('#orderplace-end_time').on('change mouseout blur', function() {
        totalSum();
    });
    
   
    
    // if($("#materialCheckbox").prop('checked') == true) { 
        $('#orderprintdata-edition_id').on('change mouseout blur', function() {
            totalSum();
        }); 
        $('#orderprintdata-print_type_id').on('change mouseout blur', function() {
            totalSum();
        }); 
        $('#orderprintdata-print_size_id').on('change mouseout blur', function() {
            totalSum();
        }); 
        $('#orderprintdata-paper_density_id').on('change mouseout blur', function() {
            totalSum();
        }); 
        $('#orderprintdata-print_time_id').on('change mouseout blur', function() {
            totalSum();
        });
        $('#orderprintdata-courier_address').on('change mouseout blur', function() {
            totalSum();
        });
        
        $('#type_worker').on('click change mouseout blur', function() {
            totalSum();
        });

        
    // }

    function totalSum() {
        $('#orderplace-time_zone').val(new Date().getTimezoneOffset());
        var first_val_count_worker = $('#orderplace-count_worker').val();  
        if ((Number(first_val_count_worker) !== first_val_count_worker && first_val_count_worker % 1 !== 0) 
            || Number(first_val_count_worker) > 1000){
            return false;  
        }
        var first_val_type_job = isNaN($('#orderplace-type_job option:selected').val()) ? 1 : $('#orderplace-type_job option:selected').val();
        // var total_count_1 = first_val_type_job.split('||');
        var order_profession = $('#type_worker option:selected').val() == '' ? '' + '||' + '' : $('#type_worker option:selected').val(); 
        var order_profession_split = order_profession.split('||');
        var order_profession_price = first_val_type_job == 1 ? order_profession_split[1] :
            order_profession_split[2];
        // alert(order_profession_price);
        var total_count = (first_val_count_worker < 1 ? 1 : first_val_count_worker) * order_profession_price;
        var hours = getHour(); 

        if($("#materialCheckbox").prop('checked') == true) { 
              
            var orderprintdata_edition_id = isNaN($('#orderprintdata-edition_id option:selected').attr('price')) ? 0 : $('#orderprintdata-edition_id option:selected').attr('price');
            var orderprintdata_print_type_id = isNaN($('#orderprintdata-print_type_id option:selected').attr('price')) ? 0 : $('#orderprintdata-print_type_id option:selected').attr('price');
            var orderprintdata_print_size_id = isNaN($('#orderprintdata-print_size_id option:selected').attr('price')) ? 0 : $('#orderprintdata-print_size_id option:selected').attr('price');
            var orderprintdata_paper_density_id = isNaN($('#orderprintdata-paper_density_id option:selected').attr('price')) ? 0 : $('#orderprintdata-paper_density_id option:selected').attr('price');
            var orderprintdata_print_time_id = isNaN($('#orderprintdata-print_time_id option:selected').attr('price')) ? 0 : $('#orderprintdata-print_time_id option:selected').attr('price');
            var orderprintdata_courier_address =  $('#courier_price').val();
            var totalPricePrint = Number(orderprintdata_edition_id) + Number(orderprintdata_print_type_id) +
            Number(orderprintdata_print_size_id) + Number(orderprintdata_paper_density_id) + 
            Number(orderprintdata_print_time_id);
            if ($('#orderprintdata-courier_address').val() != '') {
                // totalPricePrint = totalPricePrint + Number(orderprintdata_courier_address);
                $('.sum_courier').text(Number(orderprintdata_courier_address) + '₽');
                $('#orderprintdata-courier_price_id').val($('#courier_price').attr('data-id'));
               var totalCourier = Number(orderprintdata_courier_address);
            }
            else {
                $('#orderprintdata-courier_price_id').val('');
                $('.sum_courier').text('0₽');
                var totalCourier = 0;
            }
        }
        else {
            totalPricePrint = 0;
            var totalCourier = 0;
        }
        
        total_count = Number(total_count).toFixed(2); 
        totalPricePrint = Number(totalPricePrint).toFixed(2);
        totalCourier = Number(totalCourier).toFixed(2); 
        
        if (hours > 0){
            $('.order_new-price').text((total_count * hours).toFixed(2) + '₽'); 
            $('.count_print').text(Number(totalPricePrint) + '₽'); 
            $('.total_sum_order').text((total_count * hours + Number(totalPricePrint) + Number(totalCourier)).toFixed(2) + '₽');
            $('#orderplace-order_price').val((total_count * hours + Number(totalPricePrint) + Number(totalCourier)).toFixed(2));
            $('#order-total_price').val((total_count * hours + Number(totalPricePrint) + Number(totalCourier)).toFixed(2));
            $('#orderplace-count_hour').val(hours);
            $('#orderprintdata-printing_price').val(Number(totalPricePrint));
        } 
        else {
            $('.order_new-price').text(total_count + '₽');
            $('.count_print').text(Number(totalPricePrint) + '₽');
            $('.total_sum_order').text((Number(total_count) + Number(totalPricePrint)+ Number(totalCourier)).toFixed(2) + '₽');
            $('#orderplace-order_price').val(total_count + Number(totalPricePrint)+ Number(totalCourier));
            $('#order-total_price').val(total_count + Number(totalPricePrint)+ Number(totalCourier));
            
        }
    }
      
    function getHour() {
        begin_date = $('#orderplace-begin_date').val();
        var dateString = begin_date;
        var myDate = new Date(dateString); 
        var dateParts = dateString.split("/");
        var dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]); 
        var begin_date_new = dateObject.toString();
        var myDate = new Date(begin_date_new); 
        var myDate1 = new Date(begin_date_new); 
        begin_time = $('#orderplace-begin_time').val(); 
        end_time = $('#orderplace-end_time').val();
        
        var dateBeginTimeParts = begin_time.split(":");
        var dateEndTimeParts = end_time.split(":");  
        if (Number(dateBeginTimeParts[0]) > Number(dateEndTimeParts[0]) && end_time != ''){
            if (!$('#orderplace-begin_time').next().hasClass('help-block')){
                $('#orderplace-begin_time').after('<div style="color:#a94442" class="help-block">Время начала должно быть меньше время окончания</div>');
                $('#orderplace-begin_time').css('border-color', '#a94442');
            }
            return false;
        }
        else if (Number(dateBeginTimeParts[0]) == Number(dateEndTimeParts[0]) && 
             (Number(dateBeginTimeParts[1]) > Number(dateEndTimeParts[1]))){
            if (!$('#orderplace-begin_time').next().hasClass('help-block')){
                $('#orderplace-begin_time').after('<div style="color:#a94442" class="help-block">Время начала должно быть меньше время окончания</div>');
                $('#orderplace-begin_time').css('border-color', '#a94442');
            }
            return false;
        }
        else {
                $('#orderplace-begin_time').next().remove();
                $('#orderplace-begin_time').css('border-color', '');
            }
        
        var date_start = myDate;
        var date_end = myDate1;
        
        var dateSetHour = date_start.setHours(date_start.getHours() + Number(dateBeginTimeParts[0]));
        var dateSetMin = date_start.setMinutes(date_start.getMinutes() + Number(dateBeginTimeParts[1])); 
        
        var dateSetHour1 = date_end.setHours(date_end.getHours() + Number(dateEndTimeParts[0]));
        var dateSetMin1 = date_end.setMinutes(date_end.getMinutes() + Number(dateEndTimeParts[1]));
         
        var hours = (date_end - date_start) / 3600000;
        
        return Math.ceil(hours);   
    }
JS;
$this->registerJs($script);
?>

<script  type="text/javascript">
    function initMap() {
        var map = new google.maps.Map(document.getElementById('maps'),{
            center:{
                lat: 55.7522200,
                lng: 37.6155600
            },
            zoom: 10,
            disableDoubleClickZoom: true
        });

        var marker = new google.maps.Marker({
            map: map
        });

        // google.maps.event.addListener(map, 'click', function(event) {
        //     radius = new google.maps.Circle({map: map,
        //         radius: 100,
        //         center: event.latLng
        //     });
        //     var latlng = event.latLng;
        //     geocoder.geocode({'location': latlng}, function(results, status) {
        //         if (status === 'OK') {
        //             if (results[1]) {
        //                 marker.setPosition(latlng);
        //                 marker.setVisible(true);
        //
        //                 $('#orderaddress-lat').val(event.latLng.lat);
        //                 $('#orderaddress-lng').val(event.latLng.lng);
        //                 $('#orderaddress-address_order').val(results[1].formatted_address);
        //                 infowindow.setContent('<div><strong>' +  results[1].formatted_address + '</strong><br>');
        //                 infowindow.open(map, marker);
        //             } else {
        //                 window.alert('No results found');
        //             }
        //         } else {
        //             window.alert('Geocoder failed due to: ' + status);
        //         }
        //     });
        // });

        var input_region = document.getElementById('orderaddress-address_order');
        var autocomplete_region = new google.maps.places.Autocomplete(input_region);
        autocomplete_region.bindTo('bounds', map);
        var geocoder = new google.maps.Geocoder;
        var infowindow = new google.maps.InfoWindow();

        autocomplete_region.addListener('place_changed', function() {
            infowindow.close();
            var place = autocomplete_region.getPlace();

            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(10);  // Why 17? Because it looks good.
            }

            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
//            $('#dataform-location').val(place.geometry.location);
            $('#orderaddress-lat').val(place.geometry.location.lat);
            $('#orderaddress-lng').val(place.geometry.location.lng);
            infowindow.setContent('<div><strong>' + place.formatted_address + '</strong><br>');
            infowindow.open(map, marker);
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXvHPKUDIDp33st8QWTwfqUlvBkh0e59Q&libraries=places&callback=initMap&language=ru" type="text/javascript" async defer></script>


<style>
    .bootstrap-timepicker-widget.dropdown-menu.open{
        width: 240px;
    }
</style>