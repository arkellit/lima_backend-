<?php
$this->title = 'Подтверждение заказа ' . $order->name_order;

use api\modules\v1\models\order\Order;
use frontend\widgets\MakeDepositWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<section class="order">
    <div class="container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="<?= Url::to(['/customer/index']) ?>">ЛИЧНЫЙ КАБИНЕТ</a></li>
                <li><a href="<?= Url::to(['/order']) ?>">Заказы</a></li>
                <li><a href="<?= Url::to(['/order/create']) ?>">Новый заказ</a></li>
            </ul>
        </div>
        <div class="row">
            <div class="col-lg-7 offset-lg-2">
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'class' => 'order_new-form'
                    ]
                ]);?>
                    <?= $form->field($order, 'id')->hiddenInput()->label(false); ?>
                    <div class="order_new">
                        <h2 class="order_new-title">Новый заказ</h2>
                        <div class="order-new__info">
                            <div class="order-new__info-single">
                                <p class="order-new__info-title">Город</p>
                                <div class="order-new__info-text">
                                    <p><?= $order_address->city->name ?></p>
                                </div>
                            </div>
                            <div class="order-new__info-single">
                                <p class="order-new__info-title">Филиал</p>
                                <div class="order-new__info-text">
                                    <p><?= $order_address->branch_id == 0 ? 'Нет' :
                                            $order_address->branch->branch ?></p>
                                </div>
                            </div>
                            <div class="order-new__info-single">
                                <p class="order-new__info-title">Название</p>
                                <div class="order-new__info-text">
                                    <p><?= $order->name_order ?></p>
                                </div>
                            </div>
                            <div class="order-new__info-single">
                                <p class="order-new__info-title">Исполнителей</p>
                                <div class="order-new__info-text">
                                    <p><?= $order_place->count_worker ?></p>
<!--                                    <a href="#">Фильтр</a>-->
                                </div>
                            </div>
                            <div class="order-new__info-single">
                                <p class="order-new__info-title">Дата</p>
                                <div class="order-new__info-text">
                                    <p><?= Yii::$app->formatter->asDate($order_place->date_time_start,
                                            'php: d.m.Y') ?></p>
                                </div>
                            </div>
                            <div class="order-new__info-single">
                                <p class="order-new__info-title">Время</p>
                                <div class="order-new__info-text">
                                    <p class="order_time"
                                       data-time-start="<?= Yii::$app->formatter->asTime($order_place->date_time_start,
                                        'php: H:i') ?>"
                                        data-time-end="<?= Yii::$app->formatter->asTime($order_place->date_time_end,
                                            'php: H:i') ?>">
                                        <?= Yii::$app->formatter->asTime($order_place->date_time_start,
                                            'php: H:i') ?> -
                                        <?= Yii::$app->formatter->asTime($order_place->date_time_end,
                                            'php: H:i') ?></p>
                                </div>
                            </div>
                            <div class="order-new__info-single">
                                <p class="order-new__info-title">Место проведения</p>
                                <div class="order-new__info-text">
                                    <p><?= $order_address->landmark ?></p>
                                </div>
                            </div>
                            <div class="order-new__info-single">
                                <p class="order-new__info-title">Адрес</p>
                                <div class="order-new__info-text">
                                    <p><?= $order_address->address_order ?></p>
                                </div>
                            </div>
                            <div class="order-new__info-single">
                                <p class="order-new__info-title">Метро</p>
                                <div class="order-new__info-text">
                                    <p><?= $order_address->metro_station ?></p>
                                </div>
                            </div>
                            <div class="order-new__info-single">
                                <p class="order-new__info-title">Ориентир</p>
                                <div class="order-new__info-text">
                                    <p><?= $order_address->place_order ?></p>
                                </div>
                            </div>
                            <div class="order-new__info-single">
                                <p class="order-new__info-title">Супервайзер</p>
                                <div class="order-new__info-text">
                                    <p><?= $order->administrator_id == 0 ?
                                            $order->customer->name : $order->administrator->name ?></p>
                                </div>
                            </div>
                            <div class="order-new__info-single">
                                <p class="order-new__info-title">Комментарий</p>
                                <div class="order-new__info-text">
                                    <p><?= $order_place->comment ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="checkbox">
                            <input id="materialCheckbox" type="checkbox" name="is_print_flyers">
                            <label for="materialCheckbox">Напечатать раздаточный материал в LiMA</label>
                        </div>
                        <div class="order_hide">
                            <h2>Заказ на печать листовок</h2>
                            <div class="order_new-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php
                                            //                                                var_dump(array_column($printing_template, 'category'));
                                            ?>


                                            <?php

                                            $price_print_edition = [];
                                            foreach ($print_edition as $item){
                                                $price_print_edition[$item['id']] =  ['price' => $item['price']];
                                            }


                                            $price_print_data = [];
                                            foreach ($print_type as $item){
                                                $price_print_data[$item['id']] =  ['price' => $item['price']];
                                            }

                                            $price_print_site = [];
                                            foreach ($print_size as $item){
                                                $price_print_site[$item['id']] =  ['price' => $item['price']];
                                            }

                                            $price_print_paper = [];
                                            foreach ($print_paper_density as $item){
                                                $price_print_paper[$item['id']] =  ['price' => $item['price']];
                                            }

                                            $price_print_time = [];
                                            foreach ($print_time as $item){
                                                $price_print_time[$item['id']] =  ['price' => $item['price']];
                                            }


                                            ?>

                                            <?= $form->field($order_print_data, 'edition_id')
                                                ->dropDownList(ArrayHelper::map($print_edition,'id','name'),
                                                    [
                                                        'prompt' => 'Выберите тираж',
                                                        'options' =>
                                                            $price_print_edition
                                                    ],
                                                    ['class' => 'select'])
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?= $form->field($order_print_data, 'print_type_id')
                                                ->dropDownList(ArrayHelper::map($print_type,'id','name'),
                                                    [
                                                        'prompt' => 'Выберите тип печати',
                                                        'options' =>
                                                            $price_print_data
                                                    ],
                                                    ['class' => 'select'])
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?= $form->field($order_print_data, 'print_size_id')
                                                ->dropDownList(ArrayHelper::map($print_size,'id','name'),
                                                    [
                                                        'prompt' => 'Выберите размер листовок',
                                                        'options' =>
                                                            $price_print_site
                                                    ],
                                                    ['class' => 'select'])
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?= $form->field($order_print_data, 'paper_density_id')
                                                ->dropDownList(ArrayHelper::map($print_paper_density,'id','name'),
                                                    [
                                                        'prompt' => 'Выберите плотность бумаги',
                                                        'options' =>
                                                            $price_print_paper
                                                    ],
                                                    ['class' => 'select'])
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= $form->field($order_print_data, 'print_time_id')
                                        ->dropDownList(ArrayHelper::map($print_time,'id','name'),
                                            [
                                                'prompt' => 'Выберите время печати',
                                                'options' =>
                                                    $price_print_time
                                            ],
                                            ['class' => 'select'])
                                    ?>
                                </div>
                                <!--                                <div class="form-group">-->
                                <!--                                    <label>Название заказа</label>-->
                                <!--                                    <input type="text" class="form-control">-->
                                <!--                                </div>-->
                                <div class="form-group">
                                    <label>Логотип или макет</label>
                                    <?= $form->field($order_print_data, 'urls')->textarea([
                                        'placeholder' => 'Ссылка на файл для скачивания'])->label(false) ?>
                                    <!--                                    <input type="text" class="form-control" placeholder="Ссылка на файл для скачивания">-->
                                </div>
                                <div class="form-group">
                                    <label>Заголовок листовки</label>
                                    <?= $form->field($order_print_data, 'flyer_title')->textInput()->label(false) ?>
                                    <!--                                    <input type="text" class="form-control">-->
                                </div>
                                <div class="form-group">
                                    <label>Текст листовки</label>
                                    <?= $form->field($order_print_data, 'flyer_description')
                                        ->textInput()->label(false) ?>

                                    <!--                                    <input type="text" class="form-control">-->
                                </div>
                                <div class="form-group">
                                    <label>Комментарий</label>
                                    <?= $form->field($order_print_data, 'comment_order_print')
                                        ->textInput()->label(false) ?>
                                    <!--                                    <textarea class="form-control small"></textarea>-->
                                </div>
                                <div class="form-group" style="margin-bottom: 0;">
                                    <label>Если нужен курьер укажите адрес</label>
                                    <?= $form->field($order_print_data, 'courier_address')
                                        ->textInput()->label(false) ?>

                                    <input type="hidden" id="courier_price"
                                           value="<?= $print_courier_price[0]['price'] ?>"
                                           data-id="<?= $print_courier_price[0]['id'] ?>">
                                    <?= $form->field($order_print_data, 'courier_price_id')
                                        ->hiddenInput()->label(false) ?>
                                    <?= $form->field($order_print_data, 'printing_price')
                                        ->hiddenInput()->label(false) ?>
                                </div>
                            </div>
                        </div>
                        <?php if($order->is_pay == \frontend\models\order\Order::NO_PAY):?>
                            <div class="order-new__error">
                                <p>НЕДОСТАТОЧНО СРЕДСТВ</p>
                                <p>На депозите <?= $order_company_data->deposit ?>₽. Необходима доплата
                                    <?= $order->total_price - $order_company_data->deposit ?>₽ </p>
                            </div>
                            <div class="order-new__btns">
                                <a href="<?= Url::to(Yii::$app->request->referrer) ?>" class="back-btn">Назад</a>
                                <a href="#add-deposit-popup" class="orange-btn open-popup">Оплатить <?= $order->total_price - $order_company_data->deposit ?>₽</a>
                            </div>

                        <?php else: ?>
                            <div class="order-new__btns">
                                <a href="<?= Url::to(Yii::$app->request->referrer) ?>" class="back-btn">Назад</a>
                                <button type="submit" class="orange-btn">Разместить заказ</button>
                            </div>
                        <?php endif;?>
                    </div>
                <?php ActiveForm::end();?>
            </div>
            <div class="col-lg-3">
                <div class="order__check">
                    <div class="order__check-single">
                        <p class="order__check-title">Стоимость заказа</p>
                        <p class="order__check-price total_price"><?= $order->total_price ?><span>₽</span></p>
                    </div>
                    <div class="order__check-full">
                        <div class="order__check-single">
                            <p class="order__check-title">Итого:</p>
                            <p class="order__check-price total_sum_order"><?= $order->total_price +
                                ($order_print_data == null ? 0 : $order_print_data->printing_price) ?><span>₽</span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    <div id="add-deposit-popup" class="zoom-anim-dialog mfp-hide popup add-deposit-popup">
        <h2>Пополнить депозит</h2>
        <?php $form = ActiveForm::begin([
            'action' => Url::to('/customer/make-deposit'),
            'validateOnBlur' => false,
        ]); ?>
        <div class="form-group">
            <label for="makedepositform-sum">Пополнить депозит на сумму</label>
            <?= $form->field($model_make_deposit, 'sum', [
                'template' => "{input}\n{label}", 'options' =>
                    ['style' => 'margin-top:8px']])->textInput()->label(false) ?>
            <!--            <input id="ad1" type="text" class="form-control">-->
            <input type="hidden" name="order_deposit" value="true">
            <input type="hidden" name="order_deposit_id" value="<?= $order->id ?>">
            <p class="form-group-help">Будет сформирован счет на оплату указанной суммы</p>
        </div>
        <div class="add-deposit-popup-buttons">
            <button type="button" class="grey-btn">Отмена</button>
            <button type="submit" class="orange-btn">Пополнить</button>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
<?php if (Yii::$app->session->has('success_make_deposit')):?>
    <div id="info-popup1" class="zoom-anim-dialog mfp-hide popup info-popup">
        <h2>Счет на оплату отправлен</h2>
        <div class="info-popup-text">Счет на оплату
            <?= Yii::$app->session->get('success_make_deposit') ?>₽ выслан на
            e-mail: <?= $customer->companyData->email ?>
            После поступления денежных средств депозит будет пополнен</div>
    </div>
    <?php
    $script_open_popup = <<<JS
$.magnificPopup.open({
              items: {
                src: '#info-popup1'
              },
              type: 'inline'
});
JS;
    $this->registerJs($script_open_popup);
    Yii::$app->session->remove('success_make_deposit') ?>
<?php endif;?>
<?php
$script = <<<JS
    var total_price_order = "$order->total_price";
    $('#orderprintdata-edition_id').on('change mouseout blur', function() {
        totalSum();
    }); 
    $('#orderprintdata-print_type_id').on('change mouseout blur', function() {
        totalSum();
    }); 
    $('#orderprintdata-print_size_id').on('change mouseout blur', function() {
        totalSum();
    }); 
    $('#orderprintdata-paper_density_id').on('change mouseout blur', function() {
        totalSum();
    }); 
    $('#orderprintdata-print_time_id').on('change mouseout blur', function() {
        totalSum();
    });
    $('#orderprintdata-courier_address').on('change mouseout blur', function() {
        totalSum();
    });
    
    $('#type_worker').on('click change mouseout blur', function() {
        totalSum();
    });
    
    function totalSum() {
        if($("#materialCheckbox").prop('checked') == true) {
            var orderprintdata_edition_id = isNaN($('#orderprintdata-edition_id option:selected').attr('price')) ? 0 : $('#orderprintdata-edition_id option:selected').attr('price');
            var orderprintdata_print_type_id = isNaN($('#orderprintdata-print_type_id option:selected').attr('price')) ? 0 : $('#orderprintdata-print_type_id option:selected').attr('price');
            var orderprintdata_print_size_id = isNaN($('#orderprintdata-print_size_id option:selected').attr('price')) ? 0 : $('#orderprintdata-print_size_id option:selected').attr('price');
            var orderprintdata_paper_density_id = isNaN($('#orderprintdata-paper_density_id option:selected').attr('price')) ? 0 : $('#orderprintdata-paper_density_id option:selected').attr('price');
            var orderprintdata_print_time_id = isNaN($('#orderprintdata-print_time_id option:selected').attr('price')) ? 0 : $('#orderprintdata-print_time_id option:selected').attr('price');
            var orderprintdata_courier_address =  $('#courier_price').val();
            var totalPricePrint = Number(orderprintdata_edition_id) + Number(orderprintdata_print_type_id) +
            Number(orderprintdata_print_size_id) + Number(orderprintdata_paper_density_id) + 
            Number(orderprintdata_print_time_id);
            if ($('#orderprintdata-courier_address').val() != '') {
                // totalPricePrint = totalPricePrint + Number(orderprintdata_courier_address);
                $('.sum_courier').text(Number(orderprintdata_courier_address) + '₽');
                var totalCourier = Number(orderprintdata_courier_address);
            }
            else {
                var totalCourier = 0;
            }
        }
        else {
            totalPricePrint = 0;
            var totalCourier = 0;
        }
        $('.total_sum_order').text(Number(total_price_order) + Number(totalPricePrint) + 
        Number(totalCourier) + '₽');
    }
    
    var time_zone = new Date().getTimezoneOffset() / 60;
     var time_start = $('.order_time').attr('data-time-start');
    var time_end = $('.order_time').attr('data-time-end');
    var new_time_start = new Date();
    new_time_start.setHours(time_start.split(':')[0] - time_zone);
    new_time_start.setMinutes(time_start.split(':')[1]);
    
    var new_time_end = new Date();
    new_time_end.setHours(time_end.split(':')[0] - time_zone);
    new_time_end.setMinutes(time_end.split(':')[1]);
    
    $('.order_time').text(addZero(new_time_start.getHours()) + ':' + addZero(new_time_start.getMinutes()) + ' - '
        + addZero(new_time_end.getHours()) + ':' + addZero(new_time_end.getMinutes())
    ); 
    
    function addZero(i) {
      if (i < 10) {
        i = "0" + i;
      }
      return i;
    }
JS;
$this->registerJs($script);
?>