<?php

use api\modules\v1\models\order\OrderRequest;
use frontend\models\order\OrderAddress;
use yii\helpers\Url; ?>
<section class="order order-map">
    <div id="map"></div>
    <div class="container">
        <div class="order_header">
            <div class="order_header-btns">
                <a href="<?= Url::to(['/order', 'is_complete_order' => Yii::$app->request->get('is_complete_order')]) ?>">СПИСКОМ</a>
                <a href="<?= Url::to(['/order/maps', 'is_complete_order' => Yii::$app->request->get('is_complete_order')]) ?>" class="active">НА КАРТЕ</a>
            </div>
        </div>
    </div>
</section>
<div class="order">
    <div class="container">
        <div class="order_list-button">
            <a href="<?= Url::to(['/order/create']) ?>" class="orange-btn"><span>+</span>Новый заказ</a>
        </div>
    </div>
</div>
 <script>
    // var map;
    // function initMap() {
    //     map = new google.maps.Map(document.getElementById('map'), {
    //         center: {lat: 55.755788, lng: 37.614764},
    //         zoom: 12,
    //         scrollwheel: false,
    //     });
    //     var marker = new google.maps.Marker({
    //         position: {lat: 55.755788, lng: 37.614764},
    //         map: map,
    //         icon: '/img/place-marker.png',
    //     });
    //     var contentString = '<div class="info-content">'+
    //         '<h1 class="info-content-title">Заказ <span>№ 77-21345</span></h1>'+
    //         '<div class="info-content-text">'+
    //         '<div class="info-content-text-single">' +
    //         '<p>Акция «Каньон Эльдорадо»<br>Филиал: Юго-Запад<br>Исполнителей: 10<br>01.12.2018 12:00-16-0</p>'+
    //         '</div>'+
    //         '<div class="info-content-text-single">' +
    //         '<p>Место: ТЦ «Весна»<br>Проспект Вернадского, 17<br>метро: проспект Вернадского<br>Супервайзер: Петровский А.Н</p>'+
    //         '</div>'+
    //         '</div>'+
    //         '<div class="info-content-count">'+
    //         '<div class="info-content-count-all">'+
    //         '<p>7</p><span>из 10</span>'+
    //         '</div>'+
    //         '<p class="info-content-count-text">Исполнителей набрано</p>'+
    //         '</div>'+
    //         '</div>';
    //     var infowindow = new google.maps.InfoWindow({
    //         content: contentString,
    //     });
    //     marker.addListener('click', function() {
    //         infowindow2.close();
    //         infowindow.open(map, marker);
    //     });
    //     var marker2 = new google.maps.Marker({
    //         position: {lat: 55.753276, lng: 37.583694},
    //         map: map,
    //         icon: '/img/place-marker.png',
    //     });
    //     var contentString2 = '<div class="info-content info-content-column">'+
    //         '<h1 class="info-content-title">Заказ <span>№ 77-21342</span></h1>'+
    //         '<div class="info-content-text">'+
    //         '<div class="info-content-text-single">' +
    //         '<p>Акция «Каньон Эльдорадо»<br>Филиал: Юго-Запад<br>Исполнителей: 10<br>01.12.2018 12:00-16-0</p>'+
    //         '</div>'+
    //         '<div class="info-content-text-single">' +
    //         '<p>Место: ТЦ «Весна»<br>Проспект Вернадского, 17<br>метро: проспект Вернадского<br>Супервайзер: Петровский А.Н</p>'+
    //         '</div>'+
    //         '</div>'+
    //         '<div class="info-content-column-count">'+
    //         '<p>Исполнителей <span>7</span> из 10</p>'+
    //         '</div>'+
    //         '</div>';
    //     var infowindow2 = new google.maps.InfoWindow({
    //         content: contentString2,
    //     });
    //     marker2.addListener('click', function() {
    //         infowindow.close();
    //         infowindow2.open(map, marker2);
    //     });
    // }
    var map;
    var infowindow;
    var locations = [
        <?php foreach ($dataProvider as $item): ?>
            <?php $address = OrderAddress::findOne($item->orderPlace->order_address_id) ?>
            {lat: <?= $address->lat ?>, lng: <?= $address->lng ?>},
        <?php endforeach;?>
    ];
    var contents = [
        <?php foreach ($dataProvider as $item):?>
        <?php $address = OrderAddress::findOne($item->orderPlace->order_address_id) ?>
        <?php $order_count_worker = OrderRequest::find()->where(['AND', ['order_id' => $item->id],
        ['status' => OrderRequest::STATUS_ACCEPT]])->count(); ?>
            '<div class="info-content">'+
            '<h1 class="info-content-title">Заказ <span>№ <?= $item->number_order ?></span></h1>'+
            '<div class="info-content-text">'+
            '<div class="info-content-text-single">' +
            '<p><?= $item->name_order ?><br>Филиал: <?= $address->branch_id == 0 ? "Нет" : $address->branch->branch ?><br>' +
            'Исполнителей: <?= $item->orderPlace->count_worker ?>' +
            '<br><?= Yii::$app->formatter->asDate($item->orderPlace->date_time_start,"php: d.m.Y") ?>-<?= Yii::$app->formatter->asDate($item->orderPlace->date_time_end,"php: d.m.Y") ?></p>'+
            '</div>'+
            '<div class="info-content-text-single">' +
            '<p>Место: <?= $address->place_order ?>, <br><?= $address->address_order ?><br>метро: <?= $address->metro_station ?>'+
            '<br>Супервайзер: <?= $item->administrator_id == 0 ? $item->customer->name : $item->administrator->name ?></p>'+
            '</div>'+
            '</div>'+
            '<div class="info-content-count">'+
            '<div class="info-content-count-all">'+
            '<p><?= $order_count_worker ?></p><span>из <?= $item->orderPlace->count_worker ?></span>'+
            '</div>'+
            '<p class="info-content-count-text">Исполнителей набрано</p>'+
            '</div>'+
            '</div>',
        <?php endforeach;?>
    ];

    function initMap() {
        var latlng = {lat: 55.755788, lng: 37.614764};
        var mapOptions = {
            center: latlng,
            zoom: 12
        };
        map = new google.maps.Map(document.getElementById('map'), mapOptions);
        infowindow = new google.maps.InfoWindow();
        for (var i = 0; i < locations.length; i++) {
            createMarker(locations[i], contents[i]);
        }
    }

    function createMarker(location, content) {
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            icon: '/img/place-marker.png',
        });
        marker.addListener('click', function() {
            infowindow.setContent(content);
            infowindow.open(map, this);
        });
    }
    initMap();
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXvHPKUDIDp33st8QWTwfqUlvBkh0e59Q&libraries=places&callback=initMap&language=ru" type="text/javascript" async defer></script>
