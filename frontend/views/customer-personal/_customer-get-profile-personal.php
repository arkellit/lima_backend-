<?php


?>
<script>
    $.magnificPopup.open({
        items: {
            src: '#change-staff-popup'
        },
        type: 'inline'
    });
    $('.close-popup').click(function(e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
</script>
<div id="change-staff-popup" class="zoom-anim-dialog popup add-staff-popup">
    <h2>Изменить данные сотрудника</h2>
    <?= $this->render('@frontend/widgets/views/_new-personal_form', [
        'model_new_personal' => $model_new_personal,
        'branches' => $branches,
        'is_edit' => $is_edit
    ]) ?>
</div>
