<?php

use frontend\models\customer\CustomerPersonal;
use frontend\widgets\NewPersonalEditWidget;
use frontend\widgets\NewPersonalWidget;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'Персонал';
?>

<section class="staff">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="staff_branches">
                    <p>Филиалы</p>
                    <ul class="staff_branches-list">
                        <?php foreach ($model_branch as $item):?>
                            <?php if (Yii::$app->request->get('branch_id')):?>
                                <li class="<?= Yii::$app->request->get('branch_id') == $item->id ? 'active' : '' ?>">
                            <?php else:?>
                                <li class="<?= $model_branch[0]['id'] == $item->id ? 'active' : '' ?>">
                            <?php endif;?>
                                <a href="<?= Url::to(['/customer-personal/staff', 'branch_id' => $item->id]) ?>"><?= $item->branch ?></a>
                                <div class="staff_table-more-btn">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><circle cx="256" cy="256" r="64"/><circle cx="256" cy="448" r="64"/><circle cx="256" cy="64" r="64"/></svg>
                                    <ul>
<!--                                        <li><a href="#">Редактировать</a></li>-->
                                        <li>
                                            <?=
//                                            Html::a('Удалить', ['delete-branch', 'id' => $item->id], [
//                                                'data' => [
//                                                    'confirm' => 'Действительно хотите удалить?',
//                                                    'method' => 'post',
//                                                ],
//                                            ])
                                            $dataProvider->totalCount == 0 ? Html::a('Удалить', ['delete-branch', 'id' => $item->id], [
                                                'data' => [
                                                    'confirm' => 'Действительно хотите удалить?',
                                                    'method' => 'post',
                                                ],
                                            ]) : Html::a('Удалить', '#', [
                                                'onclick' => 'alert("Вы не можете удалить филиал с сотрудниками. Переместите либо удалите сотрудников")'])

                                            ?>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        <?php endforeach;?>
                    </ul>
                    <?php if (array_key_exists('customer',
                        Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId()))):?>
                        <a href="#add-brahch-popup"  class="open-popup staff_branches-add">+ Добавить филиал</a>
                    <?php endif;?>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="overflow-table">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'showHeader'=> false,
                        'id' => false,
                        'tableOptions' => [
                            'class' => 'staff_table'
                        ],
                        'layout' => '{items}<br>{pager}',
                        'pager' => [
                            'options' => ['class' => 'pagination__list'],
                            'nextPageCssClass' => 'pagination__next',
                            'prevPageCssClass' => 'pagination__prev',
                            'activePageCssClass' => 'active',
                            'maxButtonCount' => 3,
                            'prevPageLabel' => '',
                            'nextPageLabel' => '',
                        ],
                        'columns' => [
                            'id',
                            [
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return $model->role == 'supervisor' ? 'Супервайзер' : 'Администратор';
                                },
                            ],
                            [
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return $model->user->phone;
                                },
                            ],
                            [
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return $model->last_name . ' ' . $model->name . ' ' . $model->middle_name;
                                },
                            ],
                            [
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return '<div class="staff_table-more-btn">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><circle cx="256" cy="256" r="64"/><circle cx="256" cy="448" r="64"/><circle cx="256" cy="64" r="64"/></svg>
                            <ul>
                              <li><a href="#change-staff-popup" class="open-popup open-popup_change_form" data-id="' . $model->id . '">Редактировать</a></li>
                              <li>
                                    ' . Html::a('Удалить', ['delete-personal', 'id' => $model->id], [
                                            'data' => [
                                                'confirm' => 'Действительно хотите удалить?',
                                                'method' => 'post',
                                            ],
                                        ]) .' </li>
                            </ul>
                          </div>';
                                }
                            ],
                        ],
                    ]); ?>
                </div>
                <div class="staff_button">
                    <?php if (!empty($model_branch)): ?>
                        <a href="#add-staff-popup" class="orange-btn open-popup"><span>+</span>Добавить персонал</a>
                    <?php else: ?>
                        <a href="#" onclick="alert('Добавьте филиал');" class="orange-btn"><span>+</span>Добавить персонал</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="add-brahch-popup" class="zoom-anim-dialog mfp-hide popup add-brahch-popup">
    <h2>Создать новый филиал</h2>
        <?php $form = ActiveForm::begin([
            'action' => Url::to('/customer-personal/add-branch'),
            'validateOnBlur' => false,
        ]); ?>
            <div class="form-group">
                <label for="ab1">Филиал</label>
                <?= $form->field($new_branch, 'branch')
                    ->textInput(['placeholder' => 'Название филиала',
                        'onkeydown' => "return event.key != 'Enter';"])
                    ->label(false) ?>
            </div>
            <div class="add-brahch-popup-buttons">
                <button class="grey-btn close-popup">Отмена</button>
                <button type="submit" class="orange-btn">Создать</button>
            </div>
        <?php ActiveForm::end(); ?>
</div>

<?= NewPersonalWidget::widget() ?>
<?//= NewPersonalEditWidget::widget() ?>
<div class="open_change_personal"></div>
<?php
$script_open_popup = <<<JS
$('.open-popup_change_form').click(function() {
    var id = $(this).attr('data-id'); 
    $.get('/customer-personal/get-profile-personal', {"id":id}, function(response) {
        if (response != false) {
            $('.open_change_personal').empty();
            $('.open_change_personal').append(response);
        }
        else alert('Неверный запрос');
    });
});

JS;
$this->registerJs($script_open_popup);
?>
