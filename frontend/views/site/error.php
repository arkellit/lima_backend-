<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<!--<div class="site-error">-->

    <div class="container">


        <div class="row">
            <div class="col-md-4" style="margin-top: 200px">
                <h1><?= Html::encode($this->title) ?></h1>

                <div class="alert alert-danger">
                    <?= nl2br(Html::encode($message)) ?>
                </div>
            </div>
            <div class="col-md-8">
                <?= Html::img('/img/593.jpg', ['style' => 'width:100%;height:100%']) ?>
            </div>
        </div>
    </div>
<!--</div>-->
