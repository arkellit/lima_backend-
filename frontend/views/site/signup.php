<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

$this->title = 'Регистрация';
?>
<section class="login">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h2 class="login_title">Регистрация для заказчиков</h2>
                <?php $form = ActiveForm::begin([
                    'id' => 'form-signup',
                    'validateOnBlur' => false,
                ]); ?>

                <?= $form->field($model, 'phone')->widget(MaskedInput::className(), [
                    'mask' => '89999999999',
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => ('Введите номер телефона')
                    ],
                    'clientOptions' => [
                        'clearIncomplete' => true
                    ]

                ])?>
                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'repeat_password')->passwordInput() ?>

                <div class="checkbox-signup">
                    <?= $form->field($model, 'is_confirm')->checkbox([
                            'class' => 'checkbox',
                            'label' => 'Я согласен с условиями 
                            <a target="_blank" href="'.Url::to(['/conditions']).'">' .
                                'Пользовательского соглашения' . '</a>' . ' и ' .
                     '<a target="_blank" href="'.Url::to(['/offer']).'">' .
                                'Договора оферты' . '</a>']); ?>

                </div>
                <button type="submit" class="orange-btn">Зарегистрироваться</button>


            <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>
<?= \frontend\widgets\ConfirmCodeWidget::widget() ?>