<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \api\modules\v1\models\auth\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
use yii\helpers\Url;

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="login">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h2 class="login_title">Вход для заказчиков</h2>
                <?= \common\widgets\Alert::widget(); ?>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'phone')->widget(MaskedInput::className(), [
                    'mask' => '+79999999999',
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => ('Введите номер телефона')
                    ],
                    'clientOptions' => [
                        'clearIncomplete' => true
                    ]

                ])?>

                <?= $form->field($model, 'password')->passwordInput() ?>


                <div class="forgot-pass">
                    <a href="#add-code-popup" class="open-popup">Забыли пароль?</a>
                </div>
                <button class="orange-btn">Войти</button>
                <p class="no-account">Нет аккаунта ? <a href="<?= Url::to('/signup') ?>">Зарегистрируйтесь</a></p>

            <?php ActiveForm::end(); ?>
<!--                <div class="login_social">-->
<!--                    <p>Войдите через соцсети</p>-->
<!--                    <ul class="login_social-list">-->
<!--                        <li><a href="#"><img src="img/facebook.png" alt=""></a></li>-->
<!--                        <li><a href="#"><img src="img/vk.png" alt=""></a></li>-->
<!--                        <li><a href="#"><img src="img/ok.png" alt=""></a></li>-->
<!--                        <li><a href="#"><img src="img/instagram.png" alt=""></a></li>-->
<!--                    </ul>-->
<!--                </div>-->
            </div>
            <div class="col-lg-8">
                <img src="img/Street1.jpg" alt="" class="login-img">
            </div>
        </div>
    </div>
</section>
<?= \frontend\widgets\ForgotPasswordForm::widget()?>