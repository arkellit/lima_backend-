<?php

/* @var $this yii\web\View */

$this->title = 'LiMA - сервис поиска временной работы и надежных исполнителей';

use yii\helpers\Url;

?>
<section class="welcome">
    <div class="container">
        <div class="welcome__info">
            <p>LiMA - сервис поиска временной работы и надежных исполнителей </p>
        </div>
        <div class="row">
            <div class="col-lg-5">
                <div class="welcome_block">
                    <h2 class="big-title welcome__title">Исполнители<br> в вашем городе</h2>
                    <p class="welcome_text">LIMA работает на всей территории России, и объединяет лучших и надежных
                        исполнителей, специалистов в разных областях!  </p>
                    <a href="#download" class="orange-btn">Найти исполнителей</a>
                </div>
            </div>
            <!-- <div class="col-lg-5 welcome_img">
              <img src="img/welcome.jpg" alt="">
            </div> -->
        </div>
    </div>
</section>
<section class="home-performers">
    <div class="container">
<!--        <h2 class="home-performers__title">Более 5000 исполнителей</h2>-->
        <p class="home-performers__text">Более 70 видов деятельности в одном приложении в твоем смартфоне Выбери удобный график работы, время, и установи радиус поиска рядом с домом, и принимай заказы.</p>
        <a href="#download" class="orange-btn">Стань исполнителем</a>
    </div>
</section>
<section class="home-staff">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <img src="img/group-2.jpg" alt="" class="home-staff__image">
            </div>
            <div class="col-lg-4">
                <div class="home-staff__inner">
                    <h2 class="home-staff__title">В нашем сервисе только проверенные исполнители, прошедшие модерацию, тестирование, и обучение* </h2>
                    <p class="home-staff__title-hint">*обучение проходят исполнители
                        по определённым видам деятельности  </p>
                    <a href="/signup" class="orange-btn">Выбрать исполнителя</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="home-save">
    <div class="container">
        <p class="home-save__text">5 минут - именно столько время вы потратите, чтобы создать заказ и найти исполнителей.<br>Как это возможно?</p>
    </div>
</section>
<div class="home-about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="home-about__center">
                    <h2 class="home-about__title">Выбирайте понравившихся исполнителей</h2>
                    <p class="home-about__text">Создайте свой портрет исполнителя!  Рост, телосложение, цвет глаз и др. Отслеживайте выполнение задания  в режиме on-line, не отвлекайтесь от бизнеса, после завершения задачи Вы получите push-уведомление c результатом работы.</p>
                </div>
            </div>
<!--            <div class="offset-lg-1 col-lg-5">-->
<!--                <div class="home-about__right">-->
<!--                    <h2 class="home-about__title">Создайте свою промо-акцию</h2>-->
<!--                    <p class="home-about__text">Опишите своими словами,  что нужно сделать и где. Если вам нужны флаеры или листовки, то это не проблема, вы тут же все сделаете!</p>-->
<!--                </div>-->
<!--            </div>-->
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="home-about__info">
                    <img src="img/about-phone1.png" alt="" class="home-about__info-image">
                    <h2 class="home-about__info-title">Создавайте заказы в несколько кликов</h2>
                </div>
            </div>
<!--            <div class="col-lg-3 col-md-6">-->
<!--                <div class="home-about__info">-->
<!--                    <img src="img/about-phone2.png" alt="" class="home-about__info-image">-->
<!--                    <h2 class="home-about__info-title">Заказывайте печать листовок из приложения</h2>-->
<!--                </div>-->
<!--            </div>-->
            <div class="col-lg-4 col-md-6">
                <div class="home-about__info">
                    <img src="img/about-phone3.png" alt="" class="home-about__info-image">
                    <h2 class="home-about__info-title">Выбирайте лучших исполнителей</h2>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="home-about__info">
                    <img src="img/about-phone4.png" alt="" class="home-about__info-image">
                    <h2 class="home-about__info-title">Отслеживайте ваши заказы на карте</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="home-services">
    <div class="container">
        <div class="home-services__inner">
            <div class="home-services__single">
                <h2 class="home-services__single-title">Безналичная<br> оплата</h2>
                <p class="home-services__single-text">Предоставляются закрывающие документы, деньги списываются с Вашего счета в личном кабинете только после выполнения задания</p>
            </div>
            <div class="home-services__single">
                <h2 class="home-services__single-title">Надежные<br> исполнители</h2>
                <p class="home-services__single-text">Система проводит верификацию документов при регистрации. Так же все исполнители проходят обучение и тестирование.</p>
            </div>
            <div class="home-services__single">
                <h2 class="home-services__single-title">Достоверные<br> отзывы</h2>
                <p class="home-services__single-text">Мы разработали систему рейтингов Исполнителей, которая напрямую зависит от тех отзывов, которые оставляют Заказчики</p>
            </div>
        </div>
<!--        <div class="home-services__info">-->
<!--            <p>LIMA – единственный сервис в России с полностью автоматизированными функциями проведения промо-акции под ключ</p>-->
<!--        </div>-->
<!--        <div class="home-services__btn">-->
<!--            <a href="/signup" class="orange-btn">Провести промо-акцию</a>-->
<!--        </div>-->
    </div>
</section>
<!--<section class="questions">-->
<!--    <div class="questions_dark">-->
<!--        <h2 class="main-title main-title-white">Кто такие Промоутеры? </h2>-->
<!--        <div class="questions_answer">-->
<!--            <p>Работа Промоутером одна из самых доступных возможностей получить финансовую независимость для современной молодежи. Для одних - это временная подработка после учебы, для других - постоянная занятость. LIMA – это новый, удобный, и главное безопасный, сервис по поиску работы для Промоутеров. Попробуй!</p>-->
<!--            <a href="/promoter" class="more">Подробнее</a>-->
<!--        </div>-->
<!--        <a href="#download" class="orange-btn">Стань Промоутером</a>-->
<!--    </div>-->
<!--    <div class="questions_white">-->
<!--        <h2 class="main-title">Кто такие Моники?</h2>-->
<!--        <div class="questions_answer">-->
<!--            <p>Абсолютно новое слово в мировой fashion-индустрии. Моник – это живой манекен! <br>Ты любишь быть в центре внимания? <br>Хочешь представлять новые коллекции известных брендов? LIMA поможет тебе стать первопроходцем в новой fashion-профессии и сделать первый шаг в карьере модели.</p>-->
<!--            <a href="/monic" class="more">Подробнее</a>-->
<!--        </div>-->
<!--        <a href="#download" class="black-btn">Стань Моником</a>-->
<!--    </div>-->
<!--</section>-->
<section class="about">
    <div class="about__info">
        <div class="container">
            <p>Получай заказы от множества работодателей <br> в своём городе, возможно, даже рядом с домом!</p>
        </div>
    </div>
    <div class="about__tagline">
        <div class="container">
            <p>LIMA - сервис для тех, кто любит свободу.</p>
        </div>
    </div>
    </div>
</section>
<section class="benefits">
    <div class="container">
        <h2 class="main-title">Преимущества LIMA</h2>
        <div class="benefits__carousel">
            <div class="benefits__arrows">
                <a href="#" class="benefits__arrow benefits__prev"></a>
                <a href="#" class="benefits__arrow benefits__next"></a>
            </div>
            <div class="owl-carousel benefits_block">
                <div class="benefits_single">
                    <div class="benefits_single-wrap">
                        <div class="benefits_single-content">
                            <svg height="512pt" viewBox="0 0 512 512" width="512pt" xmlns="http://www.w3.org/2000/svg"><path d="M482 292.25V46c0-8.285-6.715-15-15-15h-76V15c0-8.285-6.715-15-15-15s-15 6.715-15 15v16h-60V15c0-8.285-6.715-15-15-15s-15 6.715-15 15v16h-60V15c0-8.285-6.715-15-15-15s-15 6.715-15 15v16h-60V15c0-8.285-6.715-15-15-15S91 6.715 91 15v16H15C6.715 31 0 37.715 0 46v391c0 8.285 6.715 15 15 15h249.805c24.25 36.152 65.488 60 112.195 60 74.438 0 135-60.563 135-135 0-32.07-11.25-61.563-30-84.75zM91 61v15c0 8.285 6.715 15 15 15s15-6.715 15-15V61h60v15c0 8.285 6.715 15 15 15s15-6.715 15-15V61h60v15c0 8.285 6.715 15 15 15s15-6.715 15-15V61h60v15c0 8.285 6.715 15 15 15s15-6.715 15-15V61h61v60H30V61zM30 422V151h422v113.805C430.535 250.41 404.73 242 377 242c-47.398 0-89.164 24.559-113.258 61.613A14.911 14.911 0 0 0 257 302h-30c-8.285 0-15 6.715-15 15s6.715 15 15 15h22.723a133.88 133.88 0 0 0-6.883 30H227c-8.285 0-15 6.715-15 15s6.715 15 15 15h15.84a133.666 133.666 0 0 0 6.883 30zm347 60c-57.898 0-105-47.102-105-105s47.102-105 105-105 105 47.102 105 105-47.102 105-105 105zm0 0"/><path d="M437 362h-45v-45c0-8.285-6.715-15-15-15s-15 6.715-15 15v60c0 8.285 6.715 15 15 15h60c8.285 0 15-6.715 15-15s-6.715-15-15-15zm0 0M136 182h-30c-8.285 0-15 6.715-15 15s6.715 15 15 15h30c8.285 0 15-6.715 15-15s-6.715-15-15-15zm0 0M136 242h-30c-8.285 0-15 6.715-15 15s6.715 15 15 15h30c8.285 0 15-6.715 15-15s-6.715-15-15-15zm0 0M136 302h-30c-8.285 0-15 6.715-15 15s6.715 15 15 15h30c8.285 0 15-6.715 15-15s-6.715-15-15-15zm0 0M227 212h30c8.285 0 15-6.715 15-15s-6.715-15-15-15h-30c-8.285 0-15 6.715-15 15s6.715 15 15 15zm0 0M227 272h30c8.285 0 15-6.715 15-15s-6.715-15-15-15h-30c-8.285 0-15 6.715-15 15s6.715 15 15 15zm0 0M136 362h-30c-8.285 0-15 6.715-15 15s6.715 15 15 15h30c8.285 0 15-6.715 15-15s-6.715-15-15-15zm0 0M347 212h30c8.285 0 15-6.715 15-15s-6.715-15-15-15h-30c-8.285 0-15 6.715-15 15s6.715 15 15 15zm0 0"/></svg>
                            <h2>Свободный график.<br>Работай когда хочешь</h2>
                            <p>Занят с утра или дела вечером? Можешь работать только тогда, когда удобно. Самостоятельно составляй график своей работы и не пропускай ни одного интересного и важного события в твоей жизни.</p>
                        </div>
                    </div>
                </div>
                <div class="benefits_single">
                    <div class="benefits_single-wrap">
                        <div class="benefits_single-content">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612"><path d="M587.572 186.881c-32.266-75.225-87.096-129.934-162.949-162.285C386.711 8.427 346.992.168 305.497.168c-41.488 0-80.914 8.181-118.784 24.428-75.225 32.265-130.298 86.939-162.621 162.285C7.895 224.629 0 264.176 0 305.664c0 41.496 7.895 81.371 24.092 119.127 32.323 75.346 87.403 130.348 162.621 162.621 37.877 16.247 77.295 24.42 118.784 24.42 41.489 0 81.214-8.259 119.12-24.42C500.47 555.06 555.3 500.009 587.573 424.791 603.819 386.914 612 347.16 612 305.664c0-41.488-8.174-80.907-24.428-118.783zm-48.848 253.972c-24.021 41.195-56.929 73.876-98.375 98.039-41.195 24.021-86.332 36.135-134.845 36.135-36.47 0-71.27-7.024-104.4-21.415-33.129-14.384-61.733-33.294-85.661-57.215-23.928-23.928-42.973-52.811-57.214-85.997-14.199-33.065-21.08-68.258-21.08-104.735 0-48.52 11.921-93.428 35.807-134.509 23.971-41.231 56.886-73.947 98.039-98.04 41.146-24.092 85.99-36.142 134.502-36.142 48.52 0 93.649 12.121 134.845 36.142 41.446 24.164 74.283 56.879 98.375 98.039 24.092 41.153 36.135 85.99 36.135 134.509 0 48.521-11.964 93.735-36.128 135.189z"/><path d="M324.906 302.988V129.659c0-10.372-9.037-18.738-19.41-18.738-9.701 0-18.403 8.366-18.403 18.738v176.005c0 .336.671 1.678.671 2.678-.671 6.024 1.007 11.043 5.019 15.062L392.836 423.45c6.695 6.695 19.073 6.695 25.763 0 7.694-7.695 7.188-18.86 0-26.099l-93.693-94.363z"/></svg>
                            <h2>Никаких офисов,<br>никаких начальников.</h2>
                            <p>Выбирай сам, когда работать, в каком месте и в какое время.<br>У тебя только один начальник - это ты сам. А твой офис - это твой смартфон.</p>
                        </div>
                    </div>
                </div>
                <div class="benefits_single">
                    <div class="benefits_single-wrap">
                        <div class="benefits_single-content">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.771 25.771"><path d="M24.171 13.728v9.8h-18.8v-9.8h18.8m1.6-1.6h-22v13h22v-13z" /><g><path d="M19.771 12.128l2.252 2.252c-.017.083-.05.16-.05.248 0 .717.581 1.297 1.298 1.297.088 0 .164-.033.247-.049l2.253 2.252v-6h-6z"/><circle cx="21.271" cy="18.628" r="1.298"/><ellipse cx="14.771" cy="18.628" rx="3.926" ry="3.414"/><circle cx="8.271" cy="18.628" r="1.297"/><path d="M3.771 18.128l2.252-2.252c.083.017.16.05.248.05.717 0 1.297-.581 1.297-1.298 0-.088-.033-.164-.05-.247l2.253-2.253h-6v6zM7.569 22.628c0-.717-.581-1.297-1.298-1.297-.088 0-.164.033-.247.049l-2.253-2.252v6h6l-2.252-2.252c.017-.083.05-.161.05-.248zM23.271 21.33c-.717 0-1.297.581-1.297 1.298 0 .088.033.164.05.247l-2.253 2.253h6v-6l-2.252 2.252c-.083-.017-.16-.05-.248-.05z"/></g><g><circle cx="18.473" cy="8.505" r="1.297"/><path d="M2.271 12.128a1.5 1.5 0 0 1 1.5-1.5h.82l.037-.128 11.577-6.34.885.258c.025.08.034.164.077.241.344.63 1.132.859 1.762.515.076-.042.128-.107.191-.162l.887.26 2.017 3.682-.259.886c-.079.024-.163.033-.24.075-.293.16-.488.422-.591.713h3.831l-2.586-4.723h.001L19.297.643l-5.264 2.882h.001L5.263 8.328 0 11.21l2.271 4.147v-3.229z"/><path d="M11.131 8.634a4.231 4.231 0 0 0-1.85 1.994h7.233a2.967 2.967 0 0 0-.299-.885c-.907-1.654-3.182-2.15-5.084-1.109z"/></g></svg>
                            <h2>Зарабатывай<br>хорошие деньги.</h2>
                            <p>Повышай свой рейтинг и получай больше заказов. Выбирай лучшие.<br>Получай расчет за каждый выполненный заказ. Начни зарабатывать уже сегодня.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="apps homepage-apps">
    <a name="download"></a>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="apps_info">
                    <h2 class="big-title">Начни работать прямо сейчас!</h2>
                    <p>Скачай мобильнное приложение и начни работать уже сегодня.</p>
                    <div class="apps_info-links">
                        <div class="apps_info-link">
                            <img src="img/qr.png" alt="">
                            <a href="#"><img src="img/apple.png" alt=""></a>
                        </div>
                        <div class="apps_info-link">
                            <img src="img/qr2.png" alt="">
                            <a href="#"><img src="img/google.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <img src="img/bitmap.png" alt="" class="apps_bitmap">
            </div>
        </div>
    </div>
</section>