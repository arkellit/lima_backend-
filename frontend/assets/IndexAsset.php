<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 30.01.19
 * Time: 15:15
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class IndexAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/vendor.css',
        'https://necolas.github.io/normalize.css/5.0.0/normalize.css',
        'libs/magnific-popup/magnific-popup.css',
        'libs/nice-select/nice-select.css',
        'libs/owl-carousel/owl.carousel.min.css',
        'libs/owl-carousel/owl.theme.default.min.css',
        'css/main.css',
        'css/media.css'
    ];
    public $js = [
        'https://code.jquery.com/ui/1.12.1/jquery-ui.js',
        'libs/magnific-popup/jquery.magnific-popup.min.js',
        'libs/nice-select/nice-select.min.js',
        'libs/owl-carousel/owl.carousel.min.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}