<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 25.02.19
 * Time: 13:48
 */

namespace frontend\widgets;

use frontend\models\ConfirmPasswordReset;
use frontend\models\PasswordResetRequestForm;
use yii\base\Widget;
use Yii;

/**
 * Class ForgotPasswordForm
 * @package frontend\widgets
 */
class ForgotPasswordForm extends Widget
{
    public function run(){
        return $this->render('forgot', [
            'forgot_password' => Yii::createObject(PasswordResetRequestForm::className()),
            'confirm_request_password' => Yii::createObject(ConfirmPasswordReset::className())
        ]);
    }
}