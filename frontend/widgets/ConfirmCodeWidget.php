<?php

namespace frontend\widgets;

use frontend\models\ConfirmPhoneForm;
use Yii;

/**
 * Class ConfirmCodeWidget
 * @package frontend\widgets
 */
class ConfirmCodeWidget extends \yii\base\Widget
{
    public function run()
    {
        return $this->render('confirm-code', [
            'model_confirm_code' => Yii::createObject(ConfirmPhoneForm::className()),
//            'model_resend_code' => Yii::createObject(ConfirmPhoneForm::className())
        ]);
    }
}