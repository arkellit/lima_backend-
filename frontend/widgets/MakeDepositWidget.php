<?php

namespace frontend\widgets;

use frontend\models\forms\MakeDepositForm;
use frontend\models\PasswordResetRequestForm;
use yii\base\Widget;
use Yii;

class MakeDepositWidget extends Widget
{
    public function run()
    {
        return $this->render('make-deposit', [
            'model_make_deposit' => Yii::createObject(MakeDepositForm::className())
        ]);
    }
}