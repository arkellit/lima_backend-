<?php

namespace frontend\widgets;

use api\modules\v1\models\City;
use api\modules\v1\models\worker\Appearance;
use api\modules\v1\models\worker\BodyComplexion;
use api\modules\v1\models\worker\EyeColor;
use api\modules\v1\models\worker\HairColor;
use api\modules\v1\models\worker\HairLength;
use yii\base\Widget;

/**
 * Class WorkerSearchWidget
 * @package frontend\widgets
 */
class WorkerSearchWidget extends Widget
{
    public function run()
    {
        return $this->render('worker-search', [
            'eye_colors' => EyeColor::find()->all(),
            'hair_lengths' => HairLength::find()->all(),
            'hair_colors'  => HairColor::find()->all(),
            'body_complexions' => BodyComplexion::find()->all(),
            'appearances' => Appearance::find()->all(),
            'cities' => City::find()->all()
        ]);
    }
}