<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\widgets\Alert;

/** @var $model_confirm_code frontend\models\ConfirmPhoneForm */
?>
    <a style="display:none;" href="#add-code-popup" class="orange-btn open-popup">Зарегистрироваться</a>

    <div id="add-code-popup" class="zoom-anim-dialog mfp-hide popup add-code-popup">
        <h2>Код подтверждения выслан  <br> на номер <?= Yii::$app->session->get('success_signup') ?></h2>

        <?php $form = ActiveForm::begin([
            'id' => 'form-confirm',
            'action' => Url::to('/site/confirm-code'),
            'validateOnBlur' => false,
        ]); ?>
        <div class="error_message_form" style="display: none;">
            <?php
                echo \yii\bootstrap\Alert::widget([
                    'body' => 'error',
                    'closeButton' => [],
                    'options' => [
                        'class' => 'alert-danger'
                    ]
                ]);
            ?>
        </div>
        <?= $form->field($model_confirm_code, 'code')->textInput() ?>
        <?= $form->field($model_confirm_code, 'phone')->hiddenInput(
            ['value' => Yii::$app->session->get('success_signup')]
        )->label(false) ?>

        <div class="add-brahch-popup-buttons">
            <a href="#" class="grey-btn send_resend_code">Отправить код заново</a>
            <button type="submit" class="orange-btn">Далее</button>
        </div>

        <?php ActiveForm::end();?>

    </div>
<?php //Yii::$app->session->remove('success_signup')?>
<?php if (Yii::$app->session->has('success_signup')): ?>
    <?php
    $script = <<< JS
    $('.open-popup').magnificPopup('open'); 
    
    
    $('#form-confirm').on('beforeSubmit', function() { 
        var form = $('#form-confirm');
        // confirm a user
        $.ajax({
            method: 'POST',
            url: form.attr('action'),
            data: form.serialize(),
            success: function(response) {
                console.log(response);
                if (Object.keys(response).length !== 0) {
                    // If there any errors - display it
                    $.each(response, function(key, element) {
                        $('#form-confirm-'+key).attr('aria-invalid', 'true');
                        var input_div = $('.field-confirmphoneform-'+key);
                        input_div.addClass('has-error');
                        input_div.children('.help-block').text(element);
                    })
                } else {
                    // If everything is ok ask to choose a role
                    console.log('ok');
                }
            }
        });
        return false;
    });
    
    $('.send_resend_code').on('click', function() {
        var phone = $('#confirmphoneform-phone').val(); 
        $.post('/site/resend-code', {phone:phone}, function(response) { 
           if (Object.keys(response).length !== 0) { 
               $('.error_message_form').css('display', '');
               $('.alert-danger.alert.fade.in').text(response['phone'][0]);
           }
        });
        return false;
    });
JS;
    $this->registerJs($script)
    ?>
<?php endif; ?>
