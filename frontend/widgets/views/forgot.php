<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use yii\widgets\MaskedInput;

/** @var $forgot_password frontend\models\PasswordResetRequestForm */
/** @var $confirm_request_password frontend\models\ConfirmPasswordReset */
?>
<a style="display:none;" href="#add-code-popup" class="orange-btn open-popup">Зарегистрироваться</a>

<div id="add-code-popup" class="zoom-anim-dialog mfp-hide popup add-code-popup">

    <?php if (!Yii::$app->session->has('success_request_reset_password')):?>
        <?php $form = ActiveForm::begin([
            'id' => 'form-reset-password',
            'action' => Url::to('/site/request-password-reset'),
            'validateOnBlur' => false,
        ]); ?>
        <div class="error_message_form" style="display: none;">
            <?php
            echo \yii\bootstrap\Alert::widget([
                'body' => 'error',
                'closeButton' => [],
                'options' => [
                    'class' => 'alert-danger'
                ]
            ]);
            ?>
        </div>
        <?= $form->field($forgot_password, 'phone')->widget(MaskedInput::className(), [
            'mask' => '+79999999999',
            'options' => [
                'class' => 'form-control',
                'placeholder' => ('Введите номер телефона')
            ],
            'clientOptions' => [
                'clearIncomplete' => true
            ]

        ])?>

        <div class="add-brahch-popup-buttons">
<!--            <a href="#" class="grey-btn send_resend_forgot_code_request">Не пришел код?</a>-->
            <button type="submit" class="orange-btn">Отправить</button>
        </div>

        <?php ActiveForm::end();?>
    <?php elseif (Yii::$app->session->has('success_request_reset_password')):?>
        <h2>Код подтверждения выслан  <br>на номер <?= Yii::$app->session->get('success_request_reset_password') ?></h2>

        <?php $form = ActiveForm::begin([
            'id' => 'form-confirm-reset-password',
            'action' => Url::to('/site/confirm-password-reset'),
            'validateOnBlur' => false,
        ]); ?>
        <div class="error_message_form" style="display: none;">
            <?php
            echo \yii\bootstrap\Alert::widget([
                'body' => 'error',
                'closeButton' => [],
                'options' => [
                    'class' => 'alert-danger'
                ]
            ]);
            ?>
        </div>
        <?= $form->field($confirm_request_password, 'code')->textInput() ?>
        <?= $form->field($confirm_request_password, 'phone')->hiddenInput(
        ['value' => Yii::$app->session->get('success_request_reset_password')]
    )->label(false) ?>

        <div class="add-brahch-popup-buttons">
            <a href="#" class="grey-btn send_resend_forgot_code">Отправить код заново</a>
            <button type="submit" class="orange-btn">Далее</button>
        </div>

        <?php ActiveForm::end();?>

        <?php endif;?>
</div>

<?php
if (Yii::$app->session->has('success_request_reset_password')):
$script_request = <<< JS
    $('.open-popup').magnificPopup('open');
JS;
    $this->registerJs($script_request);
 endif;
    ?>
    <?php
    $script = <<< JS
    // $('.open-popup').magnificPopup('open'); 

    $('#form-reset-password').on('beforeSubmit', function() { 
        var form = $('#form-reset-password');
        // confirm a user
        $.ajax({
            method: 'POST',
            url: form.attr('action'),
            data: form.serialize(),
            success: function(response) {
                console.log(response);
                if (Object.keys(response).length !== 0) {
                    // If there any errors - display it
                    $.each(response, function(key, element) {
                        $('#form-reset-password-'+key).attr('aria-invalid', 'true');
                        var input_div = $('.field-passwordresetrequestform-'+key);
                        input_div.addClass('has-error');
                        input_div.children('.help-block').text(element);
                    })
                } else {
                    // If everything is ok ask to choose a role
                    console.log('ok');
                }
            }
        });
        return false;
    });
    
    $('#form-confirm-reset-password').on('beforeSubmit', function() { 
        var form = $('#form-confirm-reset-password');
        // confirm a user
        $.ajax({
            method: 'POST',
            url: form.attr('action'),
            data: form.serialize(),
            success: function(response) {
                console.log(response);
                if (Object.keys(response).length !== 0) {
                    // If there any errors - display it
                    $.each(response, function(key, element) {
                        $('#form-confirm-reset-password-'+key).attr('aria-invalid', 'true');
                        var input_div = $('.field-confirmpasswordreset-'+key);
                        input_div.addClass('has-error');
                        input_div.children('.help-block').text(element);
                    })
                } else {
                    // If everything is ok ask to choose a role
                    console.log('ok');
                }
            }
        });
        return false;
    });
    
    $('.send_resend_forgot_code').on('click', function() {
        var phone = $('#confirmpasswordreset-phone').val(); 
        $.post('/site/resend-forgot-code', {phone:phone}, function(response) { 
            console.log(response);
           if (Object.keys(response).length !== 0) { 
               $('.error_message_form').css('display', '');
               $('.alert-danger.alert.fade.in').text(response['phone'][0]);
           }
        });
        return false;
    });
    
    $('.send_resend_forgot_code_request').on('click', function() {
        var phone = $('#passwordresetrequestform-phone').val(); 
        $.post('/site/resend-forgot-code', {phone:phone}, function(response) { 
            console.log(response);
           if (Object.keys(response).length !== 0) { 
               $('.error_message_form').css('display', '');
               $('.alert-danger.alert.fade.in').text(response['phone'][0]);
           }
        });
        return false;
    });
JS;
    $this->registerJs($script)
    ?>

<?php Yii::$app->session->remove('success_request_reset_password')?>