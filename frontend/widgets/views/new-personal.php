<?php

use frontend\models\customer\CustomerBranch;
use frontend\models\customer\CustomerPersonal;

/** @var $model_new_personal CustomerPersonal */
/** @var $branches CustomerBranch */
?>

<div id="add-staff-popup" class="zoom-anim-dialog mfp-hide popup add-staff-popup">
    <h2>Назначить нового сотрудника</h2>
    <?= $this->render('@frontend/widgets/views/_new-personal_form', [
        'model_new_personal' => $model_new_personal,
        'branches' => $branches
    ]) ?>
</div>

<?php
$script =<<<JS
$('#add_new_personal').on('beforeSubmit', function() {
    var form = $('#add_new_personal');
    $.ajax({
        method: 'POST',
        url: form.attr('action'),
        data: form.serialize(),
        success: function(response) {
            console.log(response);
            if (Object.keys(response).length !== 0) {
                // If there any errors - display it
                $.each(response, function(key, element) {
                    $('#add_new_personal-'+key).attr('aria-invalid', 'true');
                    var input_div = $('.field-customerpersonal-'+key);
                    input_div.addClass('has-error');
                    input_div.children('.help-block').text(element);
                })
            } else {
                // If everything is ok ask to choose a role
                console.log('ok');
            }
        }
    });
    return false;
});
JS;

$this->registerJs($script);
