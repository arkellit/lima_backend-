<?php

use frontend\models\customer\CustomerBranch;
use frontend\models\customer\CustomerPersonal;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/** @var $model_new_personal CustomerPersonal */
/** @var $branches CustomerBranch */
?>
<?php $form = ActiveForm::begin([
    'id' => 'add_new_personal',
    'action' => Url::to(['/customer-personal/add-new-personal']),
    'validateOnBlur' => false,
]);
 ?>
    <div class="add-staff-popup-blocks">
        <div class="add-staff-popup-left">
            <?php if (!isset($is_edit)): ?>
                <div class="form-group">
                    <label for="as3">Телефон <span style="color: red;">*</span></label>
                    <?= $form->field($model_new_personal, 'phone')
                        ->widget(MaskedInput::className(), [
                            'mask' => '+79999999999',
                            'options' => [
                                'class' => 'form-control',
                                'placeholder' => ('Введите номер телефона')
                            ],
                            'clientOptions' => [
                                'clearIncomplete' => true
                            ]

                        ])->label(false)?>
                </div>
            <?php endif;?>
            <div class="form-group">
                <label for="as1">Филиал <span style="color: red;">*</span></label>
                <?= $form->field($model_new_personal, 'branch_id')
                    ->dropDownList(ArrayHelper::map($branches,'id','branch'),
                        ['prompt' => 'Выберите филиал'],
                        ['class' => 'select'])
                    ->label(false) ?>
            </div>
            <div class="form-group">
                <label>Должность <span style="color: red;">*</span></label>
                <?php

                $personal_array = array_key_exists('customer',
                    Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())) ?
                    [
                        'administrator' => 'Администратор',
                        'supervisor' => 'Супервайзер'
                    ] : ['supervisor' => 'Супервайзер'];

                ?>
                <?= $form->field($model_new_personal, 'role')
                    ->dropDownList(
                        $personal_array,
                        ['prompt' => 'Выберите должность'],
                        ['class' => 'select'])->label(false) ?>
            </div>
            <div class="form-group">
                <label for="as1">E-mail</label>
                <?= $form->field($model_new_personal, 'email')
                    ->textInput()
                    ->label(false); ?>
            </div>
        </div>
        <div class="add-staff-popup-right">

            <div class="form-group">
                <label for="as4">Фамилия</label>
                <?= $form->field($model_new_personal, 'last_name')
                    ->textInput()
                    ->label(false); ?>
            </div>
            <div class="form-group">
                <label for="as4">Имя</label>
                <?= $form->field($model_new_personal, 'name')
                    ->textInput()
                    ->label(false); ?>
            </div>
            <div class="form-group">
                <label for="as4">Отчество</label>
                <?= $form->field($model_new_personal, 'middle_name')
                    ->textInput()
                    ->label(false); ?>
            </div>
            <?php if (!isset($is_edit)): ?>
                <div class="form-group">
                    <label for="as5">Пароль</label>
                    <?= $form->field($model_new_personal, 'password')
                        ->textInput()
                        ->label(false); ?>
                </div>
            <?php endif;?>

            <?php
                if(isset($is_edit)){
                    echo $form->field($model_new_personal, 'phone')->hiddenInput()->label(false) ;
                    echo '<input type="hidden" name="is_edit" value="'.$model_new_personal->id.'">';
                }
            ?>
        </div>
    </div>
    <div class="add-brahch-popup-buttons">
        <button type="button" class="grey-btn close-popup">Отмена</button>
        <button type="submit" class="orange-btn">Назначить</button>
    </div>
<?php ActiveForm::end(); ?>

