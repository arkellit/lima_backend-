<?php


use api\modules\v1\models\City;
use api\modules\v1\models\worker\Appearance;
use api\modules\v1\models\worker\BodyComplexion;
use api\modules\v1\models\worker\EyeColor;
use api\modules\v1\models\worker\HairColor;
use api\modules\v1\models\worker\HairLength;
use yii\helpers\Url;

/* @var $eye_colors EyeColor */
/* @var $hair_lengths HairLength */
/* @var $hair_colors HairColor */
/* @var $body_complexions BodyComplexion */
/* @var $appearances Appearance */
/* @var $cities City */
?>

<script>
    $.magnificPopup.open({
        items: {
            src: '#filter-popup'
        },
        type: 'inline'
    });
    $('.close-popup').click(function(e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
</script>

<div id="filter-popup" class="zoom-anim-dialog mfp-hide popup filter-popup">
    <h2>Фильтр</h2>
    <div class="row">
        <div class="col-lg-6">
            <div class="filter-popup__radio">
                <div class="filter-popup__radio-single">
                    <input id="male" type="radio" <?= Yii::$app->request->get('sex') == 1 ? 'checked' : '' ?> data-id="1" name="gnender" value="Парни">
                    <label for="male">Парни</label>
                </div>
                <div class="filter-popup__radio-single">
                    <input id="female" type="radio" <?= Yii::$app->request->get('sex') == 2 ? 'checked' : '' ?> data-id="2" name="gnender" value="Девушки">
                    <label for="female">Девушки</label>
                </div>
                <div class="filter-popup__radio-single">
                    <input id="gender-all" type="radio" <?= Yii::$app->request->get('sex') == 0 ? 'checked' : '' ?> data-id="0" name="gnender" value="Все">
                    <label for="gender-all">Все</label>
                </div>
            </div>
            <div class="form-group">
                <label>Город</label>
                <select class="select city_filter">
                    <?php foreach ($cities as $city): ?>
                        <option <?= Yii::$app->request->get('city_id') == $city->id ? 'selected' : '' ?>  data-id="<?= $city->id ?>"><?= $city->name ?></option>
                    <?php endforeach;?>
                </select>
            </div>
            <div class="filter-popup__range">
                <p class="filter-popup__range-label">Поиск в радиусе</p>
                <div id="slider-range" class="filter-popup__range-slider"></div>
                <p id="range-search-radius" class="filter-popup__range-value"><span>1</span> км</p>
            </div>
            <div class="filter-popup__range">
                <p class="filter-popup__range-label">Рост</p>
                <div id="slider-range2" class="filter-popup__range-slider"></div>
                <p id="range-height" class="filter-popup__range-value"><span>160-180</span></p>
            </div>
            <div class="filter-popup__range">
                <p class="filter-popup__range-label">Возраст</p>
                <div id="slider-range3" class="filter-popup__range-slider"></div>
                <p id="range-age" class="filter-popup__range-value"><span>18-25</span></p>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label>Цвет глаз</label>
                <select class="select eye_color_filter">
                    <option value="" data-id="0">Любой</option>
                    <?php foreach ($eye_colors as $eye_color): ?>
                        <option <?= Yii::$app->request->get('eye_color_id') == $eye_color->id ? 'selected' : '' ?> data-id="<?= $eye_color->id ?>"><?= $eye_color->name ?></option>
                    <?php endforeach;?>
                </select>
            </div>
            <div class="form-group">
                <label>Длина волос</label>
                <select class="select hair_length_filter">
                    <option value="" data-id="0">Любой</option>
                    <?php foreach ($hair_lengths as $hair_length): ?>
                        <option <?= Yii::$app->request->get('hair_length_id') == $hair_length->id ? 'selected' : '' ?> data-id="<?= $hair_length->id ?>"><?= $hair_length->name ?></option>
                    <?php endforeach;?>
                </select>
            </div>
            <div class="form-group">
                <label>Цвет волос</label>
                <select class="select hair_color_filter">
                    <option value="" data-id="0">Любой</option>
                    <?php foreach ($hair_colors as $hair_color): ?>
                        <option <?= Yii::$app->request->get('hair_color_id') == $hair_color->id ? 'selected' : '' ?> data-id="<?= $hair_color->id ?>"><?= $hair_color->name ?></option>
                    <?php endforeach;?>
                </select>
            </div>
            <div class="form-group">
                <label>Телосложение</label>
                <select class="select body_complexion_filter">
                    <option value="" data-id="0">Любой</option>
                    <?php foreach ($body_complexions as $body_complexion): ?>
                        <option <?= Yii::$app->request->get('body_complexion_id') == $body_complexion->id ? 'selected' : '' ?>  data-id="<?= $body_complexion->id ?>"><?= $body_complexion->name ?></option>
                    <?php endforeach;?>
                </select>
            </div>
            <div class="form-group">
                <label>Внешность</label>
                <select class="select appearance_filter">
                    <option value="" data-id="0">Любой</option>
                    <?php foreach ($appearances as $appearance):?>
                        <option <?= Yii::$app->request->get('appearance_id') == $appearance->id ? 'selected' : '' ?> data-id="<?= $appearance->id ?>"><?= $appearance->name ?></option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>
    </div>
    <div class="profile-popup__btns">
        <a href="#" class="grey-btn close-popup">Отмена</a>
        <a href="#" class="orange-btn url_filter">Применить</a>
    </div>
</div>

<!--<div class="radius_search" style="display:none;"></div>-->
<!--<div class="height_from" style="display:none;"></div>-->
<!--<div class="height_to" style="display:none;"></div>-->
<!--<div class="age_from" style="display:none;"></div>-->
<!--<div class="age_to" style="display:none;"></div>-->

<?php
$url = Url::base(true);
$script_filter = <<<JS
var urlParams = new URLSearchParams(location.search);
// console.log(urlParams.get('city_id'));


var url_domain = "$url"; 
$('.filter-popup__radio input').on('change', function() {
  var sex = $(this).attr('data-id');
  getParams('', '', sex);
});

$('.city_filter').on('change', function() {
  var city_option = $(this).find('option:selected'); 
  var city = city_option.attr('data-id');
  getParams(city);
});

$( "#slider-range").slider({
  range: 'min',
  min: 1,
  max: 100,
  value: urlParams.get('radius_search') == null ? 10 : urlParams.get('radius_search'),
  slide: function(event, ui) { 
    // var radius = urlParams.get('radius_search') == '' ? ui.value : urlParams.get('radius_search') ;
    $( "#range-search-radius span").text(ui.value);
    getParams('', ui.value);
  }
});
$( "#range-search-radius span" ).text($("#slider-range").slider("value"));

$( "#slider-range2" ).slider({
  range: true,
  min: 140,
  max: 210,
  values: [
      urlParams.get('height_start') == null ? 160 : urlParams.get('height_start'),
      urlParams.get('height_end') == null ? 180 : urlParams.get('height_end')
      ],
  slide: function( event, ui ) { 
    $( "#range-height span").text(ui.values[ 0 ] + "-" + ui.values[ 1 ] );
    getParams('', '', '', ui.values[ 0 ], ui.values[ 1 ]);
  }
});
// alert(urlParams.get('height_start'));
var slider_height = $("#slider-range2").slider("values")[0] + ' - ' + $("#slider-range2").slider("values")[1];
$("#range-height span" ).text(slider_height);

$( "#slider-range3" ).slider({
  range: true,
  min: 14,
  max: 65,
  // values: [18, 25],
  values: [
      urlParams.get('age_start') == null ? 18 : urlParams.get('age_start'),
      urlParams.get('age_end') == null ? 25 : urlParams.get('age_end')
      ],
  slide: function( event, ui ) { 
    $( "#range-age span").text(ui.values[ 0 ] + "-" + ui.values[ 1 ] );
    getParams('', '', '', '', '', '', '', '', '', '', ui.values[ 0 ], ui.values[ 1 ]);
  }
});
var slider_age = $("#slider-range3").slider("values")[0] + ' - ' + $("#slider-range3").slider("values")[1];
$("#range-age span" ).text(slider_age);


//     'change', function() {
//   // var search_radius = $(this).text(); 
//   alert(2);
// });

$('#range-height span').on('change', function() {
  var height = $(this).text(); 
});

$('#range-age span').on('change', function() {
  var age = $(this).text(); 
});

$('.eye_color_filter').on('change', function() {
  var eye_color_filter_option = $(this).find('option:selected'); 
  var eye_color_filter = eye_color_filter_option.attr('data-id');
  getParams('', '', '', '', '', eye_color_filter);
});
$('.hair_length_filter').on('change', function() {
  var hair_length_filter_option = $(this).find('option:selected');
  var hair_length_filter = hair_length_filter_option.attr('data-id');
  getParams('', '', '', '', '', '', hair_length_filter);
});
$('.hair_color_filter').on('change', function() {
  var hair_color_filter_option = $(this).find('option:selected'); 
  var hair_color_filter = hair_color_filter_option.attr('data-id');
  getParams('', '', '', '', '', '', '', hair_color_filter);
});
$('.body_complexion_filter').on('change', function() {
  var body_complexion_filter_option = $(this).find('option:selected'); 
  var body_complexion_filter = body_complexion_filter_option.attr('data-id');
  getParams('', '', '', '', '', '', '', '', body_complexion_filter);

});
$('.appearance_filter').on('change', function() {
  var appearance_filter_option = $(this).find('option:selected'); 
  var appearance_filter = appearance_filter_option.attr('data-id');
  getParams('', '', '', '', '', '', '', '', '', appearance_filter);
  // alert(appearance_filter);
});

 var params = { 
     city_id : urlParams.get('city_id') == null ? $('.city_filter').find('option:selected').attr('data-id') :
        urlParams.get('city_id'), 
     radius_search : urlParams.get('radius_search') == null ? $('#range-search-radius span').text() : 
        urlParams.get('radius_search'), 
     sex : urlParams.get('sex') == null ? 0 : urlParams.get('sex'),
     height_start : urlParams.get('height_start') == null ? 160 : urlParams.get('height_start') ,
     height_end : urlParams.get('height_end') == null ? 180 : urlParams.get('height_end'),
     eye_color_id : urlParams.get('eye_color_id') == 0 ? '' :
        (urlParams.get('eye_color_id') == null ? $('.eye_color_filter').find('option:selected').attr('data-id') :
        urlParams.get('eye_color_id')),
     hair_length_id :  urlParams.get('hair_length_id') == 0 ? '' :
        (urlParams.get('hair_length_id') == null ? $('.hair_length_filter').find('option:selected').attr('data-id') :
        urlParams.get('hair_length_id')),
     hair_color_id :  urlParams.get('hair_color_id') == 0 ? '' :
        (urlParams.get('hair_color_id') == null ? $('.hair_color_filter').find('option:selected').attr('data-id') : 
        urlParams.get('hair_color_id')),
     body_complexion_id :  urlParams.get('body_complexion_id') == 0 ? '' :
        (urlParams.get('body_complexion_id') == null ? $('.body_complexion_filter').find('option:selected').attr('data-id') :
        urlParams.get('body_complexion_id')),
     appearance_id : urlParams.get('body_complexion_id') == 0 ? '' : 
        (urlParams.get('appearance_id') == null ? $('.appearance_filter').find('option:selected').attr('data-id') : 
        urlParams.get('appearance_id')),
     age_start : urlParams.get('age_start') == null ? 18 : urlParams.get('age_start'),
     age_end : urlParams.get('age_end') == null ? 25 : urlParams.get('age_end'),
     order_id : urlParams.get('order_id')
    };
 
 $('.url_filter').attr('href', url_domain + '/order-offer/offer?' + jQuery.param(params));
 
function getParams(city = '', radius_search = '', sex = '', height_start = '', height_end = '',
    eye_color_id = '', hair_length_id = '', hair_color_id = '', body_complexion_id = '', appearance_id = '',
    age_start = '', age_end = '') {
    params['city_id'] = city == '' ? params['city_id'] : city;
    params['sex'] = sex == '' ? params['sex'] : sex;
    params['radius_search'] = radius_search == '' ? params['radius_search'] : radius_search;
    params['height_start'] = height_start == '' ? params['height_start'] : height_start;
    params['height_end'] = height_end == '' ? params['height_end'] : height_end;
    params['eye_color_id'] = eye_color_id == '' ? params['eye_color_id'] : eye_color_id;
    params['hair_length_id'] = hair_length_id == '' ? params['hair_length_id'] : hair_length_id;
    params['hair_color_id'] = hair_color_id == '' ? params['hair_color_id'] : hair_color_id;
    params['body_complexion_id'] = body_complexion_id == '' ? params['body_complexion_id'] : body_complexion_id;
    params['appearance_id'] = appearance_id == '' ? params['appearance_id'] : appearance_id;
    params['age_start'] = age_start == '' ? params['age_start'] : age_start;
    params['age_end'] = age_end == '' ? params['age_end'] : age_end;
    var str = jQuery.param(params);
    $('.url_filter').attr('href', url_domain + '/order-offer/offer?' + str);
    console.log(str);
}

JS;

$this->registerJs($script_filter);
?>

