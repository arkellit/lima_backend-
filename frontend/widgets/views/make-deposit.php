<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>

<div id="add-deposit-popup" class="zoom-anim-dialog mfp-hide popup add-deposit-popup">
    <h2>Пополнить депозит</h2>
        <?php $form = ActiveForm::begin([
            'action' => Url::to('/customer/make-deposit'),
            'validateOnBlur' => false,
        ]); ?>
        <div class="form-group">
            <label for="makedepositform-sum">Пополнить депозит на сумму</label>
            <?= $form->field($model_make_deposit, 'sum', [
                'template' => "{input}\n{label}", 'options' =>
                    ['style' => 'margin-top:8px']])->textInput()->label(false) ?>
<!--            <input id="ad1" type="text" class="form-control">-->
            <p class="form-group-help">Будет сформирован счет на оплату указанной суммы</p>
        </div>
        <div class="add-deposit-popup-buttons">
            <button type="button" class="grey-btn close-popup">Отмена</button>
            <button type="submit" class="orange-btn">Пополнить</button>
        </div>
    <?php ActiveForm::end(); ?>
</div>
