<?php

namespace frontend\widgets;

use common\helpers\CustomerHelpers;
use frontend\models\customer\CustomerBranch;
use frontend\models\customer\CustomerPersonal;
use yii\base\Widget;
use Yii;

/**
 * Class NewPersonalWidget
 * @package frontend\widgets
 */
class NewPersonalWidget extends Widget
{
    public function run()
    {
        if (array_key_exists('customer', Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId()))){
            $branches = CustomerBranch::findAll(['customer_id' =>
                CustomerHelpers::getCustomerId(Yii::$app->user->getId()), 'is_delete' => CustomerBranch::NO_DELETE]);
        }
        else {
            $branches = CustomerBranch::findAll(['customer_id' =>
                CustomerHelpers::getCustomerId(Yii::$app->user->getId()),
                'id' => Yii::$app->user->identity->customerPersonal->branch_id, 'is_delete' => CustomerBranch::NO_DELETE]);
        }
        return $this->render('new-personal', [
            'model_new_personal' => Yii::createObject(CustomerPersonal::className()),
            'branches' => $branches
        ]);
    }
}