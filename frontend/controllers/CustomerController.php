<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 05.03.19
 * Time: 17:55
 */

namespace frontend\controllers;

use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\Upload;
use common\helpers\CustomerHelpers;
use common\helpers\GetErrorsHelpers;
use frontend\models\customer\CustomerAllForm;
use frontend\models\customer\CustomerCompanyData;
use frontend\models\customer\Customer;
use frontend\models\customer\CustomerBankData;
use frontend\models\customer\CustomerBikForm;
use frontend\models\customer\CustomerInnForm;
use frontend\models\customer\GetDataCompany;
use frontend\models\forms\MakeDepositForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use frontend\models\order\Order;

class CustomerController extends Controller
{
    public $layout = 'customer-layout';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['check-worker-request'],
                        'roles' => ['?']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'check-worker-request'  => ['GET']
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $this->view->title = 'Профиль управляющего';
        $user_id = Yii::$app->user->getId();
        if (array_key_exists('customer', Yii::$app->authManager->getRolesByUser($user_id))){
            $customer = Customer::findOne(['user_id' => Yii::$app->user->getId()]);
            if ($customer->is_register_complete == Customer::IS_REGISTER_COMPLETE_FALSE){
                Yii::$app->session->setFlash('error', 'Ваш профиль еще не прошел модерацию');
            }
        }
        elseif (array_key_exists('administrator', Yii::$app->authManager->getRolesByUser($user_id))){
            $customer_personal = CustomerPersonal::findOne(['user_id' => Yii::$app->user->getId(),
                'role' => CustomerPersonal::ROLE_ADMINISTRATOR]);
            $customer = $customer_personal->customer;
        }
        elseif (array_key_exists('supervisor', Yii::$app->authManager->getRolesByUser($user_id))
        || array_key_exists('worker', Yii::$app->authManager->getRolesByUser($user_id))){
            Yii::$app->session->setFlash('error', 'Вы не имеете доступ к личному кабинету');
            return $this->goHome();
        }
        else {
            Yii::$app->session->setFlash('error', 'Пройдите регистрацию');
            return $this->redirect(Url::to(['/customer/register']));
        }
        return $this->render('profile', [
            'customer' => $customer
        ]);
    }

    public function actionRegister()
    {
        $this->layout = 'index-layout';
        $this->view->title = 'Регистрация управляющего';
        $customer = Customer::findOne(['user_id' => Yii::$app->user->getId()]);
        if (!empty($customer)) {
            Yii::$app->session->setFlash('error', 'Вы уже зарегистрированы');
            return $this->redirect(Url::to(['/customer/index']));
        }
        $customer = new Customer();
        if ($customer->load(Yii::$app->request->post())) {
            $customer->user_id = Yii::$app->user->getId();
            if ($customer->validate()) {
                Yii::$app->session->set('customer', $customer);
                return $this->redirect(Url::to(['/customer/register-inn']));
            }
        }
        return $this->render('register', [
            'model' => $customer
        ]);
    }

    public function actionRegisterInn()
    {
        $this->layout = 'index-layout';
        $this->view->title = 'Регистрация управляющего';
        if (!Yii::$app->session->has('customer')) {
            Yii::$app->session->setFlash('error', 'Неверные данные, заполните данные');
            return $this->redirect(Url::to(['/customer/register']));
        }
        $customer_inn = new CustomerInnForm();
        if ($customer_inn->load(Yii::$app->request->post())) {
            if ($customer_inn->validate()) {
                Yii::$app->session->set('inn_company', $customer_inn->inn);
                return $this->redirect(Url::to(['/customer/register-company']));
            }
        }
        return $this->render('register-inn', [
            'model_inn' => $customer_inn
        ]);
    }

    public function actionCheckInn()
    {
        $inn = Yii::$app->request->post('CustomerInnForm')['inn'];
        $check_company_date = CustomerCompanyData::find()
            ->where(['inn_company' => $inn])->exists();
        if ($check_company_date) {
            return Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => Response::FORMAT_JSON,
                'data' => [
                    'error' => 'Компания с таким ИНН уже зарегистрирована'
                ]
            ]);
        }
        $company_data = GetDataCompany::getData($inn);
//        return var_dump($company_data);die();
        if (!empty($company_data['suggestions'])) {
            $name_company = $company_data['suggestions'][0]['value'];
            $kpp = isset($company_data['suggestions'][0]['data']['kpp']) ? $company_data['suggestions'][0]['data']['kpp'] : null;
            $address = $company_data['suggestions'][0]['data']['address']['unrestricted_value'];
            $phones = $company_data['suggestions'][0]['data']['phones'];
            $okpo = $company_data['suggestions'][0]['data']['okpo'];
            $ogrn = $company_data['suggestions'][0]['data']['ogrn'];

            $data = [];
            $data += ['inn' => $inn];
            $data += ['name_company' => $name_company];
            $data += ['kpp' => $kpp];
            $data += ['address' => $address];
            $data += ['phone' => $phones];
            $data += ['okpo' => $okpo];
            $data += ['ogrn' => $ogrn];
            Yii::$app->session->set('customer_company_data', $data);
            return Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => Response::FORMAT_JSON,
                'data' => $data
            ]);
        }
        return Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => Response::FORMAT_JSON,
            'data' => ['error' => 'Введен неверный ИНН']
        ]);
    }

    public function actionSearchBik()
    {
        $bik = Yii::$app->request->post('CustomerBikForm')['bik'];
        $bank = GetDataCompany::getBank($bik);
        if (!is_null($bank)) {
            $name_bank = $bank['suggestions'][0]['value'];
            $city_bank = $bank['suggestions'][0]['data']['address']['data']['city_with_type'];
            $ks_bank = $bank['suggestions'][0]['data']['correspondent_account'];
            $data = [];
            $data += ['bik' => $bik];
            $data += ['name_bank' => $name_bank];
            $data += ['city_bank' => $city_bank];
            $data += ['ks_bank' => $ks_bank];
            Yii::$app->session->set('customer_bank_data', $data);
            return Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => Response::FORMAT_JSON,
                'data' => $data
            ]);
        }
        return null;
    }

    public function actionRegisterCompany()
    {
        $this->layout = 'index-layout';
        $this->view->title = 'Регистрация управляющего';
        if (!Yii::$app->session->has('customer') || !Yii::$app->session->has('customer_company_data')) {
            Yii::$app->session->setFlash('error', 'Неверные данные компании');
            return $this->redirect(Url::to(['/customer/register-inn']));
        }
        $model_customer_all = new CustomerAllForm();
        $model_bik_form = new CustomerBikForm();
        $user_id = Yii::$app->user->getId();
        if ($model_customer_all->load(Yii::$app->request->post())) {
            if ($model_customer_all->validate()) {
                $model_customer = new Customer($model_customer_all->getAttributes(['name',
                    'last_name', 'middle_name', 'email']));
                $model_customer->user_id = $user_id;
                if (!$model_customer->save()) {
                    $message = GetErrorsHelpers::getError($model_customer->getErrors());
                    Yii::$app->session->setFlash('error', $message);
                    $this->refresh();
                }
                $model_company_customer = new CustomerCompanyData($model_customer_all->getAttributes([
                    'name_company', 'inn_company', 'kpp_company', 'actual_address',
                    'email', 'contact_phone', 'logo_company'
                ]));
                $model_company_customer->customer_id = $model_customer->id;
                $image = UploadedFile::getInstance($model_customer_all, 'logo_company');
                if ($image !== null) {
                    $upload_form = new Upload();
                    $upload_form->scenario = Upload::SCENARIO_IMAGE_FILE;
                    $upload_form->image = $image;
                    $image_load = $upload_form->upload('customer');
                }
                if (isset($image_load)) {
                    $model_company_customer->logo_company = $image_load;
                }
                if (!$model_company_customer->save()) {
                    $model_customer->delete();
                    $message = GetErrorsHelpers::getError($model_company_customer->getErrors());
                    Yii::$app->session->setFlash('error', $message);
                    $this->refresh();
                }

                $model_bank_customer = new CustomerBankData($model_customer_all->getAttributes([
                    'customer_rs', 'bank_bik', 'bank_city', 'bank_name', 'bank_ks',
                    'bank_kpp', 'bank_inn'
                ]));
                $model_bank_customer->customer_id = $model_customer->id;
                if (!$model_bank_customer->save()) {
                    if($model_company_customer->logo_company !== null){
                        unlink(Yii::getAlias('@image/web'. $model_company_customer->logo_company));
                    }
                    $model_company_customer->delete();
                    $model_customer->delete();
                    $message = GetErrorsHelpers::getError($model_bank_customer->getErrors());
                    Yii::$app->session->setFlash('error', $message);
                    $this->refresh();
                }

                $userRole = Yii::$app->authManager->getRole('customer');
                $create_role = Yii::$app->authManager->assign($userRole, $user_id);
                if (!$create_role) {
                    if($model_company_customer->logo_company !== null){
                        unlink(Yii::getAlias('@image/web'. $model_company_customer->logo_company));
                    }
                    $model_company_customer->delete();
                    $model_bank_customer->delete();
                    $model_customer->delete();
                    Yii::$app->session->setFlash('error', 'Произошла ошибка при добавлении роли');
                    $this->refresh();
                }
                Yii::$app->session->set('success_register', $model_company_customer->email);
                Yii::$app->session->remove('customer');
                Yii::$app->session->remove('customer_company_data');
                Yii::$app->session->remove('customer_bank_data');
                $model_customer->trigger(\api\modules\v1\models\customer\Customer::EVENT_CONFIRMED_CUSTOMER_NOTICE);
                return $this->redirect(Url::to(['/customer/index']));
            }
        }

        return $this->render('register-company', [
            'model_customer' => $model_customer_all,
            'model_bik_form' => $model_bik_form
        ]);
    }

    public function actionMakeDeposit() {
        $model = new MakeDepositForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()){
                /** TODO удалить на прод! */
                $customer_id = CustomerHelpers::getCustomerIdSite(Yii::$app->user->getId());
                if (!$customer_id) return $this->redirect(Url::to(['/customer/index']));
                $customer = Customer::findOne($customer_id);
                $customer_company = $customer->companyData;
                $customer_company->deposit += $model->sum;
                $customer_company->save();

                $orders = Order::findAll(['customer_id' =>
                    CustomerHelpers::getCustomerId(Yii::$app->user->getId()), 'is_pay' => 0]);
                if (!empty($orders)){
                    foreach ($orders as $order) {
                        if ($customer_company->deposit > $order->total_price) {
                            $customer_company->deposit -= $order->total_price;
                            $order->is_pay = 1;
                            $order->save();
                            $customer_company->save();
                        }
                    }
                }
                Yii::$app->session->set('success_make_deposit', $model->sum);
                if (Yii::$app->request->post('order_deposit')){
                    return $this->redirect(['/order/confirm-order',
                        'id' => Yii::$app->request->post('order_deposit_id')]);
                }
                return $this->redirect(['/customer/index']);
            }
        }
        return Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => Response::FORMAT_JSON,
            'data' => $model->errors,
        ]);
    }

    /**
     * @return int|Response
     */
    public function actionCheckWorkerRequest(){
        if (Yii::$app->request->isAjax){
            $customer_id = Yii::$app->request->get('customer_id');
            $order_id_new_request = CustomerHelpers::checkNewWorkerRequest($customer_id);
            if ($order_id_new_request['order_id'] == null){
                return 0;
            }
            return $order_id_new_request['order_id'];
        }
        return false;
    }
}