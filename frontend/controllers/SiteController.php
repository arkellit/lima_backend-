<?php
namespace frontend\controllers;

use api\modules\v1\models\auth\Token;
use common\helpers\TokenCodeHelpers;
use frontend\models\ConfirmPasswordReset;
use frontend\models\ConfirmPhoneForm;
use frontend\models\ForgotResendPassword;
use frontend\models\ResendCodeForm;
use frontend\models\User;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\Response;
use yii\widgets\ActiveForm;

use YandexCheckout\Client;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
//                    [
//                        'actions' => ['login', 'signup'],
//                        'allow' => true,
//                        'roles' => ['?'],
//                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'index-layout'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'index-layout';
        return $this->render('index');
    }



    public function actionPage()
    {
        echo '<div style="text-align: center">';
        echo '<h1>' . 'Сайт находится в разработке' . '</h1>';
        $img_logo = Yii::$app->params['urlImage'] . '/img/lima-logo.jpg';
        echo '<img src="' . $img_logo . '">';
        echo '</div>';
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = 'index-layout';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (array_key_exists('customer',
                Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId()))){
                return $this->redirect(['/customer/index']);
            }
            return $this->redirect(['/customer/index']);
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return string|Response
     * @throws \yii\base\Exception
     * @throws \yii\web\HttpException
     */
    public function actionSignup()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $this->layout = 'index-layout';
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                Yii::$app->session->set('success_signup', $model->phone);
                $this->refresh();
//                if (Yii::$app->getUser()->login($user)) {
//                    return $this->goHome();
//                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Function confirm phone
     *
     * @return object|Response
     * @throws \yii\base\InvalidConfigException
     */
    public function actionConfirmCode(){
        $confirm_phone_model = new ConfirmPhoneForm();
        if ($confirm_phone_model->load(Yii::$app->request->post())){
            if ($confirm_phone_model->validate()){
                $user = $confirm_phone_model->_user;
                $user->is_confirm = User::CONFIRM_PHONE;
                $user->update();
                Yii::$app->getUser()->login($user);
                Yii::$app->session->remove('success_signup');
                return $this->redirect(['/customer/index']);
            }
        }
        return Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => Response::FORMAT_JSON,
            'data' => $confirm_phone_model->errors,
        ]);
    }

    /**
     * Function resend code
     *
     * @return object|Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\HttpException
     */
    public function actionResendCode(){
        $phone = Yii::$app->request->post('phone');
        $resend_phone_model = new ResendCodeForm(['phone' => $phone]);
        if ($resend_phone_model->validate()){
            $user = $resend_phone_model->_user;
            TokenCodeHelpers::updateSend($user->id, Token::TYPE_CONFIRMATION,
                $user->phone);
            return $this->redirect('/signup');
        }
        return Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => Response::FORMAT_JSON,
            'data' => $resend_phone_model->errors
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return object|Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\HttpException
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        $model->scenario = PasswordResetRequestForm::SCENARIO_GENERATE_TOKEN;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->generateToken()) {
                Yii::$app->session->set('success_request_reset_password', $model->phone);
                return $this->redirect('/login');
            }
        }

        return Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => Response::FORMAT_JSON,
            'data' => $model->errors
        ]);
    }

    public function actionConfirmPasswordReset(){
        $model_confirm = new ConfirmPasswordReset();
        if ($model_confirm->load(Yii::$app->request->post()) && $model_confirm->validate()){
            if ($model_confirm->validateForm()) {
                $user = $model_confirm->_user;
                $user->is_confirm_forgot_code = User::CONFIRM_CODE;
                $user->save();
                return $this->redirect(Url::to(['/site/reset-password', 'token' => $user->password_reset_token]));
            }
        }

        return Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => Response::FORMAT_JSON,
            'data' => $model_confirm->errors
        ]);
    }

    /**
     * Resets password.
     *
     * @param $token
     * @return string|Response
     * @throws \yii\base\Exception
     * @throws \yii\web\HttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = 'index-layout';

        $model = new PasswordResetRequestForm();
        $model->scenario = PasswordResetRequestForm::SCENARIO_SET_NEW_PASSWORD;
        if ($model->load(Yii::$app->request->post())) {

            if ($model->setNewPassword($token, $model->password)){
                Yii::$app->session->setFlash('success', 'Пароль успешно изменен');
                Yii::$app->session->remove('success_request_reset_password');

                return $this->redirect('/login');
            }
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionResendForgotCode(){
        $phone = Yii::$app->request->post('phone');
        $model_forgot = new ForgotResendPassword(['phone' => $phone]);
        if ($model_forgot->validateForm()){
            return $this->redirect('/login');
        }
        return Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => Response::FORMAT_JSON,
            'data' => $model_forgot->errors,
        ]);
    }

    public function actionTest(){
        return $this->render('test');
    }

    public function actionSendPayment(){
        $client = new Client();
        $client->setAuth(585095, 'test_0EXch-OspQOPMx6gOxrpD1jHSULxC5d6EAHL_00Fl5I');
        $idempotenceKey = uniqid('', true);
        $response = $client->createPayment(
            array(
                'payment_token' => 'eyJ0eXBlIjoiY2hlY2tvdXRfanNfYmFua19jYXJkIiwiZW5jcnlwdGVkIjoiOU8zYzl4WlhCNkxxNjhBcjRXaEJna1FTaVBYQVFWRGpMQktJY2N6aG9wT1JHbnRyUmNieVF5ZGZ2WkkzaU1EN0d4QXdiNjROUG9SZnU3RkphK2VaWEkzUDhYcld2d3JDcHBRMXhJRnZ3djN2YkM2Ukt3bmFLWXB3aTVMNFBNNk9kamlyUDcrMHBCa0paZ3dyUXFDcXdyaW53NHBZbE1KS2JiaFFMYWNhUTRxWTNpbXp6cENKenBpMnMrejVybkhHTGlrSjFBPT0iLCJpbml0VmVjdG9yIjoieUNHSWVtNXpZdFZRWVVBRzNCQmhkdz09Iiwia2V5SWQiOiJPak5BQmsvbVRqbmRMa1ZmVHVTUXR3PT0ifQ==',

                'amount' => array(
                    'value' => 1.00,
                    'currency' => 'RUB',
                ),
                'confirmation' => array(
                    'type' => 'redirect',
                    'locale' => 'ru_RU',
                    'return_url' => 'http://iamlima.com.local/return_url',
                ),
                'capture' => true,
                'description' => 'Заказ №1',
            ),
            $idempotenceKey
        );
        echo '<pre>' .print_r($response, true) . '</pre>';
    }

    public function actionReturnUrl(){
        echo '<h4 style="text-align: center">' . 'Success Payment'. '</h4>';
    }
}
