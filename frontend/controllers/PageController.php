<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 25.02.19
 * Time: 16:38
 */

namespace frontend\controllers;

use api\modules\v1\models\Upload;
use common\models\Feedback;
use yii\web\Controller;
use Yii;
use yii\web\UploadedFile;

/**
 * Class PageController
 * @package frontend\controllers
 */
class PageController extends Controller
{
    public $layout = 'index-layout';

    public function actionMonic(){
        return $this->render('monic');
    }

    public function actionPromoter(){
        return $this->render('promoter');
    }

    public function actionCustomer(){
        return $this->render('customer');
    }

    public function actionMore(){
        return $this->render('more');
    }
    public function actionContacts(){
        $model_feedback = new Feedback();
        if ($model_feedback->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model_feedback, 'image');
            if ($image !== null) {
                $upload_form = new Upload();
                $upload_form->scenario = Upload::SCENARIO_IMAGE_FILE;
                $upload_form->image = $image;
                $model_feedback->image = $upload_form->upload('feedback');
            }
            if ($model_feedback->save()) {
                Yii::$app->session->setFlash('success',
                    'Заявка успешно отправлена');
                return $this->refresh();
            }
        }
        return $this->render('contacts', [
            'model_feedback' => $model_feedback
        ]);
    }

    public function actionFeedback(){
        return $this->render('feedback');
    }

    public function actionFaqCustomer(){
        return $this->render('faq-customer');
    }

    public function actionFaqWorker(){
        return $this->render('faq-worker');
    }

    public function actionConditions(){
        return $this->render('conditions');
    }

    public function actionOffer(){
        return $this->render('offer');
    }

    public function actionBecomeWorker(){
        return $this->render('become-worker');
    }

    public function actionFaqFreelancer(){
        return $this->render('faq-freelancer');
    }
    public function actionFreelancerLima(){
        $model_feedback = new Feedback();
        if ($model_feedback->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model_feedback, 'image');
            if ($image !== null) {
                $upload_form = new Upload();
                $upload_form->scenario = Upload::SCENARIO_IMAGE_FILE;
                $upload_form->image = $image;
                $model_feedback->image = $upload_form->upload('feedback');
            }
            if ($model_feedback->save()) {
                Yii::$app->session->setFlash('success',
                    'Заявка успешно отправлена');
                return $this->refresh();
            }
        }
        return $this->render('freelancer-lima', [
            'model_feedback' => $model_feedback
        ]);
    }

    public function actionMassMedia() {
        return $this->render('mass-media');
    }
}