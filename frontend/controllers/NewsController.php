<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 05.07.19
 * Time: 16:18
 */

namespace frontend\controllers;


use api\modules\v1\models\news\News;
use common\models\NewsFront;
use yii\web\Controller;

class NewsController extends Controller
{
    public $layout = 'index-layout';

    public function actionIndex(){
        $news = NewsFront::find()->where(['is_published' => News::PUBLISH])
        ->orderBy(['id' => SORT_ASC])->all();
        return $this->render('index', [
            'news' => $news
        ]);
    }

    public function actionSingleNews($id){
        $news = NewsFront::findOne($id);
        return $this->render('single-news', [
            'news' => $news
        ]);
    }
    public function actionSingleNews2(){
        return $this->render('single-news-2');
    }
    public function actionSingleNews3(){
        return $this->render('single-news-3');
    }
    public function actionSingleNews4(){
        return $this->render('single-news-4');
    }

}