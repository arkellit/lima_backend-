<?php

namespace frontend\controllers;

use api\modules\v1\models\auth\User;
use api\modules\v1\models\City;
use common\helpers\CustomerHelpers;
use frontend\models\customer\CustomerBranch;
use frontend\models\customer\CustomerPersonalSearch;
use frontend\models\order\OrderPlace;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use Yii;
use yii\web\Response;
use frontend\models\customer\CustomerPersonal;

class CustomerPersonalController extends Controller
{
    public $layout = 'customer-layout';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionStaff(){
        $customer_id = CustomerHelpers::getCustomerIdSite(Yii::$app->user->getId());
        if (!$customer_id) return $this->redirect(Url::to(['/customer/index']));

        if (array_key_exists('customer', Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId()))){
            $model_branch = CustomerBranch::findAll([
                'customer_id' => $customer_id,
                'is_delete' => CustomerBranch::NO_DELETE
            ]);
            $model_personal = CustomerPersonal::findAll([
                'customer_id' => $customer_id,
                'is_delete' => CustomerBranch::NO_DELETE
            ]);
        }
        else {
            $model_branch = CustomerBranch::findAll([
                'customer_id' => $customer_id,
                'id' => Yii::$app->user->identity->customerPersonal->branch_id,
                'is_delete' => CustomerBranch::NO_DELETE
            ]);
            $model_personal = CustomerPersonal::findAll([
                'customer_id' => $customer_id,
                'branch_id' => Yii::$app->user->identity->customerPersonal->branch_id,
                'is_delete' => CustomerBranch::NO_DELETE
            ]);
            if (empty($model_branch)){
                Yii::$app->session->setFlash('error', 'Вы не назначены на филиал');
            }
        }
        $searchModel = new CustomerPersonalSearch([
            'customer_id' => $customer_id,
            'branch_id' => Yii::$app->request->get('branch_id') ? Yii::$app->request->get('branch_id') :
                $model_branch[0]['id']
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('staff', [
            'model_branch' => $model_branch,
            'model_personal' => $model_personal,
            'new_branch' => Yii::createObject(CustomerBranch::className()),
            'model_customer_personal' => Yii::createObject(CustomerPersonal::className()),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return object|Response
     * @throws \yii\base\InvalidConfigException
     */
    public function actionAddBranch(){
        $model = new CustomerBranch();
        $customer_id = CustomerHelpers::getCustomerId(Yii::$app->user->getId());
        if ($model->load(Yii::$app->request->post())){
            $model->customer_id = $customer_id;
            $model->city_id = City::find()->one()->id;
            if ($model->save()){
                return $this->redirect(['/customer-personal/staff']);
            }
        }
        return Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => Response::FORMAT_JSON,
            'data' => $model->errors,
        ]);
    }

    public function actionDeleteBranch($id){
        $model = CustomerBranch::find()->where(['id' => $id])->one();
//        $branch_id = $model->id;
        $model->is_delete = CustomerBranch::IS_DELETE;
        if ($model->save()){
//            CustomerPersonal::updateAll(['branch_id' => 0], ['branch_id' => $branch_id]);
            return $this->redirect(['/customer-personal/staff']);
        }

        return Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => Response::FORMAT_JSON,
            'data' => $model->errors,
        ]);
    }

    public function actionDeletePersonal($id){
        $model = \api\modules\v1\models\customer\CustomerPersonal::find()->where(['id' => $id])->one();
        $count_no_end_order = (new \yii\db\Query())
            ->select(['ose.id'])
            ->from(['op' => 'order_place'])
            ->leftJoin(['ose' => 'order_start_end'], 'ose.order_id = op.order_id AND ose.is_finished_job = 0')
            ->where(['op.personal_id' => $model->id])
            ->count('ose.id');
        if ($count_no_end_order > 0){
            Yii::$app->session->setFlash('error', 'У данного супервайзера есть незавершенные заказы');
            return $this->redirect(['/customer-personal/staff']);
        }
        $model_user = User::findOne($model->user_id);
        $model->is_delete = CustomerPersonal::IS_DELETE;

        if ($model->save() && $model_user->setDelete()){
            return $this->redirect(['/customer-personal/staff']);
        }
        return Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => Response::FORMAT_JSON,
            'data' => $model_user->errors,
        ]);
    }

    public function actionAddNewPersonal(){
        $model = new CustomerPersonal();
        if ($model->load(Yii::$app->request->post())){
            if (Yii::$app->request->post('is_edit')) {
                $model->is_edit = Yii::$app->request->post('is_edit');
                $create = $model->create(Yii::$app->request->post('is_edit'));
            }
            else {
                $create = $model->create();
            }
            if ($create != null){
                return $this->redirect(['/customer-personal/staff']);
            }
        }
        return Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => Response::FORMAT_JSON,
            'data' => $model->errors,
        ]);
    }

    public function actionGetProfilePersonal(){
        if ($id = (int)Yii::$app->request->get('id')){
            $model = CustomerPersonal::findOne($id);
            $model->phone = substr($model->user->phone, 1);
            if (array_key_exists('customer', Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId()))){
                $branches = CustomerBranch::findAll(['customer_id' =>
                    CustomerHelpers::getCustomerId(Yii::$app->user->getId()),
                    'is_delete' => CustomerBranch::NO_DELETE]);
            }
            else{
                $branches = CustomerBranch::findAll(['customer_id' =>
                    CustomerHelpers::getCustomerId(Yii::$app->user->getId()),
                    'id' => Yii::$app->user->identity->customerPersonal->branch_id,
                    'is_delete' => CustomerBranch::NO_DELETE]);
            }

            return $this->renderAjax('_customer-get-profile-personal', [
                'model_new_personal' => $model,
                'branches' => $branches,
                'is_edit' => true
            ]);
        }
        return false;
    }
}