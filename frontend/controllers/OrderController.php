<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 28.05.19
 * Time: 12:42
 */

namespace frontend\controllers;

use api\modules\v1\models\City;
use api\modules\v1\models\customer\CustomerBranch;
use api\modules\v1\models\customer\CustomerCompanyData;
use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\order\OrderProfession;
use api\modules\v1\models\worker\CategoryWorker;
use frontend\models\forms\MakeDepositForm;
use frontend\models\order\OrderAddress;
use api\modules\v1\models\printing\PrintingTemplate;
use common\helpers\GetErrorsHelpers;
use frontend\models\order\OrderPlace;
use api\modules\v1\models\order\OrderStartEnd;
use frontend\models\order\OrderPrintData;
use api\modules\v1\models\worker\CategoryWorkerCity;
use common\helpers\CustomerHelpers;
use frontend\models\customer\Customer;
use frontend\models\order\Order;
use frontend\models\order\OrderSearch;
use Symfony\Component\Console\Helper\ProcessHelper;
use yii\helpers\Url;
use yii\web\Controller;
use Yii;
use yii\filters\AccessControl;

class OrderController extends Controller
{
    public $layout = 'customer-layout';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex(){
        $customer_id = CustomerHelpers::getCustomerIdSite(Yii::$app->user->getId());
        if (!$customer_id) return $this->redirect(Url::to(['/customer/index']));

        $this->view->title = 'Все заказы';
        $searchModel = new OrderSearch(!Yii::$app->request->get() ? [
                'is_complete_order' => 0,
                'customer_id' => $this->getCustomerId()
            ] : [
            'is_complete_order' => Yii::$app->request->get('is_complete_order'),
            'customer_id' => $this->getCustomerId(),
            'is_published' => Yii::$app->request->get('is_published') ?? Order::IS_PUBLISHED
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionMaps(){
        $this->view->title = 'Все заказы';
        $searchModel = new OrderSearch(['is_complete_order' =>
            Yii::$app->request->get('is_complete_order'),
            'customer_id' => $this->getCustomerId()]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('maps', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider->getModels(),
        ]);
    }

    public function getRegionCode($id){
        $city = City::findOne($id);
        return $city->region->region_code;
    }

    public function getCustomerId(){
        if (Yii::$app->user->can('customer')){
            $model_customer = Customer::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = ['customer_id' => $model_customer->id, 'admin_id' => 0];
        }
        else {
            $model_customer = CustomerPersonal::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = ['customer_id' => $model_customer->customer_id, 'admin_id' => $model_customer->id];
        }
        return $customer_id;
    }

    public function actionCreate(){
        $customer_id = CustomerHelpers::getCustomerId(Yii::$app->user->getId());
        $cities = City::find()->all();
        if (array_key_exists('customer', Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId()))){
            $customer_branches = CustomerBranch::findAll(['customer_id' => $customer_id,
                'is_delete' => CustomerBranch::NO_DELETE]);
            $customer_supervisor = CustomerPersonal::findAll(['customer_id' => $customer_id,
                'role' => CustomerPersonal::ROLE_SUPERVISOR, 'is_delete' => CustomerBranch::NO_DELETE]);
        }
        else {
            $customer_branches = CustomerBranch::findAll(['customer_id' => $customer_id,
                'id' => Yii::$app->user->identity->customerPersonal->branch_id, 'is_delete' => CustomerBranch::NO_DELETE]);
            $customer_supervisor = $customer_supervisor = CustomerPersonal::findAll([
                'customer_id' => $customer_id,
                'role' => CustomerPersonal::ROLE_SUPERVISOR,
                'branch_id' => Yii::$app->user->identity->customerPersonal->branch_id
            ]);
        }
//        $customer_branches = CustomerBranch::findAll(['customer_id' => $customer_id]);
        $worker_price = CategoryWorkerCity::find()
            ->where(['category_worker_id' => 2])
            ->one();
//        $customer_supervisor = CustomerPersonal::findAll([
//            'customer_id' => $customer_id,
//            'role' => CustomerPersonal::ROLE_SUPERVISOR
//        ]);

        $printing_template = PrintingTemplate::find()->all();
        $print_courier_price = [];
        $print_size = [];
        $print_type = [];
        $print_edition = [];
        $print_time = [];
        $print_paper_density = [];
        foreach ($printing_template as $item) {
            switch ($item->category){
                case PrintingTemplate::COURIER_PRICE:
                    $print_courier_price[] = [
                        'id' => $item->id,
                        'name' => $item->name,
                        'price' => $item->price
                    ];
                    break;
                case PrintingTemplate::PRINT_SIZE:
                    $print_size[] = [
                        'id' => $item->id,
                        'name' => $item->name,
                        'price' => $item->price
                    ];
                    break;
                case PrintingTemplate::PRINT_TYPE:
                    $print_type[] = [
                        'id' => $item->id,
                        'name' => $item->name,
                        'price' => $item->price
                    ];
                    break;
                case PrintingTemplate::EDITION:
                    $print_edition[] = [
                        'id' => $item->id,
                        'name' => $item->name,
                        'price' => $item->price
                    ];
                    break;
                case PrintingTemplate::PRINT_TIME:
                    $print_time[] = [
                        'id' => $item->id,
                        'name' => $item->name,
                        'price' => $item->price
                    ];
                    break;
                case PrintingTemplate::PAPER_DENSITY:
                    $print_paper_density[] = [
                        'id' => $item->id,
                        'name' => $item->name,
                        'price' => $item->price
                    ];
                    break;
            }
        }
//        echo '<pre>' . print_r($print_paper_density, true) . '</pre>';die();

        $customer = $this->getCustomerId();
        $customer_id = $customer['customer_id'];
        $admin_id = $customer['admin_id'];
        $personal_id = Yii::$app->request->post('OrderPlace')['personal_id'];
        $is_print_flyers = Yii::$app->request->post('is_print_flyers');
        $is_courier = Yii::$app->request->post('is_courier');
        $model_order = new Order();
        $model_order_address = new OrderAddress();
        $order_print_data = new OrderPrintData();
        $model_order_place = new OrderPlace();
        $model_order_start_end = new OrderStartEnd();
        $model_order_profession = new OrderProfession();
        $model_worker_professions = CategoryWorkerCity::find()->all();
//        var_dump(Yii::$app->request->post());
//        var_dump($personal_id);die();
        if ($model_order->load(Yii::$app->request->post())){
//            var_dump($is_print_flyers);die();
            if ($model_order_address->load(Yii::$app->request->post())){
                $model_order_address->customer_id = $customer_id;
                if (!$model_order_address->save()){
                    Yii::$app->session->setFlash('error',
                        GetErrorsHelpers::getError($model_order_address->getErrors()));
                    return $this->refresh();
                }
            }

            $model_order->customer_id = $customer_id;
            $model_order->administrator_id = $admin_id;
            $model_order->is_published = Order::NO_PUBLISHED;

            if (!$model_order->save()){
                $model_order_address->delete();
                Yii::$app->session->setFlash('error', $model_order->getErrors());
                return $this->refresh();
            }

            if ($is_print_flyers != null){
                if ($order_print_data->load(Yii::$app->request->post())){
                    if ($order_print_data->courier_address == null){
                        $order_print_data->courier_price_id = 0;
                        $order_print_data->courier_address = null;
                    }
                    else {
                        $order_print_data->is_courier = 1;
                    }
                    if (!$order_print_data->save()){
                        $model_order_address->delete();
                        $model_order->delete();
                        Yii::$app->session->setFlash('error', GetErrorsHelpers::getError($order_print_data->getErrors()));
                        return $this->refresh();
                    }
                }
            }

            if ($model_order_place->load(Yii::$app->request->post())){
                $date = str_replace('/', '-', Yii::$app->request->post('OrderPlace'))['begin_date'];
                $newDate = date("Y-m-d", strtotime($date));
                $begin_time = Yii::$app->request->post('OrderPlace')['begin_time'];
                $end_time = Yii::$app->request->post('OrderPlace')['end_time'];

                $date_time_start = Yii::$app->formatter->asTimestamp($newDate) + (strtotime($begin_time) -
                        strtotime("00:00:00"));
                $date_time_end = Yii::$app->formatter->asTimestamp($newDate) + (strtotime($end_time) -
                        strtotime("00:00:00"));

                $model_order_place->begin_date = $date_time_start;

                $professions = $model_order_place->worker_profession;

                $count_worker = 0;
                foreach ($professions as $value){
                    $count_worker += $value['count_worker'];
                }
//                var_dump($date_time_start);var_dump($date_time_end);die();
                $model_order_place->date_time_start = $date_time_start  + 60 * $model_order_place->time_zone;
                $model_order_place->date_time_end = $date_time_end + 60 * $model_order_place->time_zone;
//                var_dump(Yii::$app->request->post());die();
                $model_order_place->order_id = $model_order->id;
                $model_order_place->order_address_id = $model_order_address->id;
                $model_order_place->count_worker = $count_worker;
                $model_order_place->order_print_id = $is_print_flyers == null ?
                    OrderPlace::NO_PRINTING_FLAYER : $order_print_data->id;
                if (!isset($personal_id) || $personal_id == 0){
                    $model_order_place->personal_id = $admin_id != 0 ? $admin_id : 0;
                }
                else {
                    $model_order_place->personal_id = (int)$personal_id;
                }

                $model_order_start_end->order_id = $model_order->id;
                if(!$model_order_place->save() || !$model_order_start_end->save()){
                    $model_order_address->delete();
                    $model_order->delete();
                    Yii::$app->session->setFlash('error', GetErrorsHelpers::getError($model_order_place->getErrors()));
                    return $this->refresh();
                }
                $model_order_place->trigger(OrderPlace::EVENT_SET_NOTICE);
                $model_order_place->trigger(OrderPlace::EVENT_SET_SUPERVISOR_NOTICE);

                $new_profession = [];
                foreach ($professions as $value){
                    $split_profession = explode("||", $value['type_worker']);
                    $new_profession[] = [
                        $model_order->id,
                        $model_order_place->id,
                        $split_profession[0],
                        $value['count_worker'],
                        $value['type_job']
                    ];
                }

                Yii::$app->db->createCommand()->batchInsert('order_profession', [
                    'order_id', 'order_place_id', 'worker_category_id', 'count_worker',
                    'type_job'
                ], $new_profession)->execute();

            }

            $model_order->number_order =
                $this->getRegionCode($model_order_address->city_id) . '-' . $model_order->id;
            $model_order->total_price = $model_order_place->order_price;
            $model_order->update();

//            $company_data = CustomerCompanyData::findOne(['customer_id' => $customer_id]);
//
//            $total_price = $is_print_flyers != null  ?
//                $model_order->total_price + $order_print_data->printing_price : $model_order->total_price;

            $model_order->is_pay = Order::IS_PAY;
            $model_order->type_order = Order::TYPE_LIMA_EXCHANGE;
            if ($model_order->save()){
                return $this->redirect(['confirm-order', 'id' => $model_order->id]);
            }
//            if ($company_data->deposit < $total_price){
//                $model_order->is_pay = Order::NO_PAY;
//                $model_order->type_order = Order::TYPE_LIMA_EXCHANGE;
//                $model_order->save();
//               return $this->redirect(['confirm-order', 'id' => $model_order->id]);
//            }
//            else {
//                $company_data->deposit = $company_data->deposit - $total_price;
//                $model_order->is_pay = Order::IS_PAY;
//                $model_order->type_order = Order::TYPE_LIMA_EXCHANGE;
//                if ($company_data->save() && $model_order->save()){
//                    return $this->redirect(['confirm-order', 'id' => $model_order->id]);
//                }
//            }
        }

        return $this->render('create', [
            'cities' => $cities,
            'model_order' => $model_order,
            'model_order_place' => $model_order_place,
            'customer_branches' => $customer_branches,
            'model_order_address' => $model_order_address,
            'worker_price' => $worker_price,
            'customer_supervisor' => $customer_supervisor,
            'order_print_data' => $order_print_data,
            'model_worker_professions' => $model_worker_professions,

            'print_courier_price' => $print_courier_price,
            'print_size' => $print_size,
            'print_type' => $print_type,
            'print_edition' => $print_edition,
            'print_time' => $print_time,
            'print_paper_density' => $print_paper_density
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionConfirmOrder($id){
        $order = Order::findOne($id);
        $order_place = OrderPlace::findOne(['order_id' => $order->id]);
        $order_address = $order_place->orderAddress;
        $order_company_data = CustomerCompanyData::findOne(['customer_id' => $order->customer_id]);
        $is_print_flyers = Yii::$app->request->post('is_print_flyers');
        if ($order_place->order_print_id != 0) {
            $order_print_data = OrderPrintData::findOne($order_place->order_print_id);
        }
        else {
            $order_print_data = new OrderPrintData();
        }
        $printing_template = PrintingTemplate::find()->all();
        $print_courier_price = [];
        $print_size = [];
        $print_type = [];
        $print_edition = [];
        $print_time = [];
        $print_paper_density = [];
        foreach ($printing_template as $item) {
            switch ($item->category){
                case PrintingTemplate::COURIER_PRICE:
                    $print_courier_price[] = [
                        'id' => $item->id,
                        'name' => $item->name,
                        'price' => $item->price
                    ];
                    break;
                case PrintingTemplate::PRINT_SIZE:
                    $print_size[] = [
                        'id' => $item->id,
                        'name' => $item->name,
                        'price' => $item->price
                    ];
                    break;
                case PrintingTemplate::PRINT_TYPE:
                    $print_type[] = [
                        'id' => $item->id,
                        'name' => $item->name,
                        'price' => $item->price
                    ];
                    break;
                case PrintingTemplate::EDITION:
                    $print_edition[] = [
                        'id' => $item->id,
                        'name' => $item->name,
                        'price' => $item->price
                    ];
                    break;
                case PrintingTemplate::PRINT_TIME:
                    $print_time[] = [
                        'id' => $item->id,
                        'name' => $item->name,
                        'price' => $item->price
                    ];
                    break;
                case PrintingTemplate::PAPER_DENSITY:
                    $print_paper_density[] = [
                        'id' => $item->id,
                        'name' => $item->name,
                        'price' => $item->price
                    ];
                    break;
            }
        }

        if ($is_print_flyers != null){
            if ($order_print_data->load(Yii::$app->request->post())){
                if ($order_print_data->courier_address == null){
                    $order_print_data->courier_price_id = 0;
                    $order_print_data->courier_address = null;
                }
                else {
                    $order_print_data->is_courier = 1;
                }
                if (!$order_print_data->save()){
                    Yii::$app->session->setFlash('error', GetErrorsHelpers::getError($order_print_data->getErrors()));
                    return $this->refresh();
                }
                $order_place->order_print_id = $order_print_data->id;
                $order_place->save();
            }
        }

        if ($order->load(Yii::$app->request->post())){
            $order->is_published = Order::IS_PUBLISHED;
            $order->save();
            if ($order->is_pay == Order::IS_PAY){
                return $this->redirect(['/order']);
            }
        }

        return $this->render('confirm-order', [
            'order' => $order,
            'order_place' => $order_place,
            'order_address' => $order_address,
            'order_company_data' => $order_company_data,
            'order_print_data' => $order_print_data,
            'print_courier_price' => $print_courier_price,
            'print_size' => $print_size,
            'print_type' => $print_type,
            'print_edition' => $print_edition,
            'print_time' => $print_time,
            'print_paper_density' => $print_paper_density,
            'model_make_deposit' => Yii::createObject(MakeDepositForm::className())
        ]);
    }
}