<?php

namespace frontend\controllers;

use api\modules\v1\controllers\worker\WorkerController;
use api\modules\v1\models\order\OrderRequest;
use api\modules\v1\models\order\OrderStartEnd;
use api\modules\v1\models\order\OrderWorker;
use api\modules\v1\models\worker\Worker;
use api\modules\v1\models\worker\WorkerSearch;
use common\helpers\CustomerHelpers;
use common\helpers\GetErrorsHelpers;
use common\helpers\WorkerHelpers;
use frontend\models\order\Order;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;

class OrderOfferController extends Controller
{
    public $layout = 'customer-layout';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionOffer($order_id){
        $request_ids = Yii::$app->request->get();
        $order = Order::findOne($order_id);
        $order_worker_count = OrderWorker::find()
            ->where(['order_id' => $order_id])
            ->andWhere(['NOT', ['status' => OrderWorker::STATUS_WORKER_CANCEL]])
            ->count();
//        $model_params = array_merge($request_ids, ['type_request' => WorkerSearch::SEARCH]);
        $request_ids['offset'] = 0;
        $request_ids['radius_search'] = $request_ids['radius_search'] * 1000;
        $request_ids['order_id'] = $order_id;
        $request_ids['limit'] = 1000;
//        $request_ids['radius_search'] = 20000;
//        array_merge($request_ids, ['order_id' => $id]);
//        var_dump($request_ids);
        $worker_search = new WorkerSearch($request_ids);
        $model_search = $worker_search->search($request_ids);
//        var_dump($model_search);die();
        return $this->render('offer', [
            'models' => $model_search['array_query'],
            'order' => $order,
            'order_worker_count' => $order_worker_count
        ]);
    }

    public function actionOfferWorker($order_id){
        $request_ids = Yii::$app->request->get();
        $order = Order::findOne($order_id);
        $order_worker_count = OrderWorker::find()
            ->where(['order_id' => $order_id])
            ->andWhere(['NOT', ['status' => OrderWorker::STATUS_WORKER_CANCEL]])
            ->count();
//        $model_params = array_merge($request_ids, ['type_request' => WorkerSearch::SEARCH]);
        $request_ids['offset'] = 0;
        $request_ids['limit'] = 20;
        $request_ids['radius_search'] = 20000;
        $request_ids['status_request'] = OrderRequest::STATUS_ACCEPT;
//        array_merge($request_ids, ['order_id' => $id]);
//        var_dump($request_ids);
        $worker_search = new WorkerSearch($request_ids);
        $model_search = $worker_search->search($request_ids);
        return $this->render('offer-worker', [
            'models' => $model_search['array_query'],
            'order' => $order,
            'order_worker_count' => $order_worker_count
        ]);
    }

    public function actionWorker($id){
        return $this->render('offer');
    }

    public function actionGetWorkerProfile(){
        if ($id = (int)Yii::$app->request->get('id')){
            $model_worker = Worker::find()->select(['worker.*',
                "date_part('year', age('today'::date, worker.birth_date)) as age"])
                ->where(['id' => $id])
                ->asArray()
                ->with(['city', 'imageWorker', 'appearance', 'bodyComplexion', 'clothingSizeDown', 'clothingSizeUp',
                    'clothingSizeUp', 'hairColor', 'hairLength', 'eyeColor'])
                ->one();
            $order_active = WorkerHelpers::getRequestOrder(OrderRequest::STATUS_ACCEPT, $id,
                [1,2], null, 0, 1000);
            $count_order_active = $order_active['totalCount'];
            $order_finished = WorkerHelpers::getCountOrderFinished($id);
            $count_order_finished = count($order_finished);
            return $this->renderAjax('_worker-profile', [
                'model_worker' => $model_worker,
                'count_order_active' => $count_order_active,
                'count_order_finished' => $count_order_finished,
                'order_id' => (int)Yii::$app->request->get('order_id'),
                'is_offer_worker' => !is_null(Yii::$app->request->get('is_offer_worker')) ? true : false
            ]);
        }
        return false;
    }

    public function actionConfirmWorker(){
        $order_id = (int)Yii::$app->request->post('order_id');
        $worker_ids = Yii::$app->request->post('worker_arrays');
        $customer_id = CustomerHelpers::getCustomerId(Yii::$app->user->getId());

        $order_requests = OrderRequest::find()
            ->joinWith('order')
            ->where(['order_id' => $order_id])
            ->andWhere(['worker_id' => $worker_ids])
            ->andWhere(['status' => OrderRequest::STATUS_NEW])
            ->andWhere(['NOT', ['status' => OrderRequest::STATUS_WORKER_CANCEL]])
            ->andWhere(['is_show_in_filter' => OrderRequest::SHOW_IN_FILTER])
            ->all();
        if (!empty($order_requests)){
            foreach ($order_requests as $order_request){
                $order_request->status = OrderRequest::STATUS_ACCEPT;
                if ($order_request->save()){
                    $order_worker = new OrderWorker();
                    $order_worker->order_id = $order_request->order_id;
                    $order_worker->worker_id = $order_request->worker_id;
                    $order_worker->order_place_id = $order_request->order_place_id;
                    if (!$order_worker->save()){
                        $message = GetErrorsHelpers::getError($order_worker->getErrors());
                        throw new HttpException(400, $message);
                    }
                }
            }
            return $this->redirect(Url::to(['offer', 'order_id' => $order_id]));
        }
        return false;
    }
}