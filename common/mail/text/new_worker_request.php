<?php

use yii\helpers\Html;

/**
 * @var $order_id
 * @var $name_worker
 */
?>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    На ваш заказ № <?= $order_id ?> откликнулся исполнитель <?= $name_worker ?>.
</p>
<p>Чтобы перейти в личный кабинет, нажмите на ссылку ниже.</p>

<p><?= Html::a(Html::encode($url), $url) ?></p>
