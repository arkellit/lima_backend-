<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 26.11.18
 * Time: 17:55
 */

namespace common\helpers;

/**
 * Class GetErrorsHelpers
 * @package common\helpers
 */
class GetErrorsHelpers
{
    /**
     * Function get error
     *
     * @param $errors
     * @return string
     */
    public static function getError($errors){
        $message = '';
        foreach ($errors as $key => $value) {
            $message = $value[0];
        }
        return $message;
    }
}