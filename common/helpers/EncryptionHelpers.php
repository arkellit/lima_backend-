<?php

namespace common\helpers;

use Yii;

/**
 * This helpers for encode/decode string
 *
 * Class EncryptionHelpers
 * @package app\helpers
 */
class EncryptionHelpers
{
    /**
     * Function encode/decode string
     *
     * @param $action
     * @param $string
     * @return bool|string
     */
    public static function dec_enc($action, $string) {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_key = Yii::$app->params['secret_key'];
        $secret_iv = Yii::$app->params['secret_iv'];

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        }
        else if($action == 'decrypt'){
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }
}