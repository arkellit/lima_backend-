<?php


namespace common\helpers;
use api\modules\v1\models\NotificationForm;
use Yii;


class FirebaseHelpers
{
    const CONFIRM_START_ORDER = 'confirm_start_order';
    const CONFIRM_END_ORDER = 'confirm_end_order';

    public static function sendAll($clients, $title, $description, $type, $data = null, $sound = null){
        $url = 'https://fcm.googleapis.com/fcm/send';
        $server_key = Yii::$app->fcm->apiKey;

        $fields = array();

        if($sound == NotificationForm::IS_SOUND){
            $fields['notification'] = array('title' => $title, 'body' => $description,
                'sound' => 'default');
        }
        else{
            $fields['notification'] = array('title' => $title, 'body' => $description);
        }

        if ($type == NotificationForm::TYPE_INFO){
            $fields['data'] = array('type' => 'info');
        }

        if ($type == NotificationForm::TYPE_NEWS){
            $fields['data'] = array('type' => 'news', $data);
        }

        if(is_array($clients)){
            $fields['registration_ids'] = $clients;
        }else{
            $fields['to'] = $clients;
        }
        //header with content_type api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key='.$server_key
        );
        //CURL request to route notification to FCM connection server (provided by Google)
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if (!$result) {
            return false;
        }
        curl_close($ch);
        return true;
    }

    /**
     * Test push
     *
     * @param $to
     * @param $status
     * @return bool
     */
    public static function closeQrPagePush($to, $status){
        $url = 'https://fcm.googleapis.com/fcm/send';
        $server_key = Yii::$app->fcm->apiKey;

        $fields = array();

        //header with content_type api key
        $fields['to'] = $to;
        $fields['data'] = [
            'type' => $status, //confirm_start_order confirm_end_order
            'content-available' => true
        ];
        $fields['priority'] = 'high';
        $fields['content_available'] = true;
        $fields['time_to_live'] = 360;
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key='.$server_key
        );
        //CURL request to route notification to FCM connection server (provided by Google)
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if (!$result) {
            return false;
        }
        curl_close($ch);
        return true;
    }
}