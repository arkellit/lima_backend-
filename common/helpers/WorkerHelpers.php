<?php


namespace common\helpers;


use api\modules\v1\models\order\OrderRequest;
use api\modules\v1\models\order\OrderStartEnd;

class WorkerHelpers
{
    public static function getRequestOrder($status, $worker_id, $category_worker, $is_lima_exchange, $offset, $limit){
        $is_lima_exchange_new = $is_lima_exchange == null ? [] : $is_lima_exchange;
        $query = OrderRequest::find()
            ->where($is_lima_exchange_new)
            ->andWhere([
                'status' => $status,
                'worker_id' => $worker_id])
            ->leftJoin('order_place', 'order_place.order_id = order_request.order_id')
            ->andWhere(['order_place.worker_category_id' => $category_worker])
            ->with(['order', 'orderPlace']);
        $totalCount = $query->count();
        $order_requests = $query->offset($offset)->asArray()->limit($limit)->all();

        return ['model_request' => $order_requests, 'totalCount' => $totalCount];
    }

    public static function getCountOrderFinished($id){
        $order_finished = OrderStartEnd::find()
            ->where(['is_finished_job' => 1])
            ->andWhere(['order_request.worker_id' => $id])
            ->joinWith(['orderRequest', 'orderPlace', 'order']);
        return $order_finished->all();
    }
}