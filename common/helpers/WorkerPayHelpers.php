<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 04.03.19
 * Time: 18:45
 */

namespace common\helpers;

/**
 * Refund 1 RUB to worker
 *
 * Class WorkerPayHelpers
 * @package common\helpers
 */
class WorkerPayHelpers
{
    public static function RefundPay($client, $payment_id){
        $response = $client->createRefund(
            array(
                'amount' => array(
                    'value' => '1.00',
                    'currency' => 'RUB',
                ),
                'payment_id' => $payment_id,
            )
        );
        return $response;
    }
}