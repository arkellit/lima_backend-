<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 14.02.19
 * Time: 13:04
 */

namespace common\helpers;

use api\modules\v1\models\order\Order;

/**
 * Class for Order
 *
 * Class OrderHelpers
 * @package common\helpers
 */
class OrderHelpers
{
    public static function getType($is_pay, $type){
        switch (true){
            case $is_pay == Order::NO_PAY:
                $type_order = 'waitPayment';
                break;
            case $type == Order::TYPE_LIMA_EXCHANGE:
                $type_order = 'limaExchange';
                break;
            case $type == Order::TYPE_MANUAL_SELECTION:
                $type_order = 'manualSelection';
                break;
            default:
                $type_order = 'noType';
                break;
        }
        return $type_order;
    }
}