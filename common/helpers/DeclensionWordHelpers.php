<?php


namespace common\helpers;


class DeclensionWordHelpers
{
    public static function declension_word($number, $word) {
        $ar= array (2, 0, 1, 1, 1, 2);
        return $word[ ($number%100 > 4 && $number%100 < 20) ? 2 : $ar[min($number%10, 5)] ];
    }
}