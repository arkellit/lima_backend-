<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 21.09.18
 * Time: 11:47
 */

namespace common\helpers;

use Yii;

/**
 * Class DifferenceTimeHelpers
 * @package common\helpers
 */
class DifferenceTimeHelpers
{
    /**
     * Function of calculating the difference in time
     *
     * @param $time
     * @return false|int
     */
    public static function differenceTime($time){
        return strtotime(Yii::$app->formatter->asDatetime('now', 'php: Y-m-d H:i:s')) -
        strtotime(Yii::$app->formatter->asDatetime($time, 'php: Y-m-d H:i:s'));
    }

    /**
     * @param $time
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public static function approachDate($time){
        $date = strtotime(Yii::$app->formatter->asDatetime($time, 'php: Y-m-d H:i:s'));
        $date_now = strtotime(Yii::$app->formatter->asDatetime('now', 'php: Y-m-d H:i:s'));
        if($date < $date_now) return true;
        return false;
    }
}