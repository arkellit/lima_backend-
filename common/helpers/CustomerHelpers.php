<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 14.02.19
 * Time: 14:14
 */

namespace common\helpers;


use api\modules\v1\models\customer\Customer;
use api\modules\v1\models\customer\CustomerPersonal;
use api\modules\v1\models\order\OrderRequest;
use Yii;
use yii\db\Query;
use yii\web\HttpException;

/**
 * Function for Customer
 *
 * Class CustomerHelpers
 * @package common\helpers
 */
class CustomerHelpers
{
    public static function getCustomerId($user_id){
        if (array_key_exists('customer', Yii::$app->authManager->getRolesByUser($user_id))){
            $model_customer = Customer::findOne(['user_id' => Yii::$app->user->getId()]);
            if ($model_customer->is_register_complete == Customer::IS_REGISTER_COMPLETE_FALSE){
                throw new HttpException(429, 'Ваш профиль еще не прошел модерацию');
            }
            $customer_id = $model_customer->id;
        }
        else {
            $model_customer = CustomerPersonal::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->customer_id;
        }
        return $customer_id;
    }

    public static function getCustomerIdAndRole($user_id){
        if (array_key_exists('customer', Yii::$app->authManager->getRolesByUser($user_id))){
            $model_customer = Customer::findOne(['user_id' => Yii::$app->user->getId()]);
            if ($model_customer->is_register_complete == Customer::IS_REGISTER_COMPLETE_FALSE){
                throw new HttpException(429, 'Ваш профиль еще не прошел модерацию');
            }
            $customer_id = $model_customer->id;
            return ['id' => $customer_id, 'role' => 'customer'];
        }
        else {
            $model_customer = CustomerPersonal::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->customer_id;
            return ['id' => $customer_id, 'role' => $model_customer->role, 'personal_id' =>
            $model_customer->id];
        }
    }

    public static function getCustomerIdSite($user_id){
        if (array_key_exists('customer', Yii::$app->authManager->getRolesByUser($user_id))){
            $model_customer = Customer::findOne(['user_id' => Yii::$app->user->getId()]);
            if ($model_customer->is_register_complete == Customer::IS_REGISTER_COMPLETE_FALSE){
                Yii::$app->session->setFlash('error', 'Ваш профиль еще не прошел модерацию');
                return false;
            }
            $customer_id = $model_customer->id;
        }
        else {
            $model_customer = CustomerPersonal::findOne(['user_id' => Yii::$app->user->getId()]);
            $customer_id = $model_customer->customer_id;
        }
        return $customer_id;
    }

    /**
     * Function check new worker request to order
     *
     * @param int $customer_id
     * @return array|bool
     */
    public static function checkNewWorkerRequest($customer_id = null){
        if (empty($customer_id)){
            $customer_id = self::getCustomerIdSite(Yii::$app->user->getId());
        }

        if (!empty($customer_id)){
            $order_id_new_request = (new Query())
                ->select(['o.id order_id'])
                ->from(['o' => 'order'])
                ->leftJoin(['o_r' => 'order_request'], 'o_r.order_id = o.id')
                ->where(['o.customer_id' => $customer_id])
                ->andWhere(['o_r.status' => OrderRequest::STATUS_NEW])
                ->orderBy(['o_r.created_at' => SORT_DESC])
                ->one();
        }

        return [
            'order_id' => $order_id_new_request['order_id'] ?? 0,
            'customer_id' => $customer_id
        ];
    }
}