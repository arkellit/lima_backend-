<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 05.10.18
 * Time: 17:13
 */

namespace common\helpers;

use api\modules\v1\models\auth\ChangeUser;
use yii\web\HttpException;

/**
 * Class ChangeUserHelpers
 * @package common\helpers
 */
class ChangeUserHelpers
{
    /**
     * @param $user_id
     * @param $type_token
     * @return bool
     * @throws HttpException
     */
    public static function create($user_id, $type_token){
        $change_user = new ChangeUser();
        $change_user->user_id = $user_id;
        $change_user->type = $type_token;
        if(!$change_user->save()){
            throw new HttpException(400, 'Неверные данные');
        }

        return  true;
    }
}