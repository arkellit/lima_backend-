<?php


namespace common\helpers;

use Yii;

/**
 * Class MailerHelpers
 * @package common\helpers
 *
 * @property string $sender
 * @property string $subject
 */
class MailerHelpers
{
    protected $sender = 'info@iamlima.com';
    public $viewPath = '@common/mail';
    public $subject = 'Lima';

    /**
     * MailerHelpers constructor.
     * @param string|null $sender
     * @param string|null $subject
     */
    public function __construct(string $sender = null, string $subject = null)
    {
        $this->sender = $sender ?? $this->sender;
        $this->subject = $subject ?? $this->subject;
    }

    /**
     * Send confirm mail to customer confirm
     *
     * @param $to
     * @return bool
     */
    public function sendConfirmCustomer($to)
    {
        return $this->sendMessage(
            $to,
            $this->subject,
            'confirmation_customer_moderation'
        );
    }

    /**
     * Send mail new worker request
     *
     * @param $to
     * @param $order_id
     * @param $name_worker
     * @param $url
     * @return bool
     */
    public function sendNewWorkerRequest($to, $order_id, $name_worker, $url)
    {
        return $this->sendMessage(
            $to,
            $this->subject,
            'new_worker_request',
            [
                'order_id' => $order_id,
                'name_worker' => $name_worker,
                'url' => $url
            ]
        );
    }

    protected function sendMessage($to, $subject, $view, $params = [])
    {
        $mailer = Yii::$app->mailer;
        $mailer->viewPath = $this->viewPath;

        return $mailer->compose(['html' => $view, 'text' => 'text/' . $view], $params)
            ->setTo($to)
            ->setFrom($this->sender)
            ->setSubject($subject)
            ->send();
    }
}