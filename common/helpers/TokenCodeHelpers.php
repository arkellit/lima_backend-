<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 21.09.18
 * Time: 14:49
 */

namespace common\helpers;

use api\modules\v1\models\auth\Token;
use api\modules\v1\models\Sending;
use yii\web\HttpException;

/**
 * Class TokenCodeHelpers
 * @package common\helpers
 */
class TokenCodeHelpers
{
    /**
     * Function of creating and sending code
     *
     * @param $user_id
     * @param $type_token
     * @param $phone
     * @return bool
     * @throws HttpException
     */
    public static function createSend($user_id, $type_token, $phone){
        $token = new Token();
        $token->user_id = $user_id;
        $token->code = YII_ENV_DEV ? 1234 : $token->getRandCode();
        $token->type = $type_token;

        if(!$token->save()){
            throw new HttpException(421, 'Произошла ошибка при создании токена');
        }
        $send_code = YII_ENV_DEV ? true : Sending::sendCode($phone, $token->code);

        if(!$send_code){
            throw new HttpException(422, 'Произошла ошибка при отправке смс');
        }

        return true;
    }

    /**
     * Function of creating code
     *
     * @param $user_id
     * @param $type_token
     * @return bool
     */
    public static function create($user_id, $type_token){
        $token = new Token();
        $token->user_id = $user_id;
        $token->code = $token->getRandCode();
        $token->type = $type_token;

        if(!$token->save()){
            return null;
        }

        return true;
    }

    /**
     * Function update and send code
     *
     * @param $user_id
     * @param $type_token
     * @param $phone
     * @throws HttpException
     */
    public static function updateSend($user_id, $type_token, $phone){
        $token = Token::findOne(['user_id' => $user_id]);
        $token->code = YII_ENV_DEV ? 1234 : $token->getRandCode();
        $token->type = $type_token;
        $token->touch('updated_at');
        if(!$token->save()){
            throw new HttpException(421, 'Произошла ошибка при создании токена');
        }
        $send_code  = YII_ENV_DEV ? true : Sending::sendCode($phone, $token->code);

        if(!$send_code){
            throw new HttpException(422, 'Произошла ошибка при отправке смс');
        }
    }
}