<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%feedback}}".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $message
 * @property string $image
 * @property int $created_at
 * @property int $is_view
 */
class Feedback extends ActiveRecord
{
    const IS_VIEW = 1;
    const NO_VIEW = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%feedback}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'message'], 'required'],
            [['message'], 'string'],
            ['email', 'email'],
            ['is_view', 'default', 'value' => self::NO_VIEW],
            [['created_at', 'is_view'], 'integer'],
            [['name', 'email', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp'  => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'email' => 'Email',
            'message' => 'Текст сообщения',
            'image' => 'Изображение',
            'created_at' => 'Дата создания',
            'is_view' => 'Просмотрен',
        ];
    }
}
