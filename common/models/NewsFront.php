<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%news_front}}".
 *
 * @property int $id
 * @property string $title
 * @property string $image
 * @property string $short_description
 * @property string $description
 * @property int $is_published
 * @property int $created_at
 */
class NewsFront extends ActiveRecord
{
    const PUBLISH = 1;
    const NO_PUBLISH = 0;

    const SCENARIO_CREATE = '1';
    const SCENARIO_UPDATE = '2';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%news_front}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'image', 'short_description', 'description'], 'required',
                'on' => self::SCENARIO_CREATE],
            [['short_description', 'description'], 'string'],
            [['is_published', 'created_at'], 'default', 'value' => null],
            [['is_published', 'created_at'], 'integer'],
            [['title'], 'string', 'max' => 1000],
            [['image'], 'string', 'max' => 255,
                'on' => self::SCENARIO_UPDATE],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp'  => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'image' => 'Изображение',
            'short_description' => 'Краткое описание',
            'description' => 'Описание',
            'is_published' => 'Опубликован',
            'created_at' => 'Дата создания',
        ];
    }
}
