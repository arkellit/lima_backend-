<?php
$params = require __DIR__ . '/params.php';
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'sms' => [
            'class'    => 'ladamalina\smsc\Smsc',
            'login'     => 'lima',  // login
            'password'   => 'lima12345', // plain password or lowercase password MD5-hash
            'post' => true, // use http POST method
            'https' => true,    // use secure HTTPS connection
            'charset' => 'utf-8',   // charset: windows-1251, koi8-r or utf-8 (default)
            'debug' => false,    // debug mode
        ],
        'fcm' => [
            'class' => 'understeam\fcm\Client',
            'apiKey' => 'AAAApeXKsNc:APA91bFnL5ghNk1sOKpvBeykrHGMd0pprQK1BhltGcCfpJ01yelHixXTx9JTDBTYCRTf8ziuxrHWpSLeRuaQ-TOj5ljxU2gWCt4OnFu-jejZd2oxd1FKKO9eoXGuej7fsHZn2cIbVhC6', // Server API Key (you can get it here: https://firebase.google.com/docs/server/setup#prerequisites)
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.eu1.unione.io',
                'username' => $params['senderEmail'],
                'password' => $params['senderPassword'],
                'port' => '587',
                'encryption' => 'tls',
            ]
        ],
    ],
];
